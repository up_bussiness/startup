<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateInstituitionsTable.
 */
class CreateInstituitionsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('instituitions', function(Blueprint $table) {
            $table->uuid('id')->primary()->unique();
            $table->uuid('user_id');
            $table->string('name');
            $table->string('logo')->nullable();
            $table->string('slug');
						$table->integer('license');
						$table->tinyInteger('configured');
						$table->tinyInteger('product_id');

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('instituitions', function (Blueprint $table) {
      $table->dropForeign('instituitions_user_id_foreign');
      $table->dropColumn('user_id');
    });
    
		Schema::drop('instituitions');
	}
}
