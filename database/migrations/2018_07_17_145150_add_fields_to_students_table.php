<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('students', function (Blueprint $table) {
            $table->string('face')->nullable();     
            $table->string('insta')->nullable();           
            $table->date('date')->nullable();           
            $table->string('registration');

            $table->uuid('occupation_id');
            $table->foreign('occupation_id')->references('id')->on('occupations')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('students', function (Blueprint $table) {
            $table->dropColumn('name');
            $table->dropColumn('face');
            $table->dropColumn('insta');
            $table->dropColumn('date');
            $table->dropColumn('registration');
            $table->dropForeign('students_occupation_id_foreign');
            $table->dropColumn('occupation_id');
        });
    }
}
