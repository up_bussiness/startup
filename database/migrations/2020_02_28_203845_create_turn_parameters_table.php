<?php
//#AUTO - Este arquivo foi alterado por conta da implementação da automatização do processo de simulação
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTurnParametersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('turn_parameters', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('month');
            $table->integer('macro_industry');
            $table->integer('inflation');
            $table->integer('loss_on_sale');
            $table->integer('bir');
            $table->integer('interest_providers');
            $table->integer('import');
            $table->integer('income_tax');
            $table->integer('raw_material_a');
            $table->integer('raw_material_b');
            $table->integer('raw_material_c');
            $table->integer('lp_small');
            $table->integer('lp_medium');
            $table->integer('lp_large');
            $table->integer('adv_radio');
            $table->integer('adv_journal');
            $table->integer('adv_social');
            $table->integer('adv_outdoor');
            $table->integer('adv_tv');
            $table->integer('theme_id_1')->unsigned()->nullable();
            $table->foreign('theme_id_1')->references('id')->on('themes');
            $table->string('title_1')->nullable();
            $table->text('description_1')->nullable();
            $table->integer('theme_id_2')->unsigned()->nullable();
            $table->foreign('theme_id_2')->references('id')->on('themes');
            $table->string('title_2')->nullable();
            $table->text('description_2')->nullable();
            $table->integer('theme_id_3')->unsigned()->nullable();
            $table->foreign('theme_id_3')->references('id')->on('themes');
            $table->string('title_3')->nullable();
            $table->text('description_3')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('turn_parameters');
    }
}
