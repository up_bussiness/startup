<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateTurnsTable.
 */
class CreateTurnsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('turns', function(Blueprint $table) {
            $table->uuid('id')->primary()->unique();
            $table->uuid('simulation_id');
            $table->integer('month');
            $table->boolean('published')->default(0);
            $table->dateTime('end_date');

            $table->foreign('simulation_id')->references('id')->on('simulations')->onDelete('cascade');

            $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('turns', function (Blueprint $table) {
       $table->dropForeign('turns_simulation_id_foreign');
       $table->dropColumn('simulation_id');
    });
		Schema::drop('turns');
	}
}
