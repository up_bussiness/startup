<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateDecisionCoordinatorsTable.
 */
class CreateDecisionCoordinatorsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('decision_coordinators', function(Blueprint $table) {
                  $table->uuid('id')->primary()->unique();
                  $table->uuid('turn_id');
                  $table->integer('macro_industry');
                  $table->integer('inflation');
                  $table->integer('loss_on_sale');
                  $table->integer('bir');
                  $table->integer('interest_providers');
                  $table->integer('import');
                  $table->integer('income_tax');
                  $table->integer('raw_material_a')->default(100);
                  $table->integer('raw_material_b')->default(60);
                  $table->integer('raw_material_c')->default(30);
                  $table->bigInteger('lp_small')->default(200000);
                  $table->bigInteger('lp_medium')->default(555000);
                  $table->bigInteger('lp_large')->default(900000);
                  $table->integer('adv_radio')->default(1500);
                  $table->integer('adv_journal')->default(1250);
                  $table->integer('adv_social')->default(3000);
                  $table->integer('adv_outdoor')->default(5000);
                  $table->integer('adv_tv')->default(10000);

                  $table->foreign('turn_id')->references('id')->on('turns')->onDelete('cascade');
                  $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('decision_coordinators');
	}
}
