<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTmpMachineMaintenanceFieldToCashflow extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cashflows', function (Blueprint $table) {
            $table->integer('tmp_machine_maintenance')->default(0);
            $table->integer('tmp_training')->default(0);
            $table->integer('tmp_adv')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cashflows', function (Blueprint $table) {
            $table->dropColumn('tmp_machine_maintenance');
            $table->dropColumn('tmp_training');
            $table->dropColumn('tmp_adv');
        });
    }
}
