<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNovocriterioRankingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('rankings', function (Blueprint $table) {
            $table->integer('net_worth_pontos')->default(0);
            $table->float('market_share')->default(0);
            $table->integer('market_share_pontos')->default(0);
            $table->integer('loans_financing')->default(0);
            $table->integer('loans_financing_pontos')->default(0);
            $table->integer('total_pontos')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('rankings', function (Blueprint $table) {
            $table->dropColumn('net_worth_pontos');
            $table->dropColumn('market_share');
            $table->dropColumn('market_share_pontos');
            $table->dropColumn('loans_financing');
            $table->dropColumn('loans_financing_pontos');
            $table->dropColumn('total_pontos');
        });
    }
}
