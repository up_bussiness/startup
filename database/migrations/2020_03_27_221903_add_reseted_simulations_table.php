<?php
//#AUTO - Este arquivo foi alterado por conta da implementação da automatização do processo de simulação
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddResetedSimulationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('simulations', function (Blueprint $table) {
            $table->boolean('reseted')//Nome da coluna
            ->nullable() //Preenchimento não obrigatório
            ->default('0')//Valor inicial zero
            ->after ('planning');//No final da tabela
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('simulations', function (Blueprint $table) {
            $table->dropColumn('reseted');
        });
    }
}
