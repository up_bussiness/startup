<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateCashflowsTable.
 */
class CreateCashflowsTable extends Migration
{
      /**
       * Run the migrations.
       *
       * @return void
       */
      public function up()
      {
            Schema::create('cashflows', function (Blueprint $table) {
                  $table->uuid('id')->primary()->unique();
                  $table->uuid('user_id');
                  $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
                  $table->uuid('turn_id');
                  $table->foreign('turn_id')->references('id')->on('turns')->onDelete('cascade');

                  $table->integer('opening_balance')->default(0);
                  $table->integer('receiving_sight')->default(0);
                  $table->integer('time_receipt')->default(0);
                  $table->integer('anticipated_receivables')->default(0);
                  $table->integer('application_redemption')->default(0);
                  $table->integer('sale_of_machines')->default(0);
                  $table->integer('miscellaneous_income')->default(0);
                  $table->integer('machine_financing')->default(0);
                  $table->integer('scheduled_loan')->default(0);

                  $table->integer('entry')->default(0);

                  $table->integer('payroll')->default(0);
                  $table->integer('training')->default(0);
                  $table->integer('adv')->default(0);
                  $table->integer('expense')->default(0);
                  $table->integer('stocking_costs')->default(0);
                  $table->integer('payment_suppliers')->default(0);
                  $table->integer('purchase_machines')->default(0);
                  $table->integer('machine_maintenance')->default(0);
                  $table->integer('amortization_loans')->default(0);
                  $table->integer('loan_value')->default(0);
                  $table->integer('general_delay')->default(0);
                  $table->integer('bir')->default(0);
                  $table->integer('income_tax')->default(0);
                  $table->integer('application')->default(0);
                  $table->integer('exit')->default(0);

                  $table->integer('loan_limit')->default(400000);
                  $table->integer('final_balance')->default(0);

                  $table->integer('receiving_next_turn')->default(0);
                  $table->integer('payment_next_turn')->default(0);
                  $table->integer('sell_machine_next_turn')->default(0);

                  $table->integer('next_tax_pay')->default(0);
                  $table->integer('cpv')->default(0);
                  $table->integer('stock_mp')->default(0);
                  $table->integer('stock_pa')->default(0);
                  $table->integer('tmp_machine_small')->default(0);
                  $table->integer('tmp_machine_medium')->default(0);
                  $table->integer('tmp_machine_large')->default(0);

                  $table->integer('tmp_clients')->default(0);
                  $table->integer('tmp_sell_machine_values')->default(0);
                  $table->integer('tmp_payment_suppliers')->default(0);
                  $table->integer('tmp_tax')->default(0);
                  $table->integer('tmp_stock_a')->default(0);
                  $table->integer('tmp_stock_b')->default(0);
                  $table->integer('tmp_stock_c')->default(0);
                  $table->integer('tmp_stock_product')->default(0);
                  $table->integer('tmp_delay')->default(0);
                  $table->integer('tmp_delay_bank')->default(0);
                  $table->integer('tmp_delay_suppliers')->default(0);
                  $table->integer('tmp_loan_to_due')->default(0);
                  $table->integer('tmp_stock_dre_product')->default(0);
                  $table->integer('tmp_stock_dre_raw')->default(0);
                  $table->integer('tmp_bp_last_profit')->default(0);
                  $table->integer('tmp_bir')->default(0);

                  $table->timestamps();
            });
      }

      /**
       * Reverse the migrations.
       *
       * @return void
       */
      public function down()
      {
            Schema::drop('cashflows');
      }
}
