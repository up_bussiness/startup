<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateDecisionCompaniesTable.
 */
class CreateDecisionCompaniesTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('decision_companies', function(Blueprint $table) {
            $table->uuid('id')->primary()->unique();
            $table->uuid('turn_id');
            $table->uuid('user_id');
            $table->integer('price');
            $table->integer('term');

            $table->integer('adv_radio');
            $table->integer('adv_journal');
            $table->integer('adv_social');
            $table->integer('adv_outdoor');
            $table->integer('adv_tv');

            $table->integer('scheduled_rma');
            $table->integer('scheduled_rmb');
            $table->integer('scheduled_rmc');
            $table->integer('emergency_rma');
            $table->integer('emergency_rmb');
            $table->integer('emergency_rmc');
            $table->integer('payment_rm');
            $table->integer('activity_level');
            $table->integer('extra_production');

            $table->integer('buy_small_machine');
            $table->integer('sell_small_machine');
            $table->integer('buy_medium_machine');
            $table->integer('sell_medium_machine');
            $table->integer('buy_large_machine');
            $table->integer('sell_large_machine');

            $table->integer('admitted');
            $table->integer('fired');
            $table->integer('salary');
            $table->integer('training');

            $table->integer('loan');
            $table->integer('anticipation_of_receivables');
            $table->integer('application');
            $table->integer('term_interest');
            $table->integer('income')->default(0);
            $table->integer('expense')->default(0);

            $table->integer('b_s_m')->default(0);
            $table->integer('b_m_m')->default(0);
            $table->integer('b_l_m')->default(0);

            $table->foreign('turn_id')
                  ->references('id')
                  ->on('turns')
                  ->onDelete('cascade');
            $table->foreign('user_id')
                  ->references('id')
                  ->on('users')
                  ->onDelete('cascade');

            $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('decision_companies');
	}
}
