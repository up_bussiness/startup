<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateDresTable.
 */
class CreateDresTable extends Migration
{
      /**
       * Run the migrations.
       *
       * @return void
       */
      public function up()
      {
            Schema::create('dres', function (Blueprint $table) {
                  $table->uuid('id')->primary()->unique();
                  $table->uuid('user_id');
                  $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
                  $table->uuid('turn_id');
                  $table->foreign('turn_id')->references('id')->on('turns')->onDelete('cascade');

                  $table->integer('price');
                  $table->integer('price_total');
                  $table->float('cpv');
                  $table->integer('cpv_total');
                  $table->float('difference');
                  $table->integer('difference_total');
                  $table->float('raw_a');
                  $table->integer('raw_a_total');
                  $table->float('raw_b');
                  $table->integer('raw_b_total');
                  $table->float('raw_c');
                  $table->integer('raw_c_total');
                  $table->float('machine_maintain');
                  $table->integer('machine_maintain_total');
                  $table->float('salary_production');
                  $table->integer('salary_production_total');
                  $table->float('training');
                  $table->integer('training_total');
                  $table->float('stock_raw');
                  $table->integer('stock_raw_total');
                  $table->float('machine_depreciation');
                  $table->integer('machine_depreciation_total');
                  $table->float('building_depreciation');
                  $table->integer('building_depreciation_total');
                  $table->float('gross_profit');
                  $table->integer('gross_profit_total');

                  $table->float('income_sell');
                  $table->integer('income_sell_total');
                  $table->float('adv');
                  $table->integer('adv_total');
                  $table->float('salary_sellers');
                  $table->integer('salary_sellers_total');
                  $table->float('stock_pa');
                  $table->integer('stock_pa_total');

                  $table->float('admin_sell');
                  $table->integer('admin_sell_total');
                  $table->float('salary_admin');
                  $table->integer('salary_admin_total');

                  $table->float('operational_profit');
                  $table->integer('operational_profit_total');
                  $table->float('others_value', 10, 2);
                  $table->integer('others_value_total');
                  $table->float('profit_before_ir', 10, 2);
                  $table->integer('profit_before_ir_total');
                  $table->float('ir');
                  $table->integer('ir_total');
                  $table->float('profit_after_ir', 10, 2);
                  $table->integer('profit_after_ir_total');
                  $table->float('profit_margin');
                  $table->integer('profit_margin_total');

                  $table->timestamps();
            });
      }

      /**
       * Reverse the migrations.
       *
       * @return void
       */
      public function down()
      {
            Schema::drop('dres');
      }
}
