<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateOperationalsTable.
 */
class CreateOperationalsTable extends Migration
{
      /**
       * Run the migrations.
       *
       * @return void
       */
      public function up()
      {
            Schema::create('operationals', function (Blueprint $table) {
                  $table->uuid('id')->primary()->unique();
                  $table->uuid('user_id');
                  $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
                  $table->uuid('turn_id');
                  $table->foreign('turn_id')->references('id')->on('turns')->onDelete('cascade');
                  $table->integer('initial_stock_rma');
                  $table->integer('initial_stock_rmb');
                  $table->integer('initial_stock_rmc');
                  $table->integer('initial_stock_product');
                  $table->integer('production_rma');
                  $table->integer('production_rmb');
                  $table->integer('production_rmc');
                  $table->integer('production_product');
                  $table->integer('final_stock_rma');
                  $table->integer('final_stock_rmb');
                  $table->integer('final_stock_rmc');
                  $table->integer('final_stock_product');
                  $table->integer('machine_s_qnt');
                  $table->float('machine_s_age');
                  $table->integer('machine_m_qnt');
                  $table->float('machine_m_age');
                  $table->integer('machine_l_qnt');
                  $table->float('machine_l_age');
                  $table->integer('employees');
                  $table->integer('final_employees');
                  $table->float('productivity');
                  $table->float('production_man');
                  $table->integer('advertising');
                  $table->float('advertising_avg');
                  $table->integer('salary');
                  $table->integer('demand');
                  $table->integer('sales');
                  $table->integer('tmp_buy_machine_cash')->default(0);
                  $table->integer('tmp_buy_machine_financing')->default(0);
                  $table->timestamps();
                  $table->integer('demand_entra')->default(0);
                  $table->integer('venda_extra')->default(0);
            });
      }

      /**
       * Reverse the migrations.
       *
       * @return void
       */
      public function down()
      {
            Schema::drop('operationals');
      }
}
