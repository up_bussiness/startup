<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateStrategicPlanningsTable.
 */
class CreateStrategicPlanningsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('strategic_plannings', function(Blueprint $table) {
            $table->uuid('id')->primary()->unique();
            $table->uuid('user_id');
            $table->string('slogan');
						$table->string('mission');
						$table->string('view');
						$table->text('values');
						$table->text('weaknesses');
						$table->text('threats');
						$table->text('forces');
						$table->text('opportunities');
						$table->text('goals');

						$table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('strategic_plannings', function (Blueprint $table) {
      $table->dropForeign('strategic_plannings_user_id_foreign');
      $table->dropColumn('user_id');
    });

		Schema::drop('strategic_plannings');
	}
}
