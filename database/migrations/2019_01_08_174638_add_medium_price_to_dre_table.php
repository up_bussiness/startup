<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMediumPriceToDreTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('dres', function (Blueprint $table) {
            //
            $table->float('price_medium_a',10,6);
            $table->float('price_medium_b',10,6);
            $table->float('price_medium_c',10,6);
            $table->integer('revalorizacao_stock')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('dres', function (Blueprint $table) {
            //
            $table->dropColumn('price_medium_a');
            $table->dropColumn('price_medium_b');
            $table->dropColumn('price_medium_c');
            $table->dropColumn('revalorizacao_stock');
        });
    }
}
