<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateLimitLoansTable.
 */
class CreateLimitLoansTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('limit_loans', function(Blueprint $table) {
            $table->increments('id');
            $table->uuid('user_id');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->uuid('turn_id');
            $table->foreign('turn_id')->references('id')->on('turns')->onDelete('cascade');
            $table->integer('turn_without_delay');
            $table->integer('limit');
            $table->integer('loan');
            $table->integer('amortization');
            $table->integer('new_limit');
            $table->integer('fl_delay')->default(0);

            $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('limit_loans');
	}
}
