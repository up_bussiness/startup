<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsToSimulations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('simulations', function (Blueprint $table) {
            //Add the collumn 
            $table->tinyInteger('in_operational')->default(0);
            $table->tinyInteger('in_cashflow')->default(0)->after('in_operational');
            $table->tinyInteger('in_dre')->default(0)->after('in_cashflow');
            $table->tinyInteger('in_balance')->default(0)->after('in_dre');
            $table->tinyInteger('in_ranking')->default(1)->after('in_balance');
            $table->tinyInteger('in_transport')->default(0)->after('in_ranking');
            $table->string('locale',20)->default('pt-br')->after('in_transport');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('simulations', function (Blueprint $table) {
            //Drop the new collumns
            $table->dropColumn('in_operational');
            $table->dropColumn('in_cashflow');
            $table->dropColumn('in_dre');
            $table->dropColumn('in_balance');
            $table->dropColumn('in_ranking');
            $table->dropColumn('locale');
        });
    }
}
