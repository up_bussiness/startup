<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateInstituitionsTable.
 */
class CreateInstituitionsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('instituitions', function (Blueprint $table) {
			$table->uuid('id')->primary()->unique();
			$table->uuid('user_id');
			$table->string('name');
			$table->string('logo')->nullable();
			$table->string('slug');
			$table->integer('license');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('instituitions');
	}
}
