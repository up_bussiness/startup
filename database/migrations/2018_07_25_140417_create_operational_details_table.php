<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateOperationalDetailsTable.
 */
class CreateOperationalDetailsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('operational_details', function(Blueprint $table) {
            $table->uuid('id')->primary()->unique();
            $table->uuid('turn_id');
            $table->foreign('turn_id')->references('id')->on('turns')->onDelete('cascade');
            $table->integer('import_products')->default(0);
            $table->float('interest_average')->default(0);
            $table->float('salary_average')->default(0);
            $table->integer('demand')->default(0);
            $table->float('price_average')->default(0);
            $table->float('productivity_average')->default(0);
            $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('operational_details');
	}
}
