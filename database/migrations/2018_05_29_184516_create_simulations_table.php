<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateSimulationsTable.
 */
class CreateSimulationsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('simulations', function(Blueprint $table) {
                  $table->uuid('id')->primary()->unique();
                  $table->uuid('user_id');
                  $table->string('name');
                  $table->integer('number_of_company');
                  $table->integer('demand');
                  $table->date('initial_date');
                  $table->date('end_date');

                  $table->integer('price_raw_material_a')->default(100);
                  $table->integer('price_raw_material_b')->default(60);
                  $table->integer('price_raw_material_c')->default(30);
                  $table->integer('price_small_machine')->default(200000);
                  $table->integer('price_medium_machine')->default(550000);
                  $table->integer('price_large_machine')->default(900000);
                  $table->integer('price_adv_radio')->default(1500);
                  $table->integer('price_adv_journal')->default(1250);
                  $table->integer('price_adv_social')->default(3000);
                  $table->integer('price_adv_outdoor')->default(5000);
                  $table->integer('price_adv_tv')->default(10000);

                  $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
                  $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('simulations', function (Blueprint $table) {
                  $table->dropForeign('simulations_user_id_foreign');
                  $table->dropColumn('user_id');
            });
		Schema::drop('simulations');
	}
}
