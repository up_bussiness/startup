<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddVendaTotalOperationalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('operationals', function (Blueprint $table) {
            //Add the collumn 
            $table->integer('venda_total')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('operationals', function (Blueprint $table) {
            //Drop the new collumns
            $table->dropColumn('venda_total');
        });
    }
}