<?php
//#AUTO - Este arquivo foi alterado por conta da implementação da automatização do processo de simulação
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddProductIdUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->boolean('product_id')//Nome da coluna
            ->nullable() //Preenchimento não obrigatório
            ->default('1')//Valor inicial bike
            ->after ('company_id');//No final da tabela
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('product_id');
        });
    }
}
