<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EditRoleUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("ALTER TABLE users MODIFY COLUMN role ENUM('owner', 'admin', 'coordinator','company','aluno')");
        Schema::table('users', function (Blueprint $table) {
            $table->string('token_pn')->nullable();
            $table->uuid('occupation_id')->nullable();
            $table->uuid('company_id')->nullable();
            //$table->foreign('occupation_id')->references('id')->on('occupations');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            //$table->dropForeign('users_occupation_id_foreign');
            $table->dropColumn('occupation_id');
            $table->dropColumn('token_pn');
        });
    }
}
