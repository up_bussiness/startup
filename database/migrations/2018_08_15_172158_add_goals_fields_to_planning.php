<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddGoalsFieldsToPlanning extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('strategic_plannings', function (Blueprint $table) {
            $table->text('goals_general');
            $table->text('goals_specific');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('strategic_plannings', function (Blueprint $table) {
            $table->dropColumn('goals_general');
            $table->dropColumn('goals_specific');
        });
    }
}
