<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateBalancesTable.
 */
class CreateBalancesTable extends Migration
{
      /**
       * Run the migrations.
       *
       * @return void
       */
      public function up()
      {
            Schema::create('balances', function (Blueprint $table) {
                  $table->uuid('id')->primary()->unique();
                  $table->uuid('user_id');
                  $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
                  $table->uuid('turn_id');
                  $table->foreign('turn_id')->references('id')->on('turns')->onDelete('cascade');

                  $table->integer('active_cash')->default(0);
                  $table->integer('active_application')->default(0);
                  $table->integer('active_clients')->default(0);
                  $table->integer('active_stock_product')->default(0);
                  $table->integer('active_raw_a')->default(0);
                  $table->integer('active_raw_b')->default(0);
                  $table->integer('active_raw_c')->default(0);
                  $table->integer('active_machines')->default(0);
                  $table->integer('active_depreciation_machine')->default(0);
                  $table->integer('active_buildings')->default(0);
                  $table->integer('active_depreciation_buildings')->default(0);
                  $table->integer('active_total')->default(0);

                  $table->integer('passive_suppliers_win')->default(0);
                  $table->integer('passive_suppliers_arrears')->default(0);
                  $table->integer('passive_overdue_account')->default(0);
                  $table->integer('passive_tax_pay')->default(0);
                  $table->integer('passive_loans_financing_mature')->default(0);
                  $table->integer('passive_loans_financing_interest_arrears')->default(0);
                  $table->integer('net_worth')->default(0);
                  $table->integer('share_capital')->default(900000);
                  $table->integer('accumulated_profits')->default(0);
                  $table->integer('total')->default(0);

                  $table->integer('accumulated_profits_past')->default(0);
                  $table->integer('passive_suppliers_bir')->default(0);

                  $table->timestamps();
            });
      }

      /**
       * Reverse the migrations.
       *
       * @return void
       */
      public function down()
      {
            Schema::drop('balances');
      }
}
