<?php

use Illuminate\Database\Seeder;
use LogisticsGame\Models\User;
use LogisticsGame\Models\Instituition;
use LogisticsGame\Models\Simulation;
use Ramsey\Uuid\Uuid;

class DevelopmentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = $this->createAdmin();
        $this->createInstituition($admin);
        $coordinator = $this->createCoordinator();
        // $simulation = $this->createSimulation($coordinator);
        // $this->createCompanies($simulation);
    }

    public function createAdmin()
    {
        $admin = User::create([
            'name' => 'UNIVERSIDADE',
            'email' => 'admin@lg',
            'password' => bcrypt('123456'),
            'role' => 'admin'
        ]);

        return $admin->id;
    }

    public function createInstituition($admin)
    {
        $instituition = Instituition::create([
            'user_id' => $admin,
            'name' => 'Universidade de Fortaleza',
            'slug' => 'UNIVERSIDADE',
            'license' => 10,
            'logo' => 'e30044bc79f5aa4d.png',
        ]);
    }

    public function createCoordinator()
    {
        $coordinator = User::create([
            'name' => 'Professor Gustavo',
            'email' => 'gustavo@lg',
            'password' => bcrypt('123456'),
            'password_plain' => '123456',
            'role' => 'coordinator',
            'license' => 5
        ]);

        return $coordinator->id;
    }

    public function createSimulation($coordinator)
    {
        Simulation::flushEventListeners();
        $simulation = Simulation::create([
            'id' => Uuid::uuid1(),
            'user_id' => $coordinator,
            'name' => 'Turma Piloto (DEV)',
            'number_of_company' => 3,
            'demand' => 10,
            'initial_date' => \Carbon\Carbon::now(),
            'end_date' => \Carbon\Carbon::now()->addDays(10)
        ]);

        return $simulation->id;
    }

    public function createCompanies($simulation)
    {
        $company = User::create([
            'name' => 'Empresa 1',
            'email' => 'e1@lg',
            'password' => bcrypt('123456'),
            'password_plain' => '123456',
            'role' => 'company',
            'simulation_id' => $simulation
        ]);

        $company = User::create([
            'name' => 'Empresa 2',
            'email' => 'e2@lg',
            'password' => bcrypt('123456'),
            'password_plain' => '123456',
            'role' => 'company',
            'simulation_id' => $simulation
        ]);

        $company = User::create([
            'name' => 'Empresa 3',
            'email' => 'e3@lg',
            'password' => bcrypt('123456'),
            'password_plain' => '123456',
            'role' => 'company',
            'simulation_id' => $simulation
        ]);
    }
}
