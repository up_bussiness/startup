<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(OccupationsTableSeeder::class);
        $this->call(ThemesTableSeeder::class);
        $this->call(MattersTableSeeder::class);
        $this->call(TurnParametersTableSeeder::class);
    }
}
