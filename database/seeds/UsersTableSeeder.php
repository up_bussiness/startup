<?php

use Illuminate\Database\Seeder;
use LogisticsGame\Models\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->createOwnerUser();
    }

    public function createOwnerUser()
    {
        $owner = User::create([
            'name' => 'owner',
            'email' => 'owner@lg',
            'password' => bcrypt('123456'),
            'role' => 'owner'
        ]);
    }
}
