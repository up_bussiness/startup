    <?php

use Illuminate\Database\Seeder;
use LogisticsGame\Models\Occupation;

class OccupationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $occupation = Occupation::create([
        	'name' => 'Presidente'
        ]);

        $occupation = Occupation::create([
        	'name' => 'Diretor Comercial'
        ]);

        $occupation = Occupation::create([
        	'name' => 'Diretor de Produção'
        ]);

        $occupation = Occupation::create([
        	'name' => 'Diretor de Marketing'
        ]);

        $occupation = Occupation::create([
        	'name' => 'Diretor Financeiro'
        ]);

        $occupation = Occupation::create([
            'name' => 'Diretor de RH'
        ]);
    }
}
