<?php

use Illuminate\Database\Seeder;
use LogisticsGame\Models\Theme;

class ThemesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $theme = Theme::create([
            'name' => 'Demanda'
        ]);

        $theme = Theme::create([
            'name' => 'Economia'
        ]);

        $theme = Theme::create([
            'name' => 'Mercado'
        ]);

        $theme = Theme::create([
            'name' => 'Premiação'
        ]);

        $theme = Theme::create([
            'name' => 'Fornecedores'
        ]);

        $theme = Theme::create([
            'name' => 'Saudação'
        ]);

        $theme = Theme::create([
            'name' => 'Planejamento'
        ]);
    }
}
