<?php

namespace LogisticsGame\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface UserRepository.
 *
 * @package namespace LogisticsGame\Repositories;
 */
interface UserRepository extends RepositoryInterface
{
    public function createCompanies($quantity, $simulation);

    public function createCoordinator($request);

    public function getCoordinators();

    public function getLicenses();

    public function updateCoordinator($request);

    public function updateLicenses($request);

    public function getCompaniesNameId($id);

    public function getUserEmail($email);

    public function createAluno($nome, $email, $occupation_id, $simulation_id, $company_id);

    public function getCompanies($simulation_id);
}
