<?php

namespace LogisticsGame\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface BalanceRepository.
 *
 * @package namespace LogisticsGame\Contracts;
 */
interface BalanceRepository extends RepositoryInterface
{
    public function generate($turn_id);
}
