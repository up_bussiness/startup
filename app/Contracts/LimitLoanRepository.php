<?php

namespace LogisticsGame\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface LimitLoanRepository.
 *
 * @package namespace LogisticsGame\Contracts;
 */
interface LimitLoanRepository extends RepositoryInterface
{
    public function generate($companies, $turn, $month);
}
