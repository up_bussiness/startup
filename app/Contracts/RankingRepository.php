<?php

namespace LogisticsGame\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface RankingRepository.
 *
 * @package namespace LogisticsGame\Contracts;
 */
interface RankingRepository extends RepositoryInterface
{
    public function generate($companies, $turn, $month, $simulation_id);
}
