<?php

namespace LogisticsGame\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface TurnRepository.
 *
 * @package namespace LogisticsGame\Contracts;
 */
interface TurnRepository extends RepositoryInterface
{
    public function initialTurn($simulation_id);
    public function createTurn($month, $simulation_id);
    public function getSecondLast($simulation_id);
    public function getLastTurn($simulation_id);
    public function getLastTurnSimulation($simulation_id);
    public function turnIsGenerated($simulation_id);
}
