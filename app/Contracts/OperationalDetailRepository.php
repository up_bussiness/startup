<?php

namespace LogisticsGame\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface OperationalDetailRepository.
 *
 * @package namespace LogisticsGame\Contracts;
 */
interface OperationalDetailRepository extends RepositoryInterface
{
    public function initial($turn_id, $simulation_demand, $imported, $machines);

    public function averages($turn_id);

    public function generate($turn_id, $macro_industry, $imported);
}
