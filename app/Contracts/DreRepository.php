<?php

namespace LogisticsGame\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface DreRepository.
 *
 * @package namespace LogisticsGame\Contracts;
 */
interface DreRepository extends RepositoryInterface
{
    public function generate($turn_id);

    public function generateDebug($turn_id);

    public function lastPriceMedium($month,$simulation_id,$user_id);

    public function lastPriceMedium_DEBUG($month,$simulation_id,$user_id);
}
