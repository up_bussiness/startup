<?php

namespace LogisticsGame\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface DecisionCompanyRepository.
 *
 * @package namespace LogisticsGame\Contracts;
 */
interface DecisionCompanyRepository extends RepositoryInterface
{
    public function initialDecision($companies, $turn_id);
    public function getSalaryPrice($turn_id);
    public function generateAvg($turn_id);
    public function getDecisionOp($turn_id);
    public function defaultDecision($company, $turn);
    public function getQtdDecisionCompanies($turn_id);
    public function getDecisionCompanies($turn_id, $company_id);
}
