<?php
//#AUTO - Este arquivo foi alterado por conta da implementação da automatização do processo de simulação
namespace LogisticsGame\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface TurnParameterRepository.
 *
 * @package namespace LogisticsGame\Contracts;
 */
interface TurnParameterRepository extends RepositoryInterface
{
    public function defaultTurnParameter($month);
}

