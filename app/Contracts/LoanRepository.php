<?php

namespace LogisticsGame\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface LoanRepository.
 *
 * @package namespace LogisticsGame\Contracts;
 */
interface LoanRepository extends RepositoryInterface
{
    public function scheduled($value, $company,$bir);
}
