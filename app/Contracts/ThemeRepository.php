<?php

namespace LogisticsGame\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface ThemeRepository.
 *
 * @package namespace LogisticsGame\Contracts;
 */
interface ThemeRepository extends RepositoryInterface
{
    //
}
