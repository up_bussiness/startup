<?php

namespace LogisticsGame\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface StrategicPlanningRepository.
 *
 * @package namespace LogisticsGame\Contracts;
 */
interface StrategicPlanningRepository extends RepositoryInterface
{
  public function myStrategic();
}
