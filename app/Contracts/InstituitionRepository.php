<?php

namespace LogisticsGame\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface InstituitionRepository.
 *
 * @package namespace LogisticsGame\Contracts;
 */
interface InstituitionRepository extends RepositoryInterface
{
  public function createInstituition(array $request, int $user);
}
