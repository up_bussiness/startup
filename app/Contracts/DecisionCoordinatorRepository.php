<?php

namespace LogisticsGame\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface DecisionCoordinatorRepository.
 *
 * @package namespace LogisticsGame\Contracts;
 */
interface DecisionCoordinatorRepository extends RepositoryInterface
{
  public function decisionCoordinator($turn_id);

  public function initialDecision($turn_id);

  public function lastPrices($turn_id);

  public function RodadaAnterior($turn_id);

  public function defaultInitialDecision($turn_id, $month);
}
