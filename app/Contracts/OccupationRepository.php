<?php

namespace LogisticsGame\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface OccupationRepository.
 *
 * @package namespace LogisticsGame\Contracts;
 */
interface OccupationRepository extends RepositoryInterface
{
    //
}
