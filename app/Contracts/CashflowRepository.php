<?php

namespace LogisticsGame\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface CashflowRepository.
 *
 * @package namespace LogisticsGame\Contracts;
 */
interface CashflowRepository extends RepositoryInterface
{
    public function generate($turn_id);

    public function lastPriceMedium($month,$simulation_id,$user_id);
}
