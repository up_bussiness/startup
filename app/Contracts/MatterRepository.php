<?php

namespace LogisticsGame\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface MatterRepository.
 *
 * @package namespace LogisticsGame\Contracts;
 */
interface MatterRepository extends RepositoryInterface
{
    //
}
