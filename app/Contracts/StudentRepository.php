<?php

namespace LogisticsGame\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface StudentRepository.
 *
 * @package namespace LogisticsGame\Contracts;
 */
interface StudentRepository extends RepositoryInterface
{
    //
}
