<?php

namespace LogisticsGame\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface SimulationRepository.
 *
 * @package namespace LogisticsGame\Repositories;
 */
interface SimulationRepository extends RepositoryInterface
{

	public function getMySimulations();

	public function getMyLicenses();

	public function getLastTurn($simulation_id);

	public function reset();

	public function canCreateTurn($simulation_id);

	public function getActiveSimulations();
}
