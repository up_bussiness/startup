<?php

namespace LogisticsGame\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface OperationalRepository.
 *
 * @package namespace LogisticsGame\Contracts;
 */
interface OperationalRepository extends RepositoryInterface
{
    public function generate($turn_id);
}
