<?php

namespace LogisticsGame\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface NewspaperRepository.
 *
 * @package namespace LogisticsGame\Contracts;
 */
interface NewspaperRepository extends RepositoryInterface
{

    public function getMaterials();

    public function initialInformation($turn_id);

    public function defaultTurnNews($turn_id, $month);
}
