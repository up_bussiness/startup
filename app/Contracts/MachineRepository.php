<?php

namespace LogisticsGame\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface MachineRepository.
 *
 * @package namespace LogisticsGame\Contracts;
 */
interface MachineRepository extends RepositoryInterface
{
    public function initialInventory($companies, $turn);
    public function getMachinesQuantity($company, $turn);
    public function getMachinesAge($company, $turn);
    public function getDepreciation($company, $turn);
    public function getDepreciationDre($company, $turn);
    public function getValue($company, $turn);
    public function buy($turn, $company, $quantity, $type, $price, $formPagto,$bir);
    public function spend($companies, $turn, $lastTurn);
    public function sell($companies, $turn);
    public function sell_machine_id($type, $quantity, $company, $turn);
}
