<?php

namespace LogisticsGame\ViewComposers;

use Illuminate\View\View;

use LogisticsGame\Models\Instituition;
use Illuminate\Support\Facades\Auth;

class InstituitionViewComposer
{

	public function __construct()
	{
	}

	public function compose(View $view)
	{
		$view->with('instituitionVC', Instituition::first());
	}
}
