<?php

namespace LogisticsGame\Console\Commands;

use Illuminate\Console\Command;
use LogisticsGame\Models\Machine;
use LogisticsGame\Models\OperationalDetail;
use LogisticsGame\Models\Operational;
use LogisticsGame\Models\Cashflow;
use LogisticsGame\Models\Balance;
use LogisticsGame\Models\Dre;
use LogisticsGame\Models\Ranking;
use LogisticsGame\Models\LimitLoan;
use LogisticsGame\Models\Turn;
use LogisticsGame\Tenant\ManagerTenant;
use LogisticsGame\Models\Cliente;

use LogisticsGame\Models\Newspaper;
use LogisticsGame\Models\DecisionCompany;
use LogisticsGame\Models\DecisionCoordinator;


class ExcluirRodada extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'rodada:excluir {dominio} {turn_id} {etapa}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Excluir os registros,de todas as tabelas, que faz referencia a tabela Turn. etapa = 1 = excluir processamento etapa = 2 = excuir rodada';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $id = $this->argument('turn_id');
        $etapa = $this->argument('etapa');
        $dominio = $this->argument('dominio');

        $this->ExcluirUltimaRodada($dominio, $id, $etapa);
    }

    public function ExcluirUltimaRodada($dominio, $turn_id, $etapa)
    {
        echo 'processando...';
        $manager = app(ManagerTenant::class);

        $clientes = Cliente::where('domain', $dominio)->First();

        $manager->setConnection($clientes);
        if ($etapa == 1) { // Desfazer Processamento
            Machine::where('turn_id', $turn_id)->delete();
            OperationalDetail::where('turn_id', $turn_id)->delete();
            Operational::where('turn_id', $turn_id)->delete();
            Cashflow::where('turn_id', $turn_id)->delete();
            Balance::where('turn_id', $turn_id)->delete();
            Dre::where('turn_id', $turn_id)->delete();
            Ranking::where('turn_id', $turn_id)->delete();
            LimitLoan::where('turn_id', $turn_id)->delete();
        } else if ($etapa == 2) { // Desfazer criação de Rodada
            Turn::where('id', $turn_id)->delete();
        }

        echo 'Registros excluidos com sucesso!';
    }
}
