<?php
//#AUTO - Este arquivo foi alterado por conta da implementação da automatização do processo de simulação
namespace LogisticsGame\Console\Commands;

use Illuminate\Support\Facades\DB;
use Illuminate\Console\Command;
use LogisticsGame\Tenant\ManagerTenant;

//Models
use LogisticsGame\Models\Cliente;

//Interfaces
use LogisticsGame\Contracts\SimulationRepository;
use LogisticsGame\Contracts\UserRepository;
use LogisticsGame\Contracts\DecisionCompanyRepository;
use LogisticsGame\Contracts\TurnRepository;
use LogisticsGame\Contracts\RankingRepository;
use LogisticsGame\Contracts\DecisionCoordinatorRepository;
use LogisticsGame\Contracts\NewspaperRepository;

//Controllers
use LogisticsGame\Http\Controllers\ReportsController;

use Closure;

class DecisaoDoProfessor extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'decisao:prof';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Automatizar todas as decisões do professor';

    protected $simulationRepo, $userRepo, $decisionRepo, $turnRepo, $rankingRepo, $decCoordRepo, $newsRepo, $reportsCtrl;
    
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(SimulationRepository $simulationRepo, UserRepository $userRepo, DecisionCompanyRepository $decisionRepo, TurnRepository $turnRepo, RankingRepository $rankingRepo, DecisionCoordinatorRepository $decCoordRepo, NewspaperRepository $newsRepo, ReportsController $reportsCtrl)
    {
        $this->simulationRepo = $simulationRepo;
        $this->userRepo = $userRepo;
        $this->decisionRepo = $decisionRepo;
        $this->turnRepo = $turnRepo;
        $this->rankingRepo = $rankingRepo;
        $this->decCoordRepo = $decCoordRepo;
        $this->newsRepo = $newsRepo;
        $this->reportsCtrl = $reportsCtrl;
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $manager = app(ManagerTenant::class);
        
        $this->info('-------------------------------------------------');
        $this->info('Iniciando processamento automático das simulações');
        $this->info('-------------------------------------------------');
        $this->info('*');
        
        //Busca todos os clientes existentes na base de dados
        $clients = Cliente::all();
        //Percorre a lista de clientes, pega os dados de configuração e conecta em cada base de dados registrada
        foreach($clients as $client) {
            $this->info('--------');
            $this->info('Cliente: '.$client->id.' - '.$client->name);
            $this->info('--------');
            //Conecta na base de dados do cliente com os dados obtidos na base central
            $manager->setConnection($client);
            $manager->setFileSystems($client);

            //Procura as simulações ativas para o cliente atual (data de fim da simulação menor que a data atual)
            $simulations = $this->simulationRepo->getActiveSimulations();
            
            //Verifica se existem simulações ativas e trata as que existirem
            if(count($simulations)==0){
                $this->info('Não existe nenhuma simulação ativa para a instituição!');
                continue;
            }
            
            //Percorre a lista das simulações ativas do cliente atual
            foreach($simulations as $simulation) {
                $this->info('----------------');
                $this->info('Simulação ativa: '.$simulation->name. ' - '.'Data Final: '.$simulation->end_date);
                $this->info('----------------');

                //Busca a última rodada da simulação
                $turns = $this->turnRepo->getLastTurnSimulation($simulation->id);
                $turn = $turns->first();
                //Verifica se a rodada está vencida
                if($turn->end_date > date("Y-m-d H:i:s")){
                    $this->info('Última rodada existente na simulação: '.$turn->month.' - '.$turn->end_date);
                    $this->info('Não existe nenhuma rodada vencida para a simulação. Nenhum processamenmto automático será realizado.');
                } else {//Existe um turno vencido
                    
                    $this->info('Última rodada vencida: '.$turn->month.' - '.$turn->end_date);

                    //Busca a lista de empresas da simulação para usar posteriormente no tratamento das decisões das empresas
                    $companies = $this->userRepo->getCompanies($simulation->id);
                    $this->info('Empresas: '.count($companies)); 

                    //Verifica se a rodada já foi processada
                    $generated = $this->turnRepo->turnIsGenerated($simulation->id);
                    if (!$generated) {//Se não foi processada
                        $this->info('Ainda não houve processamento para esta rodada');
                        $decisions = $this->decisionRepo->getQtdDecisionCompanies($turn->id);
                        $numComp = $simulation->number_of_company;

                        //Verifica se falta alguma decisão de alguma empresa
                        if ($decisions < $numComp){
                            $this->info('Pelo menos uma empresa ainda não fez sua decisão e será aplicada a decisão padrão automaticamente.');
                            $this->info('Foi(ram) tomada(s) '.$decisions.' decisão(ões) e são '.$numComp.' empresas na simulação.');
                            foreach($companies as $company) {
                                //Busca a decisão da empresa para a rodada tratada
                                $decision = $this->decisionRepo-> getDecisionCompanies($turn->id, $company->id);
                                //Verifica se a empresa tomou decisão para a rodada tratada
                                if (count($decision)==0){
                                    //Toma decisão pela empresa com valores padrão do turno
                                    $decisionCompany = $this->decisionRepo->defaultDecision($company->id, $turn->id);
                                    $this->info('Decisão padrão tomada automaticamente para a empresa: "'.$company->name.'" no turno: '.$turn->month);
                                } else{
                                    $this->info('A empresa: "'.$company->name.'" já tomou sua decisão e nenhuma ação automática será necessária.');
                                }    
                            }

                            //Recarrega as decisões para verificar se já pode fechar o período
                            $decisions = $this->decisionRepo->getQtdDecisionCompanies($turn->id);
                            $this->info('Recarregando decisões das empresas para verificar número de decisões.');
                        }  else {
                            $this->info('Todas as decisões já haviam sido tomadas.');
                        }
                        //Se o número de decisões é igual ao número de empresas já pode processar o período
                        if ($decisions == $numComp){
                            $this->info('Todas as decisões para o turno '.$turn->month.' já foram tomadas. Periodo será processado.');
                            //Atualiza a data final o turno com a data atual
                            $timenow = \Carbon\Carbon::now()->subSeconds(5);

                            //Processa o ranking e consequentemente o período e cria uma nova rodada com os valores default
                            $rankingProc = $this->reportsCtrl->generate ($turn->id);
                            $this->info('Turno '.$turn->month.' encerrado. Periodo processado.');
                        }//Fim do processamento do turno

                    } else {//Rodada atual já processada
                        $this->info('A rodada atual já foi processada e nenhuma ação automática será realizada.');
                    } // Fim do tratamento das rodadas não processadas
                } //Fim do tratamento das rodadas vencidas

                $this->info('--------------');
                $this->info('Fim da simulação "'.$simulation->name.'"');
                $this->info('--------------');
                $this->info('*');
            }//fim da lista de simulações

        }// Fim da lista de clientes
        
        $this->info('-------------------------------------------------');
        $this->info('Fim do processamento automático das simulações');
        $this->info('-------------------------------------------------');
    }
}
