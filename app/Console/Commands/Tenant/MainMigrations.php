<?php

namespace LogisticsGame\Console\Commands\Tenant;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;

class MainMigrations extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'main:migrations';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Carrega Migration Principal';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Artisan::call('migrate', [
            '--force' => true,
            '--path' => '/database/migrations/main',
        ]);
    }
}
