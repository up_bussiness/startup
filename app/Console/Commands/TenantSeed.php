<?php

namespace LogisticsGame\Console\Commands;

use Illuminate\Console\Command;
use LogisticsGame\Tenant\ManagerTenant;
use Illuminate\Support\Facades\Artisan;
use LogisticsGame\Models\Cliente;

class TenantSeed extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'tenants:seed {id?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Carrega o seed do banco indicado pelo Id';
    private $tenant;
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(ManagerTenant $tenant)
    {
        parent::__construct();
        $this->tenant = $tenant;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if ($id = $this->argument('id')) {
            $cli = Cliente::find($id);

            if ($cli) {
                $this->execCommand($cli);
            }
            return;
        }
    }

    public function execCommand(Cliente $cli)
    {
        $this->tenant->setConnection($cli);
        $this->info("Gerando Seeds - Cliente {$cli->name} ");

        Artisan::call('db:seed');

        $this->info("Tabelas - Cliente {$cli->name} geradas");
        $this->info("--------------------------------------");
    }
}
