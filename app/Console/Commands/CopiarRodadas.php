<?php

namespace LogisticsGame\Console\Commands;

use Illuminate\Console\Command;

//Models
use LogisticsGame\Models\Turn;

//interfaces
use LogisticsGame\Contracts\TurnRepository;
use LogisticsGame\Contracts\DecisionCoordinatorRepository;
use LogisticsGame\Contracts\NewspaperRepository;
use LogisticsGame\Contracts\DecisionCompanyRepository;
use LogisticsGame\Contracts\UserRepository;
//Controllers
use LogisticsGame\Http\Controllers\ReportsController;

class CopiarRodadas extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'decisao:copiar {idSimOr} {idSimDe}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Copia as rodadas de uma simulação para outra simulação nova, com as mesmas quantidades de empresas';

    protected $turnRepo, $decisaoCoordRepo, $newsRepo, $decisaoCompany, $usersRepo, $reportsCtrl;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(TurnRepository $turnRepo, DecisionCoordinatorRepository $decisaoCoordRepo, NewspaperRepository $newsRepo, DecisionCompanyRepository $decisaoCompany, UserRepository $usersRepo, ReportsController $reportsCtrl)
    {
        $this->turnRepo = $turnRepo;
        $this->decisaoCoordRepo = $decisaoCoordRepo;
        $this->newsRepo = $newsRepo;
        $this->decisaoCompany = $decisaoCompany;
        $this->usersRepo = $usersRepo;
        $this->reportsCtrl = $reportsCtrl;
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        \Log::info('Empresa : 8');
        // Ids das simulações $this->argument('user');
        $simIdOrigem = $this->argument('idSimOr');
        $simIdDestino = $this->argument('idSimDe');
        // $simIdOrigem = '0a688b3a-73a4-11ea-b189-226a819c58f9';
        // $simIdDestino = 'f75dfd94-a02c-11ea-8cb8-d09466a24655';

        $this->info('---' . $simIdOrigem . '--->' . $simIdDestino);

        // Atualizar dados das Empresas
        $this->info('Atualizando empresas');
        $this->usersRepo->atualizaEmpresas($simIdOrigem, $simIdDestino);

        // Copiar alunos
        $this->info('Copiando alunos para Users');
        $this->usersRepo->copiarAlunos($simIdOrigem, $simIdDestino);

        // Copias Students
        $this->info('Copiando students do PE');
        $this->usersRepo->copiarStudents($simIdOrigem, $simIdDestino);

        // Copiar PE
        $this->info('Copiando PE');
        $this->usersRepo->copiarPE($simIdOrigem, $simIdDestino);

        //Ultima rodadas
        $turnsOr = $this->turnRepo->getLastTurnSimulation($simIdOrigem);
        $turnOr = $turnsOr->first();
        $turnsDe = $this->turnRepo->getLastTurnSimulation($simIdDestino);
        $turnDe = $turnsDe->first();
        $this->info('ultima rodada Origem :' . $turnOr->month . ' Ultima rodada destino :' . $turnDe->month);

        // Gerar todas as rodadas
        //for ($z = $turnDe->month; $z <= $turnDe->month; $z++) {
        for ($z = $turnDe->month; $z < $turnOr->month; $z++) {
            // Criar Nova Rodada no Destino
            $novaRodada = $z + 1;
            $newTurn = $this->turnRepo->createTurn($novaRodada, $simIdDestino);
            $this->info('Criou rodada :' . $novaRodada);

            // Pega id da rodada origem
            $turnOri = Turn::where('simulation_id', $simIdOrigem)->where('month', $novaRodada)->get(['id'])->first();

            // Criar Decisao coodernador - retorna o id da rodada criada
            $newIdTurnDes = $this->decisaoCoordRepo->copiaDecisionCoordinator($turnOri->id, $novaRodada, $simIdDestino);
            $this->info('Decisão coordenador criada com id rodada : ' . $newIdTurnDes);

            // Criar noticias
            $this->newsRepo->copiaNewspaper($turnOri->id, $newIdTurnDes->id);
            //$this->newsRepo->copiaNewspaper('b97c6a5a-60b8-11ea-9d9c-226a819c58f9', '4b64e322-88c1-11ea-8712-d09466a24655');
            $this->info('Copiou notícias --');

            // Pega Ids Empresas Origem
            $companyIdsOri =  $this->usersRepo->getCompanies($simIdOrigem);

            // Pega Ids Empresas Destino
            $companyIdsDes =  $this->usersRepo->getCompanies($simIdDestino);
            $this->info('Pega IDs --');
            // Copia Decisao das Empresas
            for ($x = 0; $x < count($companyIdsOri); $x++) {
                $this->decisaoCompany->copiaDecision($companyIdsOri[$x]->id, $turnOri->id, $companyIdsDes[$x]->id, $newIdTurnDes->id);
            }
            $this->info('Copiou decisões de :' . count($companyIdsOri) . ' das empresas');

            // Muda status - publicar
            $timenow = \Carbon\Carbon::now()->subSeconds(5);
            $this->turnRepo->update(['published' => 1, 'end_date' => $timenow], $newIdTurnDes->id);
            $this->info('Publicado e Fechado...');

            //Processar Rodada
            $this->reportsCtrl->generate($newIdTurnDes->id);
            $this->info('Processado...Ok');
        }
    }
}
