<?php

namespace LogisticsGame\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use LogisticsGame\Contracts\TurnRepository;
use LogisticsGame\Models\Turn;
use LogisticsGame\Models\Ranking;
use LogisticsGame\Validators\TurnValidator;
use Carbon\Carbon;

use LogisticsGame\Models\DecisionCoordinator;

/**
 * Class TurnRepositoryEloquent.
 *
 * @package namespace LogisticsGame\Repositories;
 */
class TurnRepositoryEloquent extends BaseRepository implements TurnRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Turn::class;
    }



    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    public function initialTurn($simulation_id)
    {
        // 1 Turno
        $turn = $this->model->create([
            'simulation_id' => $simulation_id,
            'month' => 1,
            //aqui mudou por conta da configuração inicial a ser feita pelas equipes
            //'end_date' => Carbon::now(),
            //'published' => true
            'end_date' => Carbon::now()->addDays(7),
            'published' => false
        ]);        

        // 4 Informativo

        return $turn;
    }

    public function createTurn($month, $simulation_id)
    {
        // O jogo encerra com 16 rodadas
        if ($month > 16)
            return null;

        $turn = $this->model->create([
            'simulation_id' => $simulation_id,
            'month' => $month,
            'end_date' => Carbon::now()->addDays(7),
            'published' => false
        ]);

        return $turn;
    }

    public function getSecondLast($simulation_id)
    {
        $turn = $this->model->where('simulation_id', $simulation_id)
                            ->with('decisionCoordinator')
                            ->orderBy('month', 'desc')
                            ->skip(1)
                            ->take(1)
                            ->first();

        return $turn->decisionCoordinator;

    }

    public function getLastTurn($simulation_id)
    {
        $turn = $this->model->where('simulation_id', $simulation_id)
                            ->orderBy('month', 'desc')
                            ->skip(1)
                            ->take(1)
                            ->first();
        return $turn;
    }

    public function getLastTurnSimulation($simulation_id)
    {
        $turn = $this->model->where('simulation_id', $simulation_id)
            ->orderBy('month', 'desc')
            ->take(1)
            ->get();
        return $turn;
    }
    
    public function turnIsGenerated($simulation_id)
    {
        $turn = $this->model->where('simulation_id', $simulation_id)
                            ->orderBy('month', 'desc')
                            ->first();

        $ranking = Ranking::where('turn_id', $turn->id)->get();

        if ($ranking->count() > 0) {
            return true;
        }

        return false;
    }
    public function getDecisionCoordinatorTurn($turn_id)
    {
        $turn = $this->model->where('id', $turn_id)
                            ->take(1)
                            ->first();
        return $turn->decisionCoordinator;        
    }
}
