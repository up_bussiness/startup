<?php

namespace LogisticsGame\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use LogisticsGame\Contracts\DreRepository;
use LogisticsGame\Models\Dre;
use LogisticsGame\Models\Balance;
use LogisticsGame\Validators\DreValidator;
use LogisticsGame\Models\Turn;

use Illuminate\Container\Container as Application;

/**
 * Class DreRepositoryEloquent.
 *
 * @package namespace LogisticsGame\Repositories;
 */
class DreRepositoryEloquent extends BaseRepository implements DreRepository
{
    private $decisionCompany, $decisionCoordinator, $cashflow, $operational, $machine, $balance;

    public function __construct(Application $app)
    {
        parent::__construct($app);
        $this->decisionCompany = app('LogisticsGame\Contracts\DecisionCompanyRepository');
        $this->decisionCoordinator = app('LogisticsGame\Contracts\DecisionCoordinatorRepository');
        $this->cashflow = app('LogisticsGame\Contracts\CashflowRepository');
        $this->operational = app('LogisticsGame\Contracts\OperationalRepository');
        $this->balance = app('LogisticsGame\Contracts\BalanceRepository');
        $this->machine = app('LogisticsGame\Contracts\MachineRepository');
        $this->loan = app('LogisticsGame\Contracts\LoanRepository');
    }
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Dre::class;
    }



    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    public function generate($turn_id)
    {
        $companies = $this->operational->findWhere(['turn_id' => $turn_id]);
        $coordinator = $this->decisionCoordinator->findWhere(['turn_id' => $turn_id], ['income_tax', 'raw_material_a', 'raw_material_b', 'raw_material_c'])->first();
        //$last_price = $this->decisionCoordinator->lastPrices($turn_id);

        foreach ($companies as $c) {

            $decision = $this->decisionCompany->findWhere(['turn_id' => $turn_id, 'user_id' => $c->user_id])->first();
            $cashflow = $this->cashflow->findWhere(['turn_id' => $turn_id, 'user_id' => $c->user_id])->first();
            $UltimoPrecoMedio = $this->lastPriceMedium($c->turn->month, $c->turn->simulation_id, $c->user_id);
            // DC
            $sales = ($c->sales == 0) ? 1 : $c->sales;
            $price = $decision->price;

            $price_total = ($price * $sales);
            //$price_total = ($price * $sales) + $cashflow->anticipated_receivables;
            //$price_total = $cashflow->receiving_sight + $cashflow->tmp_clients + $cashflow->anticipated_receivables;

            // Juros de Emprestimo - @CD#EMPRESTIMO
            $loans = $this->loan->findWhere(['user_id' => $c->user_id]);
            $juros_apropriar_dif = 0;
            foreach ($loans as $lo) {
                $parcelafixa = ParcelaEmprestimoJuros($lo->value, $lo->bir);
                if ($lo->time != 5 && $lo->time != 0) {
                    $juros_apropriar_dif = $juros_apropriar_dif + EmprestimoJurosApropriarDif($lo->value, $lo->bir, $lo->time, $parcelafixa);
                }
                $lo->time != 0 ? $lo->time-- : 0;
                $lo->save();
            }

            $difference_total = $cashflow->tmp_bir + $juros_apropriar_dif;
            $difference = $difference_total / $sales;
            // Preço medio Novo
            //dump($c->user_id);        
            //dump('dre: '.$UltimoPrecoMedio[0].' - '.$UltimoPrecoMedio[1].' - '.$UltimoPrecoMedio[2]);       
            $pmf_a = precoMedioFinal($c->initial_stock_rma, $decision->scheduled_rma, $decision->emergency_rma, $UltimoPrecoMedio[0], $coordinator->raw_material_a);
            $pmf_b = precoMedioFinal($c->initial_stock_rmb, $decision->scheduled_rmb, $decision->emergency_rmb, $UltimoPrecoMedio[1], $coordinator->raw_material_b);
            $pmf_c = precoMedioFinal($c->initial_stock_rmc, $decision->scheduled_rmc, $decision->emergency_rmc, $UltimoPrecoMedio[2], $coordinator->raw_material_c);

            $raw_a = $pmf_a;
            $raw_b = ($pmf_b * 2);
            $raw_c = ($pmf_c * 3);

            /*$raw_a_total = $pmf_a * $sales;
            $raw_b_total = ($pmf_b * 2) * $sales;
            $raw_c_total = ($pmf_c * 3) * $sales;*/
            $raw_a_total = $pmf_a * $c->production_product;
            $raw_b_total = ($pmf_b * 2) * $c->production_product;
            $raw_c_total = ($pmf_c * 3) * $c->production_product;

            // dd('a:'.$pmf_a,'b:'.$pmf_b,'c:'.$pmf_c);     
            // FC
            $machine_maintain_total = $cashflow->tmp_machine_maintenance;
            $machine_maintain = $machine_maintain_total / $sales;
            // ***** Salários ******
            if ($decision->extra_production == 0) {
                $salary_production_total = $c->salary * $c->final_employees;
            } else {
                $salary_production_total = ((($c->salary * 1.5) * ($decision->extra_production / 100)) + $c->salary) * $c->final_employees;
            }
            $salary_production = $salary_production_total / $sales;
            //
            $training_total = $cashflow->tmp_training;
            $training = $training_total / $sales;
            //Estocagem das matérias-primas
            $stock_raw_total = $cashflow->tmp_stock_dre_raw;
            $stock_raw = $stock_raw_total / $sales;
            //Estocagem de produtos acabados
            $stock_pa_total = $cashflow->tmp_stock_dre_product;
            $stock_pa = $stock_pa_total / $sales;

            if ($c->turn->month != 1) {
                $machine_depreciation_total = $this->machine->getDepreciationDre($c->user_id, $turn_id);
                $building_depreciation_total = 1500;
            } else {
                $machine_depreciation_total = 210000;
                $building_depreciation_total = 91500;
            }
            $machine_depreciation = $machine_depreciation_total / $sales;
            $building_depreciation = $building_depreciation_total / $sales;

            // Custo Produto Vendido ============================================================================
            $bp = $this->balance->findWhere(['turn_id' => $turn_id, 'user_id' => $c->user_id])->first();
            //$cpv_total = $raw_a_total + $raw_b_total + $raw_c_total;
            //$cpv = round($cpv_total / $sales, 2);
            $cpv = ($pmf_a + ($pmf_b * 2) + ($pmf_c * 3)) + $stock_raw + $stock_pa + $salary_production + $machine_maintain + $training;
            $revalorizacao_stock = 0;

            if ($c->turn->month != 1) { //REVALORIZAÇÃO

                $application_bp = Balance::select(['active_application', 'active_stock_product'])
                    ->where('user_id', $c->user_id)
                    ->orderBy('created_at', 'desc')->skip(1)->take(1)->first();

                // EPA
                if ($c->final_stock_product > 0 || $c->initial_stock_product > 0) {
                    /* $revalorizacao_anterior = $application_bp->active_stock_product < 0 ? $application_bp->active_stock_product * (-1) : $application_bp->active_stock_product;

                    $re_mp_a = ($raw_a_total / $c->production_product) * $c->final_stock_product;
                    $re_mp_b = ($raw_b_total / $c->production_product) * $c->final_stock_product;
                    $re_mp_c = ($raw_c_total / $c->production_product) * $c->final_stock_product;
                    $re_estoc_mp = ($stock_raw_total / $c->production_product) * $c->final_stock_product;
                    $re_estoc_pa = ($stock_pa_total / $c->production_product) * $c->final_stock_product;
                    $re_folha = ($salary_production_total / $c->production_product) * $c->final_stock_product;
                    $re_manu_maq = ($machine_maintain_total / $c->production_product) * $c->final_stock_product;
                    $re_treina = ($training_total / $c->production_product) * $c->final_stock_product;
                    $re_totalCustoPA = ($re_mp_a + $re_mp_b + $re_mp_c + $re_estoc_mp + $re_estoc_pa + $re_folha + $re_manu_maq + $re_treina);

                    if ($c->initial_stock_product < $c->final_stock_product) {
                        $revalorizacao_stock = ($re_totalCustoPA - $revalorizacao_anterior) * -1;
                    } else {
                        $revalorizacao_stock = ($revalorizacao_anterior - $re_totalCustoPA);
                    }
                    $bp->active_stock_product = $re_totalCustoPA; */

                    /*if ($application_bp->active_stock_product > 0) {
                        if ($application_bp->active_stock_product > $bp->active_stock_product) {
                            $revalorizacao_stock = ($application_bp->active_stock_product - $bp->active_stock_product);
                        } else {
                            $revalorizacao_stock = ($bp->active_stock_product - $application_bp->active_stock_product);
                        }
                        //$revalorizacao_stock = $application_bp->active_stock_product - $bp->active_stock_product;
                    } else {
                        $revalorizacao_stock = $bp->active_stock_product;
                    } */

                    //\Log::info($c->user_id . ' - cPA:' . $re_totalCustoPA . ' revAnt:' . $revalorizacao_anterior . ' rev:' . $revalorizacao_stock);
                }
                /*if ($bp->active_stock_product > 0) { // estoque PA
                    $epa_anterior = $application_bp->active_stock_product;
                    $sobra  = ($c->production_product - $c->sales) * $cpv; // o que não vendeu em $
                    $revalorizacao_stock = ($epa_anterior + $sobra) - $bp->active_stock_product;

                      $log = 'anterior: ' . $application_bp->active_stock_product .
                        ' production_product:' . $c->production_product .
                        ' sales:' . $c->sales .
                        ' cpv:' . $cpv .
                        ' sobra:' . $sobra .
                        ' active_stock_product:' . $bp->active_stock_product;
                    \Log::info($log); 
                }*/
            }

            //$cpv_total = ($cpv * $sales) + $revalorizacao_stock;
            $cpv_total = $raw_a_total + $raw_b_total + $raw_c_total + $stock_raw_total + $stock_pa_total + $salary_production_total + $machine_maintain_total + $training_total + $revalorizacao_stock;

            // Lucro Bruto
            $gross_profit_total = $price_total - $cpv_total;
            $gross_profit = round($gross_profit_total / $sales, 2);

            /******************************************
             *
             *   Despesa de vendas
             *
             ******************************************/
            $adv_total = $cashflow->tmp_adv;
            $adv = $adv_total / $sales;
            $salary_sellers_total = $decision->salary * 10;
            $salary_sellers = $salary_sellers_total / $sales;
            $salary_admin_total = $decision->salary * 2;
            $salary_admin = $salary_admin_total / $sales;

            $income_sell_total = $difference_total + $adv_total + $salary_sellers_total + $salary_admin_total;
            $income_sell = $income_sell_total / $sales;

            /******************************************
             *
             *   Despesa adm
             *
             ******************************************/
            $admin_sell_total = $salary_admin_total;
            $admin_sell = $salary_admin;

            $operational_profit_total = $gross_profit_total - $income_sell_total;
            $operational_profit = $operational_profit_total / $sales;

            //FIM - DESPESA =============================================================================
            // Go BP

            // ==================================================
            // Aplicação juros & Revalorização do estoque acabado
            if ($c->turn->month != 1) {
                /* $revalorizacao_stock = 0;
                $application_bp = Balance::select(['active_application', 'active_stock_product'])
                    ->where('user_id', $c->user_id)
                    ->orderBy('created_at', 'desc')->skip(1)->take(1)->first();

                // EPA
                if ($bp->active_stock_product > 0) { // estoque PA
                    $epa_anterior = $application_bp->active_stock_product;
                    $sobra  = ($c->production_product - $c->sales) * $cpv; // o que não vendeu em $
                    $revalorizacao_stock = ($epa_anterior + $sobra) - $bp->active_stock_product;
                    //dd($revalorizacao_stock);
                } */
                // Aplicação 
                if ($application_bp->active_application > 0) {
                    $others_value_total = $decision->income - $decision->expense;
                    $others_value_total += ($cashflow->application_redemption - $application_bp->active_application);
                } else {
                    $others_value_total = $decision->income - $decision->expense;
                }
                if ($decision->term == 1) {
                    if ($decision->term_interest != 0 && $decision->anticipation_of_receivables == 0) { // venda a prazo - calc o juros
                        $outras_dr_total  = (($price * $sales) / 2) / (1 - ($decision->term_interest / 100));
                        $others_value_total  =  ($outras_dr_total - (($price * $sales) / 2)) + $others_value_total;
                    } else if ($decision->anticipation_of_receivables != 0) {
                        //if ($decision->anticipation_of_receivables != 0) { // antecipação
                        $var1 = ($decision->anticipation_of_receivables / 100) * (($price * $sales) / 2);
                        $var2 = $bp->active_clients - ((($price * $sales) / 2) - $var1); // juros receita
                        $var3 = $var1 - $cashflow->anticipated_receivables; // juros despesa
                        $others_value_total  = ($var2 - $var3) + $others_value_total;
                    }
                }
            } else {
                $others_value_total  = 8814;
                $revalorizacao_stock = 0;
            }
            // FIM - APLICAÇÃO



            // Outras receitas e despesas
            $others_value = $others_value_total / $sales;
            // Lucro do período antes IR
            $profit_before_ir = ($operational_profit + $others_value) - ($machine_depreciation + $building_depreciation);
            $profit_before_ir_total = $profit_before_ir * $sales;

            $ir_total = ($profit_before_ir_total < 0) ? 0 : round(ir_total($profit_before_ir_total, $coordinator->income_tax));
            $ir = $ir_total / $sales;

            $profit_after_ir = $profit_before_ir - $ir;
            $profit_after_ir_total = ($profit_after_ir * $sales) - $cashflow->loss_of_machines;
            $profit_margin = round(($profit_after_ir / $price) * 100, 2);
            $profit_margin_total = 0;

            $dre = $this->model->create([
                'turn_id' => $turn_id,
                'user_id' => $c->user_id,
                'price' => $price,
                'price_total' => $price_total,
                'cpv' => $cpv,
                'cpv_total' => $cpv_total,
                'difference' => $difference,
                'difference_total' => $difference_total,
                'raw_a' => $raw_a,
                'raw_a_total' => $raw_a_total,
                'raw_b' => $raw_b,
                'raw_b_total' => $raw_b_total,
                'raw_c' => $raw_c,
                'raw_c_total' => $raw_c_total,
                'machine_maintain' => $machine_maintain,
                'machine_maintain_total' => $machine_maintain_total,
                'salary_production' => $salary_production,
                'salary_production_total' => $salary_production_total,
                'training' => $training,
                'training_total' => $training_total,
                'stock_raw' => $stock_raw,
                'stock_raw_total' => $stock_raw_total,
                'machine_depreciation' => $machine_depreciation,
                'machine_depreciation_total' => $machine_depreciation_total,
                'building_depreciation' => $building_depreciation,
                'building_depreciation_total' => $building_depreciation_total,
                'gross_profit' => $gross_profit,
                'gross_profit_total' => $gross_profit_total,
                'income_sell' => $income_sell,
                'income_sell_total' => $income_sell_total,
                'adv' => $adv,
                'adv_total' => $adv_total,
                'salary_sellers' => $salary_sellers,
                'salary_sellers_total' => $salary_sellers_total,
                'stock_pa' => $stock_pa,
                'stock_pa_total' => $stock_pa_total,
                'admin_sell' => $admin_sell,
                'admin_sell_total' => $admin_sell_total,
                'salary_admin' => $salary_admin,
                'salary_admin_total' => $salary_admin_total,
                'operational_profit' => $operational_profit,
                'operational_profit_total' => $operational_profit_total,
                'others_value' => $others_value,
                'others_value_total' => $others_value_total,
                'profit_before_ir' => $profit_before_ir,
                'profit_before_ir_total' => $profit_before_ir_total,
                'ir' => $ir,
                'ir_total' => $ir_total,
                'profit_after_ir' => $profit_after_ir,
                'profit_after_ir_total' => $profit_after_ir_total,
                'profit_margin' => $profit_margin,
                'profit_margin_total' => $profit_margin_total,
                'loss_on_sale' => $cashflow->loss_of_machines,
                'price_medium_a' => $pmf_a,
                'price_medium_b' => $pmf_b,
                'price_medium_c' => $pmf_c,
                'revalorizacao_stock' => $revalorizacao_stock
            ]);
            // ==============================
            // Atualiza BP final
            //$bp = $this->balance->findWhere(['turn_id' => $turn_id, 'user_id' => $c->user_id])->first();
            $bp->passive_tax_pay = $ir_total;

            $bp->active_total = ($bp->active_cash + $bp->active_application + $bp->active_clients + $bp->active_machines + 450000 + $bp->active_raw_a + $bp->active_raw_b + $bp->active_raw_c + $bp->active_stock_product) - ($bp->active_depreciation_machine + $bp->active_depreciation_buildings);

            //dd($total_ativo);
            //$bp->accumulated_profits = $cashflow->tmp_bp_last_profit + $profit_after_ir_total + $revalorizacao_stock;
            $bp->accumulated_profits = $cashflow->tmp_bp_last_profit + $profit_after_ir_total;
            // dump($cashflow->tmp_bp_last_profit, $profit_after_ir_total, $revalorizacao_stock, $bp->accumulated_profits, '====');

            $bp->net_worth = $bp->share_capital + $bp->accumulated_profits;
            $bp->total = ($bp->passive_suppliers_win + $bp->passive_suppliers_bir + $bp->passive_suppliers_arrears + $bp->passive_overdue_account + $bp->passive_tax_pay + $bp->passive_loans_financing_mature + $bp->passive_loans_financing_interest_arrears + $bp->share_capital + $bp->accumulated_profits) - $bp->juros_apropriar;
            //===============================
            $diferenca = 0;
            if ($bp->active_total != $bp->total) {
                if ($bp->total > $bp->active_total) {
                    $diferenca = ($bp->total - $bp->active_total);
                    $cashflow->tmp_bonus = $diferenca;
                    $cashflow->entry = $cashflow->entry + $cashflow->tmp_bonus;
                    $cashflow->final_balance = $cashflow->final_balance + $cashflow->tmp_bonus;
                    $bp->active_cash = $cashflow->final_balance;
                } else {
                    $diferenca = $bp->active_total - $bp->total;
                    $cashflow->tmp_despesa_mp = $diferenca;
                    $cashflow->exit = $cashflow->exit + $cashflow->tmp_despesa_mp;
                    $cashflow->final_balance = $cashflow->final_balance - $cashflow->tmp_despesa_mp;
                    $bp->active_cash = $cashflow->final_balance;
                }
                $bp->active_total = ($bp->active_cash + $bp->active_application + $bp->active_clients + $bp->active_machines + 450000 + $bp->active_raw_a + $bp->active_raw_b + $bp->active_raw_c + $bp->active_stock_product) - ($bp->active_depreciation_machine + $bp->active_depreciation_buildings);
                $bp->accumulated_profits = $cashflow->tmp_bp_last_profit + $profit_after_ir_total;
                //$bp->net_worth = $bp->share_capital + $bp->accumulated_profits;
                //$bp->total = ($bp->passive_suppliers_win + $bp->passive_suppliers_bir + $bp->passive_suppliers_arrears + $bp->passive_overdue_account + $bp->passive_tax_pay + $bp->passive_loans_financing_mature + $bp->passive_loans_financing_interest_arrears + $bp->share_capital + $bp->accumulated_profits + $diferenca) - $bp->juros_apropriar;
                $bp->total = ($bp->passive_suppliers_win + $bp->passive_suppliers_bir + $bp->passive_suppliers_arrears + $bp->passive_overdue_account + $bp->passive_tax_pay + $bp->passive_loans_financing_mature + $bp->passive_loans_financing_interest_arrears + $bp->share_capital + $bp->accumulated_profits) - $bp->juros_apropriar;
            }

            //==============================//
            // Rodada 13 - atualiza - Capital Social
            if ($bp->turn->month == 13) {
                $bp->share_capital = $bp->share_capital + $bp->accumulated_profits;
                $bp->accumulated_profits = 0;
            }

            $bp->save();

            $cashflow->tmp_tax = $ir_total;
            $cashflow->tmp_bp_last_profit = $bp->accumulated_profits;
            $cashflow->save();
        }
    }
    public function lastPriceMedium($month, $simulation_id, $user_id)
    {
        if ($month == 1) {
            return [100, 60, 30];
        }
        $beforeTurn = Turn::select('id')
            ->where('simulation_id', $simulation_id)
            ->orderBy('created_at', 'desc')
            ->take(1)
            ->skip(1)
            ->first()
            ->id;
        //dump($lastTurn);                        
        //$price_medium = parent::findWhere(['turn_id' => $lastTurn], ['price_medium_a', 'price_medium_b', 'price_medium_c'])->first(); 
        $price_medium = parent::findWhere(['turn_id' => $beforeTurn, 'user_id' => $user_id], ['price_medium_a', 'price_medium_b', 'price_medium_c'])->first();
        return [$price_medium->price_medium_a, $price_medium->price_medium_b, $price_medium->price_medium_c];
    }
    public function generateDebug($turn_id)
    {
        $companies = $this->operational->findWhere(['turn_id' => $turn_id]);
        $coordinator = $this->decisionCoordinator->findWhere(['turn_id' => $turn_id], ['income_tax', 'raw_material_a', 'raw_material_b', 'raw_material_c'])->first();
        $last_price = $this->decisionCoordinator->lastPrices($turn_id);
    }
    public function lastPriceMedium_DEBUG($month, $simulation_id, $user_id)
    {
        if ($month == 1) {
            return [100, 60, 30];
        }
        $price_medium = parent::findWhere(['turn_id' => $beforeTurn, 'user_id' => $user_id], ['price_medium_a', 'price_medium_b', 'price_medium_c'])->first();
        return [$price_medium->price_medium_a, $price_medium->price_medium_b, $price_medium->price_medium_c];
    }
}
