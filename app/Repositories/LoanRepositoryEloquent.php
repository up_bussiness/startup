<?php

namespace LogisticsGame\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use LogisticsGame\Contracts\LoanRepository;
use LogisticsGame\Models\Loan;
use LogisticsGame\Validators\LoanValidator;

/**
 * Class LoanRepositoryEloquent.
 *
 * @package namespace LogisticsGame\Repositories;
 */
class LoanRepositoryEloquent extends BaseRepository implements LoanRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Loan::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
    public function scheduled($value, $company,$bir)
    {
        if ($value != 0) {
            $this->model->create([
                'user_id' => $company,
                'value' => $value,
                'bir' => $bir,
                'time' => 5
            ]);

            return parent::findWhere(['user_id' => $company]);
        }
        return 0;
    }
}
