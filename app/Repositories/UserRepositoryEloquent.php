<?php

namespace LogisticsGame\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use LogisticsGame\Contracts\UserRepository;
use LogisticsGame\Models\User;
use LogisticsGame\Validators\UserValidator;

/**
 * Class UserRepositoryEloquent.
 *
 * @package namespace LogisticsGame\Repositories;
 */
class UserRepositoryEloquent extends BaseRepository implements UserRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return User::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    public function createCompanies($quantity, $simulation)
    {
        $simulation_tag = randomDigits(4);
        $companies = [];
        for ($i = 1; $i <= $quantity; $i++) {
            $password = randomDigits(6);
            $company = $this->model->create([
                'name' => 'Empresa ' . $i,
                'email' => 'pw@' . $i . 'e' . $simulation_tag,
                'password' => bcrypt($password),
                'password_plain' => $password,
                'role' => 'company',
                'simulation_id' => $simulation,
                'configured' => 0,
                'product_id' => 0
            ]);
            array_push($companies, $company->id);
        }

        return $companies;
    }

    public function createAdmin($request)
    {
        $user = $this->model->create([
            'name' => $request['slug'],
            'email' => $request['email'],
            'password' => bcrypt($request['password']),
            'password_plain' => $request['password'],
            'role' => 'admin'
        ]);

        return $user;
    }

    public function getCoordinators()
    {
        return parent::findWhere([
            'role' => 'coordinator'
        ]);
    }

    public function createCoordinator($request)
    {
        $user = $this->model->create([
            'name' => $request['name'],
            'email' => $request['email'],
            'password' => bcrypt($request['password']),
            'password_plain' => $request['password'],
            'role' => 'coordinator',
            'license' => $request['license']
        ]);

        return $user;
    }

    public function getLicenses()
    {
        return parent::findWhere([
            ['license', '!=', null]
        ])->sum('license');
    }

    public function updateCoordinator($request)
    {
        $data = array_filter($request, function ($var) {
            return !is_null($var);
        });

        if (array_key_exists('password', $data)) {
            $c = parent::update($data, $request['coordinator_id']);

            $c->password = bcrypt($data['password']);
            $c->password_plain = $data['password'];
            $c->save();

            return $c;
        }

        return parent::update($data, $request['coordinator_id']);
    }

    public function updateLicenses($request)
    {
        return parent::update($request, $request['coordinator_id']);
    }

    public function getCompaniesNameId($id)
    {
        $companies = parent::findWhere(['simulation_id' => $id, 'role' => 'company']);

        return $companies->pluck('name', 'id');
    }

    public function getUserEmail($email)
    {
        $user = parent::findWhere(['email' => $email], ['id']);

        if (count($user) == 0) {
            return false;
        }
        return true;
    }

    public function createAluno($nome, $email, $occupation_id, $simulation_id, $company_id)
    {
        $password = randomDigits(4);
        $this->model->create([
            'name' => $nome,
            'email' => $email,
            'password' => bcrypt($password),
            'password_plain' => $password,
            'role' => 'aluno',
            'occupation_id' => $occupation_id,
            'simulation_id' => $simulation_id,
            'company_id' => $company_id
        ]);
        return true;
    }

    public function getCompanies($simulation_id)
    {
        $companies = User::where('simulation_id', $simulation_id)
            ->where('role', 'company')->get(['id']);
        return $companies;
    }
    
    public function atualizaEmpresas($simulation_idOr, $simulation_idDe)
    {
        //dd($simulation_idDe);
        $empOr = User::where('simulation_id', $simulation_idOr)->where('role', 'company')->get();

        $empDe = User::where('simulation_id', $simulation_idDe)->where('role', 'company')->get(['id']);
        $seq = 0;
        $numero = array();
        foreach ($empDe as $d) {
            $numero[$seq] = $d->id;
            //dump($numero[$seq]);
            $seq = $seq + 1;
        }
        // dd($seq);
        $seq = 0;
        foreach ($empOr as $o) {
            $email = 'd-' . $o->email;
            $num = $numero[$seq];
            $De = User::where('id', '=', $num)->first();

            $De->name = $o->name;
            $De->email = $email;
            $De->password = $o->password;
            $De->password_plain = $o->password_plain;
            $De->logo = $o->logo;
            $De->occupation_id = $o->occupation_id;
            $De->configured_id = $o->configured_id;
            $De->product_id = $o->product_id;

            $De->save();

            $seq = $seq + 1;
        }
    }

}
