<?php

namespace LogisticsGame\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use LogisticsGame\Contracts\SimulationRepository;
use LogisticsGame\Models\Simulation;
use LogisticsGame\Validators\SimulationValidator;

/**
 * Class SimulationRepositoryEloquent.
 *
 * @package namespace LogisticsGame\Repositories;
 */
class SimulationRepositoryEloquent extends BaseRepository implements SimulationRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Simulation::class;
    }

    
    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    public function getMySimulations()
    {
        return parent::findWhere([
            'user_id' => auth()->user()->id
        ]);
    }

    
    public function getMyLicenses()
    {
        $simulations = $this->getMySimulations()->count();
        $licenses = auth()->user()->license;

        return $licenses - $simulations;
    }

    public function getLastTurn($simulation_id)
    {
        $simulation = parent::with('turns')
                            ->findWhere([
                                 'id' => $simulation_id,
                                 'user_id' => auth()->user()->id])
                            ->first();

        if (!isset($simulation)){
            return null;
        }

        $simulation = $simulation->turns->sortBy('month')->last()->month;

        return $simulation + 1;
    }

    public function reset()
    {
        $this->model->query()->delete();

        return true;
    }

    public function canCreateTurn($simulation_id)
    {
        $simulation = parent::with('turns')->find($simulation_id);

        $turnsIsOpen = $simulation->turns->where('end_date', '>', \Carbon\Carbon::now());
        $turnIsPublic = $simulation->turns->where('published', false);

        if (!$turnsIsOpen->isEmpty() || !$turnIsPublic->isEmpty())
        {
            return false;
        }

        return true;
    }

    public function getActiveSimulations()
    {
        $simulations=Simulation::where('end_date', '>', date("Y-m-d H:i:s"))->get();
        return $simulations;
    }
}
