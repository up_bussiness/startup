<?php

namespace LogisticsGame\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use LogisticsGame\Contracts\StrategicPlanningRepository;
use LogisticsGame\Models\StrategicPlanning;
use LogisticsGame\Validators\StrategicPlanningValidator;

/**
 * Class StrategicPlanningRepositoryEloquent.
 *
 * @package namespace LogisticsGame\Repositories;
 */
class StrategicPlanningRepositoryEloquent extends BaseRepository implements StrategicPlanningRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return StrategicPlanning::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    public function myStrategic()
    {
        return parent::findWhere([
            'user_id' => auth()->user()->id
        ])->first();
    }
    
}
