<?php

namespace LogisticsGame\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use LogisticsGame\Contracts\DecisionCompanyRepository;
use LogisticsGame\Models\DecisionCompany;
use LogisticsGame\Validators\DecisionCompanyValidator;
use Ramsey\Uuid\Uuid;

/**
 * Class DecisionCompanyRepositoryEloquent.
 *
 * @package namespace LogisticsGame\Repositories;
 */
class DecisionCompanyRepositoryEloquent extends BaseRepository implements DecisionCompanyRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return DecisionCompany::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    public function initialDecision($companies, $turn_id)
    {
        for ($i=0; $i < count($companies); $i++) { 
            // Desabilita o observer somente nessa instrução
            $this->model->flushEventListeners();
            $de = $this->model->create([
                'id' => Uuid::uuid1(),
                'turn_id' => $turn_id,
                'user_id' => $companies[$i],
                'price' => 480,
                'term' => 1,
                'adv_radio' => 2,
                'adv_journal' => 2,
                'adv_social' => 2,
                'adv_outdoor' => 2,
                'adv_tv' => 2,
                'scheduled_rma' => 3600,
                'scheduled_rmb' => 7200,
                'scheduled_rmc' => 10800,
                'emergency_rma' => 36,
                'emergency_rmb' => 72,
                'emergency_rmc' => 108,
                'payment_rm' => 1,
                'activity_level' => 100,
                'extra_production' => 0,
                'buy_small_machine' => 0,
                'sell_small_machine' => 0,
                'buy_medium_machine' => 0,
                'sell_medium_machine' => 0,
                'buy_large_machine' => 0,
                'sell_large_machine' => 0,
                'admitted' => 0,
                'fired' => 0,
                'salary' => 3000, //$TESTE 3000
                'training' => 0,
                'loan' => 0,
                'anticipation_of_receivables' => 0,
                'application' => 0,
                'term_interest' => 1,
                'income' => 0,
                'expense' => 0
            ]);
        }
    }

    public function getSalaryPrice($turn_id)
    {
        $decision = parent::findWhere(['user_id' => auth()->user()->id,
                                       'turn_id' => $turn_id], ['salary', 'price'])
                                     ->first();

        return [$decision->salary, $decision->price];
    }

    public function generateAvg($turn_id)
    {
        $decision = parent::findWhere(['turn_id' => $turn_id], ['salary', 'price', 'term_interest']);

        $salary_avg = $decision->avg('salary');
        $price_avg = $decision->avg('price');
        $term_interest_avg = $decision->avg('term_interest');

        return [$salary_avg, $price_avg, $term_interest_avg];
    }

    public function getDecisionOp($turn_id)
    {
        $decision = parent::with('company')
                          ->findWhere(['turn_id' => $turn_id]);

        return $decision;
    }

    public function defaultDecision($company, $turn)
    {
        $lastDecision = parent::orderBy('created_at')->findWhere(['user_id' => $company], ['price' ,'salary'])->last();

        $this->model->flushEventListeners();
        $de = $this->model->create([
            'id' => Uuid::uuid1(),
            'turn_id' => $turn,
            'user_id' => $company,
            'price' => $lastDecision->price,
            'term' => 0,
            'adv_radio' => 0,
            'adv_journal' => 0,
            'adv_social' => 0,
            'adv_outdoor' => 0,
            'adv_tv' => 0,
            'scheduled_rma' => 1,
            'scheduled_rmb' => 2,
            'scheduled_rmc' => 3,
            'emergency_rma' => 0,
            'emergency_rmb' => 0,
            'emergency_rmc' => 0,
            'payment_rm' => 0,
            'activity_level' => 50,
            'extra_production' => 0,
            'buy_small_machine' => 0,
            'sell_small_machine' => 0,
            'buy_medium_machine' => 0,
            'sell_medium_machine' => 0,
            'buy_large_machine' => 0,
            'sell_large_machine' => 0,
            'admitted' => 0,
            'fired' => 0,
            'salary' => $lastDecision->salary,
            'training' => 0,
            'loan' => 0,
            'anticipation_of_receivables' => 0,
            'application' => 0,
            'term_interest' => 0,
            'income' => 0,
            'expense' => 0
        ]);

        return $de;
    }

    public function getQtdDecisionCompanies($turn_id)
    {
        $decisionCompanies = DecisionCompany::where('turn_id', $turn_id)->get();
        $qtdDecisions = count($decisionCompanies);
        return $qtdDecisions; 
    }

    public function getDecisionCompanies($turn_id, $company_id)
    {
        $decision = DecisionCompany::where('turn_id',$turn_id)
                                    ->where('user_id',$company_id)
                                    ->get();
        return $decision;                                     
    }
}