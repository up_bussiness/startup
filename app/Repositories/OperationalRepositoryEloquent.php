<?php

namespace LogisticsGame\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use LogisticsGame\Contracts\OperationalRepository;
use LogisticsGame\Models\Operational;
use LogisticsGame\Validators\OperationalValidator;

use Illuminate\Container\Container as Application;

/**
 * Class OperationalRepositoryEloquent.
 *
 * @package namespace LogisticsGame\Repositories;
 */
class OperationalRepositoryEloquent extends BaseRepository implements OperationalRepository
{

    private $machine, $decisionCompany, $opDetail, $limitLoan;

    public function __construct(Application $app)
    {
        parent::__construct($app);
        $this->machine = app('LogisticsGame\Contracts\MachineRepository');
        $this->opDetail = app('LogisticsGame\Contracts\OperationalDetailRepository');
        $this->decisionCompany = app('LogisticsGame\Contracts\DecisionCompanyRepository');
        $this->decisionCoordinator = app('LogisticsGame\Contracts\DecisionCoordinatorRepository');
        $this->limitLoan = app('LogisticsGame\Contracts\LimitLoanRepository');
    }

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Operational::class;
    }



    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    public function generate($turn_id)
    {
        $companies = $this->decisionCompany->findWhere(['turn_id' => $turn_id]);
        $coordinator = $this->decisionCoordinator->findWhere(['turn_id' => $turn_id])->first();

        /* GLOBAL */
        $salary_avg = $companies->avg('salary');
        $price_avg = $companies->avg('price');
        $interest_avg = $companies->avg('term_interest');
        $productivity_avg = 0;
        $total_demand_score = 0;
        $total_production = 0;
        $rankPercPA = array();

        // EMPRESA INDIVIDUAL
        foreach ($companies as $c) {

            if ($c->turn->month != 1) { // DEMAIS RODADAS
                $pastReport =  parent::orderBy('created_at', 'asc')->findWhere(['user_id' => $c->user_id])->last();

                $initial_employees = $pastReport->final_employees;
                $product_initial = $pastReport->final_stock_product;
                $rma = $pastReport->final_stock_rma;
                $rmb = $pastReport->final_stock_rmb;
                $rmc =  $pastReport->final_stock_rmc;
                //dump($c->salary, $salary_avg, $pastReport->productivity, $pastReport->salary);    
                $productivity = productivity($c->salary, $salary_avg, $pastReport->productivity, $pastReport->salary);
                $delay_time = $this->limitLoan->orderBy('created_at')->findWhere(['user_id' => $c->user_id], ['fl_delay'])->last()->fl_delay;
            } else { // RODADA 1
                $delay_time = 0;
                $initial_employees = 30;
                $product_initial = 0;
                $rma = 3600;
                $rmb = 7200;
                $rmc = 10800;
                $productivity = 1.01;
                $buyValueMachine = 0;
                $financingMachine = 0;
            }

            // Máquinas
            $machines = $this->machine->getMachinesQuantity($c->user_id, $turn_id);
            $machinesAge = $this->machine->getMachinesAge($c->user_id, $turn_id);
            // Empregados
            $final_employees = employees($initial_employees, $c->admitted, $c->fired);
            // Produção
            $max_production = production($c->activity_level, $c->extra_production, $productivity, $machines[0], $machines[1], $machines[2], $final_employees);
            $total_rma = stock_rm($rma, $c->emergency_rma);
            $total_rmb = stock_rm($rmb, $c->emergency_rmb);
            $total_rmc = stock_rm($rmc, $c->emergency_rmc);
            $production = mp_production($total_rma, $total_rmb, $total_rmc, $max_production);

            // Estoque Final
            $final_rma = final_stock_rm($rma, $c->emergency_rma, $production[0], $c->scheduled_rma);
            $final_rmb = final_stock_rm($rmb, $c->emergency_rmb, $production[1], $c->scheduled_rmb);
            $final_rmc = final_stock_rm($rmc, $c->emergency_rmc, $production[2], $c->scheduled_rmc);
            // Produção/Homem
            $pm = production_man($production[0], $final_employees);
            // Propaganda
            $advertising = advertising($c->adv_radio, $c->adv_journal, $c->adv_social, $c->adv_outdoor, $c->adv_tv);
            $advertising_avg = advertising_avg_single($c->adv_radio, $c->adv_journal, $c->adv_social, $c->adv_outdoor, $c->adv_tv);
            // Demanda individual (distribuição)
            $demand_adv = demand_advertising($advertising);
            $demand_price = demand_price($c->price, $price_avg);
            $demand_term = demand_term($c->term);
            $demand_interest = demand_interest($c->term_interest);
            $demand_score = demand_score($demand_adv, $demand_price, $demand_interest, $demand_term, $delay_time);

            // Compra máquinas
            if ($c->turn->month != 1) {
                $smallMachineQt = $c->buy_small_machine;
                $mediumMachineQt = $c->buy_medium_machine;
                $largeMachineQt = $c->buy_large_machine;
                $smallMachine = $smallMachineQt > 0 ? $this->machine->buy($turn_id, $c->user_id, $smallMachineQt, 1, $coordinator->lp_small, $c->b_s_m, $coordinator->bir) : [0, 0];
                $mediumMachine = $mediumMachineQt > 0 ? $this->machine->buy($turn_id, $c->user_id, $mediumMachineQt, 2, $coordinator->lp_medium, $c->b_m_m, $coordinator->bir) : [0, 0];
                $largeMachine = $largeMachineQt > 0 ? $this->machine->buy($turn_id, $c->user_id, $largeMachineQt, 3, $coordinator->lp_large, $c->b_l_m, $coordinator->bir) : [0, 0];

                $totalMachineValues = array_map(function (...$arrays) {
                    return array_sum($arrays);
                }, $smallMachine, $mediumMachine, $largeMachine);

                $buyValueMachine = $totalMachineValues[0];
                $financingMachine = $totalMachineValues[1];

                if ($buyValueMachine > 0) {
                    $machines = $this->machine->getMachinesQuantity($c->user_id, $turn_id);
                    $machinesAge = $this->machine->getMachinesAge($c->user_id, $turn_id);
                }
            }

            $operational = $this->model->create([
                'turn_id' => $turn_id,
                'user_id' => $c->user_id,
                'initial_stock_rma' => $rma,
                'initial_stock_rmb' => $rmb,
                'initial_stock_rmc' => $rmc,
                'initial_stock_product' => $product_initial,
                'production_rma' => $production[0],
                'production_rmb' => $production[1],
                'production_rmc' => $production[2],
                'production_product' => $production[0],
                'final_stock_rma' => $final_rma,
                'final_stock_rmb' => $final_rmb,
                'final_stock_rmc' => $final_rmc,
                'final_stock_product' => 0,
                'sales' => 0, // ???
                'employees' => $initial_employees,
                'final_employees' => $final_employees,
                'productivity' => $productivity, // ??
                'production_man' => $pm,
                'advertising' => $advertising,
                'demand' => $demand_score,
                'salary' => $c->salary,
                'advertising_avg' => $advertising_avg,
                'machine_s_qnt' => $machines[0],
                'machine_s_age' => $machinesAge[0],
                'machine_m_qnt' => $machines[1],
                'machine_m_age' => $machinesAge[1],
                'machine_l_qnt' => $machines[2],
                'machine_l_age' => $machinesAge[2],
                'tmp_buy_machine_cash' => $buyValueMachine,
                'tmp_buy_machine_financing' => $financingMachine,
            ]);

            $total_demand_score += $demand_score;
            $total_production += $production[0];
        } // FIM EMPRESA INDIVIDUAL

        // Distribuir a demanda, as vendas e o estoque final do produto de cada empresa
        $companiesReport = parent::findWhere(['turn_id' => $turn_id]);
        $opDetail = $this->opDetail->findWhere(['turn_id' => $turn_id], ['demand'])->first();
        //
        $venda_total = 0;
        $demanda_total = 0;
        $qtd_PA = 0;
        //
        foreach ($companiesReport as $c) {
            // Demanda
            $demand_percent = demand_percent($c->demand, $total_demand_score);
            $rankPercPA[$c->user_id] = $demand_percent;
            $c->demand = $opDetail->demand * $demand_percent;
            // Vendas
            $c->sales = round(sales($total_production, $opDetail->demand, $c->production_product, $c->demand, $c->initial_stock_product));
            // Estoque final
            $c->final_stock_product = final_stock_product($c->initial_stock_product, $c->production_product, $c->sales);
            $c->venda_total = ($c->sales);
            $c->save();
            // Produtividade Média
            $productivity_avg += $c->production_man;
            //
            $venda_total = $venda_total + $c->sales;
            $demanda_total = $demanda_total + $c->demand;
            if ($c->final_stock_product > 0) {
                $qtd_PA = $qtd_PA + 1;
            }
        }
        $productivity_avg = ($productivity_avg / $companiesReport->count());

        // Recalcular demanda para quem tem PA
        $demanda_sobrando = $demanda_total - $venda_total;
        //dump($demanda_sobrando, $demanda_total, $venda_total, '=====');
        if ($demanda_sobrando > 0 && $qtd_PA > 0) { //há sobra e empresa com PA
            foreach ($companiesReport as $c) {
                if ($c->final_stock_product > 0) {
                    $escoreDaEmpresa = $rankPercPA[$c->user_id]; // o mesmo escore da demanda normal é aplicado na demanda extra
                    //dump($c->final_stock_product . '/' . $escoreDaEmpresa);
                    $c->demand_entra = floor($demanda_sobrando * $escoreDaEmpresa);
                    //dump($escoreDaEmpresa, $c->demand_entra, '==');
                    if ($c->demand_entra < $c->final_stock_product) {
                        //$c->venda_extra = $c->final_stock_product - $c->demand_entra;
                        $c->venda_extra = $c->demand_entra;
                    } else {
                        $c->venda_extra = $c->final_stock_product;
                    }

                    $c->final_stock_product = final_stock_product($c->initial_stock_product, $c->production_product, $c->sales) - $c->venda_extra;
                    $c->venda_total = ($c->sales + $c->venda_extra);
                    $c->sales = $c->sales + $c->venda_extra;

                    $c->save();
                }
            }
        }
        return true;
    }
}
