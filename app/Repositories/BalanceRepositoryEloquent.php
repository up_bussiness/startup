<?php

namespace LogisticsGame\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use LogisticsGame\Contracts\BalanceRepository;
use LogisticsGame\Models\Balance;
use LogisticsGame\Validators\BalanceValidator;

use Illuminate\Container\Container as Application;

/**
 * Class BalanceRepositoryEloquent.
 *
 * @package namespace LogisticsGame\Repositories;
 */
class BalanceRepositoryEloquent extends BaseRepository implements BalanceRepository
{
    private $cashflow, $machine;

    public function __construct(Application $app)
    {
        parent::__construct($app);
        $this->cashflow = app('LogisticsGame\Contracts\CashflowRepository');
        $this->machine = app('LogisticsGame\Contracts\MachineRepository');
        $this->decisionCoordinator = app('LogisticsGame\Contracts\DecisionCoordinatorRepository');
        $this->loan = app('LogisticsGame\Contracts\LoanRepository');
    }
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Balance::class;
    }



    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    public function generate($turn_id)
    {
        $coordinator = $this->decisionCoordinator->findWhere(['turn_id' => $turn_id])->first();
        $cashflows = $this->cashflow->findWhere(['turn_id' => $turn_id]);

        foreach ($cashflows as $c) {
            // Machine Value + Depreciation
            $machine_total_depreciation = $this->machine->getDepreciation($c->user_id, $turn_id);
            $machine_total_value = $this->machine->getValue($c->user_id, $turn_id);
            // Building Depreciation
            $building_depreciation = building_depreciation(450000, $c->turn->month);
            // Total dos ativos
            $active_total = ($c->final_balance + $c->application + $c->tmp_clients + $machine_total_value + 450000 + $c->tmp_stock_a + $c->tmp_stock_b + $c->tmp_stock_c + $c->tmp_stock_product) - ($machine_total_depreciation + $building_depreciation);
            /* 13a RODADA SOMA SOCIAL + ACUMULADO ATÉ O MOMENTO
            if($c->turn->month == 13) {
                $share_capital = 2316000 + $c->tmp_bp_last_profit;
            } elseif ($c->turn->month < 13) {
                $share_capital = 2316000;
            } else {
                $share_capital = Balance::select('share_capital')
                    ->where('user_id', '66ed067c-d771-11e8-9769-226a819c58f9')
                    ->orderBy('created_at', 'desc')
                    ->take(1)
                    ->first()
                    ->share_capital;
            }*/

            // Emprestimo - Gerar Juros Apropriar- @CD#EMPRESTIMO

            $loans = $this->loan->findWhere(['user_id' => $c->user_id]); // Emprestimos da empresa
            $juros_apropriar = 0;
            foreach ($loans as $lo) {
                $parcelafixa = ParcelaEmprestimoJuros($lo->value, $lo->bir);
                if ($lo->time == 5) {
                    $juros_apropriar = $juros_apropriar + ((($lo->time - 1) * $parcelafixa) - $lo->value);
                    continue;
                }
                $juros_apropriar = $juros_apropriar + EmprestimoJurosApropriar($lo->value, $lo->bir, $lo->time, $parcelafixa);
            }
            //$juros_apropriar = $juros_apropriar + ((4 * ParcelaEmprestimoJuros($c->scheduled_loan,$coordinator->bir)) - $c->scheduled_loan);

            // ===================
            $share_capital = 2316000;
            if ($c->turn->month > 13) {
                //$share_capital = 6227518;
                $share_capital = Balance::select('share_capital')
                    ->where('user_id', $c->user_id)
                    ->orderBy('created_at', 'desc')
                    ->take(1)
                    ->first()
                    ->share_capital;
            }
            // ===================

            if ($c->tmp_bir==null) $c->tmp_bir = 0;
            
            $balance = $this->model->create([
                'turn_id' => $turn_id,
                'user_id' => $c->user_id,
                'active_cash' => $c->final_balance,
                'active_application' => $c->application,
                'active_clients' => $c->tmp_clients,
                'active_stock_product' => $c->tmp_stock_product,
                'active_raw_a' => $c->tmp_stock_a,
                'active_raw_b' => $c->tmp_stock_b,
                'active_raw_c' => $c->tmp_stock_c,
                'active_machines' => $machine_total_value,
                'active_depreciation_machine' => $machine_total_depreciation,
                'active_buildings' => 450000,
                'active_depreciation_buildings' => $building_depreciation,
                'active_total' => $active_total,

                'passive_suppliers_win' => $c->tmp_payment_suppliers,
                'passive_suppliers_arrears' => $c->tmp_delay_suppliers,
                'passive_overdue_account' => $c->tmp_delay,
                'passive_tax_pay' => 0,
                'passive_loans_financing_mature' => $c->tmp_loan_to_due,
                'juros_apropriar' => $juros_apropriar,
                'passive_loans_financing_interest_arrears' => $c->tmp_delay_bank,
                'net_worth' => 0,
                'share_capital' => $share_capital,
                'accumulated_profits' => 0,
                'accumulated_profits_past' => 0, // ?
                'total' => $active_total,
                'passive_suppliers_bir' => $c->tmp_bir,
            ]);
        }
    }
}
