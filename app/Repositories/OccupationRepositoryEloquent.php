<?php

namespace LogisticsGame\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use LogisticsGame\Contracts\OccupationRepository;
use LogisticsGame\Models\Occupation;
use LogisticsGame\Validators\OccupationValidator;

/**
 * Class OccupationRepositoryEloquent.
 *
 * @package namespace LogisticsGame\Repositories;
 */
class OccupationRepositoryEloquent extends BaseRepository implements OccupationRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Occupation::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
