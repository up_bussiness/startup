<?php

namespace LogisticsGame\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use LogisticsGame\Contracts\CashflowRepository;
use LogisticsGame\Models\Cashflow;
use LogisticsGame\Validators\CashflowValidator;
use LogisticsGame\Models\Balance;
use LogisticsGame\Models\Dre;
use LogisticsGame\Models\Turn;

use Illuminate\Container\Container as Application;

/**
 * Class CashflowRepositoryEloquent.
 *
 * @package namespace LogisticsGame\Repositories;
 */
class CashflowRepositoryEloquent extends BaseRepository implements CashflowRepository
{
    private $decisionCompany, $decisionCoordinator, $operational, $machine, $loan;

    public function __construct(Application $app)
    {
        parent::__construct($app);
        $this->decisionCompany = app('LogisticsGame\Contracts\DecisionCompanyRepository');
        $this->decisionCoordinator = app('LogisticsGame\Contracts\DecisionCoordinatorRepository');
        $this->operational = app('LogisticsGame\Contracts\OperationalRepository');
        $this->machine = app('LogisticsGame\Contracts\MachineRepository');
        $this->loan = app('LogisticsGame\Contracts\LoanRepository');
    }
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Cashflow::class;
    }



    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    public function generate($turn_id)
    {
        $companies = $this->decisionCompany->findWhere(['turn_id' => $turn_id]);
        $coordinator = $this->decisionCoordinator->findWhere(['turn_id' => $turn_id])->first();

        // LAST DECISION COORDINATOR

        foreach ($companies as $c) {
            $operational = $this->operational->findWhere(['turn_id' => $turn_id, 'user_id' => $c->user_id])->first();
            // Preço Médio
            $UltimoPrecoMedio = $this->lastPriceMedium($c->turn->month, $c->turn->simulation_id, $c->user_id);
            //dump('cash: '.$UltimoPrecoMedio[0].' - '.$UltimoPrecoMedio[1].' - '.$UltimoPrecoMedio[2]);           
            // Prevent sales ZERO
            $sales = $operational->sales == 0 ? 1 : $operational->sales;
            // Depreciação das máquinas

            /************* ENTRADAS *************/
            /* $receiptCash = receiptCashJuros($sales, $c->price, $c->term, $c->term_interest); // Se juros venda p2
            $tmp_clients = $c->term == 0 ? 0 : $receiptCash; // [TMP] CLIENTES DO BALANÇO
            $receiptCash = receiptCash($sales, $c->price, $c->term); // RECEBIMENTO A VISTA, se juros p1 */
            $tmp_clients = $c->term == 0 ? 0 : receiptCashJuros($sales, $c->price, $c->term, $c->term_interest, $c->anticipation_of_receivables, $coordinator->bir); // [TMP] CLIENTES DO BALANÇO, (juros venda) P2
            $receiptCash = receiptCash($sales, $c->price, $c->term); // RECEBIMENTO A VISTA, se juros P1
            // ANTECIPAÇÃO DE RECEBÍVEIS
            //\Log::info('2>>' . $sales . '=' . $c->price . '=' . $c->term . '=' . $c->term_interest . '=' . $c->anticipation_of_receivables . '=' . $coordinator->bir);
            //\Log::info($tmp_clients);
            //\Log::info($c->anticipation_of_receivables . '=' . $tmp_clients . '=' . $sales . '=' . $c->price . '=' . $coordinator->bir);

            $anticipated_receivables = $c->term == 0 ? 0 : anticipation_receipt($c->anticipation_of_receivables, $tmp_clients, $sales, $c->price, $coordinator->bir);
            //$tmp_clients = $anticipated_receivables == 0 ? $tmp_clients : $tmp_clients - (($c->anticipation_of_receivables / 100) * $tmp_clients);
            //\Log::info($anticipated_receivables);
            // Compra de máquinas
            $buyValueMachine = $operational->tmp_buy_machine_cash;
            $financingMachine = $operational->tmp_buy_machine_financing;

            if ($c->turn->month != 1) {
                $lastCashflow = parent::orderBy('created_at')->findWhere(['user_id' => $c->user_id])->last();
                $lastBP = Balance::orderBy('created_at')->where('user_id', $c->user_id)->get()->last();

                $opening_balance = $lastCashflow->final_balance; // Saldo inicial = Final do período anterior
                $receiptTerm = $lastCashflow->tmp_clients; // Recebimento a prazo
                $application_redemption = application_in($lastCashflow->application, $coordinator->bir); // Resgate de aplicação
                $sale_of_machines = $lastCashflow->tmp_sell_machine_values;
                $loss_of_machines = $lastCashflow->tmp_sell_machine_loss;

                $income_tax = $lastCashflow->tmp_tax;
                $past_profit = $lastCashflow->tmp_bp_last_profit;

                $initial_stock_a = $lastCashflow->tmp_stock_a;
                $initial_stock_b = $lastCashflow->tmp_stock_b;
                $initial_stock_c = $lastCashflow->tmp_stock_c;

                // ATRASOS (BP)
                $contas_atrasos = $lastBP->passive_overdue_account;
                $fornecedores_atrasos = $lastBP->passive_suppliers_arrears;
                $emprestimos_atrasos = $lastBP->passive_loans_financing_interest_arrears;
                // $contas_atrasos = 0;
                // $fornecedores_atrasos = 0;
                // $emprestimos_atrasos = 0;

                $extraSuppliers = $lastCashflow->tmp_payment_suppliers;
                $extraSuppliersBir = $lastCashflow->tmp_bir;
                // Preço médio

                /*$pmf_a = precoMedioFinal($operational->initial_stock_rma, $c->scheduled_rma, $c->emergency_rma, $UltimoPrecoMedio[0], $coordinator->raw_material_a);
              $pmf_b = precoMedioFinal($operational->initial_stock_rmb, $c->scheduled_rmb, $c->emergency_rmb, $UltimoPrecoMedio[1], $coordinator->raw_material_b);
              $pmf_c = precoMedioFinal($operational->initial_stock_rmc, $c->scheduled_rmc, $c->emergency_rmc, $UltimoPrecoMedio[2], $coordinator->raw_material_c);*/
                //dd('ok');
            } else {
                $opening_balance = 0;
                $receiptTerm = 0;
                $application_redemption = 0;
                $sale_of_machines = 0;
                $loss_of_machines = 0;
                $financingMachine = 0;

                $income_tax = 0;
                $past_profit = 0;

                $pmf_a = 100;
                $pmf_b = 60;
                $pmf_c = 30;
                $initial_stock_a = 360537.31;
                $initial_stock_b = 432644.78;
                $initial_stock_c = 324483.58;


                // ATRASOS
                $contas_atrasos = 0;
                $fornecedores_atrasos = 0;
                $emprestimos_atrasos = 0;
                $extraSuppliers = 0;
                $extraSuppliersBir = 0;
            }

            // ENTRADA
            $entry = $receiptCash + $receiptTerm + $anticipated_receivables + $application_redemption + $sale_of_machines + $c->income + $c->loan + $financingMachine + $opening_balance;

            // SAÍDA
            $payroll = payroll($operational->final_employees, $operational->salary, $c->extra_production); // Folha de pagto
            $training = training($operational->final_employees, $operational->salary, $c->training); // Treinamento
            $tmp_training = $training;
            // PROPAGANDA
            $adv_radio = $coordinator->adv_radio;
            $adv_journal = $coordinator->adv_journal;
            $adv_social = $coordinator->adv_social;
            $adv_outdoor = $coordinator->adv_outdoor;
            $adv_tv = $coordinator->adv_tv;
            $adv_total = adv_count($c->adv_radio, $c->adv_journal, $c->adv_social, $c->adv_outdoor, $c->adv_tv, $adv_radio, $adv_journal, $adv_social, $adv_outdoor, $adv_tv);
            $tmp_adv = $adv_total;
            $expense = $c->expense; // Despesa diversas
            //PAGAMENTO A FORNECEDORES
            /*$pma=100.15;
            $pmb=60.09;
            $pmc=30.04; */
            $cost_a = $coordinator->raw_material_a * $c->scheduled_rma;
            $cost_b = $coordinator->raw_material_b * $c->scheduled_rmb;
            $cost_c = $coordinator->raw_material_c * $c->scheduled_rmc;
            $cost_e_a = ($coordinator->raw_material_a * 1.3) * $c->emergency_rma;
            $cost_e_b = ($coordinator->raw_material_b * 1.3) * $c->emergency_rmb;
            $cost_e_c = ($coordinator->raw_material_c * 1.3) * $c->emergency_rmc;
            /* $cost_a = $pma * $c->scheduled_rma;
            $cost_b = $pmb * $c->scheduled_rmb;
            $cost_c = $pmc * $c->scheduled_rmc;
            $cost_e_a = ($pma * 1.3) * $c->emergency_rma;
            $cost_e_b = ($pmb * 1.3) * $c->emergency_rmb;
            $cost_e_c = ($pmc * 1.3) * $c->emergency_rmc; */

            $payment_suppliers_total = $cost_a + $cost_b + $cost_c; // TOTAL A SER PAGO (PROGRAMADO)
            $payment_suppliers = $c->payment_rm != 0 ? ($payment_suppliers_total / 2) : $payment_suppliers_total; // PAGO NO PERIODO ATUAL (A VISTA OU A PRAZO - DIVIDE POR 2)
            $tmp_payment_suppliers = $c->payment_rm != 0 ? ($payment_suppliers_total / 2) : 0; // SE DIVIDIR POR 2, GUARDA METADE PARA PROXIMO PERIODO
            $payment_suppliers += ($cost_e_a + $cost_e_b + $cost_e_c); // ADICIONA PRODUTOS EMERGENCIAIS NA CONTA A SER PAGA
            $payment_suppliers += $extraSuppliers;
            /*
dd('scheduled_rmABC:'.$c->scheduled_rma.'-'.$c->scheduled_rmb.'-'.$c->scheduled_rmc, 
'raq_material_ABC:'.$coordinator->raw_material_a.'-'.$coordinator->raw_material_b.'-'.$coordinator->raw_material_c,
'cost_ABC:'.$cost_a.'-'.$cost_b.'-'.$cost_c,
'cost_e_a:'.$cost_e_a.'-'.$cost_e_b.'-'.$cost_e_c,
'payment_suppliers_total:'.$payment_suppliers_total,
'tmp_payment_suppliers'.$tmp_payment_suppliers,
'payment_suppliers:'.$payment_suppliers); */

            // MANUTENCAO MAQUINAS
            $machinesQnt = $this->machine->getMachinesQuantity($c->user_id, $turn_id);
            $machinesAge = $this->machine->getMachinesAge($c->user_id, $turn_id);
            $small_maintenance = machine_maintenance(1, $machinesQnt[0], $coordinator->lp_small, $machinesAge[0]);
            $medium_maintenance = machine_maintenance(2, $machinesQnt[1], $coordinator->lp_medium, $machinesAge[1]);
            $large_maintenance = machine_maintenance(3, $machinesQnt[2], $coordinator->lp_large, $machinesAge[2]);
            $machine_total_maintenance = machine_maintenance_total($small_maintenance, $medium_maintenance, $large_maintenance);
            $tmp_machine_maintenance = $machine_total_maintenance;
            // JUROS BANCARIOS
            //$bir = bir($coordinator->bir, $tmp_payment_suppliers);
            $tmp_bir = bir($coordinator->bir, $tmp_payment_suppliers);
            $bir = $extraSuppliersBir;
            // APLICACAO
            $application = $c->application > 0 && $opening_balance > 0 ? application_out($c->application, $opening_balance) : 0;

            // PREÇO MÉDIO
            $pmf_a = precoMedioFinal($operational->initial_stock_rma, $c->scheduled_rma, $c->emergency_rma, $UltimoPrecoMedio[0], $coordinator->raw_material_a);
            $pmf_b = precoMedioFinal($operational->initial_stock_rmb, $c->scheduled_rmb, $c->emergency_rmb, $UltimoPrecoMedio[1], $coordinator->raw_material_b);
            $pmf_c = precoMedioFinal($operational->initial_stock_rmc, $c->scheduled_rmc, $c->emergency_rmc, $UltimoPrecoMedio[2], $coordinator->raw_material_c);
            // ARMAZENAMENTO DE ESTOQUE MP
            //$stocking_costs  = stock_cost($operational->initial_stock_rma, $operational->initial_stock_rmb, $operational->initial_stock_rmc, $coordinator->raw_material_a, $coordinator->raw_material_b, $coordinator->raw_material_c);
            $stocking_costs  = stock_cost($operational->initial_stock_rma, $operational->initial_stock_rmb, $operational->initial_stock_rmc, $pmf_a, $pmf_b, $pmf_c);
            //$stocking_costs  = stock_cost($operational->initial_stock_rma, $operational->initial_stock_rmb, $operational->initial_stock_rmc, $UltimoPrecoMedio[0], $UltimoPrecoMedio[1], $UltimoPrecoMedio[2]);
            /*dump('initial_stock_rma:'.$operational->initial_stock_rma,
'initial_stock_rmb:'.$operational->initial_stock_rmb,
'initial_stock_rmc:'.$operational->initial_stock_rmc,
'raw_material_a:'.$coordinator->raw_material_a,
'raw_material_b:'.$coordinator->raw_material_b,
'raw_material_c:'.$coordinator->raw_material_c);            
dd('stocking_costs:'.$stocking_costs);*/

            $stock_mp = $stocking_costs;

            // Armazenamento de Estoque (PA)
            /* $raw_a_total = new_raw_cost_unit($sales, $coordinator->raw_material_a, $c->emergency_rma);
            $raw_b_total = new_raw_cost_unit(($sales * 2), $coordinator->raw_material_b, $c->emergency_rmb);
            $raw_c_total = new_raw_cost_unit(($sales * 3), $coordinator->raw_material_c, $c->emergency_rmc);
            $raw_a = $raw_a_total / $sales;
            $raw_b = $raw_b_total / $sales;
            $raw_c = $raw_c_total / $sales;
            $last_cpv = $raw_a + $raw_b + $raw_c; */
            $last_cpv = ($pmf_a + ($pmf_b * 2) + ($pmf_c * 3));

            $stock_pa = stock_pa_cost($last_cpv, $operational->initial_stock_product);
            $stocking_costs = $stock_mp + $stock_pa;

            /**************************************
             **  AMORTIZAÇÃO/EMPRESTIMOS - @CD#EMPRESTIMO
             */
            $loans = $this->loan->findWhere(['user_id' => $c->user_id]); // Emprestimos da empresa

            /* $amortization_loans = $loans->filter(function ($loan) {
                                            return $loan->time != 0 && $loan->time != 5;})
                                        ->map(function ($loan) {
                                            return ($loan->value / 4);})
                                        ->sum(); */

            // TRIGGER DO EMPRESTIMO
            $amortization_loans = 0;
            foreach ($loans as $lo) {
                if ($lo->time != 0 && $lo->time != 5) {
                    $amortization_loans = $amortization_loans + ParcelaEmprestimoJuros($lo->value, $lo->bir);
                }
                //$lo->time != 0 ? $lo->time-- : 0;
                //$lo->save();
            }

            /*$loans->each(function ($loan) {
                if ($loan->time != 0) {
                    $amortization_loans = ParcelaEmprestimoJuros($loan->value,5);
                    $loan->time--;
                    //$loan->time != 0 ? $loan->time-- : 0;
                    $loan->save();
                }
            }); */
            //dump($amortization_loans,$teste);

            // Emprestimo programado
            $scheduled_loan = $this->loan->scheduled($c->loan, $c->user_id, $coordinator->bir);

            /**************************************
             **  PAGAMENTO (ATRASOS)
             */
            $tmp_bank = $entry; // caixa temporário é tudo que entrou
            $tmp_delay = 0; // valor em atraso (geral)
            $tmp_delay_suppliers = 0; // valor em atraso (fornecedores)
            $tmp_delay_bank = 0; // valor em atraso (emprestimos)

            // INICIA PAGANDO CONTAS EM ATRASO
            if ($tmp_bank > $contas_atrasos) {
                $tmp_bank -= $contas_atrasos;
            } else {
                $tmp_delay += $contas_atrasos - $tmp_bank;
                $contas_atrasos = $tmp_bank;
                $tmp_bank = 0;
            }
            // SEGUIDO PAGANDO FORNCEDORES EM ATRASO
            if ($tmp_bank > $fornecedores_atrasos) {
                $tmp_bank -= $fornecedores_atrasos;
            } else {
                $tmp_delay_suppliers += $fornecedores_atrasos - $tmp_bank;
                $fornecedores_atrasos = $tmp_bank;
                $tmp_bank = 0;
            }
            // SEGUIDO PAGANDO EMPRESTIMOS EM ATRASO
            if ($tmp_bank > $emprestimos_atrasos) {
                $tmp_bank -= $emprestimos_atrasos;
            } else {
                $tmp_delay_bank += $emprestimos_atrasos - $tmp_bank;
                $emprestimos_atrasos = $tmp_bank;
                $tmp_bank = 0;
            }
            $atrasos_gerais = $contas_atrasos + $fornecedores_atrasos + $emprestimos_atrasos;

            // PAGAR A FOLHA DE PAGAMENTO
            if ($tmp_bank > $payroll) {
                $tmp_bank -= $payroll;
            } else {
                $tmp_delay += $payroll - $tmp_bank;
                $payroll = $tmp_bank;
                $tmp_bank = 0;
            }
            // PAGAR TREINAMENTO
            if ($tmp_bank > $training) {
                $tmp_bank -= $training;
            } else {
                $tmp_delay += $training - $tmp_bank;
                $training = $tmp_bank;
                $tmp_bank = 0;
            }
            // PAGAR PROPAGANDA
            if ($tmp_bank > $adv_total) {
                $tmp_bank -= $adv_total;
            } else {
                $tmp_delay += $adv_total - $tmp_bank;
                $adv_total = $tmp_bank;
                $tmp_bank = 0;
            }
            // PAGAR DESPESAS DIVERSAS
            if ($tmp_bank > $expense) {
                $tmp_bank -= $expense;
            } else {
                $tmp_delay += $expense - $tmp_bank;
                $expense = $tmp_bank;
                $tmp_bank = 0;
            }
            // PAGAR CUSTOS DE ESTOCAGEM
            if ($tmp_bank > $stocking_costs) {
                $tmp_bank -= $stocking_costs;
            } else {
                $tmp_delay += $stocking_costs - $tmp_bank;
                $stocking_costs = $tmp_bank;
                $tmp_bank = 0;
            }
            // PAGAR MANUTENÇÃO DE MAQUINAS
            if ($tmp_bank > $machine_total_maintenance) {
                $tmp_bank -= $machine_total_maintenance;
            } else {
                $tmp_delay += $machine_total_maintenance - $tmp_bank;
                $machine_total_maintenance = $tmp_bank;
                $tmp_bank = 0;
            }
            // PAGAR IR
            if ($tmp_bank > $income_tax) {
                $tmp_bank -= $income_tax;
            } else {
                $tmp_delay += $income_tax - $tmp_bank;
                $income_tax = $tmp_bank;
                $tmp_bank = 0;
            }
            // PAGAR TBJ
            if ($tmp_bank > $bir) {
                $tmp_bank -= $bir;
            } else {
                $tmp_delay_suppliers += $bir - $tmp_bank;
                $bir = $tmp_bank;
                $tmp_bank = 0;
            }
            // PÀGAR FORNECEDORES
            if ($tmp_bank > $payment_suppliers) {
                $tmp_bank -= $payment_suppliers;
            } else {
                $tmp_delay_suppliers += $payment_suppliers - $tmp_bank;
                $payment_suppliers = $tmp_bank;
                $tmp_bank = 0;
            }
            // PAGAR AMORTIZAÇAO EMPRESTIMOS - @CD#EMPRESTIMO
            if ($tmp_bank > $amortization_loans) {
                $tmp_bank -= $amortization_loans;
            } else {
                $tmp_delay_bank += $amortization_loans - $tmp_bank;
                $amortization_loans = $tmp_bank;
                $tmp_bank = 0;
            }
            // PAGAR COMPRA DE MAQUINA
            if ($tmp_bank > $buyValueMachine) {
                $tmp_bank -= $buyValueMachine;
            } else {
                $tmp_delay += $buyValueMachine - $tmp_bank;
                $buyValueMachine = $tmp_bank;
                $tmp_bank = 0;
            }
            // PAGAR APLICACAO
            if ($tmp_bank > $application) {
                $tmp_bank -= $application;
            } else {
                $application = $tmp_bank;
                $tmp_bank = 0;
            }

            $exit = $payroll + $training + $adv_total + $expense + $stocking_costs + $payment_suppliers + $application + $bir + $amortization_loans + $machine_total_maintenance + $income_tax + $buyValueMachine + $atrasos_gerais;
            $final_balance = round($tmp_bank);

            //ESTOQUES BP
            //  \Log::info('=> VALOR RAW ANTIGO =====> '.$last_price[0]);
            //\Log::info('=> VALOR RAW NOVO =====> ' . $coordinator->raw_material_a);

            $tmp_stock_a = $pmf_a * $operational->final_stock_rma;
            $tmp_stock_b = $pmf_b * $operational->final_stock_rmb;
            $tmp_stock_c = $pmf_c * $operational->final_stock_rmc;
            /*$tmp_stock_a = new_stock_value($coordinator->raw_material_a, $initial_stock_a, $c->scheduled_rma, $c->emergency_rma, $operational->production_rma, $last_price[0]);
            $tmp_stock_a = new_stock_value($coordinator->raw_material_a, $initial_stock_a, $c->scheduled_rma, $c->emergency_rma, $operational->production_rma, $pmf_a);
            $tmp_stock_b = new_stock_value($coordinator->raw_material_b, $initial_stock_b, $c->scheduled_rmb, $c->emergency_rmb, $operational->production_rmb, $pmf_b);
            $tmp_stock_c = new_stock_value($coordinator->raw_material_c, $initial_stock_c, $c->scheduled_rmc, $c->emergency_rmc, $operational->production_rmc, $pmf_c);*/



            //$cpv = $last_price[0] + ($last_price[1] * 2)+ ($last_price[2] * 3);
            $cpv = $pmf_a + ($pmf_b * 2) + ($pmf_c * 3);

            $tmp_stock_product = $cpv * $operational->final_stock_product;
            //    dump($tmp_stock_product, $cpv, $operational->final_stock_product, '===');

            // Empréstimos e financiamentos a vencer - @CD#EMPRESTIMO

            /*$tmp_loan_to_due = $loans->map(function ($loan) {
                return ($loan->value / 4) * $loan->time;
            })->sum(); */
            $tmp_loan_to_due = 0;
            foreach ($loans as $lo) {
                if ($lo->time != 0) {
                    $tmp_loan_to_due = $tmp_loan_to_due + (($lo->time - 1) * ParcelaEmprestimoJuros($lo->value, $lo->bir));
                }
            }

            $tmp_loan_to_due = $tmp_loan_to_due + (4 * ParcelaEmprestimoJuros($c->loan, $coordinator->bir));


            // GERAR
            $cashflow = $this->model->create([
                'turn_id' => $turn_id,
                'user_id' => $c->user_id,
                'opening_balance' => $opening_balance, // SALDO INICIAL
                'receiving_sight' => $receiptCash, // RECEBIMENTO A VISTA
                'time_receipt' => $receiptTerm, // RECEBIMENTO A PRAZO
                'anticipated_receivables' => $anticipated_receivables, // RECEBIVEIS ANTECIPADO
                'application_redemption' => $application_redemption, // RESGATE APLICACAO
                'sale_of_machines' => $sale_of_machines, // VENDA DE MÁQUINAS
                'miscellaneous_income' => $c->income, // RECEITAS DIVERSAS
                'machine_financing' => $financingMachine, // FINANCIAMENTO DE MÁQUINAS
                'scheduled_loan' => $c->loan, // EMPRESTIMO PROGRAMADO
                'entry' => $entry,

                'payroll' => $payroll, // FOLHA PAGTO
                'training' => $training, // TREINAMENTO
                'adv' => $adv_total, // PROPAGANDA
                'expense' => $expense, // DESPESAS DIVERSAS
                'stocking_costs' => $stocking_costs, // Gastos com estocagem
                'general_delay' => $atrasos_gerais, // ATRASOS GERAIS
                'payment_suppliers' => $payment_suppliers, // PAGTO FORNECEDORES
                'purchase_machines' => $buyValueMachine, // COMPRA DE MÁQUINAS
                'machine_maintenance' => $machine_total_maintenance, // MANUTENÇÃO DE MÁQUINAS
                'amortization_loans' => $amortization_loans, // AMORTIZAÇÃO DE EMPRÉSTIMOS E FINANC
                'bir' => $bir, // JUROS BANCÁRIOS
                'income_tax' => $income_tax, // IMPOSTO DE RENDA
                'application' => $application, // APLICACAO
                'exit' => $exit,

                'final_balance' => $final_balance, // Saldo final

                'tmp_clients' => $tmp_clients,
                'tmp_sell_machine_values' => 0,
                'tmp_payment_suppliers' => $tmp_payment_suppliers,
                'tmp_stock_a' => $tmp_stock_a,
                'tmp_stock_b' => $tmp_stock_b,
                'tmp_stock_c' => $tmp_stock_c,
                'tmp_stock_product' => $tmp_stock_product,
                'tmp_delay' => $tmp_delay,
                'tmp_delay_bank' => $tmp_delay_bank,
                'tmp_delay_suppliers' => $tmp_delay_suppliers,
                'tmp_loan_to_due' => $tmp_loan_to_due,
                'tmp_stock_dre_product' => $stock_pa,
                'tmp_stock_dre_raw' => $stock_mp,
                'tmp_bp_last_profit' => $past_profit,
                'tmp_machine_maintenance' => $tmp_machine_maintenance,
                'tmp_training' => $tmp_training,
                'tmp_adv' => $tmp_adv,
                'loss_of_machines' => $loss_of_machines,
                'tmp_bir' => $tmp_bir,
                'tmp_bonus' => 0,
                'tmp_despesa_mp' => 0,
            ]);
        }
    }
    public function lastPriceMedium($month, $simulation_id, $user_id)
    {
        if ($month == 1) {
            return [100, 60, 30];
        }
        $beforeTurn = Turn::select('id')
            ->where('simulation_id', $simulation_id)
            ->orderBy('created_at', 'desc')
            ->take(1)
            ->skip(1)
            ->first()
            ->id;
        // $price_medium = Dre::select(['price_medium_a', 'price_medium_b', 'price_medium_c'])->where('turn_id', $beforeTurn)->first(); 
        $price_medium = Dre::select(['price_medium_a', 'price_medium_b', 'price_medium_c'])->where(['turn_id' => $beforeTurn, 'user_id' => $user_id])->first();
        /*$pa='lastPriceMedium2 = month:'.$month.'simulation_id:'.$simulation_id;
dump($pa,'price_medium_a:'.$price_medium->price_medium_a,'price_medium_b:'.$price_medium->price_medium_b,'price_medium_c:'.$price_medium->price_medium_c);*/

        return [$price_medium->price_medium_a, $price_medium->price_medium_b, $price_medium->price_medium_c];
    }
}
