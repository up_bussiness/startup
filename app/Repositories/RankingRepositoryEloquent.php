<?php

namespace LogisticsGame\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use LogisticsGame\Contracts\RankingRepository;
use LogisticsGame\Models\Ranking;
use LogisticsGame\Validators\RankingValidator;

use Illuminate\Container\Container as Application;

/**
 * Class RankingRepositoryEloquent.
 *
 * @package namespace LogisticsGame\Repositories;
 */
class RankingRepositoryEloquent extends BaseRepository implements RankingRepository
{
    private $balance, $operational, $turn;

    public function __construct(Application $app)
    {
        parent::__construct($app);
        $this->balance = app('LogisticsGame\Contracts\BalanceRepository');
        $this->operational = app('LogisticsGame\Contracts\OperationalRepository');
        $this->turn = app('LogisticsGame\Contracts\TurnRepository');
    }
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Ranking::class;
    }



    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    public function generate($companies, $turn, $month, $simulation_id)
    {
        $qtd_empresas = count($companies);
        $total_inicial = 0;
        $ids = array();

        // depois da rodada 2 em diante
        if ($month != 1) {
            $mes_anterior = $month - 1;
            $turn_anterior = $this->turn->findWhere(['simulation_id' => $simulation_id, 'month' => $mes_anterior])->first()->id;
            $ranking_anterior = $this->model::where(['turn_id' => $turn_anterior])->get();
            foreach ($ranking_anterior as $r) {
                $ids[$r->user_id] = $r->total_pontos;
            }
        }

        $somatorio = $this->operational->findWhere(['turn_id' => $turn])->sum('sales');

        for ($i = 0; $i < count($companies); $i++) {
            $balanco = $this->balance->findWhere(['turn_id' => $turn, 'user_id' => $companies[$i]], ['net_worth', 'passive_loans_financing_mature'])->first();
            $sales = $this->operational->findWhere(['turn_id' => $turn, 'user_id' => $companies[$i]])->first()->sales;
            $this->model->create([
                'user_id' => $companies[$i],
                'turn_id' => $turn,
                'net_worth' => $balanco->net_worth,
                'market_share' => ($sales / $somatorio) * 100,
                'loans_financing' => $balanco->passive_loans_financing_mature
            ]);
        }
        // pontos Patrimonio Liquido
        $ranking = $this->model::where(['turn_id' => $turn])->orderBy('net_worth', 'DESC')->get();
        $salto = 0;
        $pontos_maximo = 10 * 2;
        foreach ($ranking as $r) {
            if ($salto > $r->net_worth) {
                $pontos_maximo = $pontos_maximo - 1;
            }
            $r->net_worth_pontos = $pontos_maximo;

            $r->save();
            $salto = $r->net_worth;
        }
        // pontos Market Share
        $ranking = $this->model::where(['turn_id' => $turn])->orderBy('market_share', 'DESC')->get();
        $salto = 0;
        $pontos_maximo = 10;
        foreach ($ranking as $r) {
            if ($salto > $r->market_share) {
                $pontos_maximo = $pontos_maximo - 1;
            }
            $r->market_share_pontos = $pontos_maximo;

            $r->save();
            $salto = $r->market_share;
        }
        // pontos Endividamento
        $ranking = $this->model::where(['turn_id' => $turn])->orderBy('loans_financing', 'ASC')->get();
        $salto = 0;
        $pontos_maximo = 10;
        foreach ($ranking as $r) {
            if ($salto < $r->loans_financing) {
                $pontos_maximo = $pontos_maximo - 1;
            }
            $r->loans_financing_pontos = $pontos_maximo;

            $r->save();
            $salto = $r->loans_financing;
        }
        // pontos Total
        $ranking = $this->model::where(['turn_id' => $turn])->get();
        foreach ($ranking as $r) {
            if ($month != 1) {
                $total_inicial = $ids[$r->user_id];
            }
            $r->total_pontos = $total_inicial + ($r->net_worth_pontos + $r->market_share_pontos + $r->loans_financing_pontos);
            $r->save();
            //dump($r->total_pontos,$total_inicial);
        }
        //dd('ok');
    }
}
