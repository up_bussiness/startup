<?php

namespace LogisticsGame\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use LogisticsGame\Contracts\LimitLoanRepository;
use LogisticsGame\Models\LimitLoan;
use LogisticsGame\Models\Balance;
use LogisticsGame\Models\Ranking;
use LogisticsGame\Models\Cashflow;
use LogisticsGame\Validators\LimitLoanValidator;

use Illuminate\Container\Container as Application;

/**
 * Class LimitLoanRepositoryEloquent.
 *
 * @package namespace LogisticsGame\Repositories;
 */
class LimitLoanRepositoryEloquent extends BaseRepository implements LimitLoanRepository
{
    private $decisionCompany, $balance, $cashflow, $ranking, $loan;

    public function __construct(Application $app)
    {
        parent::__construct($app);
        // $this->balance = app('LogisticsGame\Contracts\BalanceRepository');
        // $this->cashflow = app('LogisticsGame\Contracts\CashflowRepository');
        // $this->ranking = app('LogisticsGame\Contracts\RankingRepository');
        $this->loan = app('LogisticsGame\Contracts\LoanRepository');
    }
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return LimitLoan::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    public function generate($companies, $turn, $month)
    {
        if($month != 1) {
            for ($i=0; $i < count($companies); $i++) {
                // Limite anterior
                $pastLimit = parent::orderBy('created_at')->findWhere(['user_id' => $companies[$i]])->last();
                $past_limit_value = $pastLimit->new_limit;
                $turn_without_delay = $pastLimit->turn_without_delay;
                $fl_delay = $pastLimit->fl_delay;

                // Emprestimos -  @CD#EMPRESTIMO
                $loans = $this->loan->findWhere(['user_id' => $companies[$i]]);
                $value_of_loan = $loans->filter(function ($loan) {
                    return $loan->time == 4;
                })->sum('value');

                // Ranking
                // $ranking = $this->ranking->orderBy('net_worth', 'desc')->findWhere(['turn_id' => $turn], ['net_worth', 'user_id'])->toArray();
                $ranking = Ranking::select('net_worth', 'user_id')->where('turn_id', $turn)->orderBy('net_worth', 'desc')->get()->toArray();
                $position = intval(array_search($companies[$i], array_column($ranking, 'user_id'))) + 1;

                // Amortização - @CD#EMPRESTIMO
                // $cashflow = $this->cashflow->findWhere(['turn_id' => $turn, 'user_id' => $companies[$i]], ['amortization_loans'])->first();
                $cashflow = Cashflow::where('turn_id', $turn)->where('user_id', $companies[$i])->first();;
                $amortization = $cashflow->amortization_loans;

                // Periodo sem atraso
                // $balance = $this->balance->findWhere(['turn_id' => $turn, 'user_id' => $companies[$i]])->first();
                $balance = Balance::where('turn_id', $turn)->where('user_id', $companies[$i])->first();
                $fornecedores_atraso = $balance->passive_suppliers_arrears;
                $contas_atraso = $balance->passive_overdue_account;
                $emp_atraso = $balance->passive_loans_financing_interest_arrears;
                $atraso = $fornecedores_atraso + $contas_atraso + $emp_atraso;
                if ($atraso > 0) {
                    $turn_without_delay = 0;
                    $fl_delay++;
                } else {
                    $turn_without_delay++;
                    $fl_delay = 0;
                }

                // Limite
                $limit_total = loan_limitation($position, $turn_without_delay, $past_limit_value, $fl_delay);

                $new_limit = $limit_total - $value_of_loan;
                $new_limit += $amortization;

                $new_limit = ($new_limit <= 0) ? 0 : $new_limit;

                $limitLoan = $this->model->create([
                    'turn_id' => $turn,
                    'user_id' => $companies[$i],
                    'turn_without_delay' => $turn_without_delay,
                    'limit' => $limit_total,
                    'loan' => $value_of_loan,
                    'amortization' => $amortization,
                    'new_limit' => $new_limit,
                    'fl_delay' => $fl_delay,
                ]);
            }
        } else {
            for ($i=0; $i < count($companies); $i++) {
                $limitLoan = $this->model->create([
                    'turn_id' => $turn,
                    'user_id' => $companies[$i],
                    'turn_without_delay' => 0,
                    'limit' => 400000,
                    'loan' => 0,
                    'amortization' => 0,
                    'new_limit' => 400000,
                    'fl_delay' => 0,
                ]);
            }
        }
    }
}
