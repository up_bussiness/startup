<?php

namespace LogisticsGame\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use LogisticsGame\Contracts\OperationalDetailRepository;
use LogisticsGame\Models\OperationalDetail;
use LogisticsGame\Validators\OperationalDetailValidator;

use Illuminate\Container\Container as Application;

/**
 * Class OperationalDetailRepositoryEloquent.
 *
 * @package namespace LogisticsGame\Repositories;
 */
class OperationalDetailRepositoryEloquent extends BaseRepository implements OperationalDetailRepository
{

    private $turn, $decisionCompany;

    public function __construct(Application $app)
    {
        parent::__construct($app);
        $this->turn = app('LogisticsGame\Contracts\TurnRepository');
        $this->decisionCompany = app('LogisticsGame\Contracts\DecisionCompanyRepository');
    }

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return OperationalDetail::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    public function initial($turn_id, $simulation_demand, $imported, $machines)
    {
        $productive_capacity = machines_production($machines[0], $machines[1], $machines[2]);

        $demand = first_demand($productive_capacity, $simulation_demand);
        $imported_products = imported_products($demand, $imported);

        $opDetail = $this->model->create([
            'turn_id' => $turn_id,
            'import_products' => $imported_products,
            'demand' => $demand,
        ]);

        return $opDetail;
    }

    public function averages($turn_id)
    {
        $averages = $this->decisionCompany->generateAvg($turn_id);
        $opDetail = parent::findWhere(['turn_id' => $turn_id])->first();

        $opDetail->salary_average = $averages[0];
        $opDetail->price_average = $averages[1];
        $opDetail->interest_average = $averages[2];
        $opDetail->save();

        return $opDetail;
    }

    public function generate($turn_id, $macro_industry, $imported)
    {
        $turn = $this->turn->find($turn_id);
        $lastTurn = $this->turn->getLastTurn($turn->simulation_id);

        $last_demand = parent::findWhere(['turn_id' => $lastTurn->id], ['demand'])->first()->demand;
        $demand = new_demand($last_demand, $macro_industry);
        $imported_products = imported_products($demand, $imported);

        $opDetail = $this->model->create([
            'turn_id' => $turn_id,
            'import_products' => $imported_products,
            'demand' => $demand,
        ]);

        return $opDetail;
    }

}
