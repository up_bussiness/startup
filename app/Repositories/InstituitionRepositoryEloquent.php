<?php

namespace LogisticsGame\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use LogisticsGame\Contracts\InstituitionRepository;
use LogisticsGame\Models\Instituition;
use LogisticsGame\Validators\InstituitionValidator;

/**
 * Class InstituitionRepositoryEloquent.
 *
 * @package namespace LogisticsGame\Repositories;
 */
class InstituitionRepositoryEloquent extends BaseRepository implements InstituitionRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Instituition::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    public function createInstituition($request, $user)
    {
        $request['user_id'] = $user;
        return $this->model->create($request);
    }
    
}
