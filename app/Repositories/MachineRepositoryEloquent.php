<?php

namespace LogisticsGame\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use LogisticsGame\Contracts\MachineRepository;
use LogisticsGame\Models\Loan;
use LogisticsGame\Models\Machine;
use LogisticsGame\Models\DecisionCoordinator;
use LogisticsGame\Models\Cashflow;
use LogisticsGame\Validators\MachineValidator;

use Illuminate\Container\Container as Application;

/**
 * Class MachineRepositoryEloquent.
 *
 * @package namespace LogisticsGame\Repositories;
 */
class MachineRepositoryEloquent extends BaseRepository implements MachineRepository
{
    private $decisionCompany, $decisionCoordinator;

    public function __construct(Application $app)
    {
        parent::__construct($app);
        $this->decisionCompany = app('LogisticsGame\Contracts\DecisionCompanyRepository');
        $this->decisionCoordinator = app('LogisticsGame\Contracts\DecisionCoordinatorRepository');
    }
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Machine::class;
    }



    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    public function initialInventory($companies, $turn)
    {
        for ($i = 0; $i < count($companies); $i++) {
            $machineSmall = $this->model->create([
                'turn_id' => $turn,
                'user_id' => $companies[$i],
                'type' => 1,
                'age' => 60,
                'buy_value' => 200000,
                'depreciation' => 100000
            ]);

            $machineMedium = $this->model->create([
                'turn_id' => $turn,
                'user_id' => $companies[$i],
                'type' => 2,
                'age' => 24,
                'buy_value' => 550000,
                'depreciation' => 110000
            ]);
        }

        return [count($companies), count($companies), 0];
    }

    public function getMachinesQuantity($company, $turn)
    {
        $smallMachine = parent::findWhere(['user_id' => $company, 'turn_id' => $turn, 'type' => 1])->count();
        $mediumMachine = parent::findWhere(['user_id' => $company, 'turn_id' => $turn, 'type' => 2])->count();
        $largeMachine = parent::findWhere(['user_id' => $company, 'turn_id' => $turn, 'type' => 3])->count();

        return [$smallMachine, $mediumMachine, $largeMachine];
    }

    public function getValue($company, $turn)
    {
        $smallMachine = parent::findWhere(['user_id' => $company, 'turn_id' => $turn, 'type' => 1])->sum('buy_value');
        $mediumMachine = parent::findWhere(['user_id' => $company, 'turn_id' => $turn, 'type' => 2])->sum('buy_value');
        $largeMachine = parent::findWhere(['user_id' => $company, 'turn_id' => $turn, 'type' => 3])->sum('buy_value');

        return $smallMachine + $mediumMachine + $largeMachine;
    }

    public function getDepreciation($company, $turn)
    {
        $smallMachine = parent::findWhere(['user_id' => $company, 'turn_id' => $turn, 'type' => 1])->sum('depreciation');
        $mediumMachine = parent::findWhere(['user_id' => $company, 'turn_id' => $turn, 'type' => 2])->sum('depreciation');
        $largeMachine = parent::findWhere(['user_id' => $company, 'turn_id' => $turn, 'type' => 3])->sum('depreciation');

        return $smallMachine + $mediumMachine + $largeMachine;
    }

    public function getDepreciationDre($company, $turn)
    {
        $smallMachine = parent::findWhere(['user_id' => $company, 'turn_id' => $turn, 'type' => 1, ['age', '>', '0']])->count();
        $mediumMachine = parent::findWhere(['user_id' => $company, 'turn_id' => $turn, 'type' => 2, ['age', '>', '0']])->count();
        $largeMachine = parent::findWhere(['user_id' => $company, 'turn_id' => $turn, 'type' => 3, ['age', '>', '0']])->count();

        $price = DecisionCoordinator::select('lp_small as small', 'lp_medium as medium', 'lp_large as large')->where('turn_id', $turn)->first();
        $small = ($price->small / 120);
        $medium = ($price->medium / 120);
        $large = ($price->large / 120);

        return ($small * $smallMachine) + ($medium * $mediumMachine) + ($large * $largeMachine);
    }

    public function getMachinesAge($company, $turn)
    {
        $smallMachine = parent::findWhere(['user_id' => $company, 'turn_id' => $turn, 'type' => 1]);
        $mediumMachine = parent::findWhere(['user_id' => $company, 'turn_id' => $turn, 'type' => 2]);
        $largeMachine = parent::findWhere(['user_id' => $company, 'turn_id' => $turn, 'type' => 3]);

        $smallMachineAge = $smallMachine->count() > 0 ? $smallMachine->avg('age') : 0;
        $mediumMachineAge = $mediumMachine->count() > 0 ? $mediumMachine->avg('age') : 0;
        $largeMachineAge = $largeMachine->count() > 0 ? $largeMachine->avg('age') : 0;

        return [$smallMachineAge, $mediumMachineAge, $largeMachineAge];
    }

    public function buy($turn, $company, $quantity, $type, $price, $formPagto, $bir)
    {
        $buyValueMachine = 0;
        $financingMachine = 0;

        for ($i = 0; $i < $quantity; $i++) {
            $this->model->create([
                'turn_id' => $turn,
                'user_id' => $company,
                'type' => $type,
                'age' => 0,
                'buy_value' => $price,
                'depreciation' => 0,
                'status' => 0
            ]);
            if ($formPagto >= 30) {
                $percent = $formPagto / 100;

                // CRIA UM EMPRÉSTIMO - @CD#EMPRESTIMO
                $loan = Loan::create([
                    'user_id' => $company,
                    'value' => ($price * (1 - $percent)),
                    'bir' => $bir,
                    'time' => 5 // 5 = 0. Paga do 4 ao 0.
                ]);
                // VALOR TOTAL A SER PAGO E A SER FINANCIADO
                $buyValueMachine += $price;
                $financingMachine += ($price * (1 - $percent));
            }
        }

        return [$buyValueMachine, $financingMachine];
    }

    public function spend($companies, $turn, $lastTurn)
    {
        for ($i = 0; $i < count($companies); $i++) {
            $allMachines = parent::findWhere(['turn_id' => $lastTurn, 'user_id' => $companies[$i], 'status' => 0]);
            foreach ($allMachines as $machine) {
                $machineAge = $machine->age + 1;
                $depreciation = ($machine->buy_value / 120) * $machineAge;
                $this->model->create([
                    'turn_id' => $turn,
                    'user_id' => $companies[$i],
                    'type' => $machine->type,
                    'age' => $machineAge,
                    'buy_value' => $machine->buy_value,
                    'depreciation' => $depreciation,
                    'status' => 0
                ]);
            }
        }
    }

    public function sell($companies, $turn)
    {
        $decisionC = $this->decisionCoordinator->findWhere(['turn_id' => $turn], ['loss_on_sale'])->first()->loss_on_sale;
        for ($i = 0; $i < count($companies); $i++) {
            $decision = $this->decisionCompany->findWhere(['user_id' => $companies[$i], 'turn_id' => $turn])->first();

            $decisionS = $decision->sell_small_machine;
            $decisionM = $decision->sell_medium_machine;
            $decisionL = $decision->sell_large_machine;
            $s = [];
            $m = [];
            $l = [];

            if ($decisionS > 0) {
                $s = $this->sell_machine_id(1, $decisionS, $companies[$i], $turn);
            }
            if ($decisionM > 0)
                $m = $this->sell_machine_id(2, $decisionM, $companies[$i], $turn);
            if ($decisionL > 0)
                $l = $this->sell_machine_id(3, $decisionL, $companies[$i], $turn);
            $total = [];
            $total = array_merge($s, $m, $l);


            if (count($total) > 0) {

                $machine_sale_value = 0;
                $machine_loss_value = 0;
                // Percorre cada maquina para ser vendida e adiciona ao cashflow_sell_nxt
                for ($j = 0; $j < count($total); $j++) {
                    $machine = parent::find($total[$j]);

                    $machine_value = ($machine->buy_value - $machine->depreciation) * (1 - ($decisionC / 100));
                    //\Log::info('valor de venda => '.$machine_value);
                    $machine_loss = ($machine->buy_value - $machine->depreciation) - $machine_value;
                    //\Log::info('valor perdido => '.$machine_loss);
                    $machine_loss_value += $machine_loss;
                    $machine_sale_value += $machine_value;
                    $machine->status = 1;
                    $machine->save();
                }

                $cashflow = Cashflow::select('id', 'tmp_sell_machine_values', 'tmp_sell_machine_loss')->where('turn_id', $turn)->where('user_id', $companies[$i])->first();
                $cashflow->tmp_sell_machine_values = intval($machine_sale_value);
                $cashflow->tmp_sell_machine_loss = intval($machine_loss_value);
                $cashflow->save();
            }
        }

        return true;
    }

    public function sell_machine_id($type, $quantity, $company, $turn)
    {
        $machine = parent::orderBy('age', 'DESC')->findWhere(['user_id' => $company, 'turn_id' => $turn, 'type' => $type]);

        if ($quantity > $machine->count())
            $quantity = $machine->count();

        $machines_id = [];
        for ($i = 0; $i < $quantity; $i++) {
            array_push($machines_id, $machine[$i]->id);
        }

        return $machines_id;
    }
}
