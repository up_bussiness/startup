<?php
//#AUTO - Este arquivo foi alterado por conta da implementação da automatização do processo de simulação
namespace LogisticsGame\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use LogisticsGame\Contracts\TurnParameterRepository;
use LogisticsGame\Models\TurnParameter;
use Carbon\Carbon;

/**
 * Class TurnParameterRepositoryEloquent.
 *
 * @package namespace LogisticsGame\Repositories;
 */
class TurnParameterRepositoryEloquent extends BaseRepository implements TurnParameterRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return TurnParameter::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    public function defaultTurnParameter($month)
    {
        $turnParameter = $this->model->where('month', $month)
                            ->get();

        return $turnParameter;

    }

}
