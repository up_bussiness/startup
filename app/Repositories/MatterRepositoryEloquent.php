<?php

namespace LogisticsGame\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use LogisticsGame\Contracts\MatterRepository;
use LogisticsGame\Models\Matter;
use LogisticsGame\Validators\MatterValidator;

/**
 * Class MatterRepositoryEloquent.
 *
 * @package namespace LogisticsGame\Repositories;
 */
class MatterRepositoryEloquent extends BaseRepository implements MatterRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Matter::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
