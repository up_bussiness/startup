<?php

namespace LogisticsGame\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use LogisticsGame\Contracts\NewspaperRepository;
use LogisticsGame\Models\Newspaper;
use LogisticsGame\Models\TurnParameter;
use LogisticsGame\Validators\NewspaperValidator;
use LogisticsGame\Models\Theme;


/**
 * Class NewspaperRepositoryEloquent.
 *
 * @package namespace LogisticsGame\Repositories;
 */
class NewspaperRepositoryEloquent extends BaseRepository implements NewspaperRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Newspaper::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
    public function getMaterials()
    {
        return Theme::with('matters')->take(5)->get();
    }

    public function initialInformation($turn_id)
    {
        // 4 - Cria o informativo
        $subject = $this->model->create([
            'turn_id' => $turn_id,
            'theme_id' => 1,
            'title' => 'EMPOSSADOS NOVOS GESTORES',
            'description' => 'As indústrias fabricantes de bicicletas renovaram suas diretorias. Empossados os novos gestores seus desafios agora são: entender como está sua empresa financeiramente, quais estratégias devem ser implementadas para que a empresa continue crescendo e obtenha o melhor resultado operacional/financeiro possível e que proporcionem a ampliação do Market share, desejamos sucesso a todos.'
        ]);

        $subject = $this->model->create([
            'turn_id' => $turn_id,
            'theme_id' => 6,
            'title' => 'REAÇÃO DO MERCADO PELA POSSE DOS NOVOS GESTORES',
            'description' => 'O mercado de forma geral encarou com bons olhos a troca dos gestores, a expectativa é que a competitividade possa impactar na relação das empresas com seus clientes e o consumidor de forma geral seja beneficiado com essas mudanças. O mercado de forma geral, está otimista e aquecido, esperando novas ações por parte das indústrias em um curto prazo.'
        ]);

        $subject = $this->model->create([
            'turn_id' => $turn_id,
            'theme_id' => 3,
            'title' => 'PRODUTIVIDADE E MERCADO',
            'description' => 'As indústrias estão montando seu planejamento estratégico com foco em ganho de participação de mercado, com isso, seus clientes esperam que sejam ofertadas maior quantidade de bicicletas para os próximos períodos, nossa expectativa é ver como eles vão equalizar demanda X capacidade produtiva X volume de vendas.'
        ]);
    }

    public function defaultTurnNews($turn_id, $month)
    {
        $turnParameter = TurnParameter::where('month', $month)->firstOrFail();

        // Cria a notícia 1 se estiver prevista nos valores padrão
        if (!is_null($turnParameter->theme_id_1)){
            $subject = $this->model->create([
                'turn_id' => $turn_id,
                'theme_id' => $turnParameter->theme_id_1,
                'title' => $turnParameter->title_1,
                'description' => $turnParameter->description_1
            ]);
        } 

        // Cria a notícia 2 se estiver prevista nos valores padrão
        if (!is_null($turnParameter->theme_id_2)){
            $subject = $this->model->create([
                'turn_id' => $turn_id,
                'theme_id' => $turnParameter->theme_id_2,
                'title' => $turnParameter->title_2,
                'description' => $turnParameter->description_2
            ]);
        }

        // Cria a notícia 3 se estiver prevista nos valores padrão
        if (!is_null($turnParameter->theme_id_3)){
            $subject = $this->model->create([
                'turn_id' => $turn_id,
                'theme_id' => $turnParameter->theme_id_3,
                'title' => $turnParameter->title_3,
                'description' => $turnParameter->description_3
            ]);
        }
    }
}
