<?php

namespace LogisticsGame\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use LogisticsGame\Contracts\DecisionCoordinatorRepository;
use LogisticsGame\Models\DecisionCoordinator;
use LogisticsGame\Models\Turn;
use LogisticsGame\Models\TurnParameter;
use LogisticsGame\Validators\DecisionCoordinatorValidator;

/**
 * Class DecisionCoordinatorRepositoryEloquent.
 *
 * @package namespace LogisticsGame\Repositories;
 */
class DecisionCoordinatorRepositoryEloquent extends BaseRepository implements DecisionCoordinatorRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return DecisionCoordinator::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    public function decisionCoordinator($turn_id)
    {
        $decision = $this->model->create([
            'turn_id' => $turn_id
        ]);

        return $decision;
    }

    public function initialDecision($turn_id)
    {
        // Decisão Coordenador
        $dc = $this->model->create([
            'turn_id' => $turn_id,
            'macro_industry' => 2,
            'inflation' => 1,
            'loss_on_sale' => 5,
            'bir' => 5,
            'interest_providers' => 5,
            'import' => 5,
            'income_tax' => 12,
            'raw_material_a' => 100,
            'raw_material_b' => 60, //$TESTE$ 60
            'raw_material_c' => 30,
            'lp_small' => 200000,
            'lp_medium' => 550000,
            'lp_large' => 900000,
            'adv_radio' => 1500,
            'adv_journal' => 1250,
            'adv_social' => 3000,
            'adv_outdoor' => 5000,
            'adv_tv' => 10000 //$TESTE$ 10000
        ]);

        return $dc;
    }

    public function lastPrices($turn_id)
    {
        // simulation
        $currentTurn = Turn::findOrFail($turn_id);

        if ($currentTurn->month == 1)
            return [100, 60, 30];

        $simulation = $currentTurn->simulation->id;
        
        $lastTurn = Turn::select('id')
                        ->where('simulation_id', $simulation)
                        ->orderBy('created_at', 'desc')
                        ->take(1)
                        ->skip(1)
                        ->first()
                        ->id;
                        
        $price = parent::findWhere(['turn_id' => $lastTurn], ['raw_material_a', 'raw_material_b', 'raw_material_c'])->first();
    
        return [$price->raw_material_a, $price->raw_material_b, $price->raw_material_c];
    }
    public function RodadaAnterior($turn_id)
    {
        $currentTurn = Turn::findOrFail($turn_id);
        $simulation = $currentTurn->simulation->id;
        
        $lastTurn = Turn::select('id')
                        ->where('simulation_id', $simulation)
                        ->orderBy('created_at', 'desc')
                        ->take(1)
                        ->skip(1)
                        ->first()
                        ->id;
                          
        return $lastTurn;                
    }

    public function defaultInitialDecision($turn_id, $month)
    {
        $turnParameter = TurnParameter::where('month', $month)->firstOrFail();
        
        // Decisão Coordenador agora terá valores default da tabela de turnsParameters
        $dc = $this->model->create([
            'turn_id' => $turn_id,
            'macro_industry' => $turnParameter->macro_industry,
            'inflation' => $turnParameter->inflation,
            'loss_on_sale' => $turnParameter->loss_on_sale,
            'bir' => $turnParameter->bir,
            'interest_providers' => $turnParameter->interest_providers,
            'import' => $turnParameter->import,
            'income_tax' => $turnParameter->income_tax,
            'raw_material_a' => $turnParameter->raw_material_a,
            'raw_material_b' => $turnParameter->raw_material_b,
            'raw_material_c' => $turnParameter->raw_material_c,
            'lp_small' => $turnParameter->lp_small,
            'lp_medium' => $turnParameter->lp_medium,
            'lp_large' => $turnParameter->lp_large,
            'adv_radio' => $turnParameter->adv_radio,
            'adv_journal' => $turnParameter->adv_journal,
            'adv_social' => $turnParameter->adv_social,
            'adv_outdoor' => $turnParameter->adv_outdoor,
            'adv_tv' => $turnParameter->adv_tv
        ]);

        return $dc;
    }
}
