<?php

namespace LogisticsGame\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SimulationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'=>'required|unique:simulations,name,NULL,id,user_id,'.auth()->user()->id,
            'initial_date'=>'required|date|after_or_equal:today',
            'end_date'=>'required|date|after:initial_date',
            'number_of_company'=>'required|integer|between:3,10',
            'demand'=>'required|integer|between:10,20'
        ];
    }

    public function attributes()
    {
       return [
            'name' => 'Nome',
            'initial_date' => 'Data Inicial',
            'end_date' => 'Data Final',
            'number_of_company' => 'Número de Empresas',
            'demand' => 'Rodada',
       ];
    }
}
