<?php

namespace LogisticsGame\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use LogisticsGame\Contracts\DecisionCompanyRepository;
use LogisticsGame\Contracts\TurnRepository;
use LogisticsGame\Contracts\OperationalRepository;
use LogisticsGame\Rules\PriceRule;
use LogisticsGame\Rules\SalaryRule;
use LogisticsGame\Rules\EmergencyRule;
use LogisticsGame\Rules\FiredRule;

class DecisionCompanyRequest extends FormRequest
{
    protected $repo, $turn, $opera;

    public function __construct(DecisionCompanyRepository $repo, TurnRepository $turn, OperationalRepository $opera)
    {
        $this->repo = $repo;
        $this->turn = $turn;
        $this->opera = $opera;
    }
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'price' => ['required', 'integer', new PriceRule($this->avgPrice())],
            'term' => 'required|integer|min:0|max:1',
            'adv_radio' => 'required|integer|min:0|max:5',
            'adv_journal' => 'required|integer|min:0|max:5',
            'adv_social' => 'required|integer|min:0|max:5',
            'adv_outdoor' => 'required|integer|min:0|max:5',
            'adv_tv' => 'required|integer|min:0|max:5',
            'scheduled_rma' => 'required|integer|min:1',
            'scheduled_rmb' => 'required|integer|min:2',
            'scheduled_rmc' => 'required|integer|min:3',
            'emergency_rma' => ['required', 'integer', new EmergencyRule(1)],
            'emergency_rmb' => ['required', 'integer', new EmergencyRule(2)],
            'emergency_rmc' => ['required', 'integer', new EmergencyRule(3)],
            'payment_rm' => 'required|integer|min:0|max:1',
            'activity_level' => 'required|integer|min:50|max:100',
            'extra_production' => 'required|integer|min:0|max:25',
            'buy_small_machine' => 'required|integer|min:0|max:10',
            'sell_small_machine' => 'required|integer|min:0|max:10',
            'buy_medium_machine' => 'required|integer|min:0|max:10',
            'sell_medium_machine' => 'required|integer|min:0|max:10',
            'buy_large_machine' => 'required|integer|min:0|max:10',
            'sell_large_machine' => 'required|integer|min:0|max:10',
            'admitted' => 'required|integer|min:0|max:100',
            'fired' => ['required', 'integer', new FiredRule($this->QtdFuncionarios())],
            'salary' => ['required', 'integer', new SalaryRule($this->pastSalary(), $this->avgSalary())],
            'training' => 'required|integer|min:0|max:20',
            'loan' => 'required|integer|min:0',
            'anticipation_of_receivables' => 'required|integer|min:0|max:100',
            'application' => 'required|integer|min:0|max:70',
            'term_interest' => 'required|integer|min:0|max:15',
            'income' => 'integer|min:0',
            'expense' => 'integer|min:0'
        ];
    }

    public function sanitize()
    {
        $attr = $this->all();

        $attr['scheduled_rma'] = removeDot($attr['scheduled_rma']);
        $attr['scheduled_rmb'] = removeDot($attr['scheduled_rmb']);
        $attr['scheduled_rmc'] = removeDot($attr['scheduled_rmc']);
        $attr['emergency_rma'] = removeDot($attr['emergency_rma']);
        $attr['emergency_rmb'] = removeDot($attr['emergency_rmb']);
        $attr['emergency_rmc'] = removeDot($attr['emergency_rmc']);
        $attr['salary'] = removeDot($attr['salary']);
        $attr['loan'] = removeDot($attr['loan']);
        $attr['price'] = removeDot($attr['price']);

        $attr['b_s_m'] = ($attr['buy_small_machine'] == 0) ? 0 : $attr['b_s_m'];
        $attr['b_m_m'] = ($attr['buy_medium_machine'] == 0) ? 0 : $attr['b_m_m'];
        $attr['b_l_m'] = ($attr['buy_large_machine'] == 0) ? 0 : $attr['b_l_m'];

        $this->replace($attr);
    }

    public function prepareForValidation()
    {
        $this->sanitize();
    }

    public function lastTurn()
    {
        $turn = $this->turn->find($this->input('turn_id'));
        
        //Se for o turno 1 trata diferente
        if ($turn->month == 1){
            return $this->turn->getDecisionCoordinatorTurn($turn->id);    
        }else{
            return $this->turn->getSecondLast($turn->simulation_id);
        }
        
    }

    public function pastSalary()
    {
        $salary = $this->repo->orderBy('created_at')
            ->findWhere(['user_id' => auth()->user()->id])
            ->last();
        return $salary->salary;
    }

    public function avgSalary()
    {
        $salary = $this->repo->findWhere(['turn_id' => $this->lastTurn()->turn_id])
            ->avg('salary');

        return $salary;
    }

    public function avgPrice()
    {
        $price = $this->repo->findWhere(['turn_id' => $this->lastTurn()->turn_id])
            ->avg('price');


        return $price;
    }

    public function QtdFuncionarios()
    {

        $qtd = $this->opera->findWhere(['turn_id' => $this->lastTurn()->turn_id, 'user_id' => auth()->user()->id], ['final_employees'])->first();
        return $qtd->final_employees - 1;
    }

    public function attributes()
    {
        return [
            'price' => 'Preço à vista',
            'term' => 'Prazo',
            'adv_radio' => 'Radio',
            'adv_journal' => 'Jornal',
            'adv_social' => 'Mídias Sociais',
            'adv_outdoor' => 'Outdoor',
            'adv_tv' => 'TV',
            'scheduled_rma' => 'Quadro',
            'scheduled_rmb' => 'Kit 1',
            'scheduled_rmc' => 'Kit 2',
            'emergency_rma' => 'Emergencial - Quadro',
            'emergency_rmb' => 'Emergencial - Kit 1',
            'emergency_rmc' => 'Emergencial - Kit 2',
            'payment_rm' => 'Pagamento MP',
            'activity_level' => 'Nível de atividade',
            'extra_production' => 'Produção extra',
            'buy_small_machine' => 'Compra P',
            'sell_small_machine' => 'Venda P',
            'buy_medium_machine' => 'Compra M',
            'sell_medium_machine' => 'Venda M',
            'buy_large_machine' => 'Compra G',
            'sell_large_machine' => 'Venda G',
            'admitted' => 'Admitidos',
            'fired' => 'Demitidos',
            'salary' => 'Salário',
            'training' => 'Treinamento',
            'loan' => 'Empréstimo',
            'anticipation_of_receivables' => 'Antecipação de Recebíveis',
            'application' => 'Aplicação',
            'term_interest' => 'Juros nas vendas a prazo',
            'income' => '',
            'expense' => '',
        ];
    }
}
