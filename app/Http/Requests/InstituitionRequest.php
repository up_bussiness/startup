<?php

namespace LogisticsGame\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use LogisticsGame\Contracts\UserRepository;
use LogisticsGame\Rules\CheckLicenseRule;

class InstituitionRequest extends FormRequest
{

    public $repo;

    public function __construct(UserRepository $repo)
    {
        $this->repo = $repo;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'slug' => 'required',
            'email' => 'required',
            'logo' => 'nullable|file|image',
            'password' => 'nullable|string|min:6|max:10',
            'license' => ['required', new CheckLicenseRule($this->repo->getLicenses())]
        ];
    }

    public function attributes()
    {
        return [
            'name' => 'Instituição',
            'slug' => 'Abreviação',
            'license' => 'Licenças',
            'email' => 'Email',
            'password' => 'Senha'
        ];
    }
}
