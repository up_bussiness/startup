<?php

namespace LogisticsGame\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StudentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string',
            'email'=> 'required|string',    
            'phone' => ['required', 'regex:/^\([1-9]{2}\) (?:[2-8]|9[1-9])[0-9]{3}\-[0-9]{4}$/'],
            'face' => 'nullable|string',
            'insta' => 'nullable|string',
            'date' => 'nullable|date|before_or_equal:'.\Carbon\Carbon::now()->subYears(14)->format('Y-m-d'),
            'registration' => 'required|string',
            'occupation_id' => 'required'
        ];
    }
}
