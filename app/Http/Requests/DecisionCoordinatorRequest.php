<?php

namespace LogisticsGame\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use LogisticsGame\Contracts\TurnRepository;
use LogisticsGame\Rules\CheckPercentRule;

class DecisionCoordinatorRequest extends FormRequest
{

    public $repo;

    public function __construct(TurnRepository $repo)
    {
        $this->repo = $repo;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'macro_industry' => 'required|integer|min:-30|max:50',
            'inflation' => 'required|integer|min:-5|max:5',
            'loss_on_sale' => 'required|integer|min:-20|max:20',
            'bir' => 'required|integer|min:0|max:10',
            'interest_providers' => 'required|integer|min:1|max:10',
            'import' => 'required|integer|min:1|max:20',
            'income_tax' => 'required|integer|min:5|max:17',
            'raw_material_a' => ['required', 'integer', new CheckPercentRule($this->lastTurn()->raw_material_a)],
            'raw_material_b' => ['required', 'integer', new CheckPercentRule($this->lastTurn()->raw_material_b)],
            'raw_material_c' => ['required', 'integer', new CheckPercentRule($this->lastTurn()->raw_material_c)],
            'lp_small' => ['required', 'integer', new CheckPercentRule($this->lastTurn()->lp_small)],
            'lp_medium' => ['required', 'integer', new CheckPercentRule($this->lastTurn()->lp_medium)],
            'lp_large' => ['required', 'integer', new CheckPercentRule($this->lastTurn()->lp_large)],
            'adv_radio' => ['required', 'integer', new CheckPercentRule($this->lastTurn()->adv_radio)],
            'adv_journal' => ['required', 'integer', new CheckPercentRule($this->lastTurn()->adv_journal)],
            'adv_social' => ['required', 'integer', new CheckPercentRule($this->lastTurn()->adv_social)],
            'adv_outdoor' => ['required', 'integer', new CheckPercentRule($this->lastTurn()->adv_outdoor)],
            'adv_tv' => ['required', 'integer', new CheckPercentRule($this->lastTurn()->adv_tv)],
        ];
    }

    public function sanitize()
    {
        $attr = $this->all();

        $attr['raw_material_a'] = removeDot($attr['raw_material_a']);
        $attr['raw_material_b'] = removeDot($attr['raw_material_b']);
        $attr['raw_material_c'] = removeDot($attr['raw_material_c']);
        $attr['lp_small'] = removeDot($attr['lp_small']);
        $attr['lp_medium'] = removeDot($attr['lp_medium']);
        $attr['lp_large'] = removeDot($attr['lp_large']);
        $attr['adv_radio'] = removeDot($attr['adv_radio']);
        $attr['adv_journal'] = removeDot($attr['adv_journal']);
        $attr['adv_social'] = removeDot($attr['adv_social']);
        $attr['adv_outdoor'] = removeDot($attr['adv_outdoor']);
        $attr['adv_tv'] = removeDot($attr['adv_tv']);

        $this->replace($attr);
    }

    public function prepareForValidation()
    {
        $this->sanitize();
    }

    public function lastTurn()
    {
        $turn = $this->repo->find($this->input('turn_id'));

        return $this->repo->getSecondLast($turn->simulation_id);
    }

    public function attributes()
    {
        return [
            'macro_industry' => 'Macro setor',
            'inflation' => 'Inflação',
            'loss_on_sale' => 'Prejuízo na venda de máquina',
            'bir' => 'Taxa básica de juros',
            'interest_providers' => 'Juros dos fornecedores',
            'import' => 'Importação dos produtos',
            'income_tax' => 'Imposto de Renda',
            'raw_material_a' => 'Quadro',
            'raw_material_b' => 'Kit 1',
            'raw_material_c' => 'Kit 2',
            'lp_small' => 'Linha de Produção - Pequena',
            'lp_medium' => 'Linha de Produção - Média',
            'lp_large' => 'Linha de Produção - Grande',
            'adv_radio' => 'Propanga - Rádio',
            'adv_journal' => 'Propanga - Jornal',
            'adv_social' => 'Propanga - Mídia Social',
            'adv_outdoor' => 'Propanga - Outdoor',
            'adv_tv' => 'Propanga - TV'
        ];
    }
}
