<?php

namespace LogisticsGame\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use LogisticsGame\Contracts\UserRepository;
use LogisticsGame\Rules\CheckUserLicenseRule;

class LicenseCoordinatorRequest extends FormRequest
{

    public $repo;

    public function __construct(UserRepository $repo)
    {
        $this->repo = $repo;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'license' => ['required', new CheckUserLicenseRule($this->repo->find($this->request->get('coordinator_id'))->simulations->count())]
        ];
    }

    public function attributes()
    {
        return [
            'license' => 'Licenças',
        ];
    }
}
