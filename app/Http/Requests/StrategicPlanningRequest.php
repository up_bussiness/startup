<?php

namespace LogisticsGame\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StrategicPlanningRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'slogan' => 'required|max:255', 
            'mission' => 'required|max:255', 
            'view' => 'required|max:255', 
            'values' => 'required', 
            'weaknesses' => 'required', 
            'threats' => 'required', 
            'forces' => 'required', 
            'opportunities' => 'required', 
            'goals' => 'required',
            'goals_general' => 'required', 
            'goals_specific' => 'required'
        ]; 
    }

    public function attributes()
    {
       return [
            'mission' => 'Missão',
            'view' => 'Visão',
            'values' => 'Valores',
            'weaknesses' => 'Fraquezas',
            'threats' => 'Ameaças',
            'forces' => 'Forças',
            'opportunities' => 'Oportunidades',
            'goals' => 'Objetivos Estratégicos',
            'goals_general' => 'Objetivos Gerais',
            'goals_specific' => 'Objetivos Específicos'
       ];
    }
}
