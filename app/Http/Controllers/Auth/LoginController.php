<?php

namespace LogisticsGame\Http\Controllers\Auth;

use LogisticsGame\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        App::getLocale();
        $this->middleware('guest')->except('logout');
    }

    public function logout()
    {
        auth()->logout();
        if (session()->has('primeiro')) {
            session()->forget('primeiro');
        }
        return redirect('/login');
        //return redirect('http://upbgame.com.br');
    }

    protected function authenticated(Request $request, $user)
    {
        if ($request->getHost() === env('LOCAL_ADM')) {
            return redirect()->route('tenant');
        }
    }
}
