<?php

namespace LogisticsGame\Http\Controllers;

use LogisticsGame\Models\Balance;
use LogisticsGame\Exports\BalanceExport;
use Maatwebsite\Excel\Facades\Excel;

use Illuminate\Http\Request;

class BalanceController extends Controller
{
    public function export($turn_id, $id = null) 
    {
        return Excel::download(new BalanceExport($turn_id,$id), 'balanco.xlsx');
    }
}
