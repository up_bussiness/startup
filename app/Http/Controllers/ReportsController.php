<?php

namespace LogisticsGame\Http\Controllers;

use Illuminate\Http\Request;
use LogisticsGame\Models\User;
use LogisticsGame\Contracts\DecisionCompanyRepository;
use LogisticsGame\Contracts\DecisionCoordinatorRepository;
use LogisticsGame\Contracts\OperationalDetailRepository;
use LogisticsGame\Contracts\OperationalRepository;
use LogisticsGame\Contracts\CashflowRepository;
use LogisticsGame\Contracts\TurnRepository;
use LogisticsGame\Contracts\MachineRepository;
use LogisticsGame\Contracts\LimitLoanRepository;
use LogisticsGame\Contracts\BalanceRepository;
use LogisticsGame\Contracts\DreRepository;
use LogisticsGame\Contracts\RankingRepository;
use LogisticsGame\Contracts\NewspaperRepository;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Log;
use Illuminate\Console\Command;

class ReportsController extends Controller
{
    protected $companyRepo, $coordinatorRepo, $opDetail, $operational, $cashflow, $machine, $turn, $loan, $balance, $dre, $ranking;

    public function __construct(
        DecisionCompanyRepository $companyRepo,
        DecisionCoordinatorRepository $coordinatorRepo,
        OperationalDetailRepository $opDetail,
        OperationalRepository $operational,
        CashflowRepository $cashflow,
        TurnRepository $turn,
        MachineRepository $machine,
        LimitLoanRepository $loan,
        BalanceRepository $balance,
        DreRepository $dre,
        RankingRepository $ranking,
        NewspaperRepository $newsRepo
    ) {
        $this->companyRepo = $companyRepo;
        $this->coordinatorRepo = $coordinatorRepo;
        $this->opDetail = $opDetail;
        $this->operational = $operational;
        $this->cashflow = $cashflow;
        $this->turn = $turn;
        $this->machine = $machine;
        $this->loan = $loan;
        $this->balance = $balance;
        $this->dre = $dre;
        $this->ranking = $ranking;
        $this->newsRepo = $newsRepo;
    }

    public function operational($turn_id, $id = null)
    {
        $decisionCoordinator = $this->coordinatorRepo->findWhere(['turn_id' => $turn_id])->first();
        $operationalDetail = $this->opDetail->findWhere(['turn_id' => $turn_id])->first();
        $companies = $this->companyRepo->getDecisionOp($turn_id);
        $operationals = $this->operational->findWhere(['turn_id' => $turn_id]);
        $dre = $this->dre->findWhere(['turn_id' => $turn_id]);
        $balance = $this->balance->orderBy('created_at')->findWhere(['turn_id' => $turn_id]);

        if (Gate::allows('is-company')) {
            $decisionCompany = $this->companyRepo->findWhere(['turn_id' => $turn_id, 'user_id' => auth()->user()->id])->first();
            $operational = $this->operational->findWhere(['turn_id' => $turn_id, 'user_id' => auth()->user()->id])->first();

            if (is_null($operational)) {
                notification('Este relatório mercadológico ainda não foi gerado!', 'warning', 'Atenção!');
                return back();
            }

            return view('reports.operational', compact('decisionCoordinator', 'decisionCompany', 'operationalDetail', 'companies', 'operational', 'operationals', 'dre', 'balance'));
        }

        $operational = $this->operational->findWhere(['turn_id' => $turn_id, 'user_id' => $id])->first();
        $decisionCompany = $this->companyRepo->findWhere(['turn_id' => $turn_id, 'user_id' => $id])->first();
        return view('reports.operational', compact('decisionCoordinator', 'decisionCompany', 'operationalDetail', 'companies', 'operational', 'operationals', 'dre', 'balance'));
    }

    public function dre($turn_id, $id = null)
    {
        $dre = $this->dre->findWhere(['turn_id' => $turn_id, 'user_id' => $id])->first();

        if (Gate::allows('is-company')) {
            $dre = $this->dre->findWhere(['turn_id' => $turn_id, 'user_id' => auth()->user()->id])->first();
        }

        return view('reports.dre', compact('dre'));
    }

    public function cashflow($turn_id, $id = null)
    {
        $cashflow = $this->cashflow->findWhere(['turn_id' => $turn_id, 'user_id' => $id])->first();

        if (Gate::allows('is-company')) {
            $cashflow = $this->cashflow->findWhere(['turn_id' => $turn_id, 'user_id' => auth()->user()->id])->first();
            $limit_loan = $this->loan->findWhere(['turn_id' => $turn_id, 'user_id' => auth()->user()->id], ['new_limit'])->first()->new_limit;
            return view('reports.cashflow', compact('cashflow', 'limit_loan'));
        }

        $limit_loan = $this->loan->findWhere(['turn_id' => $turn_id, 'user_id' => $id], ['new_limit'])->first()->new_limit;
        return view('reports.cashflow', compact('cashflow', 'limit_loan'));
    }

    public function balance($turn_id, $id = null)
    {
        $balance = $this->balance->findWhere(['turn_id' => $turn_id, 'user_id' => $id])->first();

        if (Gate::allows('is-company')) {
            $balance = $this->balance->findWhere(['turn_id' => $turn_id, 'user_id' => auth()->user()->id])->first();
        }

        return view('reports.balance', compact('balance'));
    }

    public function ranking($turn_id)
    {
        $ranking_ef = $this->ranking->orderBy('loans_financing', 'ASC')->findWhere(['turn_id' => $turn_id]);
        $ranking_ms = $this->ranking->orderBy('market_share', 'DESC')->findWhere(['turn_id' => $turn_id]);
        $ranking_pl = $this->ranking->orderBy('net_worth', 'DESC')->findWhere(['turn_id' => $turn_id]);
        $ranking = $this->ranking->orderBy('total_pontos', 'DESC')->findWhere(['turn_id' => $turn_id]);
        return view('reports.ranking', compact('ranking', 'ranking_pl', 'ranking_ms', 'ranking_ef'));
    }

    public function generate($turn_id)
    {
        $turn = $this->turn->find($turn_id);
        $simulation = $turn->simulation->find($turn->simulation_id);
        $lastTurn = $this->turn->getLastTurn($turn->simulation_id);
        $decisionCoordinator = $this->coordinatorRepo->findWhere(['turn_id' => $turn_id])->first();

        // Verificar se já foi gerado.
        $machineExists = $this->machine->findWhere(['turn_id' => $turn_id])->count();
        if ($machineExists == 0) {
            $this->machine->spend($turn->simulation->companies->pluck('id'), $turn_id, $lastTurn->id);
            $this->opDetail->generate($turn_id, $decisionCoordinator->macro_industry, $decisionCoordinator->import);
            $this->opDetail->averages($turn_id);
            $this->operational->generate($turn_id);
            $this->cashflow->generate($turn_id);
            $this->balance->generate($turn_id);
            $this->dre->generate($turn_id);
            $this->ranking->generate($turn->simulation->companies->pluck('id'), $turn->id, $turn->month, $turn->simulation_id);
            $this->loan->generate($turn->simulation->companies->pluck('id'), $turn->id, $turn->month);
            $this->machine->sell($turn->simulation->companies->pluck('id'), $turn->id);
        }
        // Aqui deve chamar a criação de uma nova rodada se não estiver no turno 2 ou turno 16
        // Se a rodada for a 2 mas a simulação já tiver sido reiniciada, também cria o novo turno
        if (($turn->month != 2 or $simulation->reseted == 1) and $turn->month != 16) {
            Log::info('========================================================================');
            Log::info('Uma nova rodada sera criada automaticamente.');
            //Aguarda 20 segundos antes da criação da rodada para não ficarem as duas no mesmo timestamp
            sleep(20);
            $NovaRodada = $turn->month + 1;
            //Cria a nova rodada
            $newTurn = $this->turn->createTurn($NovaRodada, $turn->simulation_id);;
            Log::info('Rodada '.$NovaRodada.' criada com sucesso.');

            //Vai criar a decisão do professor baseado nos parâmetros default da rodada
            $iniDecCood = $this->coordinatorRepo->defaultInitialDecision($newTurn->id, $newTurn->month);
            Log::info('Decisao do professor tomada a partir dos valores padrao da nova rodada.');

            //Vai criar as notícias
            $this->newsRepo->defaultTurnNews($newTurn->id, $newTurn->month);
            //Vai publicar a rodada
            Log::info('Turno: '.$newTurn->month.' sera publicado.');
            $this->turn->update(['published' => 1], $newTurn->id);
            Log::info('Periodo publicado de forma automatica!');
            Log::info('========================================================================');
            Log::info('                    ');
        }
        notification('Período processado com sucesso!');
        return back();
    }

    public function generateFirst($turn_id)
    {
        $turn = $this->turn->find($turn_id);

        $companies = $turn->simulation->companies->pluck('id');
        $machines = $this->machine->initialInventory($companies, $turn->id);
        $import = $this->coordinatorRepo->findWhere(['turn_id' => $turn->id], ['import'])->first()->import;
        $this->turn->update(['published' => 1], $turn_id);

        return redirect()->route('simulation.email', $turn->simulation_id);
    }

    public function completeFirst($turn_id)
    {
        $turn = $this->turn->find($turn_id);
        $simulation = $turn->simulation->find($turn->simulation_id);
        $companies = $turn->simulation->companies->pluck('id');
        $machines = $this->machine->initialInventory($companies, $turn->id);
        $import = $this->coordinatorRepo->findWhere(['turn_id' => $turn->id], ['import'])->first()->import;

        //Completa os dados da primeira rodada e cria a rodada 2 da simulação
        $this->opDetail->initial($turn->id, $turn->simulation->demand, $import, $machines);
        $this->opDetail->averages($turn->id);
        
        $this->operational->generate($turn->id);
        $this->cashflow->generate($turn->id);
        $this->balance->generate($turn->id);
        $this->dre->generate($turn->id);
        $this->ranking->generate($companies, $turn->id, $turn->month, $turn->simulation_id);
        $this->loan->generate($companies, $turn->id, $turn->month);
        $this->machine->sell($turn->simulation->companies->pluck('id'), $turn->id);

        //Atualiza a data final o turno com a data atual
        $timenow = \Carbon\Carbon::now()->subSeconds(5);
        $turn = $this->turn->update(['end_date' => $timenow], $turn->id);
        
        Log::info('========================================================================');
        Log::info('Uma nova rodada sera criada automaticamente.');
        $NovaRodada = $turn->month + 1;
        //Cria a nova rodada
        $newTurn = $this->turn->createTurn($NovaRodada, $turn->simulation_id);;
        Log::info('Rodada '.$NovaRodada.' criada com sucesso.');

        //Vai criar a decisão do professor baseado nos parâmetros default da rodada
        $iniDecCood = $this->coordinatorRepo->defaultInitialDecision($newTurn->id, $newTurn->month);
        Log::info('Decisao do professor tomada a partir dos valores padrao da nova rodada.');

        //Vai criar as notícias
        $this->newsRepo->defaultTurnNews($newTurn->id, $newTurn->month);
        //Vai publicar a rodada
        Log::info('Turno: '.$newTurn->month.' sera publicado.');
        $this->turn->update(['published' => 1], $newTurn->id);
        Log::info('Periodo publicado de forma automatica!');
        Log::info('========================================================================');
        Log::info('                    ');

        return true;
    }
}
