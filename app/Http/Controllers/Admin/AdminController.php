<?php

namespace LogisticsGame\Http\Controllers\Admin;

use Illuminate\Http\Request;
use LogisticsGame\Http\Controllers\Controller;
use LogisticsGame\Contracts\SimulationRepository;
use LogisticsGame\Contracts\UserRepository;

use LogisticsGame\Http\Requests\CoordinatorRequest;
use LogisticsGame\Jobs\SendPasswordsJob;
use Illuminate\Support\Facades\Mail;
use LogisticsGame\Mail\SendPasswordsMail;
use LogisticsGame\Models\User;

class AdminController extends Controller
{

	public $simulation, $user;

	public function __construct(SimulationRepository $simulation, UserRepository $user)
	{
		$this->simulation = $simulation;
		$this->user = $user;
	}

	public function list()
	{
		$simulations = $this->simulation->all();
		$coordinators = $this->user->getCoordinators();

		$licenses = $this->user->getLicenses();

		return view('admin.index', compact('simulations', 'coordinators', 'licenses'));
	}

	public function store(Request $request)
	{
		$validatedData = $request->validate([
			'name' => 'required',
			'email' => 'required',
			'password' => 'required|min:6|max:10',
		]);

		$usuario =  User::where('email', $request->email)->get(['id'])->First();
		if (!is_null($usuario)) {
			notification('Esse e-mail já foi cadastrado!', 'error', 'ATENÇÃO', 4000);

			return redirect()->route('admin');
		}

		$user = $this->user->createCoordinator($request->toArray());
		$url = request()->getHost();
		$posicao = strpos($url, '.');
		$id = substr($url, 0, $posicao);
		$coordinator = ['email' => $user->email, 'name' => $user->name, 'login' => $user->email, 'password' => $user->password_plain, 'url' => $id];
		//$emailJob = (new SendPasswordsJob($coordinator))->delay(\Carbon\Carbon::now()->addSeconds(3));
		//dispatch($emailJob)->onQueue(env('REDIS_QUEUE'));
		Mail::to($user->email)->send(new SendPasswordsMail($coordinator));

		notification('Professor cadastrado com sucesso!');

		return redirect()->route('admin');
	}

	public function destroy($id)
	{
		$this->user->delete($id);

		notification('Professor excluído com sucesso!');

		return back();
	}

	public function edit($id)
	{
		$coordinator = $this->user->find($id);
		$licenses = $this->user->getLicenses();

		return view('admin.edit', compact('coordinator', 'licenses'));
	}

	public function updateCoordinator(CoordinatorRequest $request)
	{
		$coordinator = $this->user->updateCoordinator($request->toArray());

		notification('Dados do coordenador atualizado com sucesso!');

		return back();
	}

	public function destroySimulation($id)
	{
		$simulation = $this->simulation->delete($id);

		notification('Simulação excluída com sucesso!');

		return back();
	}
}
