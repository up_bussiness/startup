<?php

namespace LogisticsGame\Http\Controllers;

use LogisticsGame\Models\Dre;
use LogisticsGame\Exports\DreExport;
use Maatwebsite\Excel\Facades\Excel;

use Illuminate\Http\Request;

class DreController extends Controller
{
    public function export($turn_id, $id = null) 
    {
        return Excel::download(new DreExport($turn_id,$id), 'dre.xlsx');
    }
}
