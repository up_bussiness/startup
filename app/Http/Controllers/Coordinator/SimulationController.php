<?php

namespace LogisticsGame\Http\Controllers\Coordinator;

use Illuminate\Http\Request;
use LogisticsGame\Http\Controllers\Controller;
use LogisticsGame\Contracts\SimulationRepository;
use LogisticsGame\Contracts\UserRepository;
use LogisticsGame\Contracts\TurnRepository;
use LogisticsGame\Contracts\DecisionCoordinatorRepository;
use LogisticsGame\Contracts\DecisionCompanyRepository;
use LogisticsGame\Contracts\NewspaperRepository;
use LogisticsGame\Contracts\MachineRepository;
use LogisticsGame\Contracts\OperationalDetailRepository;
use LogisticsGame\Contracts\OperationalRepository;
use LogisticsGame\Http\Requests\SimulationRequest;
use LogisticsGame\Jobs\SendPasswordsJob;
use Illuminate\Support\Facades\Mail;
use LogisticsGame\Mail\SendPasswordsMail;
use LogisticsGame\Contracts\InstituitionRepository;

class SimulationController extends Controller
{

    protected $repo, $user, $turn, $coordinator, $company, $newspaper, $machine, $operational, $insti;

    public function __construct(
        SimulationRepository $repo,
        UserRepository $user,
        TurnRepository $turn,
        DecisionCoordinatorRepository $coordinator,
        DecisionCompanyRepository $company,
        NewspaperRepository $newspaper,
        MachineRepository $machine,
        OperationalDetailRepository $opDetail,
        OperationalRepository $operational,
        InstituitionRepository $insti
    ) {
        $this->repo = $repo;
        $this->user = $user;
        $this->turn = $turn;
        $this->coordinator = $coordinator;
        $this->company = $company;
        $this->newspaper = $newspaper;
        $this->machine = $machine;
        $this->opDetail = $opDetail;
        $this->operational = $operational;
        $this->insti = $insti;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $simulations = $this->repo->getMySimulations();

        $instituition = $this->insti->first();

        $licenses = $this->repo->getMyLicenses();
        if (session()->has('primeiro')) {
            session()->put('primeiro', 2);
        } else {
            session()->put('primeiro', 1);
        }
        return view('coordinator.simulation.index', compact('simulations', 'licenses', 'instituition'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $simulations = $this->repo->getMySimulations();
        $licenses = $this->repo->getMyLicenses();

        if ($licenses != 0) {
            return view('coordinator.simulation.create');
        }

        notification('Você não possui mais licenças!', 'error', 'Acesso negado!');

        return back();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SimulationRequest $request)
    {
        // 0 - Cria simulação
        $simulation = $this->repo->create($request->toArray());
        // 1 - Cria empresas
        $companies = $this->user->createCompanies($simulation->number_of_company, $simulation->id);
        // 2 - Cria rodada
        $turn = $this->turn->initialTurn($simulation->id);
        // 3 - Decisão do coordenador
        $decisionCoordinator = $this->coordinator->initialDecision($turn->id);
        // 4 - Informativo
        $this->newspaper->initialInformation($turn->id);
        // 5 - Decisão das empresas
        $this->company->initialDecision($companies, $turn->id);
        notification('Simulação criada com sucesso!');
        return redirect()->route('simulation.report.first', $turn->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $simulation = $this->repo->with([
            'turns', 'companies'
            => function ($query) {
                $query->where('role', '=', 'company');
            }
        ])->findWhere(['id' => $id, 'user_id' => auth()->user()->id])->first();

        if (is_null($simulation)) {
            notification('Você não tem permissão para acessar essa simulação!', 'error', 'ERRO!');
            return redirect()->route('simulation.index');
        }

        $turn = $simulation->turns->sortByDesc('month')->first();

        $turns = $simulation->turns->sortByDesc('month')->pluck('month', 'id');

        return view('coordinator.simulation.show', compact('simulation', 'turn', 'turns'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $simulation = $this->repo->find($id);

        return view('coordinator.simulation.edit', compact('simulation'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //TODO: Mudar aqui para verificar se o nome mudou e só validar se realmente mudou, se não não deve validar.
        $validatedData = $request->validate([
            //'name' => 'required|unique:simulations,name,NULL,id,user_id,' . auth()->user()->id,
            'name' => 'required:simulations,name,NULL,id,user_id,' . auth()->user()->id,
        ]);

        //$simulation = $this->repo->update($request->only(['name']), $id);
        $simulation = $this->repo->update($request->toArray(), $id);

        notification('Simulação atualizada com sucesso');

        return redirect()->route('simulation.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // TODO: Validate backend if 2 turns
        $simulation = $this->repo->delete($id);

        notification('Simulação excluída com sucesso!');

        return back();
    }

    public function showTurn($simulation, $id)
    {
        $simulation = $this->repo->with([
            'turns', 'companies'
            => function ($query) {
                $query->where('role', '=', 'company');
            }
        ])->find($simulation);

        $turn = $this->turn->find($id);

        $turns = $simulation->turns->sortByDesc('month')->pluck('month', 'id');

        return view('coordinator.simulation.show', compact('simulation', 'turn', 'turns'));
    }

    public function team($simulation)
    {
        $simulation = $this->repo->with(['companies'])->findWhere(['id' => $simulation])->first();

        return view('coordinator.simulation.companies', compact('simulation'));
    }

    public function sendPasswordMail(Request $request)
    {
        $data = $request->toArray();

        /*for ($i=0; $i < count($data['name']); $i++) { 
            if ($data['email'][$i] != null) {
                $company = ['email' => $data['email'][$i], 'name' => $data['name'][$i], 'login' => $data['login'][$i], 'password' => $data['password'][$i], 'url' => env('APP_URL')];
                $emailJob = (new SendPasswordsJob($company))->delay(\Carbon\Carbon::now()->addSeconds(3));
                dispatch($emailJob)->onQueue(env('REDIS_QUEUE'));
            }
        }*/
        $url = request()->getHost();
        $posicao = strpos($url, '.');
        $id = substr($url, 0, $posicao);
        for ($i = 0; $i < count($data['name']); $i++) {
            if ($data['email'][$i] != null) {
                $company = ['email' => $data['email'][$i], 'name' => $data['name'][$i], 'login' => $data['login'][$i], 'password' => $data['password'][$i], 'url' => $id];
                Mail::to($data['email'][$i])->send(new SendPasswordsMail($company));
            }
        }
        /*$i=0;
        $company = ['email' => $data['email'][$i], 'name' => $data['name'][$i], 'login' => $data['login'][$i], 'password' => $data['password'][$i], 'url' => env('APP_URL')];
        Mail::to('elasawilmar@gmail.com')->send(new SendPasswordsMail($company)); */

        notification('Simulação criada com sucesso!');

        return redirect()->route('simulation.show', $request->simulation_id);
    }
}
