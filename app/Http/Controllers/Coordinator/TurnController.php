<?php

namespace LogisticsGame\Http\Controllers\Coordinator;

use Illuminate\Http\Request;
use LogisticsGame\Http\Controllers\Controller;
use LogisticsGame\Contracts\TurnRepository;
use LogisticsGame\Contracts\SimulationRepository;
use LogisticsGame\Contracts\DecisionCoordinatorRepository;
use LogisticsGame\Models\Simulation;
use LogisticsGame\Models\Newspaper;
use LogisticsGame\Models\DecisionCompany;

use LogisticsGame\Models\Machine;
use LogisticsGame\Models\OperationalDetail;
use LogisticsGame\Models\Operational;
use LogisticsGame\Models\Cashflow;
use LogisticsGame\Models\Balance;
use LogisticsGame\Models\Dre;
use LogisticsGame\Models\Ranking;
use LogisticsGame\Models\LimitLoan;

class TurnController extends Controller
{

	protected $repo, $simulation, $decisionCoordinator;

	public function __construct(TurnRepository $repo,
								SimulationRepository $simulation,
								DecisionCoordinatorRepository $decisionCoordinator)
	{
		$this->repo = $repo;
		$this->simulation = $simulation;
		$this->decisionCoordinator = $decisionCoordinator;
	}

	public function create($simulation_id)
	{
		$month = $this->simulation->getLastTurn($simulation_id);

			if (is_null($month)) {
				notification('Você não tem permissão para criar um turno nessa simulação!', 'error', 'Acesso negado!');
				return redirect()->route('simulation.index');
			}

		$canCreateTurn = $this->simulation->canCreateTurn($simulation_id);

			if (!$canCreateTurn) {
				notification('Você não pode criar uma nova rodada enquanto tiver outra aberta ou não publicada!', 'error', 'Acesso negado!');
				return redirect()->route('simulation.show', $simulation_id);
			}

    // SE NÃO TIVER PROCESSADO 
    $generated = $this->repo->turnIsGenerated($simulation_id);

    if (!$generated) {
      notification('Você não pode criar uma nova rodada enquanto tiver outra não processada!', 'error', 'Acesso negado!');
      return redirect()->route('simulation.show', $simulation_id);
    }

		$turn = $this->repo->createTurn($month, $simulation_id);

			if (is_null($turn)) {
				notification('O jogo possui apenas 16 rodadas!', 'error', 'Acesso negado!');
				return redirect()->route('simulation.show', $simulation_id);
			}

		return redirect()->route('decision.create', $turn->id);
	}

	
  	public function chooseDate($id)
  	{
  		$turn = $this->repo->find($id);

  		if ($turn->simulation->user_id != auth()->user()->id) {
  		  notification('Você não tem permissão para acessar esse informativo!', 'error', 'ERRO!');
			  return redirect()->route('simulation.show', $turn->simulation_id);
  		}

  		return view('coordinator.turn.date', compact('turn'));
  	}

  	public function setDate(Request $request)
  	{
  		$end_date = date('Y-m-d H:i:s', strtotime($request->end_date.' '.$request->hour));
  		$turn = $this->repo->update(['end_date' => $end_date], $request->turn_id);

  		notification('Período '.$turn->month.' criado/atualizado com sucesso!');
  		return redirect()->route('simulation.show', $turn->simulation_id);
  	}

  	public function publish($id)
  	{
  		$turn = $this->repo->find($id);

  		// Verificar se o coordenador tomou a decisão
        $decisionCoordinator = $this->decisionCoordinator->findWhere(['turn_id' => $turn->id])->first();

        if (is_null($decisionCoordinator)) {
        	notification('Você só pode publicar a rodada após tomar sua decisão!', 'error', 'ERRO!');
            return redirect()->route('simulation.show', $turn->simulation_id);
        }

        // Verificar se o coordenador preencheu o informativo
        $newspapers = Newspaper::where('turn_id', $turn->id)->get();

        if ($newspapers->count() == 0) {
        	notification('Você só pode publicar a rodada após preencher o informativo!', 'error', 'ERRO!');
            return redirect()->route('simulation.show', $turn->simulation_id);
        }

        $this->repo->update(['published' => 1], $turn->id);

        notification('Período publicado com sucesso!');
        return redirect()->route('simulation.show', $turn->simulation_id);
  	}

    public function closeTurn($id)
    {
      $turn = $this->repo->find($id);

        if ($turn->simulation->user_id != auth()->user()->id) {
          notification('Você não tem permissão executar essa ação!', 'error', 'ERRO!');
          return redirect()->route('simulation.show', $turn->simulation_id);
        }

      $decisions = \DB::table('decision_companies')->where('turn_id', $turn->id)->distinct('user_id')->count('user_id');
      if ($decisions != $turn->simulation->number_of_company) {
        notification('Para encerrar a rodada é necessário que todas as empresas tomem sua decisão!', 'error', 'ERRO!');
        return redirect()->route('simulation.show', $turn->simulation_id);
      }

      $timenow = \Carbon\Carbon::now()->subSeconds(5);
      $turn = $this->repo->update(['end_date' => $timenow], $turn->id);

      notification('Período encerrado com sucesso!');
      return redirect()->route('simulation.show', $turn->simulation_id);
    }

	public function restart($simulation_id)
	{
		//$turn = $this->repo->find($id);
		$simulation	= $this->simulation->find($simulation_id);
		//$simulation	= Simulation::where ('id', $simulation_id)->first();
		//Verifica se já houve reset
		if ($simulation->reseted==true){
			notification('Esta simulação já foi reiniciada uma vez e não pode ser reiniciada novamente!', 'error', 'Acesso negado!');
			return redirect()->route('simulation.index');
		}
		$turn = $this->repo->getLastTurnSimulation($simulation_id)->first();

		if (is_null($turn)) {
			notification('Você não tem permissão para reiniciar o turno 2 nessa simulação!', 'error', 'Acesso negado!');
			return redirect()->route('simulation.index');
		}

		if ($turn->month!=2) {
			notification('Só é possível reiniciar a simulação estando na rodada 2!', 'error', 'Acesso negado!');
			return redirect()->route('simulation.show', $simulation_id);
		}

		 // Desfazer Processamento
		$turn_id = $turn->id;
 		Machine::where('turn_id', $turn_id)->delete();
 		OperationalDetail::where('turn_id', $turn_id)->delete();
 		Operational::where('turn_id', $turn_id)->delete();
 		Cashflow::where('turn_id', $turn_id)->delete();
 		Balance::where('turn_id', $turn_id)->delete();
 		Dre::where('turn_id', $turn_id)->delete();
 		Ranking::where('turn_id', $turn_id)->delete();
		LimitLoan::where('turn_id', $turn_id)->delete();
		DecisionCompany::where('turn_id', $turn_id)->delete();

		$this->simulation->update(['reseted' => 1], $simulation->id);
		//Acrescenta 7 dias na rodada
		$endDate = \Carbon\Carbon::now()->addDays(7)->subSeconds(5);
     	$this->repo->update(['end_date' => $endDate], $turn->id);

		notification('Rodada 2 reiniciada com sucesso!');
		return redirect()->route('simulation.show', $simulation_id);	
	}
}
