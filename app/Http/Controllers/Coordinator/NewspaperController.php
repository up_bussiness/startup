<?php

namespace LogisticsGame\Http\Controllers\Coordinator;

use Illuminate\Http\Request;
use LogisticsGame\Http\Controllers\Controller;
use LogisticsGame\Contracts\TurnRepository;
use LogisticsGame\Contracts\DecisionCoordinatorRepository;
use LogisticsGame\Contracts\NewspaperRepository;

use LogisticsGame\Models\Matter;

class NewspaperController extends Controller
{
    protected $repo, $turn, $dc;

    public function __construct(TurnRepository $turn, NewspaperRepository $repo, DecisionCoordinatorRepository $dc)
    {
        $this->repo = $repo;
        $this->turn = $turn;
        $this->dc = $dc;
    }

    public function create($id)
    {
        $turn = $this->turn->find($id);

        if ($turn->simulation->user_id != auth()->user()->id) {
            notification('Você não tem permissão para acessar esse informativo!', 'error', 'ERRO!');
            return redirect()->route('simulation.show', $turn->simulation_id);
        }

        $themes = $this->repo->getMaterials();
        $newspapers = $this->repo->findWhere(['turn_id' => $turn->id]);
        $newspapers = is_null($newspapers) ? [] : $newspapers;

        return view('coordinator.subject.create', compact('turn', 'newspapers', 'themes'));
    }

    public function destroy($id)
    {
        $newspaper = $this->repo->with('turn')->find($id);

        if ($newspaper->turn->simulation->user_id != auth()->user()->id) {
            notification('Você não tem permissão para acessar esse informativo!', 'error', 'ERRO!');
            return redirect()->route('simulation.show', $newspaper->turn->simulation_id);
        }

        $newspaper->delete();
        notification('Matéria excluída com sucesso!');
        return redirect()->route('newspaper.index', $newspaper->turn->id);
    }

    
    public function showForCompanies($id)
    {
        setLocale(LC_TIME, 'Portuguese_Brazilian');
        $newspapers = $this->repo->findWhere(['turn_id' => $id]);
        $dc = $this->dc->findWhere(['turn_id' => $id])->first();

        return view('company.subject.show', compact('newspapers', 'dc'));
    }

    /**
     *
     * API METHODS
     *
     */
    public function store(Request $request)
    {
        // TODO: Refactor to repository
        $newspaper = $this->repo->create([
            'turn_id' => $request->turn,
            'theme_id' => $request->theme,
            'title' => $request->title,
            'description' => $request->description
        ]);

        $newspapers = $this->repo->with('theme')->findWhere(['turn_id' => $request->turn]);

        return $newspapers;
    }

    public function edit($id)
    {
        $newspaper = $this->repo->find($id);

        $themes = $this->repo->getMaterials();
        return view('coordinator.subject.editar', compact('newspaper', 'themes'));
    }

    public function update(Request $request, $id)
    {
        $newspaper = $this->repo->update($request->toArray(), $id);
        notification('Notícia editada com sucesso!');

        return redirect()->route('newspaper.index', $request->turn_id);
    }

    public function getMatters($id)
    {
        // TODO: Refactor to repository
        return Matter::where('theme_id', $id)->get();
    }

    public function getMattersContent($id)
    {
        // TODO: Refactor to repository
        return Matter::find($id);
    }
}
