<?php

namespace LogisticsGame\Http\Controllers\Coordinator;

use Illuminate\Http\Request;
use LogisticsGame\Http\Controllers\Controller;
use LogisticsGame\Contracts\DecisionCoordinatorRepository;
use LogisticsGame\Contracts\TurnRepository;
use LogisticsGame\Http\Requests\DecisionCoordinatorRequest;


class DecisionCoordinatorController extends Controller
{
  protected $repo;

  public function __construct(DecisionCoordinatorRepository $repo, TurnRepository $turn)
  {
    $this->repo = $repo;
    $this->turn = $turn;
  }

  public function index()
  {
    return view('coordinator.decision.index');
  }

  public function create($turn_id)
  {
    $turn = $this->turn->find($turn_id);

    // Se o dono da simulação for diferente do user autenticado
    if ($turn->simulation->user_id != auth()->user()->id) {
      notification('Você não tem permissão para tomar essa decisão!', 'error', 'ERRO!');
      return redirect()->route('simulation.show', $turn->simulation_id);
    }

    // Se já foi tomada a decisão para este turno
    if ($turn->decisionCoordinator) {
      notification('Esta decisão já foi tomada!', 'error', 'ERRO!');
      return redirect()->route('simulation.show', $turn->simulation_id);
    }

    $lastTurn = $this->turn->getSecondLast($turn->simulation_id);

    return view('coordinator.decision.create', compact('turn', 'lastTurn'));
  }

  public function store(DecisionCoordinatorRequest $request)
  {
    $decision = $this->repo->create($request->toArray());

    return redirect()->route('newspaper.index', $decision->turn_id);
  }

  public function edit($id)
  {
    $turn = $this->turn->find($id);

    // Se o dono da simulação for diferente do user autenticado
    if ($turn->simulation->user_id != auth()->user()->id) {
      notification('Você não tem permissão para editar essa decisão!', 'error', 'ERRO!');
      return redirect()->route('simulation.show', $turn->simulation_id);
    }

    // Se não foi tomada a decisão para este turno
    if (!$turn->decisionCoordinator) {
      notification('Esta decisão ainda não foi tomada!', 'error', 'ERRO!');
      return redirect()->route('decision.create', $turn->id);
    }

    $decision = $turn->decisionCoordinator;

    return view('coordinator.decision.create', compact('turn', 'decision'));
  }

  public function update(DecisionCoordinatorRequest $request, $id)
  {
    $decision = $this->repo->update($request->toArray(), $id);

    notification('Decisão atualizada com sucesso!');
    return redirect()->route('simulation.show', $decision->turn->simulation_id);
  }

  public function show($id)
  {
    $decision = $this->repo->find($id);

    return view('coordinator.decision.show', compact('decision'));
  }

  public function getManualJogo()
  {
    return view('reports.manual');
  }

  public function getLinkManual()
  {
    $base = '/images/manual.pdf';
    return $base;
  }
}
