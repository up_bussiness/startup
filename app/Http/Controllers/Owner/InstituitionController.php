<?php

namespace LogisticsGame\Http\Controllers\Owner;

use Illuminate\Http\Request;
use LogisticsGame\Http\Controllers\Controller;
use LogisticsGame\Http\Requests\InstituitionRequest;
use LogisticsGame\Contracts\UserRepository;
use LogisticsGame\Contracts\InstituitionRepository;
use LogisticsGame\Contracts\SimulationRepository;

use LogisticsGame\Models\Student;
use LogisticsGame\Http\Requests\LicenseCoordinatorRequest;

class InstituitionController extends Controller
{

	protected $repo, $user, $simulation;

	public function __construct(InstituitionRepository $repo, UserRepository $user, SimulationRepository $simulation)
	{
		$this->repo = $repo;
		$this->user = $user;
		$this->simulation = $simulation;
		$this->middleware(['auth', 'owner']);
	}

	public function manage()
	{
		$instituition = $this->repo->first();
		$licenses = $this->user->getLicenses();
		$coordinators = $this->user->getCoordinators();
		$students = Student::count();

		return view('owner.index', compact('instituition', 'licenses', 'coordinators', 'students'));
	}

	public function create()
	{
		$instituition = $this->repo->first();

		$licenses = $this->user->getLicenses();

		$simulations = $this->simulation->all();

		return view('owner.instituition.manage', compact('instituition', 'licenses', 'simulations'));
	}

	public function store(Request $request)
	{
		$validatedData = $request->validate([
			'name' => 'required',
			'email' => 'required',
			'password' => 'required|min:6|max:10',
			'license' => 'required',
		]);

		$user = $this->user->createAdmin($request->toArray());

		$instituition = $this->repo->createInstituition($request->toArray(), $user->id);


		return redirect()->route('owner')->with('flash_message', 'Criada!');
	}

	public function update(InstituitionRequest $request)
	{
		$instituition = $this->repo->update($request->toArray(), $request->instituition_id);

		if (!is_null($request->password)) {
			$data = $request->only(['email', 'password', 'coordinator_id']);
			$this->user->updateCoordinator($data);
		}

		// $users = $this->user->updateLicenses($instituition->license);

		return redirect()->back()->with('flash_message', 'Atualizada com sucesso!');
	}

	public function reset()
	{
		$simulation = $this->simulation->reset();

		return redirect()->route('owner')->with('flash_message', 'Criada!');
	}

	public function destroySimulation($id)
	{
		$simulation = $this->simulation->delete($id);

		return redirect()->back()->with('flash_message', 'Simulação excluída com sucesso!');
	}

	public function manageCoordinator($id)
	{
		$coordinator = $this->user->find($id);
		$licenses = $this->user->getLicenses();

		return view('owner.instituition.coordinator', compact('coordinator', 'licenses'));
	}

	public function updateCoordinator(LicenseCoordinatorRequest $request)
	{
		$coordinator = $this->user->updateLicenses($request->toArray());

		return redirect()->route('owner')->with('flash_message', 'Número de licenças do coordenador atualizado com sucesso!');
	}

	public function students()
	{
		$students = Student::all();

		return view('owner.students', compact('students'));
	}
}
