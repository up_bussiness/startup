<?php

namespace LogisticsGame\Http\Controllers;

use LogisticsGame\Models\Cashflow;
use LogisticsGame\Exports\CashflowExportView;
use LogisticsGame\Exports\CashflowExport;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Http\Request;

class CashflowController extends Controller
{
    public function exportview() 
    {
        return Excel::download(new CashflowExportView, 'users.xlsx');
    }
    public function export($turn_id, $id = null) 
    {
        return Excel::download(new CashflowExport($turn_id,$id), 'fluxodecaixa.xlsx');
    }
}
