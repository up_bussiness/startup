<?php

namespace LogisticsGame\Http\Controllers\Tenant;

use Illuminate\Http\Request;
use LogisticsGame\Http\Requests\Tenant\StoreUpdateCompanyFormRequest;
use LogisticsGame\Events\Tenant\ClienteCreated;
use LogisticsGame\Events\Tenant\DatabaseCreated;
use LogisticsGame\Http\Controllers\Controller;
use LogisticsGame\Models\Cliente;
use Illuminate\Support\Facades\Auth;
use LogisticsGame\Tenant\ManagerTenant;
use LogisticsGame\Models\User;
use Illuminate\Support\Facades\Mail;
use LogisticsGame\Mail\SendPasswordsMail;

class ClienteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $cliente;

    public function __construct(Cliente $cliente)
    {
        $this->cliente = $cliente;
    }

    private function fazDominioParaBanco($id)
    {
        //$base_dominio = env('LOCAL_ADMIN');
        $base_dominio = env('LOCAL_ADM');
        $parte2 = str_replace("adm.", "", $base_dominio);
        return $id . '.' . $parte2;
    }

    public function index()
    {
        $clientes = $this->cliente->paginate();
        return view('tenants.clientes.index', compact('clientes'));
    }

    public function home()
    {
        $clientes =  Cliente::all();
        return view('tenants.home.index', compact('clientes'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('tenants.clientes.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreUpdateCompanyFormRequest $request)
    {
        /*    $cliente = $this->cliente->create(
            [
                'name' => 'Empresa x ' . str_random(5),
                'domain' => str_random(5) . 'minhaempresa.com',
                'bd_database' => 'multi_tenant' . str_random(5),
                'bd_hostname' => '127.0.0.1',
                'bd_username' => 'root',
                'uuid' => '1234',
                'bd_password' => ''
            ]
        );
        //dd($cliente);
        if (true) { // cria banco e tabelas
             event(new ClienteCreated($cliente));
        } else { //cria somente as tabelas
             event(new DatabaseCreated($cliente));
        } */

        $cliente = $this->cliente->create($request->all());

        if ($request->has('create_database'))
            event(new ClienteCreated($cliente));
        else
            event(new DatabaseCreated($cliente));

        return redirect()
            ->route('cliente.index')
            ->withSuccess('Cadastro realizado com sucesso!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($domain)
    {
        $cliente = $this->cliente->where('domain', $domain)->first();

        if (!$cliente)
            return redirect()->back();

        return view('tenants.clientes.show', compact('cliente'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($domain)
    {
        $cliente = $this->cliente->where('domain', $domain)->first();

        if (!$cliente)
            return redirect()->back();

        return view('tenants.clientes.edit', compact('cliente'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreUpdateCompanyFormRequest $request, $id)
    {
        if (!$cliente = $this->cliente->find($id))
            return redirect()->back()->withInput();

        $cliente->update($request->all());

        return redirect()
            ->route('cliente.index')
            ->withSuccess('Atualizado com sucesso!');
    }

    public function playing()
    {
        if (session()->has('primeiro')) {
            session()->forget('primeiro');
        }
        return view('auth.logintenant');
    }

    public function autenticando(Request $request)
    {
        $manager = app(ManagerTenant::class);

        $dominio = $this->fazDominioParaBanco($request->id);
        //dd($request);
        if ($dominio == "adm.upmaster.test") {

            if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
                $url = 'http://' . $dominio . '/tenants';
                return redirect()->to($url);
            } else {
                return redirect()
                    ->back()
                    ->with('error', 'E-mail ou senha invalido(s)!');
            }
        }

        //        config()->set('session.domain', '.upmaster.test');

        $cliente = Cliente::where('domain', $dominio)->First();

        if (!$cliente) {
            return redirect()
                ->back()
                ->with('error', 'ID invalido !');
        }

        $manager->setConnection($cliente);
        $manager->setFileSystems($cliente);

        $users = User::where('email', $request->email)->where('password_plain', $request->password)->get()->First();

        //dd($users);
        if (!$users) {
            return redirect()->back()->with('error', 'E-mail ou senha invalido(s) !!');
        }
        $role = $users->role;
        $urlinicial = '';
        switch ($role) {
            case "company":
                $urlinicial = '';
                break;
            case "coordinator":
                $urlinicial = '/coordinator/simulation';
                break;
            case "owner":
                $urlinicial = '/setup';
                break;
            case "admin":
                $urlinicial = '/admin';
                break;
        }

        if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
            $url = 'http://' . $dominio . $urlinicial;

            return redirect()->to($url);
        } else {
            return redirect()
                ->back()
                ->with('error', 'E-mail ou senha invalido(s) !');
        }
    }

    public function enviarSenha(Request $request)
    {
        $manager = app(ManagerTenant::class);

        $validatedData = $request->validate([
            'id' => 'required',
            'email' => 'required',
        ]);

        $dominio = $this->fazDominioParaBanco($request->id); //gera dominio desejado
        $cliente = Cliente::where('domain', $dominio)->First();

        if (!$cliente) {
            return redirect()
                ->back()
                ->with('error', 'ID invalido !');
        }

        $manager->setConnection($cliente);
        $users = User::where('email', $request->email)->get()->First();

        if (!$users) {
            return redirect()->back()->with('error', 'E-mail invalido !!');
        }
        $role = $users->role;
        if ($role == 'company') {
            return redirect()->back()->with('error', 'Solicite sua senha com o professor !!');
        }
        $id = ' '; //url
        $coordinator = ['email' => $users->email, 'name' => $users->name, 'login' => $users->email, 'password' => $users->password_plain, 'url' => $id];
        Mail::to($users->email)->send(new SendPasswordsMail($coordinator));

        return redirect()->back()->with('error', 'Enviado...acesse sua caixa de e-mail!');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
