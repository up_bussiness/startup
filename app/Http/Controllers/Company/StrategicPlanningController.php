<?php

namespace LogisticsGame\Http\Controllers\Company;

use Illuminate\Http\Request;
use LogisticsGame\Http\Controllers\Controller;
use LogisticsGame\Contracts\UserRepository;
use LogisticsGame\Contracts\StrategicPlanningRepository;
use Laravolt\Avatar\Avatar;
use LogisticsGame\Http\Requests\StrategicPlanningRequest;

class StrategicPlanningController extends Controller
{

    protected $repo, $userRepo;

    public function __construct(StrategicPlanningRepository $repo, UserRepository $userRepo)
    {
        $this->repo = $repo;
        $this->userRepo = $userRepo;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $planning = $this->repo->myStrategic();

        if ($planning != null) {
            return view('company.planning.manage', compact('planning'));
        }

        return view('company.planning.manage');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StrategicPlanningRequest $request)
    {
       $planning = $this->repo->create($request->toArray());

       notification('Planejamento salvo!');

       return redirect()->route('company');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $companies = $this->userRepo->getCompaniesNameId($id);
        $simulation = $id;

        return view('coordinator.strategic_plan.index', compact('companies', 'simulation'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StrategicPlanningRequest $request, $id)
    {
        $planning = $this->repo->update($request->toArray(), $id);

        return redirect()->route('planning.create')->with('flash_message', 'Editado!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function apiPlanning($id)
    {
        $planning = $this->repo->findWhere(['user_id' => $id], ['user_id', 'slogan', 'mission', 'view', 'values', 'weaknesses', 'threats', 'forces', 'opportunities', 'goals', 'goals_general', 'goals_specific'])->first();

        if(is_null($planning)) {
            return response()->json(['error' => 'adpsaldpla'], 404);
        }

        $companyName = $planning->user->name;
        $avatar = $planning->user->logo;

        if (is_null($avatar)) {
            $avatar = new Avatar();
            $avatar = $avatar->create($companyName)->toBase64();
        }

        return response()->json(['planning' => $planning, 'name' => $companyName, 'avatar' => $avatar], 200);
    }
}
