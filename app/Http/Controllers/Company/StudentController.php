<?php

namespace LogisticsGame\Http\Controllers\Company;

use Illuminate\Http\Request;
use LogisticsGame\Http\Controllers\Controller;
use LogisticsGame\Contracts\StudentRepository;
use LogisticsGame\Contracts\UserRepository;
use LogisticsGame\Contracts\OccupationRepository;
use LogisticsGame\Http\Requests\StudentRequest;
use LogisticsGame\Models\User;
use Illuminate\Support\Facades\Mail;
use LogisticsGame\Mail\SendMailAluno;

class StudentController extends Controller
{

    protected $repo, $occupation, $user;


    public function __construct(StudentRepository $repo, OccupationRepository $occupation, UserRepository $user)
    {
        $this->repo = $repo;
        $this->occupation = $occupation;
        $this->user = $user;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $students = $this->repo->findWhere(['user_id' => auth()->user()->id]);

        return view('company.student.index', compact('students'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $occupation = $this->occupation->all()->pluck('name', 'id');

        return view('company.student.manage', compact('occupation'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StudentRequest $request)
    {
        if (!$this->user->getUserEmail($request->email)) {
            $this->user->createAluno($request->name, $request->email, $request->occupation_id, auth()->user()->simulation_id, auth()->user()->id);
        } else {
            notification('Este e-mail já esta em uso no UpBgame!');
            return redirect()->route('student.index');
        }

        $student = $this->repo->create($request->toArray());

        $usuario =  User::where('email', $request->email)->get(['name', 'email', 'password_plain'])->First();
        /* empresa */
        $email_empresa = auth()->user()->email;
        $pass_empresa = auth()->user()->password_plain;
        /* aluno */
        $url = request()->getHost();
        $posicao = strpos($url, '.');
        $id = substr($url, 0, $posicao);

        //dd($id);

        $company = ['email' => $usuario['email'], 'name' => $id, 'login' => $usuario['email'], 'password' => $usuario['password_plain'], 'url' => env('APP_URL'), 'emailemp' => $email_empresa, 'passemp' => $pass_empresa];
        Mail::to($usuario['email'])->send(new SendMailAluno($company));

        notification('Estudante adicionado com sucesso!');
        return redirect()->route('student.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $student = $this->repo->find($id);

        $occupation = $this->occupation->all()->pluck('name', 'id');

        return view('company.student.manage', compact('student', 'occupation'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $anterior = $this->repo->find($id);

        if ($anterior->email != $request->email) {
            notification('Para alterar o e-mail. Excluir o aluno, e incluir novamente!', 'error', 'Atenção!', 5000);
        } else {
            $student = $this->repo->update($request->toArray(), $id);
            notification('Estudante editado com sucesso!');
        }

        return redirect()->route('student.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $nouser = $this->repo->find($id);
        $usuario =  User::where('email', $nouser->email)->get(['id'])->First();

        $student = $this->repo->delete($id);
        if (!is_null($usuario)) {
            $this->user->delete($usuario->id);
        }

        notification('Estudante deletado com sucesso!');

        return redirect()->route('student.index');
    }

    public function apiStudent($id)
    {
        $student = $this->repo->with('occupation')->findWhere(['user_id' => $id], ['name', 'email', 'registration', 'occupation_id', 'phone', 'date', 'face', 'insta']);

        if (count($student) <= 0) {
            return response()->json(['error' => 'not found'], 404);
        }

        return $student;
    }

    public function getUsuarioQrCode($email)
    {
        $usuario =  User::where('email', $email)->get(['name', 'email', 'password_plain'])->First();
        return view('company.student.qrcode', compact('usuario'));
    }

    public function getUsuarioAcessoApp($email)
    {
        $usuario =  User::where('email', $email)->get(['name', 'email', 'password_plain'])->First();
        /* empresa */
        $email_empresa = auth()->user()->email;
        $pass_empresa = auth()->user()->password_plain;
        /* aluno */
        $url = request()->getHost();
        $posicao = strpos($url, '.');
        $id = substr($url, 0, $posicao);
        $company = ['email' => $usuario['email'], 'name' => $id, 'login' => $usuario['email'], 'password' => $usuario['password_plain'], 'url' => env('APP_URL'), 'emailemp' => $email_empresa, 'passemp' => $pass_empresa];
        // dd($email_empresa, $pass_empresa);
        Mail::to($usuario['email'])->send(new SendMailAluno($company));
        notification('Email enviado com sucesso!');
        return redirect()->route('student.index');
    }

    public function getManualJogo()
    {
        return view('reports.manual');
    }

    public function getLinkManual()
    {
        $base = '/images/manual.pdf';
        return $base;
    }
}
