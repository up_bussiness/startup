<?php

namespace LogisticsGame\Http\Controllers\Company;

use Illuminate\Http\Request;
use LogisticsGame\Http\Controllers\Controller;
use LogisticsGame\Contracts\DecisionCompanyRepository;
use LogisticsGame\Contracts\DecisionCoordinatorRepository;
use LogisticsGame\Contracts\TurnRepository;
use LogisticsGame\Contracts\BalanceRepository;
use LogisticsGame\Contracts\LimitLoanRepository;
use LogisticsGame\Contracts\MachineRepository;
use LogisticsGame\Http\Requests\DecisionCompanyRequest;
use Illuminate\Support\Facades\Gate;

class DecisionCompanyController extends Controller
{

	protected $repo, $turn, $dc, $balance, $limit, $machine;

	public function __construct(DecisionCompanyRepository $repo, TurnRepository $turn, DecisionCoordinatorRepository $dc, BalanceRepository $balance, LimitLoanRepository $limit, MachineRepository $machine)
	{
        $this->repo = $repo;
        $this->dc = $dc;
		$this->balance = $balance;
        $this->turn = $turn;
        $this->limit = $limit;
		$this->machine = $machine;
	}

    public function create($turn_id)
    {
    	$turn = $this->turn->find($turn_id);

        if (!turnIsOpen($turn->end_date)) {
            notification('Você não pode tomar uma decisão quando o turno estiver encerrado!', 'error', 'ERRO!');
            return redirect()->route('company');
        }

        $decision = $this->repo->findWhere(['user_id' => auth()->user()->id, 'turn_id' => $turn_id])->first();

        if(!is_null($decision)) {
            return redirect()->route('company.decision.edit', $decision->id);
        }

        $lastTurn = $this->turn->getLastTurn($turn->simulation_id);
        $lastDecision = $this->repo->getSalaryPrice($lastTurn->id);
        // Refactor
        $total_cash = $this->balance->findWhere(['turn_id' => $lastTurn->id, 'user_id' => auth()->user()->id], ['active_cash'])->first()->active_cash;
        // Refactor
        $limit_loan = $this->limit->findWhere(['turn_id' => $lastTurn->id, 'user_id' => auth()->user()->id], ['new_limit'])->first()->new_limit;

        $salary = $lastDecision[0];
        $price = $lastDecision[1];
        //return view('company.decision.create', compact('turn', 'salary', 'price', 'total_cash', 'limit_loan'));
    	return view('company.decision.praca', compact('turn', 'salary', 'price', 'total_cash', 'limit_loan'));
    }

    public function store(DecisionCompanyRequest $request)
    {
        $data = $request->toArray();
        $lastTurn = $this->turn->getLastTurn(auth()->user()->simulation_id);
        $limitLoan = $this->limit->findWhere(['turn_id' => $lastTurn->id, 'user_id' => auth()->user()->id], ['new_limit'])->first()->new_limit;
        $caixa = $this->balance->findWhere(['turn_id' => $lastTurn->id, 'user_id' => auth()->user()->id], ['active_cash'])->first()->active_cash;
        /**
         * COMPRA DE MAQUINA
         */
        $buy_s = $data['buy_small_machine'];
        $buy_m = $data['buy_medium_machine'];
        $buy_l = $data['buy_large_machine'];

        if ($buy_s > 0) {
            $value_s = $this->dc->findWhere(['turn_id' => $data['turn_id']], ['lp_small'])->first()->lp_small;

            if ($data['b_s_m'] >= 30) {

                $financiamentoProprio = $data['b_s_m'] / 100;

                if ($caixa < (($value_s * $financiamentoProprio) * $buy_s)) {
                    return redirect()->back()->withInput()->with('flash_type', 'alert-danger')->with('flash_message', 'Você não tem dinheiro em caixa suficiente para financiar ('.$data['b_s_m'].'%) esta quantidade de máquina pequena!');
                }

                if ($limitLoan < (($value_s * (1 - $financiamentoProprio)) * $buy_s)) {
                    return redirect()->back()->withInput()->with('flash_type', 'alert-danger')->with('flash_message', 'Você não tem limite de empréstimo suficiente para comprar esta quantidade de máquina pequena!');
                }

                $caixa -= ($value_s * $financiamentoProprio) * $buy_s;
                $limitLoan -= ($value_s * (1 - $financiamentoProprio)) * $buy_s;
            }
        }

        if ($buy_m > 0) {
            $value_m = $this->dc->findWhere(['turn_id' => $data['turn_id']], ['lp_medium'])->first()->lp_medium;

            if ($data['b_m_m'] >= 30) {

                $financiamentoProprio = $data['b_m_m'] / 100;

                if ($caixa < (($value_m * $financiamentoProprio) * $buy_m)) {
                    return redirect()->back()->withInput()->with('flash_type', 'alert-danger')->with('flash_message', 'Você não tem dinheiro em caixa suficiente para financiar ('.$data['b_m_m'].'%) esta quantidade de máquina média à vista!');
                }

                if ($limitLoan < (($value_m * (1 - $financiamentoProprio)) * $buy_m)) {
                    return redirect()->back()->withInput()->with('flash_type', 'alert-danger')->with('flash_message', 'Você não tem limite de empréstimo suficiente para comprar esta quantidade de máquina média!');
                }

                $caixa -= ($value_m * $financiamentoProprio) * $buy_m;
                $limitLoan -= ($value_m * (1 - $financiamentoProprio)) * $buy_m;
            }
        }

        if ($buy_l > 0) {
            $value_l = $this->dc->findWhere(['turn_id' => $data['turn_id']], ['lp_large'])->first()->lp_large;

            if ($data['b_l_m'] >= 30) {

                $financiamentoProprio = $data['b_l_m'] / 100;

                // Caixa tem que ter 30%
                if ($caixa < (($value_l * $financiamentoProprio) * $buy_l)) {
                    return redirect()->back()->withInput()->with('flash_type', 'alert-danger')->with('flash_message', 'Você não tem dinheiro em caixa suficiente para financiar ('.$data['b_l_m'].'%) esta quantidade de máquina grande à vista!');
                }
                // Limite tem que ter 70%
                if ($limitLoan < (($value_l * (1 - $financiamentoProprio)) * $buy_l)) {
                    return redirect()->back()->withInput()->with('flash_type', 'alert-danger')->with('flash_message', 'Você não tem limite de empréstimo suficiente para comprar esta quantidade de máquina grande!');
                }

                $caixa -= ($value_l * $financiamentoProprio) * $buy_l;
                $limitLoan -= ($value_l * (1 - $financiamentoProprio)) * $buy_l;
            }
        }

        // Verifica limite de empréstimo
        if ($data['loan'] > $limitLoan) {
            return redirect()->back()->withInput()->with('flash_type', 'alert-danger')->with('flash_message', 'Você não tem limite suficiente para pedir esse valor de empréstimo!');
        }

        /**************************************
        **
        **  VENDA DE MAQUINAS
        */

        $s = $data['sell_small_machine'];
        $m = $data['sell_medium_machine'];
        $l = $data['sell_large_machine'];

        $machines = $this->machine->findWhere(['user_id' => auth()->user()->id, 'turn_id' => $lastTurn->id, 'status' => 0]);
        // Total de maquinas
        $machines_qtd = $machines->count();
        $total_sell = $s + $m + $l;

        // Verifica se o usuário quer vender mais ou tudo que tem de máquina
        if ($total_sell >= $machines_qtd) {
            return redirect()->back()->withInput()->with('flash_type', 'alert-danger')->with('flash_message', 'Você deve manter pelo menos uma máquina em operação.');
        }

        // Se o cara pediu para vender máquina pequena
        if ($s > 0) {
            // Verifica quantas maquinas pequenas o cara tem
            $total_machine_s = $machines->where('type', 1)->count();
            if ($s > $total_machine_s) {
                return redirect()->back()->withInput()->with('flash_type', 'alert-danger')->with('flash_message', 'Você não possui esse número de máquina pequena para vender.');
            }
        }

        // Se o cara pediu para vender máquina media
        if ($m > 0) {
            // Verifica quantas maquinas media o cara tem
            $total_machine_m = $machines->where('type', 2)->count();
            if ($m > $total_machine_m) {
                return redirect()->back()->withInput()->with('flash_type', 'alert-danger')->with('flash_message', 'Você não possui esse número de máquina média para vender.');
            }
        }

        // Se o cara pediu para vender máquina grande
        if ($l > 0) {
            // Verifica quantas maquinas grande o cara tem
            $total_machine_l = $machines->where('type', 3)->count();
            if ($l > $total_machine_l) {
                return redirect()->back()->withInput()->with('flash_type', 'alert-danger')->with('flash_message', 'Você não possui esse número de máquina grande para vender.');
            }
        }

        if ($this->repo->findWhere(['user_id' => auth()->user()->id, 'turn_id' => $data['turn_id']])->count() < 1) {
    	   $decision = $this->repo->create($data);
        }

        notification('Decisão salva com sucesso!');
    	return redirect()->route('company');
    }

    public function show($id)
    {
        $decision = $this->repo->find($id);

        if (Gate::allows('is-company')) {
            if($decision->user_id != auth()->user()->id){
                notification('Você não tem permissão para acessar essa informassão!', 'error', 'ERRO!');

                return redirect()->route('company');
            }
        }        

        return view('company.decision.show', compact('decision'));
    }

    public function edit($id)
    {
        $decision = $this->repo->find($id);
        $lastTurn = $this->turn->getLastTurn($decision->turn->simulation_id);
        //$lastTurn = $decision->turn->find($decision->turn_id);
        // Refactor
        $total_cash = $this->balance->findWhere(['turn_id' => $lastTurn->id, 'user_id' => auth()->user()->id], ['active_cash'])->first()->active_cash;
        // Refactor
        $limit_loan = $this->limit->findWhere(['turn_id' => $lastTurn->id, 'user_id' => auth()->user()->id], ['new_limit'])->first()->new_limit;

        if (!turnIsOpen($decision->turn->end_date)) {
            notification('Essa rodada já foi fechada!', 'error', 'ERRO!');
            return redirect()->route('company');
        }

        //return view('company.decision.create', compact('decision', 'total_cash', 'limit_loan'));
        return view('company.decision.praca', compact('decision', 'total_cash', 'limit_loan'));
    }

    public function update(DecisionCompanyRequest $request, $id)
    {
        $data = $request->toArray();
        $lastTurn = $this->turn->getLastTurn(auth()->user()->simulation_id);
        $limitLoan = $this->limit->findWhere(['turn_id' => $lastTurn->id, 'user_id' => auth()->user()->id], ['new_limit'])->first()->new_limit;
        $caixa = $this->balance->findWhere(['turn_id' => $lastTurn->id, 'user_id' => auth()->user()->id], ['active_cash'])->first()->active_cash;
        /**
         * COMPRA DE MAQUINA
         */
        $buy_s = $data['buy_small_machine'];
        $buy_m = $data['buy_medium_machine'];
        $buy_l = $data['buy_large_machine'];

        if ($buy_s > 0) {
            $value_s = $this->dc->findWhere(['turn_id' => $data['turn_id']], ['lp_small'])->first()->lp_small;

            if ($data['b_s_m'] >= 30) {

                $financiamentoProprio = $data['b_s_m'] / 100;

                if ($caixa < (($value_s * $financiamentoProprio) * $buy_s)) {
                    return redirect()->back()->withInput()->with('flash_type', 'alert-danger')->with('flash_message', 'Você não tem dinheiro em caixa suficiente para financiar ('.$data['b_s_m'].'%) esta quantidade de máquina pequena!');
                }

                if ($limitLoan < (($value_s * (1 - $financiamentoProprio)) * $buy_s)) {
                    return redirect()->back()->withInput()->with('flash_type', 'alert-danger')->with('flash_message', 'Você não tem limite de empréstimo suficiente para comprar esta quantidade de máquina pequena!');
                }

                $caixa -= ($value_s * $financiamentoProprio) * $buy_s;
                $limitLoan -= ($value_s * (1 - $financiamentoProprio)) * $buy_s;
            }
        }

        if ($buy_m > 0) {
            $value_m = $this->dc->findWhere(['turn_id' => $data['turn_id']], ['lp_medium'])->first()->lp_medium;

            if ($data['b_m_m'] >= 30) {

                $financiamentoProprio = $data['b_m_m'] / 100;

                if ($caixa < (($value_m * $financiamentoProprio) * $buy_m)) {
                    return redirect()->back()->withInput()->with('flash_type', 'alert-danger')->with('flash_message', 'Você não tem dinheiro em caixa suficiente para financiar ('.$data['b_m_m'].'%) esta quantidade de máquina média à vista!');
                }

                if ($limitLoan < (($value_m * (1 - $financiamentoProprio)) * $buy_m)) {
                    return redirect()->back()->withInput()->with('flash_type', 'alert-danger')->with('flash_message', 'Você não tem limite de empréstimo suficiente para comprar esta quantidade de máquina média!');
                }

                $caixa -= ($value_m * $financiamentoProprio) * $buy_m;
                $limitLoan -= ($value_m * (1 - $financiamentoProprio)) * $buy_m;
            }
        }

        if ($buy_l > 0) {
            $value_l = $this->dc->findWhere(['turn_id' => $data['turn_id']], ['lp_large'])->first()->lp_large;

            if ($data['b_l_m'] >= 30) {

                $financiamentoProprio = $data['b_l_m'] / 100;
                // Caixa tem que ter 30%
                if ($caixa < (($value_l * $financiamentoProprio) * $buy_l)) {
                    return redirect()->back()->withInput()->with('flash_type', 'alert-danger')->with('flash_message', 'Você não tem dinheiro em caixa suficiente para financiar ('.$data['b_l_m'].'%) esta quantidade de máquina grande à vista!');
                }
                // Limite tem que ter 70%
                if ($limitLoan < (($value_l * (1 - $financiamentoProprio)) * $buy_l)) {
                    return redirect()->back()->withInput()->with('flash_type', 'alert-danger')->with('flash_message', 'Você não tem limite de empréstimo suficiente para comprar esta quantidade de máquina grande!');
                }

                $caixa -= ($value_l * $financiamentoProprio) * $buy_l;
                $limitLoan -= ($value_l * (1 - $financiamentoProprio)) * $buy_l;
            }
        }

        // Verifica limite de empréstimo
        if ($data['loan'] > $limitLoan) {
            return redirect()->back()->withInput()->with('flash_type', 'alert-danger')->with('flash_message', 'Você não tem limite suficiente para pedir esse valor de empréstimo!');
        }

        /**************************************
        **
        **  VENDA DE MAQUINAS
        */

        $s = $data['sell_small_machine'];
        $m = $data['sell_medium_machine'];
        $l = $data['sell_large_machine'];

        $machines = $this->machine->findWhere(['user_id' => auth()->user()->id, 'turn_id' => $lastTurn->id, 'status' => 0]);
        // Total de maquinas
        $machines_qtd = $machines->count();
        $total_sell = $s + $m + $l;

        // Verifica se o usuário quer vender mais ou tudo que tem de máquina
        if ($total_sell >= $machines_qtd) {
            return redirect()->back()->withInput()->with('flash_type', 'alert-danger')->with('flash_message', 'Você deve manter pelo menos uma máquina em operação.');
        }

        // Se o cara pediu para vender máquina pequena
        if ($s > 0) {
            // Verifica quantas maquinas pequenas o cara tem
            $total_machine_s = $machines->where('type', 1)->count();
            if ($s > $total_machine_s) {
                return redirect()->back()->withInput()->with('flash_type', 'alert-danger')->with('flash_message', 'Você não possui esse número de máquina pequena para vender.');
            }
        }

        // Se o cara pediu para vender máquina media
        if ($m > 0) {
            // Verifica quantas maquinas media o cara tem
            $total_machine_m = $machines->where('type', 2)->count();
            if ($m > $total_machine_m) {
                return redirect()->back()->withInput()->with('flash_type', 'alert-danger')->with('flash_message', 'Você não possui esse número de máquina média para vender.');
            }
        }

        // Se o cara pediu para vender máquina grande
        if ($l > 0) {
            // Verifica quantas maquinas grande o cara tem
            $total_machine_l = $machines->where('type', 3)->count();
            if ($l > $total_machine_l) {
                return redirect()->back()->withInput()->with('flash_type', 'alert-danger')->with('flash_message', 'Você não possui esse número de máquina grande para vender.');
            }
        }

        $decision = $this->repo->update($data, $id);

        notification('Decisão atualizada com sucesso!');
        return redirect()->route('company');
    }

    public function updateExtra(Request $request)
    {
        $decision = $this->repo->update($request->toArray(), $request->decision_company_id);
        notification('Valores extras adicionados com sucesso!');

        return redirect()->back();
    }

    public function default(Request $request)
    {
        $decisionHasExists = $this->repo->findWhere(['turn_id' => $request->turn_id, 'user_id' => $request->company_id])->first();

        if (!is_null($decisionHasExists)) {
            notification('Esta empresa já tomou sua decisão!');
            return redirect()->back();
        }

        $decision = $this->repo->defaultDecision($request->company_id, $request->turn_id);

        notification('Decisão padrão tomada com sucesso!');
        return redirect()->route('simulation.show', $decision->turn->simulation_id);
    }
}
