<?php

namespace LogisticsGame\Http\Controllers\Company;

use Illuminate\Http\Request;
use LogisticsGame\Models\User;
use LogisticsGame\Http\Controllers\Controller;
use LogisticsGame\Http\Controllers\ReportsController;
use LogisticsGame\Contracts\UserRepository;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\App;

class CompanyController extends Controller
{
	protected $repo;

	public function __construct(UserRepository $repo, ReportsController $reportsCtrl)
	{
		$this->repo = $repo;
		$this->ReportsController = $reportsCtrl;
	}

	public function index()
	{
		$simulation = auth()->user()->simulation;

		App::setlocale($simulation->locale);

		$company = auth()->user();

		//Verifica se a simulação já está configurada
		if ($simulation->configured==1) {
			//Vai para a página do dashboard se já estiver configurado
			return view('company.index', compact('simulation'));
		} else {	
			if ($company->configured==1) {
				//A empresda atual está configurada mas nem todas estão, então o sistema redireciona 
				//para uma página que indica o status de configuração de cada uma das empresas da simulação
				$companys = User::where('simulation_id', $simulation->id)->where('role', '=', 'company')->get();
  			return view('company.waitConfig', compact('simulation', 'companys' ));
		  } else {
				//Redireciona para iniciar a configuração pois a empresa atual não está configurada
				return redirect()->route('company.initConfig', compact('simulation'));				
			}
		}
		
	}

	public function edit()
	{
		return view('company.edit');
	}

	public function update(Request $request)
	{
		$company = $this->repo->update($request->toArray(), auth()->user()->id);


		// TODO: Refatorar
		$logo = $request->file('photo');

		if (!empty($logo)) {
			$ext = ['jpg', 'png', 'jpeg'];

			if ($logo->isValid() && in_array($logo->extension(), $ext)) {
				$image = Image::make($logo);

				$image->fit(300, 300, function ($constraint) {
					$constraint->aspectRatio();
				});
				$novoNome = auth()->user()->id . '.' . $logo->extension();
				Storage::put('/' . $logo->getClientOriginalName(), (string) $image->encode());
				$company->logo = $logo->getClientOriginalName();

				$company->save();
			}
		}

		notification('Empresa atualizada com sucesso!');

		return redirect()->route('company.edit');
	}
	
	public function initConfig (Request $request) {
		$company = $this->repo->update($request->toArray(), auth()->user()->id);
		$simulation = auth()->user()->simulation;

		return view('company.config', compact('simulation'));
	}


	public function preparationConfig(Request $request)
	{
		$company = $this->repo->update($request->toArray(), auth()->user()->id);
		$simulation = auth()->user()->simulation;

		// TODO: Refatorar
		$logo = $request->file('photo');

		if (!empty($logo)) {
			$ext = ['jpg', 'png', 'jpeg'];

			if ($logo->isValid() && in_array($logo->extension(), $ext)) {
				$image = Image::make($logo);

				$image->fit(300, 300, function ($constraint) {
					$constraint->aspectRatio();
				});
				//$file = $request->file('file');

				$novoNome = bin2hex(openssl_random_pseudo_bytes(8)) . '.' . $logo->extension();
				//Storage::put('public/companies/' . $logo->getClientOriginalName(), (string) $image->encode());
				//$company->logo = $logo->getClientOriginalName();
				$logo->storeAs('public/companies/', $novoNome);
				//Storage::put('public/companies/' . $novoNome, (string) $image->encode());
				//$company->logo = $novoNome;
				$company->logo = $novoNome;
				$company->save();
			}
		}

		//Vai para a página das compras iniciais
		//return view('company.shop', compact('simulation'));
		//Aqui vai passar a decisão iniciada
		return view('company.shop', compact('simulation','company'));
	}

	public function configCompany(Request $request) 
	{
		//dd( $request->toArray() );
		$company = $this->repo->update($request->toArray(), auth()->user()->id);
		
		$simulation = auth()->user()->simulation;
		$turn = $simulation->turns->sortBy('month')->last();
		$decision = $turn->decisionCompany->where('user_id', auth()->user()->id)->first()->update($request->toArray(), auth()->user()->toArray());


		$company->configured = 1;
		$company->save();
		notification('Empresa configurada com sucesso!');

		//Verifica se todas as empresas foram configuradas para saber se vai para a página de wait ou para a do dashboard
		$companys = User::where('simulation_id', $simulation->id)->where('role', '=', 'company')->get();
		$numComp = $simulation->number_of_company;
		$qttConf = 0;

		foreach($companys as $company) {
			if ($company->configured==1){
				$qttConf = $qttConf + 1;
			}
		}
	
		//dd($qttConf ==$numComp);

		if ($qttConf ==$numComp) {
			//Coloca a simulação como configurada
			$simulation->configured = 1;
			$simulation->save();
			//Processa a rodada
			$this->ReportsController->completeFirst($turn->id);
			
		}
		return view('company.waitConfig', compact('simulation', 'companys' ));
	}
}
