<?php

namespace LogisticsGame\Http\Controllers\Api\v1;

use Illuminate\Http\Request;
use LogisticsGame\Http\Controllers\Controller;
use LogisticsGame\Models\User;
use LogisticsGame\Models\Simulation;
use LogisticsGame\Models\Cliente;
use LogisticsGame\Tenant\ManagerTenant;

class AcessoController extends Controller
{
    private function fazDominioParaBanco($id)
    {
        $base_dominio = env('APP_URL');
        $parte1 = str_replace("http://", "", $base_dominio);
        $parte2 = str_replace("adm.", "", $parte1);
        return $id . '.' . $parte2;
    }

    public function logar(User $user, Request $request)
    {
        $manager = app(ManagerTenant::class);


        $dominio = $this->fazDominioParaBanco($request->id);
        //$dominio = $request->id . '.' . $parte2;

        $clientes = Cliente::where('domain', $dominio)->First();
        if (!$clientes) {
            return response()->json(['erro' => 'id invalido'], 404);
        }

        $manager->setConnection($clientes);
        $users = User::where('email', $request->email)->where('password_plain', $request->password)->get()->First();
        if (!$users) {
            return response()->json(['erro' => 'usuario invalido'], 404);
        }
        $nomeempresa = null;
        if ($users->company_id) {
            $useremp = User::where('id', $users->company_id)->get(['name']);
            $nomeempresa = $useremp[0]->name;
        }

        if ($request->primeiro == 0) {
            $users->token_pn = $request->token;
            $users->save();
        }
        return response()->json(['cliente' => $clientes->name, 'nome' => $users->name, 'user_id' => $users->id, 'perfil' => $users->role, 'id_simulacao' => $users->simulation_id, 'company_id' => $users->company_id, 'empresa' => $nomeempresa, 'token' => $users->token_pn]);
    }

    public function enviarpn(Request $request)
    {
        $id = $request->id;
        $email = $request->email;
        $simulation_id = $request->idSimulacao;
        $mensagem = $request->mensagem;
        $plataforma = $request->plataforma;
        //
        $manager = app(ManagerTenant::class);
        // pega o subdomio
        $dominio = $this->fazDominioParaBanco($request->id);
        // pesquisa ID cliente
        $clientes = Cliente::where('domain', $dominio)->First();
        if (!$clientes) {
            return response()->json(['erro' => 'id invalido'], 404);
        }
        //\Log::info('id:' . $request->id . ' e: ' . $request->email . ' sim:' . $request->idSimulacao . ' msg:' . $request->mensagem . ' pl:' . $request->plataforma . ' emp:' . $request->empresa_id);
        // Conecta ao BD
        $manager->setConnection($clientes);
        // Pesquisa usuario q enviou msg
        $users = User::where('email', $request->email)->get()->First();
        if (!$users) {
            return response()->json(['erro' => 'usuario invalido'], 404);
        }
        //
        //\Log::info('1xxx');
        if ($users->role == 'coordinator') { //professor enviou
            if ($request->empresa_id == '0') {
                $quipe = User::where('simulation_id', $request->idSimulacao)->where('role', '!=', 'company')->get();
            } else {
                $quipe = User::where('simulation_id', $request->idSimulacao)->where('role', '!=', 'company')->where('company_id', $request->empresa_id)->get();
            }
            //dd($quipe);
            foreach ($quipe as $eq) {
                $this->firebase_pn($eq->token_pn, $request->mensagem);
            }
        } else if ($users->role == 'aluno') { // aluno enviou
            //\Log::info('2xxx');
            if ($request->empresa_id == '0') {
                $quipe = User::where('simulation_id', $request->idSimulacao)->where('role', '=', 'aluno')->where('email', '!=', $request->email)->get();
            } else {
                $quipe = User::where('simulation_id', $request->idSimulacao)->where('role', '=', 'aluno')->where('email', '!=', $request->email)->where('company_id', $request->empresa_id)->get();
            }
            //\Log::info('3xxx');
            //dd($quipe);
            foreach ($quipe as $eq) {
                //\Log::info($eq->name);
                $this->firebase_pn($eq->token_pn, $request->mensagem);
            }
            //\Log::info('4xxx');
            $sim = Simulation::with('user')->where('id', $request->idSimulacao)->first();
            $this->firebase_pn($sim->user->token_pn, $request->mensagem);
            //\Log::info('5xxx');
        }
        //dd('ok');
        //$rpn = $this->firebase_pn($users->token_pn, $request->mensagem);
        return response()->json(['pn' => 'Ok'], 200);
    }

    private function firebase_pn($token, $msg)
    {
        $fields = array(
            'registration_ids'  => array($token),
            'priority' => 'high',
            'notification'      => array("title" => 'LG', "body" => $msg, "default_sound" => true),
        );
        //'apns' =>  array("payload" => array("sound" => 'default')),
        $headers = array(
            'Authorization: key=' . 'AAAAN14Vuus:APA91bHUtQYW79q9C41Uoro-l-Ctj_COv6i_5cQpdVp1gL7r3BruukTwM7wnKyarQU8GKYUD60ZAndOpoqHR3fxh37cy_7fzTnc96hwmJeaASBkUJVT7-HmlbNfqIhjPno8n01FAVQfG',
            'Content-Type: application/json'
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

        $result = curl_exec($ch);
        if (!$result) {
            curl_close($ch);
            return $result;
        }
        curl_close($ch);
        return $result;
    }
}
