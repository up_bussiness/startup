<?php

namespace LogisticsGame\Http\Controllers\Api\v1;

use Illuminate\Http\Request;
use LogisticsGame\Http\Controllers\Controller;
use LogisticsGame\Models\Cliente;
use LogisticsGame\Tenant\ManagerTenant;
use LogisticsGame\Models\Simulation;

class SimulacoesController extends Controller
{
    private function fazDominioParaBanco($id)
    {
        $base_dominio = env('APP_URL');
        $parte1 = str_replace("http://", "", $base_dominio);
        $parte2 = str_replace("adm.", "", $parte1);
        return $id . '.' . $parte2;
    }

    public function lista(Simulation $simulation, Request $request)
    {
        $manager = app(ManagerTenant::class);


        $dominio = $this->fazDominioParaBanco($request->id); //gera id cliente

        $clientes = Cliente::where('domain', $dominio)->First(); //pega o database
        if (!$clientes) {
            return response()->json(['erro' => 'id invalido'], 404);
        }
        if (!isset($request->perfil)) {
            return response()->json(['erro' => 'parametro invalido'], 404);
        }

        $manager->setConnection($clientes); //muda o database

        if ($request->perfil == 'coordinator') { // professor
            $simulation = Simulation::where('user_id', $request->user_id)->get(['name', 'id', 'number_of_company']);
            if (!$simulation) {
                return response()->json(['erro' => 'simulacao invalida'], 404);
            }
            return response()->json($simulation);
        } else {
            $simulation = Simulation::where('id', $request->simulation_id)->get(['name', 'id', 'number_of_company']);
            if (!$simulation) {
                return response()->json(['erro' => 'simulacao invalida'], 404);
            }
            return response()->json($simulation);
            //return response()->json(['name' => $simulation->name, 'id' => $simulation->id, 'number_of_company' => $simulation->number_of_company]);
        }
    }
}
