<?php

namespace LogisticsGame\Http\Controllers\api\v1;

use Illuminate\Http\Request;
use LogisticsGame\Http\Controllers\Controller;
use LogisticsGame\Models\User;
use LogisticsGame\Models\Cliente;
use LogisticsGame\Tenant\ManagerTenant;

class SalvarToken extends Controller
{
    private function fazDominioParaBanco($id)
    {
        $base_dominio = env('APP_URL');
        $parte1 = str_replace("http://", "", $base_dominio);
        $parte2 = str_replace("adm.", "", $parte1);
        return $id . '.' . $parte2;
    }

    public function salvartoken(User $user, Request $request)
    {
        $manager = app(ManagerTenant::class);

        if (!isset($request->user_id)) {
            return response()->json(['erro' => 'parametro invalido'], 404);
        }
        if (!isset($request->token)) {
            return response()->json(['erro' => 'parametro invalido'], 404);
        }

        $dominio = $this->fazDominioParaBanco($request->id);

        $clientes = Cliente::where('domain', $dominio)->First();
        if (!$clientes) {
            return response()->json(['erro' => 'id invalido'], 404);
        }
        $manager->setConnection($clientes);
        // pesquisa usuario
        $users = User::where('id', '=', $request->user_id)->first();
        if (!$users) {
            return response()->json(['erro' => 'usuario invalido'], 404);
        }
        // atualiza token
        $users->update(['token_pn' => $request->token,]);

        return response()->json(['ok' => 'token atualizado'], 200);
    }
}
