<?php

namespace LogisticsGame\Http\Controllers\Api\v1;

use Illuminate\Http\Request;
use LogisticsGame\Http\Controllers\Controller;
use LogisticsGame\Models\User;
use LogisticsGame\Models\Cliente;
use LogisticsGame\Tenant\ManagerTenant;

class EmpresasController extends Controller
{
    private function fazDominioParaBanco($id)
    {
        $base_dominio = env('APP_URL');
        $parte1 = str_replace("http://", "", $base_dominio);
        $parte2 = str_replace("adm.", "", $parte1);
        return $id . '.' . $parte2;
    }

    public function lista(User $user, Request $request)
    {
        $manager = app(ManagerTenant::class);

        $dominio = $this->fazDominioParaBanco($request->id); //gera id cliente
        $clientes = Cliente::where('domain', $dominio)->First(); //pega o database
        if (!$clientes) {
            return response()->json(['erro' => 'id invalido'], 404);
        }
        if (!isset($request->id_simulacao)) {
            return response()->json(['erro' => 'parametro invalido sim'], 404);
        }

        $manager->setConnection($clientes);

        if (!isset($request->company_id) || is_null($request->company_id)) {
            $users = User::where('simulation_id', $request->id_simulacao)->where('company_id', $request->company_id)->where('role', 'company')->get(['id', 'name', 'logo']);
            if (!$users) {
                return response()->json(['erro' => 'usuario invalido'], 404);
            }
        } else {
            $users = User::where('id', $request->company_id)->get(['id', 'name', 'logo']);
            if (!$users) {
                return response()->json(['erro' => 'usuario invalido'], 404);
            }
        }

        $te = [];

        array_push($te, array('id' => $request->id_simulacao, 'name' => 'Todas as Empresas', 'logo' => 'https://elasalearning.com.br/logisticsgame/grupo.png'));

        foreach ($users as $us) {
            $locallogo = 'https://elasalearning.com.br/logisticsgame/grupo.png';
            if ($us->logo != null) {
                $locallogo = env('APP_URL') . '/storage/' . $clientes->uuid . '/' . $us->logo;
            }
            array_push($te, array('id' => $us->id, 'name' => $us->name, 'logo' => $locallogo));
        }
        return response()->json($te);
    }
}
