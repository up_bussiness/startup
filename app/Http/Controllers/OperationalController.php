<?php

namespace LogisticsGame\Http\Controllers;

use LogisticsGame\Models\Operational;
use LogisticsGame\Exports\OperationalExport;
use Maatwebsite\Excel\Facades\Excel;

use Illuminate\Http\Request;

class OperationalController extends Controller
{
    public function export($turn_id, $id = null) 
    {
        return Excel::download(new OperationalExport($turn_id,$id), 'mercado.xlsx');
    }
}
