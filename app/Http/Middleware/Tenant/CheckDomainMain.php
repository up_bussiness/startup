<?php

namespace LogisticsGame\Http\Middleware\Tenant;

use Closure;

class CheckDomainMain
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        
        if (request()->getHost() != env('LOCAL_ADM')) {
            abort(401, 'Acesso não permitido!');
        }
        return $next($request);
    }
}
