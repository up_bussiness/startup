<?php

namespace LogisticsGame\Observers;

use LogisticsGame\Models\Instituition;
use LogisticsGame\Support\Traits\UploadTraitSON;

/**
 * Class SimulationObserver.
 *
 * @package namespace LogisticsGame\Observers;
 */
class InstituitionObserver
{

	use UploadTraitSON;

	protected $field = 'logo';
	protected $path = '/public/uploads';

	public function creating(Instituition $instituition)
	{
		$this->sendFile($instituition);
	}

	public function updating(Instituition $instituition)
	{
		$this->updateFile($instituition);
	}
}
