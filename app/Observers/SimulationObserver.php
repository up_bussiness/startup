<?php

namespace LogisticsGame\Observers;

use LogisticsGame\Models\Simulation;

/**
 * Class SimulationObserver.
 *
 * @package namespace LogisticsGame\Observers;
 */
class SimulationObserver
{

  public function creating(Simulation $simulation)
  {
    $simulation->user_id = auth()->user()->id;
  }

}