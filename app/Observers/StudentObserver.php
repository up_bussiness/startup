<?php

namespace LogisticsGame\Observers;

use LogisticsGame\Models\Student;

/**
 * Class StudentObserver.
 *
 * @package namespace LogisticsGame\Observers;
 */
class StudentObserver
{

  public function creating(Student $student)
  {
    $student->user_id = auth()->user()->id;
  }

}