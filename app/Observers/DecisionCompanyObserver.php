<?php

namespace LogisticsGame\Observers;

use LogisticsGame\Models\DecisionCompany;

/**
 * Class DecisionCompanyObserver.
 *
 * @package namespace LogisticsGame\Observers;
 */
class DecisionCompanyObserver
{

  public function creating(DecisionCompany $decision)
  {
    $decision->user_id = auth()->user()->id;
  }

}