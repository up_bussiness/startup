<?php

namespace LogisticsGame\Observers;

use LogisticsGame\Models\StrategicPlanning;

/**
 * Class SimulationObserver.
 *
 * @package namespace LogisticsGame\Observers;
 */
class StrategicPlanningObserver
{

  public function creating(StrategicPlanning $strategic)
  {
    $strategic->user_id = auth()->user()->id;
  }

}