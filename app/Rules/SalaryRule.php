<?php

namespace LogisticsGame\Rules;

use Illuminate\Contracts\Validation\Rule;

class SalaryRule implements Rule
{
    protected $quantity, $avg;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($quantity, $avg)
    {
        $this->quantity = $quantity;
        $this->avg = $avg;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if($value > ($this->avg * 1.5) || $value < $this->quantity)
            return false;

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Você só pode colocar um valor entre $'.numberDot($this->quantity).' e $'.numberDot($this->avg * 1.5).' para o campo <b>:attribute</b> (no máximo 50% do valor médio atual e no mínimo o valor atual).';
    }
}
