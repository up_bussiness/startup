<?php

namespace LogisticsGame\Rules;

use Illuminate\Contracts\Validation\Rule;

class EmergencyRule implements Rule
{
    protected $quantity;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($quantity)
    {
        $this->quantity = $quantity;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if($value > 0 && $value < $this->quantity)
            return false;

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'O campo :attribute deve conter um número superior ou igual a '.$this->quantity;
    }
}
