<?php

namespace LogisticsGame\Rules;

use Illuminate\Contracts\Validation\Rule;

class FiredRule implements Rule
{
    protected $limite;
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($limite)
    {
        $this->limite = $limite;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if ($value > $this->limite) {
            return false;
        }
        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Você só pode demitir ' . $this->limite . ' funcionários.';
    }
}
