<?php

namespace LogisticsGame\Rules;

use Illuminate\Contracts\Validation\Rule;
use LogisticsGame\Models\Simulation;

class CheckUserLicenseRule implements Rule
{

    public $quantity;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($quantity)
    {
        $this->quantity = $quantity;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if ($value < $this->quantity)
            return false;

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Você não pode definir o número de licenças menor que o número de licenças em uso. Se você deseja fazer isso, primeiro exclua simulações abaixo.';
    }
}
