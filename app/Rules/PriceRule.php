<?php

namespace LogisticsGame\Rules;

use Illuminate\Contracts\Validation\Rule;

class PriceRule implements Rule
{
    protected $quantity;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($quantity)
    {
        $this->quantity = $quantity;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if($value > ($this->quantity * 1.5) || $value < ($this->quantity * 0.5))
            return false;

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Você só pode colocar um valor entre $'.numberDot($this->quantity * 0.5).' e $'.numberDot($this->quantity * 1.5).' para o campo <b>:attribute</b> (50% a mais ou a menos do valor médio atual).';
    }
}
