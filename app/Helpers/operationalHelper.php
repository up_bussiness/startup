<?php

/*
*
* Estoque final (matéria prima)
* Material prima => inicial, emergencial, produzida, programada
*
*/
if (!function_exists('final_stock_rm')) {
	function final_stock_rm($initial, $emergency, $produced, $scheduled_rm)
	{
		return (($initial + $emergency) - $produced) + $scheduled_rm;
	}
}

/*
*
* Estoque final (produto)
* produto => inicial, produzida, vendido
*
*/
if (!function_exists('final_stock_product')) {
	function final_stock_product($initial, $produced, $sales)
	{
		return round(($initial + $produced) - $sales);
	}
}

/*
*
* Estoque MP
* Material prima => inicial, emergencial
*
*/
if (!function_exists('stock_rm')) {
	function stock_rm($initial, $emergency)
	{
		return $initial + $emergency;
	}
}

/*
*
* Número de empregados final
* Empregados no inicio + admitidos - demitidos
*
*/
if (!function_exists('employees')) {
	function employees($initial, $admitted, $fired)
	{
		return abs(($initial + $admitted) - $fired);
	}
}

/*
*
* Produção/homem (periodo final)
*
*/
if (!function_exists('production_man')) {
	function production_man($production, $employees)
	{
		return ($production / $employees);
	}
}

/*
*
* Produtos importados (10% da demanda)
*
*/
if (!function_exists('imported_products')) {
	function imported_products($demand, $import)
	{
		return round($demand * ($import / 100));
	}
}

/*
*
* Propaganda
*
*/
if (!function_exists('advertising')) {
	function advertising($radio, $journal, $social, $outdoor, $tv)
	{
		// Score table
		$score_radio = 3;
		$score_journal = 2;
		$score_social = 4;
		$score_outdoor = 5;
		$score_tv = 8;
		// Calc
		$total_radio = $score_radio * $radio;
		$total_journal = $score_journal * $journal;
		$total_social = $score_social * $social;
		$total_outdoor = $score_outdoor * $outdoor;
		$total_tv = $score_tv * $tv;
		// Final
		return ($total_radio + $total_journal + $total_social + $total_outdoor + $total_tv) / 110;
	}
}

if (!function_exists('advertising_avg_single')) {
	function advertising_avg_single($radio, $journal, $social, $outdoor, $tv)
	{
		return ($radio + $journal + $social + $outdoor + $tv) / 5;
	}
}

/*
*
* Rotular Propaganda
*
*/
if (!function_exists('show_advertising')) {
	function show_advertising($advertising)
	{
		switch ($advertising) {
			case $advertising == 0:
				$message =  "Zero";
				break;
			case $advertising >= 0.01 && $advertising <= 0.24:
				$message =  "Muito baixo";
				break;
			case $advertising >= 0.25 && $advertising <= 0.44:
				$message =  "Baixo";
				break;
			case $advertising >= 0.45 && $advertising <= 0.55:
				$message =  "Bom";
				break;
			case $advertising >= 0.56 && $advertising <= 0.80:
				$message =  "Muito Bom";
				break;
			case $advertising >= 0.81:
				$message =  "Excelente";
				break;
		}
		return $message;
	}
}

/* ====================================== P R O D U Ç A O ======================================= */

/*
*
* Produção
* Nível de atividade + Produção extra (%) * produtividade * capacidade produtiva
*
*/
if (!function_exists('production')) {
	function production($activity, $extra, $productivity, $ms, $mm, $ml, $employees)
	{
		// Total de funcionarios por maquina
		$s = $ms * 10;
		$m = $mm * 20;
		$l = $ml * 42;
		$total_needed = $s + $m + $l;
		$capacity = machines_production($ms, $mm, $ml);
		if ($employees > $total_needed) {
			$final_activity = $activity;
		} else {
			$final_activity = ($employees / $total_needed) * $activity;
		}
		$final_activity = $final_activity >= 100 ? 100 : $final_activity;
		// Limita a produtividade a 10%.
		$productivity = $productivity >= 1.1 ? 1.1 : $productivity;
		$level = ($final_activity + $extra) / 100;
		return round($level * $productivity * $capacity);
	}
}

/*
*
* Produção individual de MP
*
*/
if (!function_exists('mp_production')) {
	function mp_production($stock_rma, $stock_rmb, $stock_rmc, $max_production)
	{
		$possible_production = [$stock_rma, ($stock_rmb / 2), ($stock_rmc / 3)];
		$minor = min($possible_production);
		if ($minor > $max_production) {
			$production = [$max_production, $max_production * 2, $max_production * 3];
		} else {
			$production = [$minor, $minor * 2, $minor * 3];
		}
		return $production;
	}
}

/*
*
* Produtividade
*
*/
if (!function_exists('productivity')) {
	function productivity($salaryActual, $salaryAverage, $productivityActual, $salaryPast)
	{
		$factor = 0.2;
		// Teve reajuste no salário
		if ($salaryPast != $salaryActual) {
			// Salário maior/igual que a média do mercado
			if ($salaryActual > $salaryAverage) {
				$factor = 0.5;
				$percent = round(1 - ($salaryAverage / $salaryActual), 2);
				$productivity = $productivityActual + ($productivityActual * ($percent * $factor));
			}
			if ($salaryActual == $salaryAverage) {
				$percent = round(1 - ($salaryAverage / $salaryActual), 2);
				$productivity = $productivityActual + ($productivityActual * ($percent * $factor));
			}
			// Salário menor que a média do mercado
			if ($salaryActual < $salaryAverage) {
				$percentSalary = round(1 - ($salaryPast / $salaryActual), 2); // REAJUSTE SALARIO
				$percentAvg = round(($salaryActual / $salaryAverage), 2); // DIFERENCA AJUSTADO / MEDIA
				$productivity = $productivityActual + ($productivityActual * (($percentSalary * $factor) * $percentAvg));
			}
		}
		// Não reajustou/mexeu no salário
		if ($salaryPast == $salaryActual) {
			if ($salaryActual > $salaryAverage) {
				$productivity =  ($productivityActual * 1.005);
			}
			if ($salaryActual == $salaryAverage) {
				//$productivity = $productivityActual - ($productivityActual * 0.02);
				$productivity =  $productivityActual;
			}
			// Salário menor que a média do mercado
			if ($salaryActual < $salaryAverage) {
				/*$percent = round(1 - ($salaryActual / $salaryAverage), 2);
				$relation = round($percent * $factor, 2);
				$productivity = $productivityActual - ($productivityActual * $relation); */
				$productivity = $productivityActual - ($productivityActual * 0.01);
			}
		}
		return round($productivity, 2);
	}
}

/*
*
* Resto da produção individual de MP (3 mps)
*
*/
if (!function_exists('rest_production')) {
	function rest_production($stock_rma, $stock_rmb, $stock_rmc)
	{
		$possible_production = [$stock_rma, ($stock_rmb / 2), ($stock_rmc / 3)];
		$minor = min($possible_production);
		$minor_index = array_search(min($possible_production), $possible_production);
		$rest_of_rma = (($minor * 1) - $stock_rma) * -1;
		$rest_of_rmb = (($minor * 2) - $stock_rmb) * -1;
		$rest_of_rmc = (($minor * 3) - $stock_rmc) * -1;
		return [$rest_of_rma, $rest_of_rmb, $rest_of_rmc];
	}
}

/* ======================================= D E M A N D A ======================================= */

/*
*
* Capacidade produtiva total
*
*/
if (!function_exists('machines_production')) {
	function machines_production($small, $medium, $large = 0)
	{
		$machine_s = $small * 1000;
		$machine_m = $medium * 2600;
		$machine_l = $large * 6000;

		return ($machine_s + $machine_m + $machine_l);
	}
}

/*
*
* Demanda inicial (capacidade produtiva)
*
*/
if (!function_exists('first_demand')) {
	function first_demand($productive_capacity, $simulationDemand)
	{
		$demand = $productive_capacity * 1.05;

		return  $demand * (1 + $simulationDemand / 100);
	}
}

/*
 * Demanda outros turnos (anterior + macro setor)
 */
if (!function_exists('new_demand')) {
	function new_demand($past_demand, $macro_industry)
	{
		return  round($past_demand * (1 + ($macro_industry / 100)));
	}
}

/*
*
* Propaganda
*
*/
if (!function_exists('demand_advertising')) {
	function demand_advertising($advertising)
	{
		$advertising = round($advertising, 2);
		switch ($advertising) {
			case $advertising == 0:
				$score = 0;
				break;
			case $advertising >= 0.01 && $advertising <= 0.24:
				$score = 1;
				break;
			case $advertising >= 0.25 && $advertising <= 0.44:
				$score = 2;
				break;
			case $advertising >= 0.45 && $advertising <= 0.55:
				$score = 3;
				break;
			case $advertising >= 0.56 && $advertising <= 0.80:
				$score = 4;
				break;
			case $advertising >= 0.81:
				$score = 5;
				break;
		}
		return $score * 3;
	}
}

/*
*
* Preço (média / individual)
*
*/
if (!function_exists('demand_price')) {
	function demand_price($price, $average)
	{
		$value = $average / $price;
		$value = round($value, 2);
		switch ($value) {
			case $value <= 0.79:
				$score = -2;
				break;
			case $value >= 0.80 && $value <= 0.89:
				$score = -1;
				break;
			case $value >= 0.90 && $value <= 0.97:
				$score = 0;
				break;
			case $value >= 0.98 && $value <= 1.02:
				$score = 3;
				break;
			case $value >= 1.03 && $value <= 1.10:
				$score = 4;
				break;
			case $value >= 1.11 && $value <= 1.20:
				$score = 5;
				break;
			case $value >= 1.21 && $value <= 1.30:
				$score = 6;
				break;
			case $value >= 1.31 && $value <= 1.40:
				$score = 7;
				break;
			case $value >= 1.41 && $value <= 1.50:
				$score = 8;
				break;
			case $value >= 1.51 && $value <= 1.60:
				$score = 9;
				break;
			case $value >= 1.61:
				$score = 10;
				break;
		}
		return $score * 5;
	}
}

/*
*
* Prazo
*
*/
if (!function_exists('demand_term')) {
	function demand_term($term)
	{
		// a vista
		if ($term = 0)
			return 2 * 5;
		// a prazo
		return 4 * 5;
	}
}

/*
*
* Juros
*
*/
if (!function_exists('demand_interest')) {
	function demand_interest($interest)
	{
		switch ($interest) {
			case $interest == 0:
				$score = 12;
				break;
			case $interest == 1:
				$score = 9;
				break;
			case $interest == 2:
				$score = 8;
				break;
			case $interest == 3:
				$score = 7;
				break;
			case $interest == 4:
				$score = 6;
				break;
			case $interest == 5:
				$score = 5;
				break;
			case $interest == 6:
				$score = 4;
				break;
			case $interest == 7:
				$score = 3;
				break;
			case $interest == 8:
				$score = 2;
				break;
			case $interest == 9:
				$score = 1;
				break;
			case $interest >= 10:
				$score = 0;
				break;
		}
		return $score * 2;
	}
}

/*
*
* Score individual
*
*/
if (!function_exists('demand_score')) {
	function demand_score($advertising, $price, $interest, $term, $delay)
	{
		if ($delay >= 3) {
			switch ($delay) {
				case 3:
					$indicator = 0.85;
					break;
				case 4:
					$indicator = 0.75;
					break;
				case 5:
					$indicator = 0.50;
					break;
				case 6:
					$indicator = 0.25;
					break;
				default:
					$indicator = 0.10;
					break;
			}
		} else {
			$indicator = 1;
		}
		return ($advertising + $price + $interest + $term) * $indicator;
	}
}

/*
*
* Distribuição de demanda (%)
*
*/
if (!function_exists('demand_percent')) {
	function demand_percent($score, $total)
	{
		return ($score / $total);
	}
}

/* ======================================= V  E  N  D  A ======================================= */
/*
*
* Vendas
*
*/
if (!function_exists('sales')) {
	function sales($produced, $demand, $individual_produced, $individual_demand, $stock)
	{
		$total_products = $individual_produced + $stock;
		// DEMANDA GERAL FOR MAIOR QUE A PRODUÇÃO GERAL (2 CASOS)
		if ($demand > $produced) {
			// Demanda individual maior que a produção individual = vende tudo que produziu + stock
			if ($individual_demand > $total_products) {
				return $total_products;
			}
			// Demanda individual menor a quantidade produzida
			if ($individual_demand < $total_products) {
				// ANTIGO
				// $percent = round($individual_demand / $total_products, 2);
				// $indicator = demand_indicator($percent);
				// $sales = $total_products * $indicator;
				return round($individual_demand);
			}
		}
		if ($demand < $produced) {
			// Vende toda a produção e a demanda que sobra se perde
			if ($individual_demand > $total_products) {
				return $total_products;
			}
			// Vende toda a demanda e fica em estoque o resto
			if ($individual_demand < $total_products) {
				return round($individual_demand);
			}
		}
	}
}

/*
 * Indicador de demanda
 */

if (!function_exists('demand_indicator')) {
	function demand_indicator($percent_demand)
	{
		switch ($percent_demand) {
			case $percent_demand <= 1 && $percent_demand >= 0.95:
				$indicator = 1;
				break;
			case $percent_demand <= 0.94 && $percent_demand >= 0.90:
				$indicator = 0.95;
				break;
			case $percent_demand <= 0.89 && $percent_demand >= 0.85:
				$indicator = 0.90;
				break;
			case $percent_demand <= 0.84 && $percent_demand >= 0.80:
				$indicator = 0.85;
				break;
			case $percent_demand <= 0.79 && $percent_demand >= 0.75:
				$indicator = 0.80;
				break;
			case $percent_demand <= 0.74 && $percent_demand >= 0.70:
				$indicator = 0.50;
				break;
			case $percent_demand <= 0.69 && $percent_demand >= 0.60:
				$indicator = 0.20;
				break;
			case $percent_demand <= 0.59 && $percent_demand >= 0.50:
				$indicator = 0.10;
				break;
			case $percent_demand <= 0.49 && $percent_demand >= 0:
				$indicator = 0.1;
				break;
			default:
				break;
		}
		return $indicator;
	}
}
