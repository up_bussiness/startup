<?php

/* ======================================= FLUXO DE CAIXA: ENTRADA ======================================= */

// Recebimento à vista
if (!function_exists('receiptCash')) {
	function receiptCash($sales, $price, $formOfPayment)
	{
		if ($formOfPayment == 0) {
			return ($sales * $price);
		}
		return ($sales * $price) / 2;
	}
}
if (!function_exists('receiptCashJuros')) {
	function receiptCashJuros($sales, $price, $formOfPayment, $juros, $antecipacao, $taxabasica)
	{
		$vendeu = ($sales * $price);

		if ($antecipacao != 0) {
			$var1 = ($antecipacao / 100) * ($vendeu / 2);
			$var2 = ($vendeu / 2) - $var1;
			$var3 = $var2 / (1 - ($juros / 100));
			return $var3;
		} else {
			$var1 = 1 - ($juros / 100);
			$acrescidojuros = ($vendeu / 2) / $var1;
			return $acrescidojuros;
		}
	}
}
// Recebimento à prazo
if (!function_exists('receiptTerm')) {
	function receiptTerm($formOfPayment, $newReceipt, $oldReceipt)
	{
		if ($formOfPayment == 0) {
			$newReceipt = 0;
		} else {
			$newReceipt = $newReceipt / 2;
		}
		return $oldReceipt + $newReceipt;
	}
}
// Antecipação de recebíveis
if (!function_exists('anticipation_receipt')) {
	function anticipation_receipt($percent, $parcela2, $vendas, $preco, $bir)
	{
		$parcela = ($vendas * $preco) / 2;
		if ($percent == 0) {
			return 0;
		} else {
			$var1 = ($percent / 100) * $parcela;
			//return ($cash * ($percent / 100)) * (1 - ($bir + 2) / 100);
			return $var1 * (1 - (($bir + 2) / 100));
		}
	}
}
// Venda de maquinas
if (!function_exists('salesMachine')) {
	function salesMachine($value, $age)
	{
		return ($value / 120) * $age;
	}
}

// Valor contabil
if (!function_exists('machine_sell_value')) {
	function machine_sell_value($buy_value, $depreciation)
	{
		return $buy_value - $depreciation;
	}
}

/* ======================================= FLUXO DE CAIXA: SAÍDAS ======================================= */

// Folha pagto
if (!function_exists('payroll')) {
	function payroll($numberOfEmployees, $salary, $producao_extra)
	{
		$percentual = $producao_extra / 100;
		if ($producao_extra == 0) {
			return $salary * ($numberOfEmployees + 12);
		} else {
			return (((($salary * 1.5) * $percentual) + $salary) * $numberOfEmployees) + ($salary * 12);
		}
	}
}
// Treinamento
if (!function_exists('training')) {
	function training($numberOfEmployees, $salary, $training)
	{
		if ($training == 0) {
			return 0;
		}
		return ($salary * $numberOfEmployees) * ($training / 100);
	}
}
// PROPAGANDA
if (!function_exists('adv_count')) {
	function adv_count($radio, $journal, $social, $outdoor, $tv, $price_radio, $price_journal, $price_social, $price_outdoor, $price_tv)
	{
		$radio = $price_radio * $radio;
		$journal = $price_journal * $journal;
		$social = $price_social * $social;
		$outdoor = $price_outdoor * $outdoor;
		$tv = $price_tv * $tv;
		return $radio +	$journal +	$social +	$outdoor +	$tv;
	}
}
// TBJ
if (!function_exists('bir')) {
	function bir($bir, $payment)
	{
		if ($payment == 0) {
			return 0;
		}
		return $payment * ($bir / 100);
	}
}
// Manutenção das máquinas
if (!function_exists('machine_maintenance')) { //$TESTE$
	function machine_maintenance($type, $qtd, $cost, $age)
	{
		switch ($type) {
			case 1:
				$value = 0.002; // 0.0002
				break;
			case 2:
				$value = 0.0016; // 0.00016
				break;
			case 3:
				$value = 0.001; // 0.0001
				break;
			default:
				break;
		}
		return ($cost * $value * $age) * $qtd;
	}
}
if (!function_exists('machine_maintenance_total')) {
	function machine_maintenance_total($s, $m, $g)
	{
		return $s + $m + $g;
	}
}
// Aplicacao
if (!function_exists('application')) {
	function application($percent, $cash, $bir)
	{
		$applicated = ($cash * ($percent / 100));
		$term = (($bir + 1) / 100);
		return $applicated + ($applicated * $term);
	}
}
if (!function_exists('application_out')) {
	function application_out($percent, $cash)
	{
		return $cash * ($percent / 100);
	}
}
if (!function_exists('application_in')) {
	function application_in($application, $bir)
	{
		$term = (($bir + 1) / 100);
		return $application + ($application * $term);
	}
}
// Estocagem de MP
if (!function_exists('stock_cost')) {
	function stock_cost($initial_rma, $initial_rmb, $initial_rmc, $price_rma, $price_rmb, $price_rmc)
	{
		$rma = $initial_rma * $price_rma;
		$rmb = $initial_rmb * $price_rmb;
		$rmc = $initial_rmc * $price_rmc;

		return ($rma + $rmb + $rmc) * 0.05;
	}
}
// Estocagem de PA
if (!function_exists('stock_pa_cost')) {
	function stock_pa_cost($unit_cost, $stock)
	{
		return round(($unit_cost * $stock) * 0.1);
	}
}
// CUSTO UNITARIO DE MP
if (!function_exists('new_raw_cost_unit')) {
	function new_raw_cost_unit($sales, $price, $emergency)
	{
		$normal = ($sales - $emergency) * $price;
		$emergency = ($emergency * $price) * 1.3;

		return $normal + $emergency;
	}
}
// DRE
function ir_total($profit, $tax)
{
	$tax = ($tax / 100);
	return $profit * $tax;
}

function ir_percent($total, $unit)
{
	$unit = $unit * 100;
	return $unit / $total;
}
