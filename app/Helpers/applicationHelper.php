<?php

if (!function_exists('isAdmin')) {

  function isAdmin()
  {
    return auth()->user()->role == 'admin' ? true : false;
  }
}

if (!function_exists('isCoordinator')) {

  function isCoordinator()
  {
    return auth()->user()->role == 'coordinator' ? true : false;
  }
}

if (!function_exists('isCompany')) {

  function isCompany()
  {
    return auth()->user()->role == 'company' ? true : false;
  }
}

if (!function_exists('isOwner')) {

  function isOwner()
  {
    return auth()->user()->role == 'owner' ? true : false;
  }
}

if (!function_exists('layoutColor')) {

  function layoutColor()
  {
    $user = auth()->user()->role;

    if ($user == 'admin') {
      return 'skin-blue';
    }

    if ($user == 'coordinator') {
      return 'skin-yellow';
    }

    return 'skin-green';
  }
}

if (!function_exists('dateBRL')) {

  function dateBRL($value)
  {

    return date('d/m/Y', strtotime($value));
  }
}

if (!function_exists('dateTimeBRL')) {

  function dateTimeBRL($value)
  {

    return date('d/m \à\s H:i', strtotime($value));
  }
}

if (!function_exists('typeColor')) {

  function typeColor()
  {
    $user = auth()->user()->role;

    if ($user == 'admin') {
      return 'primary';
    }

    if ($user == 'coordinator') {
      return 'warning';
    }

    return 'success';
  }
}

if (!function_exists('randomDigits')) {

  function randomDigits($length)
  {
    $numbers = range(0, 9);
    shuffle($numbers);
    $digits = null;
    for ($i = 0; $i < $length; $i++)
      $digits .= $numbers[$i];
    return $digits;
  }
}

if (!function_exists('removeDot')) {

  function removeDot($value)
  {
    return str_replace('.', '', $value);
  }
}

if (!function_exists('numberDot')) {

  function numberDot($value)
  {
    return number_format(intval($value), 0, ',', '.');
  }
}


if (!function_exists('notification')) {
  function notification($message, $type = 'success', $title = 'Ação realizada!', $duration = 2000)
  {
    session()->flash('toast-type', $type);
    session()->flash('toast-title', $title);
    session()->flash('toast-message', $message);
    session()->flash('toast-duration', $duration);
  }
}

if (!function_exists('toastNotification')) {

  function toastNotification()
  {
    if (session()->get('toast-message')) {
      echo "<script type=\"text/javascript\" src=\"//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js\"></script>";
      echo "<script type=\"text/javascript\">
              $(function() {
                  toastr." . session()->get('toast-type') . "('" . session()->get('toast-message') . "', '" . session()->get('toast-title') . "', {timeOut: " . session()->get('toast-duration') . "});
              });
            </script>";
    }
  }
}

if (!function_exists('turnIsOpen')) {

  function turnIsOpen($date)
  {

    if (\Carbon\Carbon::now() > $date) {
      return false;
    }

    return true;
  }
}

if (!function_exists('turnStatusLabel')) {

  function turnStatusLabel($published, $end_date)
  {
    if (!$published) {
      echo '<span class="label label-default">NÃO PUBLICADA</span>';
    } else {
      if (!turnIsOpen($end_date)) {
        echo '<span class="label label-danger">ENCERRADA</span>';
      } else {
        echo '<span class="label label-success">ANDAMENTO</span>';
      }
    }
  }
}

if (!function_exists('actionButton')) {

  function actionButton($name, $icon, $state, $url)
  {
  }
}
if (!function_exists('precoMedioFinal')) {

  function precoMedioFinal($saldo_inicial, $compra_programada, $compra_emergencial, $ultimo_preco_medio_final, $preco_atual_da_materia_prima)
  {
    $si = $saldo_inicial * $ultimo_preco_medio_final;
    $cp = $compra_programada * $preco_atual_da_materia_prima;
    $ce = $compra_emergencial * ($preco_atual_da_materia_prima * 1.3);

    $saldo_total_producao = $saldo_inicial + $compra_programada + $compra_emergencial;
    $valor_total_producao = $si + $cp + $ce;

    /*$pa='saldo_inicial:'.$saldo_inicial.' compra_programada:'.$compra_programada.' compra_emergencial:'.$compra_emergencial.' ultimo_preco_medio_final:'.$ultimo_preco_medio_final.' preco_atual_da_materia_prima:'.$preco_atual_da_materia_prima;
$re=$valor_total_producao / $saldo_total_producao;
dump($pa,'si:'.$si, 'cp:'.$cp,'ce:'.$ce,'saldo_total_producao:'.$saldo_total_producao,'valor_total_producao:'.$valor_total_producao,$re); */

    return $valor_total_producao / $saldo_total_producao;
  }
}
if (!function_exists('ParcelaEmprestimoJuros')) {

  function ParcelaEmprestimoJuros($total, $taxa)
  {
    // parcelas 4 x
    $parcela = 0;
    if ($total != 0) {
      $taxa2 = $taxa / 100;
      $parcela = $total / (((1 + $taxa2) ** 4 - 1) / ((1 + $taxa2) ** 4 * $taxa2));
    }
    return $parcela;
  }
}
if (!function_exists('EmprestimoJurosApropriar')) {

  function EmprestimoJurosApropriar($total, $bir, $time, $parcelafixa)
  {
    $juros_apropriar = 0;
    $interacoes = 5 - $time;
    $juros_total = ((4 * $parcelafixa) - $total);
    $base = $total;
    $saldo = 0;
    for ($i = 1; $i <= $interacoes; $i++) {
      $juros = ($base * $bir) / 100;
      $amortizado = $parcelafixa - $juros;
      $base = $base - $amortizado;
      $juros_apropriar = $juros_total - $juros;
      $juros_total = $juros_apropriar;
    }

    return $juros_apropriar;
  }
}
if (!function_exists('EmprestimoJurosApropriarDif')) {

  function EmprestimoJurosApropriarDif($total, $bir, $time, $parcelafixa)
  {
    $juros_apropriar = 0;
    $interacoes = 5 - $time;
    $juros_total = ((4 * $parcelafixa) - $total);
    $base = $total;
    $juros = 0;
    for ($i = 1; $i <= $interacoes; $i++) {
      $juros = ($base * $bir) / 100;
      $amortizado = $parcelafixa - $juros;
      $base = $base - $amortizado;
    }
    $juros_apropriar = $juros;
    return $juros_apropriar;
  }
}

if (!function_exists('LocalUploadUUID')) {

  function LocalUploadUUID()
  {
    return substr(config('filesystems.disks.tenant.root'), -36);
  }
}

if (!function_exists('TextoParaQrcodeAcesso')) {

  function TextoParaQrcodeAcesso($idIES)
  {
    $posicao = strpos($idIES, '.');
    $email = auth()->user()->email;
    $senha = auth()->user()->password_plain;
    $id = substr($idIES, 0, $posicao);
    $formato = '{"id":"' . $id . '", "login":"' . $email . '","senha":"' . $senha . '"}';
    return $formato;
  }
}

if (!function_exists('TextoParaQrcodeAcessoUsuario')) {

  function TextoParaQrcodeAcessoUsuario($idIES, $emailUsuario, $senhaUsuario)
  {
    $posicao = strpos($idIES, '.');
    $email = $emailUsuario;
    $senha = $senhaUsuario;
    $id = substr($idIES, 0, $posicao);
    $formato = '{"id":"' . $id . '", "login":"' . $email . '","senha":"' . $senha . '"}';
    return $formato;
  }
}
