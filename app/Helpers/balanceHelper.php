<?php
// Depreciação de prédio
if (!function_exists('building_depreciation')) {
	function building_depreciation($building_value, $month) {
		return ($building_value / 300) * (60 + $month);
	}
}
// Depreciação maq
if (!function_exists('machine_depreciation')) {
	function machine_depreciation($buy_value, $age) {
		return ($buy_value / 120) * $age;
	}
}
// ESTOQUE VALUE
if (!function_exists('new_stock_value')) {
	function new_stock_value($price, $last_stock, $mp, $mp_emergency, $production, $lp)
{
	$scheduled_production = ($production - $mp_emergency) * $lp; //$lp = last_price
 
	$x = $scheduled_production - $last_stock;

/*$z ='price:'.$price.'-last_stock:'.$last_stock.'-mp:'.$mp.'-mp_emergency:'.$mp_emergency.'-production:'. $production.'-lp:'. $lp;	
$y = abs($x) + ($mp * $price);
dump($z,'scheduled_production:'.$scheduled_production,'x:'.$x,'return:'.$y);*/

	return abs($x) + ($mp * $price);
}
}
// Estoque UNIT PA
if (!function_exists('stock_pa')) {
	function stock_pa($price, $stc_pa) {
		return round($price * $stc_pa);
	}
}

if (!function_exists('loan_limitation')) {
	function loan_limitation($ranking, $turn_without_delay, $past_limit, $fl_delay) {
		// Atrasou (1a diminui ultimo limite para 90%, 2a vez em diante 0)
		if ($turn_without_delay == 0) {
			if ($fl_delay == 1) {
				return $past_limit * 0.9;
			}
		}
		// Ranking
		switch ($ranking) {
			case 1:
				$value_by_ranking = 12;
				break;
			case 2:
				$value_by_ranking = 9;
				break;
			case 3:
				$value_by_ranking = 8;
				break;
			case 4:
				$value_by_ranking = 6;
				break;
			case 5:
				$value_by_ranking = 4;
				break;
			case 6:
				$value_by_ranking = 2;
				break;
			case 7:
				$value_by_ranking = 0;
				break;
			case 8:
				$value_by_ranking = -2;
				break;
			case 9:
				$value_by_ranking = -4;
				break;
			case 10:
				$value_by_ranking = -6;
				break;
			default:
				# code...
				break;
		}

		// Periodo sem atraso
		switch ($turn_without_delay) {
			case 1:
				$value_by_delay = 10;
				break;
			case 2:
				$value_by_delay = 12;
				break;
			case 3:
				$value_by_delay = 15;
				break;
			case 4:
				$value_by_delay = 18;
				break;
			case 5:
				$value_by_delay = 20;
				break;
			case 6:
				$value_by_delay = 23;
				break;
			default:
				$value_by_delay = 25;
				break;
		}

		$limit = $past_limit + ($past_limit * (($value_by_delay + $value_by_ranking) / 100));

		return $limit;

	}
}