<?php

namespace LogisticsGame\Exports;

use LogisticsGame\Models\Balance;
use Maatwebsite\Excel\Concerns\FromCollection;

class BalanceExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function __construct($turn_id,$id)
    {
        $this->turn_id = $turn_id;
        $this->id = $id;
    }
    public function collection()
    {
        $data = Balance::where('turn_id',$this->turn_id)->where('user_id',$this->id)->get()->First();
        return collect([
            ['a1' => 'LOGISTICSGAME','b1' => 'BALANÇO PATRIMONIAL'],
            ['a1' => 'ATIVO','b1' => ' '],
            ['a1' => ' Caixa','b1' => $data->active_cash],
            ['a1' => ' Aplicação','b1' => $data->active_application],
            ['a1' => ' Clientes','b1' => $data->active_clients],
            ['a1' => ' Estoque produto acabado','b1' => $data->active_stock_product],
            ['a1' => ' Estoque Quadro','b1' => $data->active_raw_a],
            ['a1' => ' Estoque Kit 1','b1' => $data->active_raw_b],
            ['a1' => ' Estoque Kit 2','b1' => $data->active_raw_c],
            ['a1' => ' Máquinas','b1' => $data->active_machines],
            ['a1' => ' ( - ) Depreciação acumulada','b1' => $data->active_depreciation_machine],
            ['a1' => ' Prédios e instalações','b1' => $data->active_buildings],
            ['a1' => ' ( - ) Depreciação acumulada','b1' => $data->active_depreciation_buildings],
            ['a1' => ' Total','b1' => $data->active_total],
            ['a1' => ' PASSIVO','b1' => ' '],
            ['a1' => ' Fornecedores a vencer','b1' => $data->passive_suppliers_win],
            ['a1' => ' Fornecedores em atraso','b1' => $data->passive_suppliers_arrears],
            ['a1' => ' Contas em atraso','b1' => $data->passive_overdue_account],
            ['a1' => ' Imposto de renda a pagar','b1' => $data->passive_tax_pay],
            ['a1' => ' Empréstimos e financiamentos a vencer','b1' => $data->passive_loans_financing_mature],
            ['a1' => ' Empréstimos, financiamentos em atraso','b1' => $data->passive_loans_financing_interest_arrears],
            ['a1' => ' ( - ) Juros a Apropriar','b1' => $data->juros_apropriar],
            ['a1' => ' Capital social','b1' => $data->share_capital],
            ['a1' => ' Lucros/Prejuízos acumulados','b1' => $data->accumulated_profits],
            ['a1' => ' Total','b1' => $data->total]
        ]); 
    }
}
