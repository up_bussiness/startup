<?php

namespace LogisticsGame\Exports;

use LogisticsGame\Models\Operational;
use LogisticsGame\Models\DecisionCompany;
use LogisticsGame\Models\Dre;
use LogisticsGame\Models\Balance;
use LogisticsGame\Models\OperationalDetail;
use Maatwebsite\Excel\Concerns\FromCollection;

class OperationalExport implements FromCollection
{
    /**
     * @return \Illuminate\Support\Collection
     */

    public function __construct($turn_id, $id)
    {
        $this->turn_id = $turn_id;
        $this->id = $id;
    }
    public function collection()
    {
        $decision = DecisionCompany::with('company')->Where(['turn_id' => $this->turn_id])->get();
        $operacional = Operational::with('company')->Where(['turn_id' => $this->turn_id])->get();
        $dre = Dre::with('company')->Where(['turn_id' => $this->turn_id])->get();
        $balance = Balance::with('company')->Where(['turn_id' => $this->turn_id])->get();
        $opdetail = OperationalDetail::Where(['turn_id' => $this->turn_id])->get()->First();

        //$x = $opdetail->price_average;

        $empresas = array();
        $empresas['a0'] = ' ';
        $x = 1;
        foreach ($decision->sortBy('company.email', SORT_NATURAL) as $c) {
            $empresas['a' . $x] = $c->company->name;
            $x = $x + 1;
        }
        $precos = array();
        $precos['a0'] = 'Preço';
        $x = 1;
        foreach ($decision->sortBy('company.email', SORT_NATURAL) as $c) {
            $precos['a' . $x] = $c->price;
            $x = $x + 1;
        }
        $prazos = array();
        $prazos['a0'] = 'Prazo';
        $x = 1;
        foreach ($decision->sortBy('company.email', SORT_NATURAL) as $c) {
            $prazos['a' . $x] = $c->term == 0 ? 'À vista' : '1+1';
            $x = $x + 1;
        }
        $demanda = array();
        $demanda['a0'] = 'Demanda (un.)';
        $x = 1;
        foreach ($operacional->sortBy('company.email', SORT_NATURAL) as $c) {
            if (auth()->user()->role == 'company') {
                if ($c->user_id == auth()->user()->id) {
                    $demanda['a' . $x] = $c->demand;
                    $x = $x + 1;
                } else {
                    $demanda['a' . $x] = ' ';
                    $x = $x + 1;
                }
            }
            if (auth()->user()->role == 'coordinator') {
                $demanda['a' . $x] = $c->demand;
                $x = $x + 1;
            }
        }
        $venda = array();
        $venda['a0'] = 'Vendas (un.)';
        $x = 1;
        foreach ($operacional->sortBy('company.email', SORT_NATURAL) as $c) {
            $venda['a' . $x] = $c->sales;
            $x = $x + 1;
        }
        $participa = array();
        $participa['a0'] = 'Participação no mercado (%)';
        $x = 1;
        foreach ($operacional->sortBy('company.email', SORT_NATURAL) as $c) {
            $participa['a' . $x] = (($c->sales / $operacional->sum('sales')) * 100);
            $x = $x + 1;
        }
        $lucro = array();
        $lucro['a0'] = 'Lucro no período ($)';
        $x = 1;
        foreach ($dre->sortBy('company.email', SORT_NATURAL) as $c) {
            $lucro['a' . $x] = $c->profit_after_ir_total;
            $x = $x + 1;
        }
        $patri = array();
        $patri['a0'] = 'Patrimônio líquido ($)';
        $x = 1;
        foreach ($balance->sortBy('company.email', SORT_NATURAL) as $c) {
            $patri['a' . $x] = $c->net_worth;
            $x = $x + 1;
        }

        $data = Operational::where('turn_id', $this->turn_id)->where('user_id', $this->id)->get()->First();
        return collect([
            ['a1' => 'LOGISTICSGAME', 'b1' => 'MERCADOLÓGICO', 'c1' => ' '],
            ['a1' => 'PRODUÇÃO', 'b1' => ' ', 'c1' => ' '],
            ['a1' => 'Estoque', 'b1' => 'Quadro', 'c1' => 'Kit 1', 'd1' => 'Kit 2', 'e1' => 'Produto acabado'],
            ['a1' => 'Estoque inicial', 'b1' => $data->initial_stock_rma, 'c1' => $data->initial_stock_rmb, 'd1' => $data->initial_stock_rmc, 'e1' => $data->initial_stock_product],
            ['a1' => '(+) Compras emergenciais', 'b1' => $data->emergency_rma, 'c1' => $data->emergency_rmb, 'd1' => $data->emergency_rmc, 'e1' => '-'],
            ['a1' => '(-) Consumo ou produção', 'b1' => $data->production_rma, 'c1' => $data->production_rmb, 'd1' => $data->production_rmc, 'e1' => $data->production_product],
            ['a1' => '(-) Vendas', 'b1' => '-', 'c1' => '-', 'd1' => '-', 'e1' => $data->sales],
            ['a1' => '(+) Compras programadas', 'b1' => $data->scheduled_rma, 'c1' => $data->scheduled_rmb, 'd1' => $data->scheduled_rmc, 'e1' => '-'],
            ['a1' => 'Estoque final', 'b1' => $data->final_stock_rma, 'c1' => $data->final_stock_rmb, 'd1' => $data->final_stock_rmc, 'e1' => $data->final_stock_product],
            ['a1' => 'LINHAS DE PRODUÇÃO', 'b1' => ' ', 'c1' => ' '],
            ['a1' => 'Tipo', 'b1' => 'Quantidade', 'c1' => 'Idade Média'],
            ['a1' => 'Pequena', 'b1' => $data->machine_s_qnt, 'c1' => $data->machine_s_age],
            ['a1' => 'Média', 'b1' => $data->machine_m_qnt, 'c1' => $data->machine_m_age],
            ['a1' => 'Grande', 'b1' => $data->machine_l_qnt, 'c1' => $data->machine_l_age],
            ['a1' => 'RECURSOS HUMANOS', 'b1' => ' ', 'c1' => ' '],
            ['a1' => 'Colaboradores da produção no início do período', 'b1' => $data->employees],
            ['a1' => '(+) Admitidos', 'b1' => $data->admitted],
            ['a1' => '(-) Demitidos', 'b1' => $data->fired],
            ['a1' => '(=) Colaboradores da produção no final do período', 'b1' => $data->final_employees],
            ['a1' => 'Salário base da produção', 'b1' => $data->salary],
            ['a1' => 'Treinamento (%)', 'b1' => $data->training],
            ['a1' => 'Produtividade', 'b1' => $data->productivity],
            ['a1' => 'Produção/Homem', 'b1' => $data->production_man],
            ['a1' => 'Colaboradores administrativos', 'b1' => 2],
            ['a1' => 'Colaboradores comerciais', 'b1' => 10],
            ['a1' => 'DADOS DE MERCADO - EMPRESAS', 'b1' => ' ', 'c1' => ' '],
            $empresas,
            $precos,
            $prazos,
            $demanda,
            $venda,
            $participa,
            $lucro,
            $patri,
            ['a1' => 'Preço Médio', 'b1' => 'Propaganda Média', 'c1' => 'Demanda total (un.)', 'd1' => 'Produção total (un.)', 'e1' => 'Venda total (un.)'],
            ['a1' => $opdetail->price_average, 'b1' => $dre->avg('adv_total'), 'c1' => $opdetail->demand, 'd1' => $operacional->sum('production_product'), 'e1' => $operacional->sum('sales')]
        ]);
    }
}
