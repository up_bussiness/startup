<?php

namespace LogisticsGame\Exports;


use LogisticsGame\Models\Cashflow;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;



class CashflowExportView implements FromView
{
    public function view(): View
    {
       // $cashflow = $this->cashflow->findWhere(['turn_id' => $turn_id, 'user_id' => $id])->first();
       $turn_id = '07a9f7da-8093-11e9-a3da-d09466a24655';
       $id = '0762e520-8093-11e9-96fb-d09466a24655';
       $cashflow = Cashflow::Where(['turn_id' => $turn_id, 'user_id' => $id])->first();
       //$limit_loan = $this->loan->findWhere(['turn_id' => $turn_id, 'user_id' => $id], ['new_limit'])->first()->new_limit;
    return view('reports.cashflow', compact('cashflow'));
    }
}
