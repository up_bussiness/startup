<?php

namespace LogisticsGame\Exports;

use LogisticsGame\Models\Dre;
use Maatwebsite\Excel\Concerns\FromCollection;

class DreExport implements FromCollection
{
    /**
     * @return \Illuminate\Support\Collection
     */
    public function __construct($turn_id, $id)
    {
        $this->turn_id = $turn_id;
        $this->id = $id;
    }
    public function collection()
    {
        $data = Dre::where('turn_id', $this->turn_id)->where('user_id', $this->id)->get()->First();
        return collect([
            ['a1' => 'LOGISTICSGAME', 'b1' => 'DRE', 'c1' => ' '],
            ['a1' => 'Resultado econômico', 'b1' => 'Custo', 'c1' => 'DRE'],
            ['a1' => ' ', 'b1' => 'Unitário', 'c1' => 'Total'],
            ['a1' => 'Preço de venda / Receita total', 'b1' => $data->price, 'c1' => $data->price_total],
            ['a1' => '( - ) Custo Produto Vendido (CPV)', 'b1' => $data->cpv, 'c1' => $data->cpv_total],
            ['a1' => ' Matéria-Prima Quadro', 'b1' => $data->raw_a, 'c1' => $data->raw_a_total],
            ['a1' => ' Matéria-Prima Kit 1', 'b1' => $data->raw_b, 'c1' => $data->raw_b_total],
            ['a1' => ' Matéria-Prima Kit 2', 'b1' => $data->raw_c, 'c1' => $data->raw_c_total],
            ['a1' => ' Estocagem das matérias-primas', 'b1' => $data->stock_raw, 'c1' => $data->stock_raw_total],
            ['a1' => ' Estocagem de produtos acabados', 'b1' => $data->stock_pa, 'c1' => $data->stock_pa_total],
            ['a1' => ' Folha de pagamento da produção', 'b1' => $data->salary_production, 'c1' => $data->salary_production_total],
            ['a1' => ' Manutenção de máquinas', 'b1' => $data->machine_maintain, 'c1' => $data->machine_maintain_total],
            ['a1' => ' Treinamento da produção', 'b1' => $data->training, 'c1' => $data->training_total],
            ['a1' => ' Revalorização estoque produto acabado', 'b1' => ' ', 'c1' => $data->revalorizacao_stock],
            ['a1' => '( = ) Lucro bruto', 'b1' => $data->gross_profit, 'c1' => $data->gross_profit_total],
            ['a1' => '( - ) Total de despesas', 'b1' => $data->income_sell, 'c1' => $data->income_sell_total],
            ['a1' => ' Folha de pagamento dos vendedores', 'b1' => $data->salary_sellers, 'c1' => $data->salary_sellers_total],
            ['a1' => ' Folha de pagamento dos colaboradores administrativos', 'b1' => $data->salary_admin, 'c1' => $data->salary_admin_total],
            ['a1' => ' Propaganda', 'b1' => $data->adv, 'c1' => $data->adv_total],
            ['a1' => ' Juros na compra a prazo', 'b1' => $data->difference, 'c1' => $data->difference_total],
            ['a1' => '( = ) Lucro operacional', 'b1' => $data->operational_profit, 'c1' => $data->operational_profit_total],
            ['a1' => ' Depreciação de máquinas', 'b1' => $data->machine_depreciation, 'c1' => $data->machine_depreciation_total],
            ['a1' => ' Depreciação de prédios e instalações (100%)', 'b1' => $data->building_depreciation, 'c1' => $data->building_depreciation_total],
            ['a1' => ' Outras receitas e despesas', 'b1' => $data->others_value, 'c1' => $data->others_value_total],
            ['a1' => 'Lucro do período antes IR', 'b1' => $data->profit_before_ir, 'c1' => $data->profit_before_ir_total],
            ['a1' => '( - ) Provisão para o IR', 'b1' => $data->ir, 'c1' => $data->ir_total],
            ['a1' => '( - ) Prejuízo na venda de máquinas', 'b1' => ' ', 'c1' => $data->loss_on_sale],
            ['a1' => 'Lucro do período após IR', 'b1' => $data->profit_after_ir, 'c1' => $data->profit_after_ir_total],
            ['a1' => 'Margem de lucro (%)', 'b1' => $data->profit_margin, 'c1' => $data->profit_margin]
        ]);
    }
}
