<?php

namespace LogisticsGame\Exports;

use LogisticsGame\Models\Cashflow;
use Maatwebsite\Excel\Concerns\FromCollection;
use Excel;

class CashflowExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function __construct($turn_id,$id)
    {
        $this->turn_id = $turn_id;
        $this->id = $id;
    }
    public function collection()
    {
        //return Cashflow::all();
       // return Cashflow::where('turn_id',$this->turn_id)->where('user_id',$this->id)->get();
        //$data = Cashflow::get()->toArray();
        //$type = 'xlsx';
        //$data = Cashflow::where('turn_id',$this->turn_id)->get()->toArray();

      //  $data = Cashflow::where('turn_id',$this->turn_id)->get()->First();
        $data = Cashflow::where('turn_id',$this->turn_id)->where('user_id',$this->id)->get()->First();

        //return Excel::download($data,'mySheet',$type);
       /* return Excel::download('laravelcode', function($excel) use ($data) {
            $excel->sheet('mySheet', function($sheet) use ($data)
            {
                $sheet->cell('A1', function($cell) {$cell->setValue('First Name');   });
                $sheet->cell('B1', function($cell) {$cell->setValue('Last Name');   });
                $sheet->cell('C1', function($cell) {$cell->setValue('Email');   });
            });
        })->download($type);*/
        //return Cashflow::where('turn_id',$this->turn_id)->get();

        return collect([
            ['a1' => 'LOGISTICSGAME','b1' => 'Fluxo de Caixa'],
            ['a1' => 'Entradas','b1' => ' '],
            ['a1' => ' Saldo inicial do período','b1' => $data->opening_balance],
            ['a1' => ' Recebimento à vista','b1' => $data->receiving_sight],
            ['a1' => ' Recebimento a prazo','b1' => $data->time_receipt],
            ['a1' => ' Recebíveis antecipados','b1' => $data->anticipated_receivables],
            ['a1' => ' Resgate da aplicação','b1' => $data->application_redemption],
            ['a1' => ' Venda de máquinas','b1' => $data->sale_of_machines],
            ['a1' => ' Receitas diversas','b1' => $data->miscellaneous_income],
            ['a1' => ' Financiamento de máquinas','b1' => $data->machine_financing],
            ['a1' => ' Empréstimo programado','b1' => $data->scheduled_loan],
            ['a1' => ' ( + ) Total','b1' => $data->entry],
            ['a1' => 'SAÍDAS','b1' => ' '],
            ['a1' => ' Folha de pagamento','b1' => $data->payroll],
            ['a1' => ' Treinamento','b1' => $data->training],
            ['a1' => ' Propaganda','b1' => $data->adv],
            ['a1' => ' Despesas diversas','b1' => $data->expense],
            ['a1' => ' Gastos com estocagem','b1' => $data->stocking_costs],
            ['a1' => ' Atrasos Gerais','b1' => $data->general_delay],
            ['a1' => ' Pagamento a fornecedores','b1' => $data->payment_suppliers],
            ['a1' => ' Compra de máquinas','b1' => $data->purchase_machines],
            ['a1' => ' Manutenção de máquinas','b1' => $data->machine_maintenance],
            ['a1' => ' Amortização de empréstimos e financiamentos','b1' => $data->amortization_loans],
            ['a1' => ' Juros bancários','b1' => $data->bir],
            ['a1' => ' Imposto de renda','b1' => $data->income_tax],
            ['a1' => ' Aplicação','b1' => $data->application],
            ['a1' => '( - ) Total','b1' => $data->exit],
            ['a1' => 'Saldo inicial do período','b1' => $data->opening_balance],
            ['a1' => 'Saldo final do período','b1' => $data->final_balance]
        ]); 
    }
}
