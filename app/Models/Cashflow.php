<?php

namespace LogisticsGame\Models;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use LogisticsGame\Support\Traits\Uuids;

/**
 * Class Cashflow.
 *
 * @package namespace LogisticsGame\Models;
 */
class Cashflow extends Model implements Transformable
{
	use TransformableTrait, Uuids;

	public $incrementing = false;

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'turn_id',
		'user_id',
		'opening_balance',
		'receiving_sight',
		'time_receipt',
		'anticipated_receivables',
		'application_redemption',
		'sale_of_machines',
		'miscellaneous_income',
		'machine_financing',
		'scheduled_loan',
		'receiving_next_turn',
		'entry',
		'payroll',
		'training',
		'adv',
		'expense',
		'stocking_costs',
		'payment_suppliers',
		'purchase_machines',
		'machine_maintenance',
		'amortization_loans',
		'loan_value',
		'general_delay',
		'bir',
		'income_tax',
		'application',
		'exit',
		'loan_limit',
		'payment_next_turn',
		'sell_machine_next_turn',
		'next_tax_pay',
		'final_balance',
		'cpv',
		'stock_mp',
		'stock_pa',
		'tmp_machine_small',
		'tmp_machine_medium',
		'tmp_machine_large',
		'tmp_clients',
		'tmp_payment_suppliers',
		'tmp_sell_machine_values',
		'tmp_tax',
		'tmp_stock_a',
		'tmp_stock_b',
		'tmp_stock_c',
		'tmp_stock_product',
		'tmp_delay',
		'tmp_delay_bank',
		'tmp_delay_suppliers',
		'tmp_loan_to_due',
		'tmp_stock_dre_product',
		'tmp_stock_dre_raw',
		'tmp_bp_last_profit',
		'tmp_machine_maintenance',
		'tmp_training',
		'tmp_adv',
		'tmp_sell_machine_loss',
		'loss_of_machines',
		'tmp_bir',
		'tmp_bonus',
		'tmp_despesa_mp'
	];

	public function turn()
	{
		return $this->belongsTo(Turn::class);
	}

	public function company()
	{
		return $this->belongsTo(User::class, 'user_id');
	}
}
