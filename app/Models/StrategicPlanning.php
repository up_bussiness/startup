<?php

namespace LogisticsGame\Models;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use LogisticsGame\Support\Traits\Uuids;

/**
 * Class StrategicPlanning.
 *
 * @package namespace LogisticsGame\Models;
 */
class StrategicPlanning extends Model implements Transformable
{
    use TransformableTrait, Uuids;

public $incrementing = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
    	'slogan',
    	'mission',
    	'view',
    	'values',
    	'weaknesses',
    	'threats',
    	'forces',
    	'opportunities',
        'goals',
        'goals_general',
    	'goals_specific'
    ];

    public function user()
    {
    	return $this->belongsTo(User::class);
    }

}
