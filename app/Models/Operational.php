<?php

namespace LogisticsGame\Models;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use LogisticsGame\Support\Traits\Uuids;

/**
 * Class Operational.
 *
 * @package namespace LogisticsGame\Models;
 */
class Operational extends Model implements Transformable
{
	use TransformableTrait, Uuids;

	public $incrementing = false;

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'turn_id',
		'user_id',
		'initial_stock_rma',
		'initial_stock_rmb',
		'initial_stock_rmc',
		'initial_stock_product',
		'production_rma',
		'production_rmb',
		'production_rmc',
		'production_product',
		'final_stock_rma',
		'final_stock_rmb',
		'final_stock_rmc',
		'final_stock_product',
		'machine_s_qnt',
		'machine_s_age',
		'machine_m_qnt',
		'machine_m_age',
		'machine_l_qnt',
		'machine_l_age',
		'employees',
		'final_employees',
		'productivity',
		'production_man',
		'advertising',
		'advertising_avg',
		'salary',
		'demand',
		'sales',
		'tmp_buy_machine_cash',
		'tmp_buy_machine_financing',
		'demand_entra',
		'venda_extra',
		'venda_total'
	];

	public function turn()
	{
		return $this->belongsTo(Turn::class);
	}

	public function company()
	{
		return $this->belongsTo(User::class, 'user_id');
	}
}
