<?php

namespace LogisticsGame\Models;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class Newspaper.
 *
 * @package namespace LogisticsGame\Models;
 */
class Newspaper extends Model implements Transformable
{
    use TransformableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    	'turn_id', 'theme_id', 'title', 'description'
    ];

    public function theme()
    {
        return $this->belongsTo(Theme::class);
    }

    public function turn()
    {
        return $this->belongsTo(Turn::class);
    }

}
