<?php

namespace LogisticsGame\Models;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use LogisticsGame\Support\Traits\Uuids;

/**
 * Class DecisionCoordinator.
 *
 * @package namespace LogisticsGame\Models;
 */
class DecisionCoordinator extends Model implements Transformable
{
    use TransformableTrait, Uuids;

    public $incrementing = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    	'turn_id',
    	'macro_industry',
    	'inflation',
    	'loss_on_sale',
    	'bir',
    	'interest_providers',
    	'import',
    	'income_tax',
        'raw_material_a',
        'raw_material_b',
        'raw_material_c',
        'lp_small',
        'lp_medium',
        'lp_large',
        'adv_radio',
        'adv_journal',
        'adv_social',
        'adv_outdoor',
        'adv_tv'
    ];

    public function turn()
    {
    	return $this->belongsTo(Turn::class);
    }

}
