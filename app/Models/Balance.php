<?php

namespace LogisticsGame\Models;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use LogisticsGame\Support\Traits\Uuids;

/**
 * Class Balance.
 *
 * @package namespace LogisticsGame\Models;
 */
class Balance extends Model implements Transformable
{
	use TransformableTrait, Uuids;

	public $incrementing = false;

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'turn_id',
		'user_id',
		'active_cash',
		'active_application',
		'active_clients',
		'active_stock_product',
		'active_raw_a',
		'active_raw_b',
		'active_raw_c',
		'active_machines',
		'active_depreciation_machine',
		'active_buildings',
		'active_depreciation_buildings',
		'active_total',
		'passive_suppliers_win',
		'passive_suppliers_arrears',
		'passive_overdue_account',
		'passive_tax_pay',
		'passive_loans_financing_mature',
		'juros_apropriar',
		'passive_loans_financing_interest_arrears',
		'net_worth',
		'share_capital',
		'accumulated_profits',
		'accumulated_profits_past',
		'total',
		'passive_suppliers_bir'
	];

	public function turn()
	{
		return $this->belongsTo(Turn::class);
	}

	public function company()
	{
		return $this->belongsTo(User::class, 'user_id');
	}
}
