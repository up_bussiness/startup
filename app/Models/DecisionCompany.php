<?php

namespace LogisticsGame\Models;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use LogisticsGame\Support\Traits\Uuids;

/**
 * Class DecisionCompany.
 *
 * @package namespace LogisticsGame\Models;
 */
class DecisionCompany extends Model implements Transformable
{
    use TransformableTrait, Uuids;

    public $incrementing = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    	'id', 'turn_id', 'user_id', 'price', 'term', 'adv_radio', 'adv_journal', 'adv_social', 'adv_outdoor', 'adv_tv', 'scheduled_rma', 'scheduled_rmb', 'scheduled_rmc', 'emergency_rma', 'emergency_rmb', 'emergency_rmc', 'payment_rm', 'activity_level', 'extra_production', 'buy_small_machine', 'sell_small_machine', 'buy_medium_machine', 'sell_medium_machine', 'buy_large_machine', 'sell_large_machine', 'admitted', 'fired', 'salary', 'training', 'loan', 'anticipation_of_receivables', 'application', 'term_interest', 'income', 'expense',
        'b_m_m', 'b_s_m', 'b_l_m'
    ];

    public function turn()
	{
	    return $this->belongsTo(Turn::class);
	}

	public function company()
	{
	    return $this->belongsTo(User::class, 'user_id');
	}

}
