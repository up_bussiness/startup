<?php

namespace LogisticsGame\Models;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use LogisticsGame\Support\Traits\Uuids;

/**
 * Class Instituition.
 *
 * @package namespace LogisticsGame\Models;
 */
class Instituition extends Model implements Transformable
{
    use TransformableTrait, Uuids;

    public $incrementing = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    	'name',
    	'logo',
    	'slug',
    	'license',
    	'user_id'
    ];

    public function user()
    {
    	return $this->belongsTo(User::class);
    }

}
