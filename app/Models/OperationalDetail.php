<?php

namespace LogisticsGame\Models;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use LogisticsGame\Support\Traits\Uuids;

/**
 * Class OperationalDetail.
 *
 * @package namespace LogisticsGame\Models;
 */
class OperationalDetail extends Model implements Transformable
{
    use TransformableTrait, Uuids;

    public $incrementing = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    	'turn_id',
    	'import_products',
    	'interest_average',
    	'salary_average',
    	'demand',
    	'price_average',
    	'productivity_average',
    ];

}
