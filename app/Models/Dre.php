<?php

namespace LogisticsGame\Models;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use LogisticsGame\Support\Traits\Uuids;

/**
 * Class Dre.
 *
 * @package namespace LogisticsGame\Models;
 */
class Dre extends Model implements Transformable
{
    use TransformableTrait, Uuids;

    public $incrementing = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    		'turn_id',
    		'user_id',
    	  'price',
    		'price_total',
    		'cpv',
    		'cpv_total',
    		'difference',
    		'difference_total',
    		'raw_a',
    		'raw_a_total',
    		'raw_b',
    		'raw_b_total',
    		'raw_c',
    		'raw_c_total',
    		'machine_maintain',
    		'machine_maintain_total',
    		'salary_production',
    		'salary_production_total',
    		'training',
    		'training_total',
    		'stock_raw',
    		'stock_raw_total',
    		'machine_depreciation',
    		'machine_depreciation_total',
    		'building_depreciation',
    		'building_depreciation_total',
    		'gross_profit',
    		'gross_profit_total',

    		'income_sell',
    		'income_sell_total',
    		'adv',
    		'adv_total',
    		'salary_sellers',
    		'salary_sellers_total',
    		'stock_pa',
    		'stock_pa_total',

    		'admin_sell',
    		'admin_sell_total',
    		'salary_admin',
    		'salary_admin_total',

    		'operational_profit',
    		'operational_profit_total',
    		'others_value',
    		'others_value_total',
    		'profit_before_ir',
    		'profit_before_ir_total',
    		'ir',
    		'ir_total',
    		'profit_after_ir',
    		'profit_after_ir_total',
    		'profit_margin',
    		'profit_margin_total',
			'loss_on_sale',
			'price_medium_a',
			'price_medium_b',
			'price_medium_c',
			'revalorizacao_stock'
    ];

    public function turn()
    {
        return $this->belongsTo(Turn::class);
    }

    public function company()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
