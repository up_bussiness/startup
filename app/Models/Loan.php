<?php

namespace LogisticsGame\Models;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class Loan.
 *
 * @package namespace LogisticsGame\Models;
 */
class Loan extends Model implements Transformable
{
    use TransformableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    	'user_id',
    	'value',
        'time',
        'bir'
    ];

    public function company()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

}
