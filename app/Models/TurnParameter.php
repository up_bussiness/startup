<?php
//#AUTO - Este arquivo foi alterado por conta da implementação da automatização do processo de simulação
namespace LogisticsGame\Models;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class TurnParameter.
 *
 * @package namespace LogisticsGame\Models;
 */
class TurnParameter extends Model implements Transformable
{
    use TransformableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];

    public function turnParameter()
	{
	    return $this->belongsTo(TurnParameter::class);
	}

}
