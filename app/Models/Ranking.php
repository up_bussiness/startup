<?php

namespace LogisticsGame\Models;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class Ranking.
 *
 * @package namespace LogisticsGame\Models;
 */
class Ranking extends Model implements Transformable
{
    use TransformableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    	'user_id',
    	'turn_id',
		'net_worth',
		'market_share',
		'loans_financing'
    ];

    public function company()
	{
	    return $this->belongsTo(User::class, 'user_id');
	}

	public function turn()
	{
	    return $this->belongsTo(Turn::class);
	}

}
