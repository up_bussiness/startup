<?php

namespace LogisticsGame\Models;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class LimitLoan.
 *
 * @package namespace LogisticsGame\Models;
 */
class LimitLoan extends Model implements Transformable
{
    use TransformableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    		'turn_id',
    	    'user_id',
    		'turn_without_delay',
    		'limit',
    		'loan',
    		'amortization',
    		'new_limit',
    		'fl_delay'
    ];

    public function company()
	{
	    return $this->belongsTo(User::class, 'user_id');
	}

	public function turn()
	{
	    return $this->belongsTo(Turn::class);
	}

}
