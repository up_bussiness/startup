<?php

namespace LogisticsGame\Models;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use LogisticsGame\Support\Traits\Uuids;

/**
 * Class Turn.
 *
 * @package namespace LogisticsGame\Models;
 */
class Turn extends Model implements Transformable
{
    use TransformableTrait, Uuids;

public $incrementing = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'simulation_id',
        'month',
        'end_date',
        'published'
    ];

    public function simulation()
    {
    	return $this->belongsTo(Simulation::class);
    }

    public function decisionCoordinator()
    {
        return $this->hasOne(DecisionCoordinator::class);
    }

    public function newspaper()
    {
      return $this->hasMany(Newspaper::class);
    }

    public function decisionCompany()
    {
        return $this->hasMany(DecisionCompany::class);
    }

    public function operational()
    {
        return $this->hasOne(Operational::class);
    }

    public function operationalDetail()
    {
        return $this->hasOne(OperationalDetail::class);
    }

    public function balance()
    {
        return $this->hasOne(Balance::class);
    }

    public function dre()
    {
        return $this->hasOne(Dre::class);
    }

    public function cashflow()
    {
        return $this->hasOne(Cashflow::class);
    }

    public function ranking()
    {
        return $this->hasOne(Ranking::class);
    }

}
