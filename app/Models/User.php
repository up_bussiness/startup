<?php

namespace LogisticsGame\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use LogisticsGame\Support\Traits\Uuids;
use LogisticsGame\Notifications\ResetPassword;


class User extends Authenticatable implements Transformable
{
    use Notifiable, TransformableTrait, Uuids;

    public $incrementing = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'password_plain', 'role', 'simulation_id', 'logo', 'company_id', 'occupation_id', 'license', 'token_pn', 'configured', 'product_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function students()
    {
        return $this->hasMany(Student::class);
    }

    public function institution()
    {
        return $this->hasOne(Institution::class);
    }

    public function simulation()
    {
        return $this->belongsTo(Simulation::class);
    }

    public function simulations()
    {
        return $this->hasMany(Simulation::class);
    }

    public function strategicPlanning()
    {
        return $this->hasOne(StrategicPlanning::class);
    }

    public function decisions()
    {
        return $this->hasMany(DecisionCompany::class);
    }

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPassword($token));
    }

}
