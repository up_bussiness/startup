<?php

namespace LogisticsGame\Models;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class Machine.
 *
 * @package namespace LogisticsGame\Models;
 */
class Machine extends Model implements Transformable
{
    use TransformableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    	'user_id',
    	'turn_id',
    	'type',
    	'age',
    	'buy_value',
    	'depreciation',
    	'status'
    ];

    public function company()
	{
	    return $this->belongsTo(User::class, 'user_id');
	}

}
