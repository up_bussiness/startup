<?php

namespace LogisticsGame\Models;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use LogisticsGame\Support\Traits\Uuids;

/**
 * Class Student.
 *
 * @package namespace LogisticsGame\Models;
 */
class Student extends Model implements Transformable
{
    use TransformableTrait, Uuids;

    public $incrementing = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'name',
        'email',
        'phone',
        'face',
        'insta',
        'date',
        'registration',
        'occupation_id',
    ];

    public function company()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function occupation()
    {
        return $this->belongsTo(occupation::class);
    }
}
