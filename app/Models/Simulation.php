<?php

namespace LogisticsGame\Models;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use LogisticsGame\Support\Traits\Uuids;

/**
 * Class Simulation.
 *
 * @package namespace LogisticsGame\Models;
 */
class Simulation extends Model implements Transformable
{
    use TransformableTrait, Uuids;

    public $incrementing = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'initial_date',
        'end_date',
        'number_of_company',
        'demand',
        'user_id',
        'planning',
        'reseted',
        'in_operational',
        'in_cashflow',
        'in_dre',
        'in_balance',
        'in_ranking',
        'in_transport',
        'locale'
    ];

    public function turns()
    {
        return $this->hasMany(Turn::class);
    }

    public function companies()
    {
        //return $this->hasMany(User::class, 'simulation_id');
        return $this->hasMany(User::class, 'simulation_id')->where('role', 'company');
        
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
