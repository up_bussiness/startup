/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 12);
/******/ })
/************************************************************************/
/******/ ({

/***/ 12:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(13);


/***/ }),

/***/ 13:
/***/ (function(module, exports) {

$(function () {
  $('#form-newspaper').submit(function (e) {
    e.preventDefault();
    var turn = $('input[name="turn_id"]').val();
    var theme = $('#theme_id').val();
    var title = $('#title').val();
    var description = $('#description').val();
    var CSRF_TOKEN = $('input[name="_token"]').val();
    $.ajax({
      url: 'store',
      type: 'POST',
      data: { _token: CSRF_TOKEN, turn: turn, theme: theme, title: title, description: description },
      dataType: 'JSON',
      success: function success(data) {
        $('#table-newspaper > tbody').empty();
        $.each(data, function (i, item) {
          var deleteURI = "";
          $('#table-newspaper > tbody').append('<tr><td>' + item.theme.name + '</td><td class="table-collapse">' + item.title + '</td><td>' + item.description + '</td><td><a href="' + item.id + '/destroy" class="btn btn-sm btn-danger" onclick="return confirm(\'Tem certeza que deseja remover essa matéria?\')" ><span class="glyphicon glyphicon-remove-circle"></span></td></tr>');
        });
        $('#title').val('');
        $('#description').val('');
        $('#newspaper').val(null);
        $('#modal-newspaper').modal('toggle');
      },
      fail: function fail(jqXHR, txt) {
        console.log(txt);
      }
    });
  });

  $('#theme_id').change(function () {
    getMatters($(this).val());
  });

  $('#newspaper').change(function () {
    getContent($(this).val());
  });

  function getMatters(theme) {

    init_preloader();

    $.get('api/matters/' + theme, function (data) {
      $('#title').val('');
      $('#description').val('');
      $('#newspaper').find('option').remove().end();
      $('#newspaper').append($('<option>', {
        value: null,
        text: '-'
      }));
      $.each(data, function (i, item) {
        $('#newspaper').append($('<option>', {
          value: item.id,
          text: item.title
        }));
      });

      stop_preloader();
    });
  }

  function getContent(newspaper) {

    init_preloader();

    $.get('api/matters/content/' + newspaper, function (data) {
      $('#title').val(data.title);
      $('#description').val(data.description);

      stop_preloader();
    });
  }
});

/***/ })

/******/ });