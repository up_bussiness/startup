/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 10);
/******/ })
/************************************************************************/
/******/ ({

/***/ 10:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(11);


/***/ }),

/***/ 11:
/***/ (function(module, exports) {

$(function () {
  $('#macro_industry').maskMoney({ decimal: '', allowNegative: true, precision: '0' });
  $('#inflation').maskMoney({ decimal: '', allowNegative: true, precision: '0' });
  $('#loss_on_sale').maskMoney({ decimal: '', allowNegative: true, precision: '0' });
  $('#bir').maskMoney({ decimal: '', allowNegative: true, precision: '0' });
  $('#interest_providers').maskMoney({ decimal: '', allowNegative: true, precision: '0' });
  $('#import').maskMoney({ decimal: '', allowNegative: true, precision: '0' });
  $('#income_tax').maskMoney({ decimal: '', allowNegative: true, precision: '0' });
  $('#raw_material_a').maskMoney({ thousands: '.', decimal: '', precision: '0' });
  $('#raw_material_b').maskMoney({ thousands: '.', decimal: '', precision: '0' }).maskMoney('mask');
  $('#raw_material_c').maskMoney({ thousands: '.', decimal: '', precision: '0' }).maskMoney('mask');
  $('#lp_small').maskMoney({ thousands: '.', decimal: '', precision: '0' }).maskMoney('mask');
  $('#lp_medium').maskMoney({ thousands: '.', decimal: '', precision: '0' }).maskMoney('mask');
  $('#lp_large').maskMoney({ thousands: '.', decimal: '', precision: '0' }).maskMoney('mask');
  $('#adv_radio').maskMoney({ thousands: '.', decimal: '', precision: '0' }).maskMoney('mask');
  $('#adv_journal').maskMoney({ thousands: '.', decimal: '', precision: '0' }).maskMoney('mask');
  $('#adv_social').maskMoney({ thousands: '.', decimal: '', precision: '0' }).maskMoney('mask');
  $('#adv_outdoor').maskMoney({ thousands: '.', decimal: '', precision: '0' }).maskMoney('mask');
  $('#adv_tv').maskMoney({ thousands: '.', decimal: '', precision: '0' }).maskMoney('mask');
  /*students*/
  $('#phone').mask("(99) 99999-9999");

  /*Decision*/
  $('#price').maskMoney({ thousands: '.', decimal: '', allowEmpty: true, allowZero: true, precision: '0' }).maskMoney('mask');
  $('#emergency_rma').maskMoney({ thousands: '.', decimal: '', allowEmpty: true, allowZero: true, precision: '0' }).maskMoney('mask');
  $('#emergency_rmb').maskMoney({ thousands: '.', decimal: '', allowEmpty: true, allowZero: true, precision: '0' }).maskMoney('mask');
  $('#emergency_rmc').maskMoney({ thousands: '.', decimal: '', allowEmpty: true, allowZero: true, precision: '0' }).maskMoney('mask');
  $('#salary').maskMoney({ thousands: '.', decimal: '', allowEmpty: true, allowZero: true, precision: '0' }).maskMoney('mask');
  $('#scheduled_rma').maskMoney({ thousands: '.', decimal: '', allowEmpty: true, allowZero: true, precision: '0' }).maskMoney('mask');
  $('#scheduled_rmb').maskMoney({ thousands: '.', decimal: '', allowEmpty: true, allowZero: true, precision: '0' }).maskMoney('mask');
  $('#scheduled_rmc').maskMoney({ thousands: '.', decimal: '', allowEmpty: true, allowZero: true, precision: '0' }).maskMoney('mask');
  $('#loan').maskMoney({ thousands: '.', decimal: '', allowEmpty: true, allowZero: true, precision: '0' }).maskMoney('mask');
});

/***/ })

/******/ });