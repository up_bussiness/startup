$(function() {
  $('#macro_industry').maskMoney({decimal:'', allowNegative: true, precision:'0'});
  $('#inflation').maskMoney({decimal:'', allowNegative: true, precision:'0'});
  $('#loss_on_sale').maskMoney({decimal:'', allowNegative: true, precision:'0'});
  $('#bir').maskMoney({decimal:'', allowNegative: true, precision:'0'});
  $('#interest_providers').maskMoney({decimal:'', allowNegative: true, precision:'0'});
  $('#import').maskMoney({decimal:'', allowNegative: true, precision:'0'});
  $('#income_tax').maskMoney({decimal:'', allowNegative: true, precision:'0'});
  $('#raw_material_a').maskMoney({thousands:'.', decimal:'', precision:'0'});
  $('#raw_material_b').maskMoney({thousands:'.', decimal:'', precision:'0'}).maskMoney('mask');
  $('#raw_material_c').maskMoney({thousands:'.', decimal:'', precision:'0'}).maskMoney('mask');
  $('#lp_small').maskMoney({thousands:'.', decimal:'', precision:'0'}).maskMoney('mask');
  $('#lp_medium').maskMoney({thousands:'.', decimal:'', precision:'0'}).maskMoney('mask');
  $('#lp_large').maskMoney({thousands:'.', decimal:'', precision:'0'}).maskMoney('mask');
  $('#adv_radio').maskMoney({thousands:'.', decimal:'', precision:'0'}).maskMoney('mask');
  $('#adv_journal').maskMoney({thousands:'.', decimal:'', precision:'0'}).maskMoney('mask');
  $('#adv_social').maskMoney({thousands:'.', decimal:'', precision:'0'}).maskMoney('mask');
  $('#adv_outdoor').maskMoney({thousands:'.', decimal:'', precision:'0'}).maskMoney('mask');
  $('#adv_tv').maskMoney({thousands:'.', decimal:'', precision:'0'}).maskMoney('mask');
  /*students*/
  $('#phone').mask("(99) 99999-9999");

  /*Decision*/
  $('#price').maskMoney({thousands:'.', decimal:'', allowEmpty: true, allowZero: true, precision:'0'}).maskMoney('mask');
  $('#emergency_rma').maskMoney({thousands:'.', decimal:'', allowEmpty: true, allowZero: true, precision:'0'}).maskMoney('mask');
  $('#emergency_rmb').maskMoney({thousands:'.', decimal:'', allowEmpty: true, allowZero: true, precision:'0'}).maskMoney('mask');
  $('#emergency_rmc').maskMoney({thousands:'.', decimal:'', allowEmpty: true, allowZero: true, precision:'0'}).maskMoney('mask');
  $('#salary').maskMoney({thousands:'.', decimal:'', allowEmpty: true, allowZero: true, precision:'0'}).maskMoney('mask');
  $('#scheduled_rma').maskMoney({thousands:'.', decimal:'', allowEmpty: true, allowZero: true, precision:'0'}).maskMoney('mask');
  $('#scheduled_rmb').maskMoney({thousands:'.', decimal:'', allowEmpty: true, allowZero: true, precision:'0'}).maskMoney('mask');
  $('#scheduled_rmc').maskMoney({thousands:'.', decimal:'', allowEmpty: true, allowZero: true, precision:'0'}).maskMoney('mask');
  $('#loan').maskMoney({thousands:'.', decimal:'', allowEmpty: true, allowZero: true, precision:'0'}).maskMoney('mask');
});