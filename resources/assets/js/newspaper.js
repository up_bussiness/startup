$(function() {
   $('#form-newspaper').submit(function(e) {
      e.preventDefault();
      var turn =  $('input[name="turn_id"]').val();
      var theme = $('#theme_id').val();
      var title = $('#title').val();
      var description = $('#description').val();
      var CSRF_TOKEN = $('input[name="_token"]').val();
      $.ajax({
          url: 'store',
          type: 'POST',
          data: {_token: CSRF_TOKEN, turn: turn, theme: theme, title: title, description: description},
          dataType: 'JSON',
          success: function (data) {
            $('#table-newspaper > tbody').empty();
            $.each(data, function (i, item) {
                var deleteURI = "";
                $('#table-newspaper > tbody').append('<tr><td>' + item.theme.name + '</td><td class="table-collapse">' + item.title + '</td><td>' + item.description + '</td><td><a href="' + item.id + '/destroy" class="btn btn-sm btn-danger" onclick="return confirm(\'Tem certeza que deseja remover essa matéria?\')" ><span class="glyphicon glyphicon-remove-circle"></span></td></tr>');
            });
            $('#title').val('');
            $('#description').val('');
            $('#newspaper').val(null);
            $('#modal-newspaper').modal('toggle');
          },
          fail: function(jqXHR, txt) {
            console.log(txt);
          }
      });
   });

  $('#theme_id').change(function() {
    getMatters($(this).val());
  });

  $('#newspaper').change(function() {
    getContent($(this).val());
  });

  function getMatters(theme) {

    init_preloader();

    $.get('api/matters/' + theme, function(data) {
      $('#title').val('');
      $('#description').val('');
      $('#newspaper').find('option').remove().end();
      $('#newspaper').append($('<option>', {
        value: null,
        text : '-'
      }));
      $.each(data, function (i, item) {
          $('#newspaper').append($('<option>', {
              value: item.id,
              text : item.title
          }));
      });

      stop_preloader();
    });
  }

  function getContent(newspaper) {

    init_preloader();

    $.get('api/matters/content/' + newspaper, function(data) {
      $('#title').val(data.title);
      $('#description').val(data.description);

      stop_preloader();
    });
  }
});
