@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
<h1>{{ is_null($decisionCoordinator->turn) ? '' : $decisionCoordinator->turn->simulation->name }} {{ isCoordinator() ? ' - '.$decisionCompany->company->name : '' }}</h1>
@stop

@section('content_title')
<h3 class="box-title"><i class="fa fa-bar-chart"></i> Relatório Mercadológico - Rodada {{ is_null($decisionCoordinator->turn) ? '' : $decisionCoordinator->turn->month}}</h3>
<div class="pull-right">
    @can('is-company')
    <a href="{{ route('company.exportaroperational',['turn_id' => $operational->turn_id, 'id' => $operational->user_id]) }}" class="btn btn-xs btn-success hidden-print" title="Exportar para planilha Excel">EXPORTAR Excel</a>
    @endcan
    @can('is-coordinator')
    <a href="{{ route('exportaroperational',['turn_id' => $operational->turn_id, 'id' => $operational->user_id]) }}" class="btn btn-xs btn-success hidden-print" title="Exportar para planilha Excel">EXPORTAR Excel</a>
    @endcan

    <a href="javascript:window.print()" class="btn btn-xs btn-primary hidden-print">IMPRIMIR</a>
    @if(!is_null($decisionCoordinator->turn))
    <a href="{{ isCoordinator() ? route('simulation.turn.show', ['simulation' => $decisionCoordinator->turn->simulation_id, 'id' => $decisionCoordinator->turn->id]) : route('company') }}" class="btn btn-xs btn-default hidden-print">VOLTAR</a>
    @endif
</div>
@stop

@section('content')
<div class="row">
    <div class="col-md-6">
        <div class="table-responsive">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th colspan="100" class="info">
                            PRODUÇÃO
                            <span class="badge badge-dark pull-right" style="cursor: pointer;" onclick="MostrarExplicacao(1)"> Ajuda </span>
                        </th>
                    </tr>
                    <tr class="active">
                        <th>Estoque(un.) de MP</th>
                        <th class="table-collapse">Quadro</th>
                        <th class="table-collapse">Kit 1</th>
                        <th class="table-collapse">Kit 2</th>
                    </tr>
                </thead>
                <tbody class="text-right">
                    <tr>
                        <td class="text-left">Estoque inicial</td>
                        <td class="production-initial-stock-rma">{{ numberDot($operational->initial_stock_rma) }}</td>
                        <td class="production-initial-stock-rmb">{{ numberDot($operational->initial_stock_rmb) }}</td>
                        <td class="production-initial-stock-rmc">{{ numberDot($operational->initial_stock_rmc) }}</td>
                    </tr>
                    <tr>
                        <td class="text-left">(+) Compras emergenciais</td>
                        <td class="decision-emergency-rma">{{ numberDot($decisionCompany->emergency_rma)}}</td>
                        <td class="decision-emergency-rmb">{{ numberDot($decisionCompany->emergency_rmb)}}</td>
                        <td class="decision-emergency-rmc">{{ numberDot($decisionCompany->emergency_rmc)}}</td>
                    </tr>
                    <tr>
                        <td class="text-left">(-) Consumo ou produção</td>
                        <td class="production-rma">{{ numberDot($operational->production_rma) }}</td>
                        <td class="production-rmb">{{ numberDot($operational->production_rmb) }}</td>
                        <td class="production-rmc">{{ numberDot($operational->production_rmc) }}</td>
                    </tr>
                    <tr>
                        <td class="text-left"></td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                    </tr>
                    <tr>
                        <td class="text-left">(+) Compras programadas</td>
                        <td class="decision-scheduled-rma">{{ numberDot($decisionCompany->scheduled_rma) }}</td>
                        <td class="decision-scheduled-rmb">{{ numberDot($decisionCompany->scheduled_rmb) }}</td>
                        <td class="decision-scheduled-rmc">{{ numberDot($decisionCompany->scheduled_rmc) }}</td>
                    </tr>
                    <tr>
                        <td class="text-left">Estoque final (disponível para próxima rodada)</td>
                        <td class="production-final-stock-rma">{{ numberDot($operational->final_stock_rma) }}</td>
                        <td class="production-final-stock-rmb">{{ numberDot($operational->final_stock_rmb) }}</td>
                        <td class="production-final-stock-rmc">{{ numberDot($operational->final_stock_rmc) }}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="col-md-6">
        <div class="table-responsive">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th colspan="100" class="info">
                            ESTOQUE PRODUTO ACABADO
                            <span class="badge badge-dark pull-right" style="cursor: pointer;" onclick="MostrarExplicacao(2)"> Ajuda </span>
                        </th>
                    </tr>
                    <tr class="active">
                        <th>Estoque(un.)</th>
                        <th class="table-collapse">Produto acabado</th>
                    </tr>
                </thead>
                <tbody class="text-right">
                    <tr>
                        <td class="text-left">Estoque inicial</td>
                        <td class="production-initial-stock-product">{{ numberDot($operational->initial_stock_product) }}</td>
                    </tr>
                    <tr>
                        <td class="text-left"></td>
                        <td>-</td>
                    </tr>
                    <tr>
                        <td class="text-left">(-) Produção</td>
                        <td class="production-product">{{ numberDot($operational->production_product) }}</td>
                    </tr>
                    <tr>
                        <td class="text-left">(-) Vendas</td>
                        <td class="production-sales">{{ numberDot($operational->sales - $operational->venda_extra) }}</td>
                    </tr>
                    <tr>
                        <td class="text-left">(-) Venda extra</td>
                        <td>{{ numberDot($operational->venda_extra) }}</td>
                    </tr>
                    <tr>
                        <td class="text-left">Estoque final</td>
                        <td class="production-final-stock-product">{{ numberDot($operational->final_stock_product) }}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>

    <div class="col-md-6">
        <div class="table-responsive">
            <table class="table  table-bordered">
                <thead>
                    <tr>
                        <th colspan="100" class="active">
                            LINHAS DE PRODUÇÃO
                            <span class="badge badge-dark pull-right" style="cursor: pointer;" onclick="MostrarExplicacao(3)"> Ajuda </span>
                        </th>
                    </tr>
                    <tr class="active">
                        <th>Tipo</th>
                        <th>Quantidade</th>
                        <th>Idade Média em meses</th>
                    </tr>
                </thead>
                <tbody class="text-right">
                    <tr class="{{ $decisionCompany->sell_small_machine > 0 ? 'danger' : '' }}">
                        <td class="text-left">Pequena</td>
                        <td class="small-machine-qtd">{{ $operational->machine_s_qnt }}</td>
                        <td class="small-machine-age">{{ $operational->machine_s_age }}</td>
                    </tr>
                    <tr class="{{ $decisionCompany->sell_medium_machine > 0 ? 'danger' : '' }}">
                        <td class="text-left">Média</td>
                        <td class="medium-machine-qtd">{{ $operational->machine_m_qnt }}</td>
                        <td class="medium-machine-age">{{ $operational->machine_m_age }}</td>
                    </tr>
                    <tr class="{{ $decisionCompany->sell_large_machine > 0 ? 'danger' : '' }}">
                        <td class="text-left">Grande</td>
                        <td class="large-machine-qtd">{{ $operational->machine_l_qnt }}</td>
                        <td class="large-machine-age">{{ $operational->machine_l_age }}</td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="table-responsive">
            <table class="table  table-bordered">
                <thead>
                    <tr>
                        <th colspan="100" class="active">
                            PROGRAMAÇÃO
                        </th>
                    </tr>
                </thead>
                <tbody class="text-right">
                    <tr>
                        <td class="text-left">Nível de atividade (%)</td>
                        <td class="decision-activity-level">{{$decisionCompany->activity_level}}</td>
                    </tr>
                    <tr>
                        <td class="text-left">Produção extra (%)</td>
                        <td class="decision-extra-production">{{$decisionCompany->extra_production}}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="col-md-6">
        <div class="table-responsive">
            <table class="table  table-bordered">
                <thead>
                    <tr>
                        <th colspan="100" class="active">
                            RECURSOS HUMANOS
                            <span class="badge badge-dark pull-right" style="cursor: pointer;" onclick="MostrarExplicacao(4)"> Ajuda </span>
                        </th>
                    </tr>
                </thead>
                <tbody class="text-right">
                    <tr>
                        <td class="text-left">Colaboradores da produção no início do período</td>
                        <td class="production-employees">{{ $operational->employees }}</td>
                    </tr>
                    <tr>
                        <td class="text-left">(+) Admitidos</td>
                        <td class="decision-admitted">{{$decisionCompany->admitted}}</td>
                    </tr>
                    <tr>
                        <td class="text-left">(-) Demitidos</td>
                        <td class="decision-fired">{{$decisionCompany->fired}}</td>
                    </tr>
                    <tr>
                        <td class="text-left">(=) Colaboradores da produção no final do período</td>
                        <td class="production-final-employees">{{ $operational->final_employees }}</td>
                    </tr>
                    <tr>
                        <td class="text-left">Salário base da produção</td>
                        <td class="decision-salary">{{numberDot($decisionCompany->salary)}}</td>
                    </tr>
                    <tr>
                        <td class="text-left">Treinamento (%)</td>
                        <td class="decision-training">{{$decisionCompany->training}}</td>
                    </tr>
                    <tr>
                        <td class="text-left">Produtividade</td>
                        <td class="production-productivity">{{ $operational->productivity }}</td>
                    </tr>
                    <tr>
                        <td class="text-left">Produção/Homem</td>
                        <td class="production-pm">{{ $operational->production_man }}</td>
                    </tr>
                    <tr>
                        <td class="text-left">Colaboradores administrativos</td>
                        <td>2</td>
                    </tr>
                    <tr>
                        <td class="text-left">Colaboradores comerciais</td>
                        <td>10</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="col-md-12">
        <div class="table-responsive">
            <table class="table  table-bordered">
                <thead>
                    <tr>
                        <th colspan="100" class="info">
                            DADOS DE MERCADO - EMPRESAS
                        </th>
                    </tr>
                    <tr class="active">
                        <th></th>
                        @foreach($companies->sortBy('company.email',SORT_NATURAL) as $c)
                        <th class="table-collapse">{{$c->company->name}}</th>
                        @endforeach
                    </tr>
                </thead>
                <tbody class="text-right">
                    <tr>
                        <td class="text-left label-price">Preço</td>
                        @foreach($companies->sortBy('company.email',SORT_NATURAL) as $c)
                        <td class="table-collapse">{{ $c->price }}</td>
                        @endforeach
                    </tr>
                    <tr>
                        <td class="text-left label-term">Prazo</td>
                        @foreach($companies->sortBy('company.email',SORT_NATURAL) as $c)
                        <td class="table-collapse">{{ $c->term == 0 ? 'À vista' : '1+1' }}</td>
                        @endforeach
                    </tr>
                    <tr>
                        <td class="text-left label-demand">Demanda (un.)</td>
                        @foreach($operationals->sortBy('company.email',SORT_NATURAL) as $c)
                        @can('is-company')
                        @if ($c->user_id == auth()->user()->id)
                        <td class="table-collapse">{{ numberDot($c->demand) }}</td>
                        @else
                        <td class="table-collapse">-</td>
                        @endif
                        @endcan
                        @can('is-coordinator')
                        <td class="table-collapse">{{ numberDot($c->demand) }}</td>
                        @endcan
                        @endforeach
                    </tr>
                    <tr>
                        <td class="text-left label-demand">Demanda extra</td>
                        @foreach($operationals->sortBy('company.email',SORT_NATURAL) as $c)
                        <td class="table-collapse">{{ numberDot($c->demand_entra) }}</td>
                        @endforeach
                    </tr>
                    <tr>
                        <td class="text-left label-sales">Vendas (un.)</td>
                        @foreach($operationals->sortBy('company.email',SORT_NATURAL) as $c)
                        <td class="table-collapse">{{ numberDot($c->sales - $c->venda_extra) }}</td>
                        @endforeach
                    </tr>
                    <tr>
                        <td class="text-left label-demand">Venda extra</td>
                        @foreach($operationals->sortBy('company.email',SORT_NATURAL) as $c)
                        <td class="table-collapse">{{ numberDot($c->venda_extra) }}</td>
                        @endforeach
                    </tr>
                    <tr>
                        <td class="text-left label-demand">Venda total</td>
                        @foreach($operationals->sortBy('company.email',SORT_NATURAL) as $c)
                        <td class="table-collapse">{{ numberDot($c->venda_total) }}</td>
                        @endforeach
                    </tr>
                    <tr>
                        <td class="text-left label-market-share">Participação no mercado (%)</td>
                        @foreach($operationals->sortBy('company.email',SORT_NATURAL) as $c)
                        <td class="table-collapse">{{ number_format(($c->sales / $operationals->sum('sales')) * 100, 2) }}</td>
                        @endforeach
                    </tr>
                    <tr>
                        <td class="text-left">Lucro no período ($)</td>
                        @foreach($dre->sortBy('company.email',SORT_NATURAL) as $d)
                        <td class="table-collapse">{{ numberDot($d->profit_after_ir_total) }}</td>
                        @endforeach
                    </tr>
                    <tr>
                        <td class="text-left">Patrimônio líquido ($)</td>
                        @foreach($balance->sortBy('company.email',SORT_NATURAL) as $b)
                        <td class="table-collapse">{{ numberDot($b->net_worth) }}</td>
                        @endforeach
                    </tr>
                </tbody>
            </table>
            <table class="table  table-bordered">
                <tr>
                    <td>Preço Médio</td>
                    <td>Propaganda Média</td>
                    <td>Demanda total (un.)</td>
                    <td>Produção total (un.)</td>
                    <td>Venda total (un.)</td>
                    <td>Venda extra</td>
                    <td>Venda geral</td>
                </tr>
                @php
                $somarVendaExtra = $operationals->sum('venda_extra');
                $somarSales = $operationals->sum('sales') - $somarVendaExtra;

                $totalGeral = $somarSales+$somarVendaExtra;
                @endphp
                <tr>
                    <td class="label-avg-price">{{ numberDot($operationalDetail->price_average) }}</td>
                    <td class="label-avg-adv">{{ numberDot($dre->avg('adv_total'))}}</td>
                    <td class="label-total-demand">{{ numberDot($operationalDetail->demand) }}</td>
                    <td class="label-total-production">{{ numberDot($operationals->sum('production_product')) }}</td>
                    <td class="label-total-sales">{{ numberDot($somarSales) }}</td>
                    <td class="label-total-extra">{{ numberDot($somarVendaExtra) }}</td>
                    <td class="label-total-geral">{{ numberDot($totalGeral) }}</td>
                </tr>
            </table>
        </div>
    </div>
    <div class="col-md-12">
        <div class="table-responsive">
            <table class="table  table-bordered">
                <thead>
                    <tr>
                        <th colspan="100" class="info">
                            DADOS DE MERCADO - CONJUNTURA ECONÔMICA
                        </th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
    <div class="col-md-6">
        <div class="table-responsive">
            <table class="table  table-bordered">
                <thead>
                    <tr class="active">
                        <th>
                            PREÇO DOS FORNECEDORES
                        </th>
                        <th class="text-center">($)</th>
                    </tr>
                </thead>
                <tbody class="text-right">
                    <tr>
                        <td class="text-left">Quadro</td>
                        <td class="economy-emergency-rma">{{$decisionCoordinator->raw_material_a}}</td>
                    </tr>
                    <tr>
                        <td class="text-left">Kit 1</td>
                        <td class="economy-emergency-rmb">{{$decisionCoordinator->raw_material_b}}</td>
                    </tr>
                    <tr>
                        <td class="text-left">Kit 2</td>
                        <td class="economy-emergency-rmc">{{$decisionCoordinator->raw_material_c}}</td>
                    </tr>
                    <tr>
                        <td class="text-left">Linha de Produção - Pequena</td>
                        <td class="economy-machine-s">{{numberDot($decisionCoordinator->lp_small)}}</td>
                    </tr>
                    <tr>
                        <td class="text-left">Linha de Produção - Média</td>
                        <td class="economy-machine-m">{{numberDot($decisionCoordinator->lp_medium)}}</td>
                    </tr>
                    <tr>
                        <td class="text-left">Linha de Produção - Grande</td>
                        <td class="economy-machine-l">{{numberDot($decisionCoordinator->lp_large)}}</td>
                    </tr>
                    <tr>
                        <td class="text-left">Radio</td>
                        <td>{{numberDot($decisionCoordinator->adv_radio)}}</td>
                    </tr>
                    <tr>
                        <td class="text-left">Jornal</td>
                        <td>{{numberDot($decisionCoordinator->adv_journal)}}</td>
                    </tr>
                    <tr>
                        <td class="text-left">Mídias Sociais</td>
                        <td>{{numberDot($decisionCoordinator->adv_social)}}</td>
                    </tr>
                    <tr>
                        <td class="text-left">Outdoor</td>
                        <td>{{numberDot($decisionCoordinator->adv_outdoor)}}</td>
                    </tr>
                    <tr>
                        <td class="text-left">Propaganda - TV</td>
                        <td>{{numberDot($decisionCoordinator->adv_tv)}}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="col-md-6">
        <div class="table-responsive">
            <table class="table  table-bordered">
                <thead>
                    <tr>
                        <th colspan="100" class="active">
                            INDICADORES
                        </th>
                    </tr>
                </thead>
                <tbody class="text-right">
                    <tr>
                        <td class="text-left">Macro setor (%)</td>
                        <td class="economy-macro-industry">{{$decisionCoordinator->macro_industry}}</td>
                    </tr>
                    <tr>
                        <td class="text-left">Inflação (%)</td>
                        <td class="economy-inflation">{{$decisionCoordinator->inflation}}</td>
                    </tr>
                    <tr>
                        <td class="text-left">Taxa básica de juros (%)</td>
                        <td class="economy-bir">{{$decisionCoordinator->bir}}</td>
                    </tr>
                    <tr>
                        <td class="text-left">Juros dos fornecedores (%)</td>
                        <td class="economy-interest-providers">{{$decisionCoordinator->interest_providers}}</td>
                    </tr>
                    <tr>
                        <td class="text-left">Impostos (%)</td>
                        <td class="economy-interest-providers">{{$decisionCoordinator->income_tax}}</td>
                    </tr>
                    <tr>
                        <td class="text-left">Juros médios de vendas (%)</td>
                        <td class="economy-interest-average">{{$operationalDetail->interest_average}}</td>
                    </tr>
                    <tr>
                        <td class="text-left">Produtos importados (un.)</td>
                        <td class="economy-import-products">{{numberDot($operationalDetail->import_products)}}</td>
                    </tr>
                    <tr>
                        <td class="text-left">Salário Médio ($)</td>
                        <td class="economy-salary-avg">{{numberDot($operationalDetail->salary_average)}}</td>
                    </tr>
                    <tr>
                        <td class="text-left">Produção/Homem Média</td>
                        <td class="economy-production-man-avg">0</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="col-md-12 text-center">
        <b>DECISÕES DA EMPRESA DO PERÍODO ANTERIOR.</b>
        <hr>
    </div>
    <div class="col-md-6">
        <div class="table-responsive">
            <table class="table  table-bordered">
                <thead>
                    <tr>
                        <th colspan="100" class="active">
                            VENDAS
                        </th>
                    </tr>
                </thead>
                <tbody class="text-right">
                    <tr>
                        <td class="text-left">Preço ($)</td>
                        <td class="decision-price">{{numberDot($decisionCompany->price)}}</td>
                    </tr>
                    <tr>
                        <td class="text-left">Prazo</td>
                        <td class="decision-term">{{ $decisionCompany->term == 0 ? 'À vista' : '1+1' }}</td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="table-responsive">
            <table class="table  table-bordered">
                <thead>
                    <tr>
                        <th colspan="100" class="active">
                            PROPAGANDA
                        </th>
                    </tr>
                </thead>
                <tbody class="text-right">
                    <tr>
                        <td class="text-left">Radio</td>
                        <td class="decision-adv-radio">{{$decisionCompany->adv_radio}}</td>
                    </tr>
                    <tr>
                        <td class="text-left">Jornal</td>
                        <td class="decision-adv-journal">{{$decisionCompany->adv_journal}}</td>
                    </tr>
                    <tr>
                        <td class="text-left">Mídias Sociais</td>
                        <td class="decision-adv-social">{{$decisionCompany->adv_social}}</td>
                    </tr>
                    <tr>
                        <td class="text-left">Outdoor</td>
                        <td class="decision-adv-outdoor">{{$decisionCompany->adv_outdoor}}</td>
                    </tr>
                    <tr>
                        <td class="text-left">TV</td>
                        <td class="decision-adv-panfletch">{{$decisionCompany->adv_tv}}</td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="table-responsive">
            <table class="table  table-bordered">
                <thead>
                    <tr>
                        <th colspan="100" class="active">
                            PRODUÇÃO
                        </th>
                    </tr>
                </thead>
                <tbody class="text-right">
                    <tr>
                        <td class="text-left">Compras - Quadro (Nº)</td>
                        <td class="decision-scheduled-rma">{{numberDot($decisionCompany->scheduled_rma)}}</td>
                    </tr>
                    <tr>
                        <td class="text-left">Compras - Kit 1 (Nº)</td>
                        <td class="decision-scheduled-rmb">{{numberDot($decisionCompany->scheduled_rmb)}}</td>
                    </tr>
                    <tr>
                        <td class="text-left">Compras - Kit 2 (Nº)</td>
                        <td class="decision-scheduled-rmc">{{numberDot($decisionCompany->scheduled_rmc)}}</td>
                    </tr>
                    <tr>
                        <td class="text-left">Pagamento das MP</td>
                        <td class="decision-payment-rm">{{ $decisionCompany->payment_rm == 0 ? 'À vista' : '1+1' }}</td>
                    </tr>
                    <tr>
                        <td class="text-left">Nível de atividade (%)</td>
                        <td class="decision-activity-level">{{$decisionCompany->activity_level}}</td>
                    </tr>
                    <tr>
                        <td class="text-left">Produção extra (%)</td>
                        <td class="decision-extra-production">{{$decisionCompany->extra_production}}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="col-md-6">
        <div class="table-responsive">
            <table class="table  table-bordered">
                <thead>
                    <tr class="active">
                        <th>Linha de Produção</th>
                        <th>Pequena</th>
                        <th>Média</th>
                        <th>Grande</th>
                    </tr>
                </thead>
                <tbody class="text-right">
                    <tr>
                        <td class="text-left">Compras (Nº)</td>
                        <td class="decision-buy-small-machine">{{$decisionCompany->buy_small_machine}}</td>
                        <td class="decision-buy-medium-machine">{{$decisionCompany->buy_medium_machine}}</td>
                        <td class="decision-buy-large-machine">{{$decisionCompany->buy_large_machine}}</td>
                    </tr>
                    <tr>
                        <td class="text-left">Recurso Próprio (%)</td>
                        <td class="decision-buy-small-machine">{{$decisionCompany->b_s_m}}</td>
                        <td class="decision-buy-medium-machine">{{$decisionCompany->b_m_m}}</td>
                        <td class="decision-buy-large-machine">{{$decisionCompany->b_l_m}}</td>
                    </tr>
                    <tr>
                        <td class="text-left">Vendas (Nº)</td>
                        <td class="decision-sell-small-machine">{{$decisionCompany->sell_small_machine}}</td>
                        <td class="decision-sell-medium-machine">{{$decisionCompany->sell_medium_machine}}</td>
                        <td class="decision-sell-large-machine">{{$decisionCompany->sell_large_machine}}</td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="table-responsive">
            <table class="table  table-bordered">
                <thead>
                    <tr>
                        <th colspan="100" class="active">
                            RECURSOS HUMANOS
                        </th>
                    </tr>
                </thead>
                <tbody class="text-right">
                    <tr>
                        <td class="text-left">Admitidos (Nº)</td>
                        <td class="decision-admitted">{{$decisionCompany->admitted}}</td>
                    </tr>
                    <tr>
                        <td class="text-left">Demitidos (Nº)</td>
                        <td class="decision-fired">{{$decisionCompany->fired}}</td>
                    </tr>
                    <tr>
                        <td class="text-left">Salário ($)</td>
                        <td class="decision-salary">{{numberDot($decisionCompany->salary)}}</td>
                    </tr>
                    <tr>
                        <td class="text-left">Treinamento (%)</td>
                        <td class="decision-training">{{$decisionCompany->training}}</td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="table-responsive">
            <table class="table  table-bordered">
                <thead>
                    <tr>
                        <th colspan="100" class="active">
                            FINANÇAS
                        </th>
                    </tr>
                </thead>
                <tbody class="text-right">
                    <tr>
                        <td class="text-left">Empréstimo ($)</td>
                        <td class="decision-loan">{{numberDot($decisionCompany->loan)}}</td>
                    </tr>
                    <tr>
                        <td class="text-left">Antecipação de recebíveis ($)</td>
                        <td class="decision-anticipation-of-receivables">{{numberDot($decisionCompany->anticipation_of_receivables)}}</td>
                    </tr>
                    <tr>
                        <td class="text-left">Aplicação ($)</td>
                        <td class="decision-application">{{numberDot($decisionCompany->application)}}</td>
                    </tr>
                    <tr>
                        <td class="text-left">Juros na venda a prazo (%)</td>
                        <td class="decision-term-interest">{{$decisionCompany->term_interest}}</td>
                    </tr>
                    <tr>
                        <td class="text-left">Receitas diversas ($)</td>
                        <td class="decision-income">{{numberDot($decisionCompany->income)}}</td>
                    </tr>
                    <tr>
                        <td class="text-left">Despesas diversas ($)</td>
                        <td class="decision-expense">{{numberDot($decisionCompany->expense)}}</td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="table-responsive">
            <table class="table  table-bordered">
                <thead>
                    <tr>
                        <th colspan="100" class="active">
                            COMPRAS EMERGENCIAIS
                        </th>
                    </tr>
                </thead>
                <tbody class="text-right">
                    <tr>
                        <td class="text-left">Quadro (Nº)</td>
                        <td class="decision-emergency-rma">{{$decisionCompany->emergency_rma}}</td>
                    </tr>
                    <tr>
                        <td class="text-left">Kit 1 (Nº)</td>
                        <td class="decision-emergency-rmb">{{$decisionCompany->emergency_rmb}}</td>
                    </tr>
                    <tr>
                        <td class="text-left">Kit 2 (Nº)</td>
                        <td class="decision-emergency-rmc">{{$decisionCompany->emergency_rmc}}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">
                    <div id="mdHead"></div>
                </h4>
            </div>
            <div class="modal-body">
                <p>
                <div id="mdTexto1" style="display:none;">
                    Verifique seu estoque de matérias-primas.<br />
                    Para que seja fabricada uma bicicleta, você precisa de:<br />
                    1 unidade de quadro<br />
                    2 unidades do kit 1 (kit de rodas e pneus)<br />
                    3 unidades do kit 2 (kit de montagem)<br />
                    Obs.: No início do jogo sua empresa tem duas máquinas, cuja capacidade produtiva total é de 3.600 bicicletas, então, faça sua gestão de estoques de acordo com seu planejamento de produção.<br />
                    Lembrando que pode adquirir matérias-primas de duas formas:<br />
                    Emergencial: o preço é 30% maior, porém não tem custo de armazenagem, pois vai direto para produção e seu pagamento se dá de forma a vista.<br />
                    Programada: não tem seu preço onerado, porém, tem custo de armazenagem, pois ele será armazenado para ser utilizado no próximo período e seu pagamento se dá tanto de forma a vista com a prazo.
                </div>
                <div id="mdTexto2" style="display:none;">
                    Uma boa gestão da produção é imprescindível para que obtenha o melhor resultado nas vendas.<br />
                    Aqui você verifica o fluxo de entrada, venda e saldo de bicicletas em estoque.<br />
                    É importante que suas decisões no marketing (preço e promoção) sejam assertivas para gerar uma demanda suficiente para vender todos os produtos em estoque.
                </div>
                <div id="mdTexto3" style="display:none;">
                    No início do jogo, sua empresa está equipada com 2 máquinas, sendo uma pequena e a outra de porte médio.<br />
                    É importante que saiba:<br />
                    1º - Cada máquina pequena produz 1000 unidades, precisa de 10 operários para ter o máximo de produtividade,<br />
                    Cada máquina média produz 2.600 unidades e precisa de 20 operários para ter o máximo de produtividade.<br />
                    Cada máquina grande produz 6000 unidades e precisa de 42 operários para ter o máximo de produtividade.<br />
                    Ou seja, sua empresa no início do jogo tem uma capacidade produtiva de 3.600 unidades, caso opte por 100% de nível de atividade na decisão de produção.
                </div>
                <div id="mdTexto4" style="display:none;">
                    No início do jogo, sua empresa conta com 30 operários, sendo 10 para operar a máquina de porte pequeno e 20 para operar a máquina de porte médio, caso venda ou compre máquinas, deverá observar a necessidade de demitir ou contratar novos colaboradores.<br />
                    Observe:<br />
                    Máquina pequena. = 10 operários.<br />
                    Máquina média. = 20 operários.<br />
                    Máquina grande. = 42 operários.

                </div>
                </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
            </div>
        </div>

    </div>
</div>
<script>
    function MostrarExplicacao(id) {
        var texto = 'okokoko';
        var head = '';
        switch (id) {
            case 1:
                head = 'Estoque MP';
                break;
            case 2:
                head = 'Estoque PA'
                break;
            case 3:
                head = 'Linhas de Produção'
                break;
            case 4:
                head = 'RH'
                break;
        }
        for (i = 1; i <= 4; i++) {
            if (i != id) {
                $('#mdTexto' + i).hide();
            }
        }
        /* $('#mdTexto1').hide();
         $('#mdTexto2').hide();
         $('#mdTexto2').hide();
         $('#mdTexto2').hide(); */

        $('#mdHead').html(head);
        $('#mdTexto' + id).show();
        $('#myModal').modal();
    }
</script>
@stop

@section('js')
@include('layouts.partials.storage', ['key' => $operational->user_id])
@stop