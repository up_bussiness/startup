@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
<h1>{{$dre->turn->simulation->name}} {{ isCoordinator() ? ' - '.$dre->company->name : '' }}</h1>
@stop

@section('content_title')
<h3 class="box-title"><i class="fa fa-users"></i> DRE - Rodada {{$dre->turn->month}}</h3>
<div class="pull-right">
    @can('is-company')
    <a href="{{ route('company.exportardre',['turn_id' => $dre->turn_id, 'id' => $dre->user_id]) }}" class="btn btn-xs btn-success hidden-print" title="Exportar para planilha Excel">EXPORTAR Excel</a>
    @endcan
    @can('is-coordinator')
    <a href="{{ route('exportardre',['turn_id' => $dre->turn_id, 'id' => $dre->user_id]) }}" class="btn btn-xs btn-success hidden-print" title="Exportar para planilha Excel">EXPORTAR Excel</a>
    @endcan

    <a href="javascript:window.print()" class="btn btn-xs btn-primary hidden-print">IMPRIMIR</a>
    <a href="{{ isCoordinator() ? route('simulation.turn.show', ['simulation' => $dre->turn->simulation_id, 'id' => $dre->turn_id]) : route('company') }}" class="btn btn-xs btn-default hidden-print">VOLTAR</a>
</div>
@stop

@section('content')
<div class="box-body">

    <div class="row">
        <div class="col-md-8 col-md-offset-3">

            <div class="table-responsive">
                <table class="table table-bordered">
                    <thead>
                        <tr class="active">
                            <th>Resultado econômico</th>
                            <th class="text-center">Custo</th>
                            <th class="text-center">DRE</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td></td>
                            <td class="text-center"><b>Unitário</b></td>
                            <td class="text-center"><b>Total</b></td>
                        </tr>
                        <tr>
                            <td><b>Preço de venda / Receita total</b></td>
                            <td class="text-center">{{ numberDot($dre->price) }}</td>
                            <td class="text-center">{{ numberDot($dre->price_total) }}</td>
                        </tr>
                        <tr>
                            <td><b>( - ) Custo Produto Vendido (CPV)</b></td>
                            <td class="text-center">{{ $dre->cpv }}</td>
                            <td class="text-center">{{ numberDot($dre->cpv_total) }}</td>
                        </tr>
                        <tr>
                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Matéria-Prima Quadro</td>
                            <td class="text-center">{{ $dre->raw_a }}</td>
                            <td class="text-center">{{ numberDot($dre->raw_a_total) }}</td>
                        </tr>
                        <tr>
                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Matéria-Prima Kit 1</td>
                            <td class="text-center">{{ $dre->raw_b }}</td>
                            <td class="text-center">{{ numberDot($dre->raw_b_total) }}</td>
                        </tr>
                        <tr>
                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Matéria-Prima Kit 2</td>
                            <td class="text-center">{{ $dre->raw_c }}</td>
                            <td class="text-center">{{ numberDot($dre->raw_c_total) }}</td>
                        </tr>
                        <tr>
                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Estocagem das matérias-primas</td>
                            <td class="text-center">{{ $dre->stock_raw }}</td>
                            <td class="text-center">{{ numberDot($dre->stock_raw_total) }}</td>
                        </tr>
                        <tr>
                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Estocagem de produtos acabados</td>
                            <td class="text-center">{{ $dre->stock_pa }}</td>
                            <td class="text-center">{{ numberDot($dre->stock_pa_total) }}</td>
                        </tr>
                        <tr>
                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Folha de pagamento da produção</td>
                            <td class="text-center">{{ $dre->salary_production }}</td>
                            <td class="text-center">{{ numberDot($dre->salary_production_total) }}</td>
                        </tr>
                        <tr>
                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Manutenção de máquinas</td>
                            <td class="text-center">{{ $dre->machine_maintain }}</td>
                            <td class="text-center">{{ numberDot($dre->machine_maintain_total) }}</td>
                        </tr>
                        <tr>
                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Treinamento da produção</td>
                            <td class="text-center">{{ $dre->training }}</td>
                            <td class="text-center">{{ numberDot($dre->training_total) }}</td>
                        </tr>
                        <tr>
                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Revalorização estoque produto acabado</td>
                            <td class="text-center"></td>
                            <td class="text-center">{{ numberDot($dre->revalorizacao_stock) }}</td>
                        </tr>
                        <tr>
                            <td><b>( = ) Lucro bruto</b></td>
                            <td class="text-center">{{ $dre->gross_profit }}</td>
                            <td class="text-center">{{ numberDot($dre->gross_profit_total) }}</td>
                        </tr>
                        <tr>
                            <td><b>( - ) Total de despesas</b></td>
                            <td class="text-center">{{ $dre->income_sell }}</td>
                            <td class="text-center">{{ numberDot($dre->income_sell_total) }}</td>
                        </tr>


                        <tr>
                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Folha de pagamento dos vendedores</td>
                            <td class="text-center">{{ $dre->salary_sellers }}</td>
                            <td class="text-center">{{ numberDot($dre->salary_sellers_total) }}</td>
                        </tr>
                        <tr>
                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Folha de pagamento dos colaboradores administrativos</td>
                            <td class="text-center">{{ $dre->salary_admin }}</td>
                            <td class="text-center">{{ numberDot($dre->salary_admin_total) }}</td>
                        </tr>

                        <tr>
                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Propaganda</td>
                            <td class="text-center">{{ $dre->adv }}</td>
                            <td class="text-center">{{ numberDot($dre->adv_total) }}</td>
                        </tr>


                        <tr>
                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Juros: compra a prazo, empréstimo e financimento</td>
                            <td class="text-center">{{ $dre->difference }}</td>
                            <td class="text-center">{{ numberDot($dre->difference_total) }}</td>
                        </tr>
                        <tr>
                            <td><b>( = ) Lucro operacional</b></td>
                            <td class="text-center">{{ $dre->operational_profit }}</td>
                            <td class="text-center">{{ numberDot($dre->operational_profit_total) }}</td>
                        </tr>
                        <tr>
                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Depreciação de máquinas</td>
                            <td class="text-center">{{ $dre->machine_depreciation }}</td>
                            <td class="text-center">{{ numberDot($dre->machine_depreciation_total) }}</td>
                        </tr>
                        <tr>
                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Depreciação de prédios e instalações (100%)</td>
                            <td class="text-center">{{ $dre->building_depreciation }}</td>
                            <td class="text-center">{{ numberDot($dre->building_depreciation_total) }}</td>
                        </tr>
                        <tr>
                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Outras receitas e despesas</td>
                            <td class="text-center">{{ $dre->others_value }}</td>
                            <td class="text-center">{{ numberDot($dre->others_value_total) }}</td>
                        </tr>
                        <tr>
                            <td><b>Lucro do período antes IR</b></td>
                            <td class="text-center">{{ $dre->profit_before_ir }}</td>
                            <td class="text-center">{{ numberDot($dre->profit_before_ir_total) }}</td>
                        </tr>
                        <tr>
                            <td>( - ) Provisão para o IR ({{round(ir_percent($dre->profit_before_ir_total, $dre->ir_total), 2)}} %)</td>
                            <td class="text-center">{{ $dre->ir }}</td>
                            <td class="text-center">{{ numberDot($dre->ir_total) }}</td>
                        </tr>
                        <tr>
                            <td>( - ) Prejuízo na venda de máquinas</td>
                            <td class="text-center">-</td>
                            <td class="text-center">{{ numberDot($dre->loss_on_sale) }}</td>
                        </tr>
                        <tr>
                            <td><b>Lucro do período após IR</b></td>
                            <td class="text-center">{{ $dre->profit_after_ir }}</td>
                            <td class="text-center">{{ numberDot($dre->profit_after_ir_total) }}</td>
                        </tr>
                        <tr>
                            <td><b>Margem de lucro (%)</b></td>
                            <td class="text-center">{{ $dre->profit_margin }}</td>
                            <td class="text-center">{{ $dre->profit_margin }}</td>
                        </tr>
                    </tbody>
                </table>
            </div>

        </div>
    </div>

</div>
@include('layouts.partials.fortes')
@stop

@section('js')
@include('layouts.partials.storage', ['key' => $dre->user_id])
@stop