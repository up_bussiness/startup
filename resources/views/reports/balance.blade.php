@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
<h1>{{$balance->turn->simulation->name}} {{ isCoordinator() ? ' - '.$balance->company->name : '' }}</h1>
@stop

@section('content_title')
<h3 class="box-title"><i class="fa fa-balance-scale"></i> Balanço Patrimonial - Rodada {{$balance->turn->month}}</h3>
<div class="pull-right">
    @can('is-company')
    <a href="{{ route('company.exportarbalance',['turn_id' => $balance->turn_id, 'id' => $balance->user_id]) }}" class="btn btn-xs btn-success hidden-print" title="Exportar para planilha Excel">EXPORTAR Excel</a>
    @endcan
    @can('is-coordinator')
    <a href="{{ route('exportarbalance',['turn_id' => $balance->turn_id, 'id' => $balance->user_id]) }}" class="btn btn-xs btn-success hidden-print" title="Exportar para planilha Excel">EXPORTAR Excel</a>
    @endcan

    <a href="javascript:window.print()" class="btn btn-xs btn-primary hidden-print">IMPRIMIR</a>
    <a href="{{ isCoordinator() ? route('simulation.turn.show', ['simulation' => $balance->turn->simulation_id, 'id' => $balance->turn_id]) : route('company') }}" class="btn btn-xs btn-default hidden-print">VOLTAR</a>
</div>
@stop

@section('content')
<div class="col-md-10 col-md-offset-1">

    <div class="row">
        <div class="col-md-6">

            <table class="table  table-bordered">
                <thead>
                    <tr>
                        <th colspan="100" class="active">
                            ATIVO
                        </th>
                    </tr>
                </thead>
                <tbody class="text-right">
                    <tr>
                        <td colspan="100" align="left"><b>Ativo Circulante</b></td>
                    </tr>
                    <tr>
                        <td class="text-left">Caixa</td>
                        <td>{{ numberDot($balance->active_cash) }}</td>
                    </tr>
                    <tr>
                        <td class="text-left">Aplicação</td>
                        <td>{{ numberDot($balance->active_application) }}</td>
                    </tr>
                    <tr>
                        <td class="text-left">Clientes</td>
                        <td>{{ numberDot($balance->active_clients) }}</td>
                    </tr>
                    <tr>
                        <td class="text-left">Estoque produto acabado</td>
                        <td>{{ numberDot($balance->active_stock_product)  }}</td>
                    </tr>
                    <tr>
                        <td class="text-left">Estoque Quadro</td>
                        <td>{{ numberDot($balance->active_raw_a)  }}</td>
                    </tr>
                    <tr>
                        <td class="text-left">Estoque Kit 1</td>
                        <td>{{ numberDot($balance->active_raw_b)  }}</td>
                    </tr>
                    <tr>
                        <td class="text-left">Estoque Kit 2</td>
                        <td>{{ numberDot($balance->active_raw_c)  }}</td>
                    </tr>
                    <tr>
                        <td colspan="100" align="left"><b>Ativo não circulante</b></td>
                    </tr>
                    <tr>
                        <td class="text-left">Máquinas</td>
                        <td>{{ numberDot($balance->active_machines)  }}</td>
                    </tr>
                    <tr>
                        <td class="text-left">( - ) Depreciação acumulada</td>
                        <td>{{ numberDot($balance->active_depreciation_machine)  }}</td>
                    </tr>
                    <tr>
                        <td class="text-left">Prédios e instalações</td>
                        <td>{{ numberDot($balance->active_buildings)  }}</td>
                    </tr>
                    <tr>
                        <td class="text-left">( - ) Depreciação acumulada</td>
                        <td>{{ numberDot($balance->active_depreciation_buildings)  }}</td>
                    </tr>
                    <tr>
                        <td class="text-left"><b>Total</b></td>
                        <td>{{ numberDot($balance->active_total)  }}</td>
                    </tr>
                </tbody>
            </table>

        </div>

        <div class="col-md-6">

            <table class="table  table-bordered">
                <thead>
                    <tr>
                        <th colspan="100" class="active">
                            PASSIVO
                        </th>
                    </tr>
                </thead>
                <tbody class="text-right">
                    <tr>
                        <td class="text-left">Fornecedores a vencer</td>
                        <td>{{ numberDot($balance->passive_suppliers_win) }}</td>
                    </tr>
                    <tr>
                        <td class="text-left">Juros Fornecedores </td>
                        <td>{{ numberDot($balance->passive_suppliers_bir) }}</td>
                    </tr>
                    <tr>
                        <td class="text-left">Fornecedores em atraso</td>
                        <td>{{ numberDot($balance->passive_suppliers_arrears) }}</td>
                    </tr>
                    <tr>
                        <td class="text-left">Contas em atraso</td>
                        <td>{{ numberDot($balance->passive_overdue_account) }}</td>
                    </tr>
                    <tr>
                        <td class="text-left">Impostos a pagar</td>
                        <td>{{ numberDot($balance->passive_tax_pay) }}</td>
                    </tr>
                    <tr>
                        <td class="text-left">Empréstimos e financiamentos a vencer</td>
                        <td>{{ numberDot($balance->passive_loans_financing_mature) }}</td>
                    </tr>
                    <tr>
                        <td class="text-left">Empréstimos, financiamentos em atraso</td>
                        <td>{{ numberDot($balance->passive_loans_financing_interest_arrears) }}</td>
                    </tr>
                    <tr>
                        <td class="text-left">( - ) Juros a Apropriar </td>
                        <td>{{ numberDot($balance->juros_apropriar) }}</td>
                    </tr>
                    <tr>
                        <td colspan="100" class="text-left">-</td>
                    </tr>
                    <tr>
                        <td colspan="100" class="text-left"><b>Patrimônio líquido</b></td>
                    </tr>
                    <tr>
                        <td class="text-left">Capital social</td>
                        <td>{{ numberDot($balance->share_capital) }}</td>
                    </tr>
                    <tr>
                        <td class="text-left">Lucros/Prejuízos acumulados</td>
                        <td>{{ numberDot($balance->accumulated_profits) }}</td>
                    </tr>
                    <tr>
                        <td class="text-left"><b>Total</b></td>
                        <td>{{ numberDot($balance->total) }}</td>
                    </tr>
                </tbody>
            </table>

        </div>

    </div>

</div>
@include('layouts.partials.fortes')
@stop

@section('js')
@include('layouts.partials.storage', ['key' => $balance->user_id])
@stop