@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>{{$cashflow->turn->simulation->name}} {{ isCoordinator() ? ' - '.$cashflow->company->name : '' }}</h1>
@stop

@section('content_title')
  <h3 class="box-title"><i class="fa fa-users"></i> Fluxo de Caixa - Rodada {{$cashflow->turn->month}}</h3> 
  <div class="pull-right">
    @can('is-company')
        <a href="{{ route('company.exportarfdc',['turn_id' => $cashflow->turn_id, 'id' => $cashflow->user_id]) }}" class="btn btn-xs btn-success hidden-print" title="Exportar para planilha Excel">EXPORTAR Excel</a> 
    @endcan
    @can('is-coordinator')
        <a href="{{ route('exportarfdc',['turn_id' => $cashflow->turn_id, 'id' => $cashflow->user_id]) }}" class="btn btn-xs btn-success hidden-print" title="Exportar para planilha Excel">EXPORTAR Excel</a> 
    @endcan
    <a href="javascript:window.print()" class="btn btn-xs btn-primary hidden-print">IMPRIMIR</a>
    <a href="{{ isCoordinator() ? route('simulation.turn.show', ['simulation' => $cashflow->turn->simulation_id, 'id' => $cashflow->turn_id]) : route('company') }}" class="btn btn-xs btn-default hidden-print">VOLTAR</a>
  </div> 
@stop

@section('content')
    <div class="box-body">

        <div class="row hidden-print">
                <div class="col-md-6">

                    <table class="table  table-bordered">
                        <thead>
                            <tr>
                                <th colspan="100" class="active">
                                    ENTRADAS
                                </th>
                            </tr>
                        </thead>
                        <tbody class="text-right">
                            <tr>
                                <td class="text-left"><b>Saldo inicial do período</b></td>
                                <td>{{ numberDot($cashflow->opening_balance) }}</td>
                            </tr>
                            <tr>
                                <td class="text-left">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Recebimento à vista</td>
                                <td>{{ numberDot($cashflow->receiving_sight) }}</td>
                            </tr>
                            <tr>
                                <td class="text-left">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Recebimento a prazo</td>
                                <td>{{ numberDot($cashflow->time_receipt) }}</td>
                            </tr>
                            <tr>
                                <td class="text-left">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Recebíveis antecipados</td>
                                <td>{{ numberDot($cashflow->anticipated_receivables) }}</td>
                            </tr>
                            <tr>
                                <td class="text-left">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Resgate da aplicação</td>
                                <td>{{ numberDot($cashflow->application_redemption) }}</td>
                            </tr>
                            <tr>
                                <td class="text-left">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Venda de máquinas</td>
                                <td>{{ numberDot($cashflow->sale_of_machines) }}</td>
                            </tr>
                            <tr>
                                <td class="text-left">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Receitas diversas</td>
                                <td>{{ numberDot($cashflow->miscellaneous_income) }}</td>
                            </tr>
                            <tr>
                                <td class="text-left">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Financiamento de máquinas</td>
                                <td>{{ numberDot($cashflow->machine_financing) }}</td>
                            </tr>
                            <tr>
                                <td class="text-left">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Empréstimo programado</td>
                                <td>{{ numberDot($cashflow->scheduled_loan) }}</td>
                            </tr>
                            <tr>
                                <td class="text-left">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Bônus</td>
                                <td>{{ numberDot($cashflow->tmp_bonus) }}</td>
                            </tr>
                            <tr>
                                <td class="text-left"><b>( + ) Total</b></td>
                                <td>{{ numberDot($cashflow->entry) }}</td>
                            </tr>
                        </tbody>
                    </table>

                    

                </div>
                <div class="col-md-6">

                    <table class="table  table-bordered">
                        <thead>
                            <tr>
                                <th colspan="100" class="active">
                                    SAÍDAS
                                </th>
                            </tr>
                        </thead>
                        <tbody class="text-right">
                            <tr>
                                <td class="text-left">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Folha de pagamento</td>
                                <td>{{ numberDot($cashflow->payroll) }}</td>
                            </tr>
                            <tr>
                                <td class="text-left">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Treinamento</td>
                                <td>{{ numberDot($cashflow->training) }}</td>
                            </tr>
                            <tr>
                                <td class="text-left">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Propaganda</td>
                                <td>{{ numberDot($cashflow->adv) }}</td>
                            </tr>
                            <tr>
                                <td class="text-left">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Despesas diversas</td>
                                <td>{{ numberDot($cashflow->expense) }}</td>
                            </tr>
                            <tr>
                                <td class="text-left">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Gastos com estocagem</td>
                                <td>{{ numberDot($cashflow->stocking_costs) }}</td>
                            </tr>
                            <tr>
                                <td class="text-left">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Atrasos Gerais</td>
                                <td>{{ numberDot($cashflow->general_delay) }}</td>
                            </tr>
                            <tr>
                                <td class="text-left">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Pagamento a fornecedores</td>
                                <td>{{ numberDot($cashflow->payment_suppliers) }}</td>
                            </tr>
                            <tr>
                                <td class="text-left">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Compra de máquinas</td>
                                <td>{{ numberDot($cashflow->purchase_machines) }}</td>
                            </tr>
                            <tr>
                                <td class="text-left">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Manutenção de máquinas</td>
                                <td>{{ numberDot($cashflow->machine_maintenance) }}</td>
                            </tr>
                            <tr>
                                <td class="text-left">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Amortização de empréstimos e financiamentos</td>
                                <td>{{ numberDot($cashflow->amortization_loans) }}</td>
                            </tr>
                            <tr>
                                <td class="text-left">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Juros bancários</td>
                                <td>{{ numberDot($cashflow->bir) }}</td>
                            </tr>
                            <tr>
                                <td class="text-left">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Impostos</td>
                                <td>{{ numberDot($cashflow->income_tax) }}</td>
                            </tr>
                            <tr>
                                <td class="text-left">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Aplicação</td>
                                <td>{{ numberDot($cashflow->application) }}</td>
                            </tr>
                            <tr>
                                <td class="text-left">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Despesas de manutenção predial</td>
                                <td>{{ numberDot($cashflow->tmp_despesa_mp) }}</td>
                            </tr>
                            <tr>
                                <td class="text-left"><b>( - ) Total</b></td>
                                <td>{{ numberDot($cashflow->exit) }}</td>
                            </tr>
                        </tbody>
                    </table>

                    <table class="table  table-bordered">
                        <thead>
                            <tr>
                                <th colspan="100" class="active">
                                    FLUXO DE CAIXA
                                </th>
                            </tr>
                        </thead>
                        <tbody class="text-right">
                           {{--  <tr>
                                <td class="text-left"><b>Saldo inicial do período</b></td>
                                <td>{{ numberDot($cashflow->opening_balance) }}</td>
                            </tr> --}}
                            <tr>
                                <td class="text-left"><b>Saldo final do período</b></td>
                                <td>{{ numberDot($cashflow->final_balance) }}</td>
                            </tr>
                            <tr>
                                <td class="text-left"><b>Limite empréstimo para o período {{$cashflow->turn->month + 1}}</b></td>
                                <td>{{ numberDot($limit_loan)}}</td>
                            </tr>
                        </tbody>
                    </table>

                </div>
            </div>

            <div class="row visible-print">
                <table class="table  table-bordered">
                    <thead>
                        <tr>
                            <th colspan="100" class="active">
                                ENTRADAS
                            </th>
                        </tr>
                    </thead>
                    <tbody class="text-right">
                        <tr>
                            <td class="text-left"><b>Saldo inicial do período</b></td>
                            <td>{{ numberDot($cashflow->opening_balance) }}</td>
                        </tr>
                        <tr>
                            <td class="text-left">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Recebimento à vista</td>
                            <td>{{ numberDot($cashflow->receiving_sight) }}</td>
                        </tr>
                        <tr>
                            <td class="text-left">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Recebimento a prazo</td>
                            <td>{{ numberDot($cashflow->time_receipt) }}</td>
                        </tr>
                        <tr>
                            <td class="text-left">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Recebíveis antecipados</td>
                            <td>{{ numberDot($cashflow->anticipated_receivables) }}</td>
                        </tr>
                        <tr>
                            <td class="text-left">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Resgate da aplicação</td>
                            <td>{{ numberDot($cashflow->application_redemption) }}</td>
                        </tr>
                        <tr>
                            <td class="text-left">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Venda de máquinas</td>
                            <td>{{ numberDot($cashflow->sale_of_machines) }}</td>
                        </tr>
                        <tr>
                            <td class="text-left">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Receitas diversas</td>
                            <td>{{ numberDot($cashflow->miscellaneous_income) }}</td>
                        </tr>
                        <tr>
                            <td class="text-left">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Financiamento de máquinas</td>
                            <td>{{ numberDot($cashflow->machine_financing) }}</td>
                        </tr>
                        <tr>
                            <td class="text-left">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Empréstimo programado</td>
                            <td>{{ numberDot($cashflow->scheduled_loan) }}</td>
                        </tr>
                        <tr>
                            <td class="text-left"><b>( + ) Total</b></td>
                            <td>{{ numberDot($cashflow->entry) }}</td>
                        </tr>
                    </tbody>
                </table>
                <table class="table  table-bordered">
                        <thead>
                            <tr>
                                <th colspan="100" class="active">
                                    SAÍDAS
                                </th>
                            </tr>
                        </thead>
                        <tbody class="text-right">
                            <tr>
                                <td class="text-left">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Folha de pagamento</td>
                                <td>{{ numberDot($cashflow->payroll) }}</td>
                            </tr>
                            <tr>
                                <td class="text-left">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Treinamento</td>
                                <td>{{ numberDot($cashflow->training) }}</td>
                            </tr>
                            <tr>
                                <td class="text-left">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Propaganda</td>
                                <td>{{ numberDot($cashflow->adv) }}</td>
                            </tr>
                            <tr>
                                <td class="text-left">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Despesas diversas</td>
                                <td>{{ numberDot($cashflow->expense) }}</td>
                            </tr>
                            <tr>
                                <td class="text-left">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Gastos com estocagem</td>
                                <td>{{ numberDot($cashflow->stocking_costs) }}</td>
                            </tr>
                            <tr>
                                <td class="text-left">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Atrasos Gerais</td>
                                <td>{{ numberDot($cashflow->general_delay) }}</td>
                            </tr>
                            <tr>
                                <td class="text-left">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Pagamento a fornecedores</td>
                                <td>{{ numberDot($cashflow->payment_suppliers) }}</td>
                            </tr>
                            <tr>
                                <td class="text-left">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Compra de máquinas</td>
                                <td>{{ numberDot($cashflow->purchase_machines) }}</td>
                            </tr>
                            <tr>
                                <td class="text-left">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Manutenção de máquinas</td>
                                <td>{{ numberDot($cashflow->machine_maintenance) }}</td>
                            </tr>
                            <tr>
                                <td class="text-left">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Amortização de empréstimos e financiamentos</td>
                                <td>{{ numberDot($cashflow->amortization_loans) }}</td>
                            </tr>
                            <tr>
                                <td class="text-left">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Juros bancários</td>
                                <td>{{ numberDot($cashflow->bir) }}</td>
                            </tr>
                            <tr>
                                <td class="text-left">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Impostos</td>
                                <td>{{ numberDot($cashflow->income_tax) }}</td>
                            </tr>
                            <tr>
                                <td class="text-left">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Aplicação</td>
                                <td>{{ numberDot($cashflow->application) }}</td>
                            </tr>
                            <tr>
                                <td class="text-left"><b>( - ) Total</b></td>
                                <td>{{ numberDot($cashflow->exit) }}</td>
                            </tr>
                        </tbody>
                    </table>
                    <br><br>
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th colspan="100" class="active">
                                    FLUXO DE CAIXA
                                </th>
                            </tr>
                        </thead>
                        <tbody class="text-right">
                            <tr>
                                <td class="text-left"><b>Saldo inicial do período</b></td>
                                <td>{{ numberDot($cashflow->opening_balance) }}</td>
                            </tr>
                            <tr>
                                <td class="text-left"><b>Saldo final do período</b></td>
                                <td>{{ numberDot($cashflow->final_balance) }}</td>
                            </tr>
                            <tr>
                                <td class="text-left"><b>Limite empréstimo para o período {{$cashflow->turn->month + 1}}</b></td>
                                <td>{{ numberDot($limit_loan)}}</td>
                            </tr>
                        </tbody>
                    </table>
            </div>
            
        </div>
        
    </div>  
    @include('layouts.partials.fortes')
@stop

@section('js')
    @include('layouts.partials.storage', ['key' => $cashflow->user_id])
@stop