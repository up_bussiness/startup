@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
<h1>Manual do jogo</h1>
<style>

</style>
@stop

@section('content_title')
<h3 class="box-title">Manual </h3>
@stop



@section('content')

<input type="hidden" id="jpath" name="jpath" value="{{ request()->url() }}">
<input type="hidden" id="xpath" name="xpath" value="{{ request()->getHost() }}">

@can('is-coordinator')
<input type="hidden" id="ypath" name="ypath" value="/coordinator/linkmanualjogo">
@endcan

@can('is-company')
<input type="hidden" id="ypath" name="ypath" value="/linkmanualjogo">
@endcan

<div id='pdf-viewer' style="width: 100%; height: 700px; overflow-y: scroll;">
</div>



@stop

@section('js')

<script src="https://cdnjs.cloudflare.com/ajax/libs/pdf.js/2.3.200/pdf.js"></script>

<script type="text/javascript">
    window.onload = function() {
        document.addEventListener('contextmenu', function(e) {
            e.preventDefault();
            if (event.keyCode == 123) {
                disableEvent(e);
            }
        }, false);

        function disableEvent(e) {
            if (e.stopPropagation) {
                e.stopPropagation();
            } else if (window.event) {
                window.event.cancelBubble = true;
            }
        }
    }
    $(document).contextmenu(function() {
        return false;
    });
    var thePdf = null;
    var scale = 1;


    $(document).ready(function(e) {
        Testex();
    });

    function CarregaPdf(url) {
        var loadingTask = pdfjsLib.getDocument(url);
        loadingTask.promise.then(function(pdf) {
            thePdf = pdf;
            viewer = document.getElementById('pdf-viewer');
            for (page = 1; page <= pdf.numPages; page++) {
                canvas = document.createElement('canvas');
                canvas.className = 'pdf-page-canvas';
                viewer.appendChild(canvas);
                renderPage(page, canvas);
            }
        });

        function renderPage(pageNumber, canvas) {
            thePdf.getPage(pageNumber).then(function(page) {
                viewport = page.getViewport(scale);
                canvas.height = viewport.height;
                canvas.width = viewport.width;
                page.render({
                    canvasContext: canvas.getContext('2d'),
                    viewport: viewport
                });
            });
        }

    }


    function Testex() {
        var x = $('#xpath').val();
        var z = $('#ypath').val();
        var j = $('#jpath').val();
        var pos = j.indexOf("http://");
        var inicio = '';
        if (pos == -1) {
            inicio = 'https://';
        } else {
            inicio = 'http://';
        }
        var y = inicio + x + z;
        //alert(pos);
        jQuery.ajax({
            type: "GET",
            url: y,
            success: function(data) {
                // alert(data);
                CarregaPdf(inicio + x + data);
                return (data);
            },
            error: function() {
                return "erro";
            }
        });
    }
</script>


@stop