@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>{{$ranking[0]->turn->simulation->name}}</h1>
@stop

@section('content_title')
  <h3 class="box-title"><i class="fa fa-users"></i> Ranking</h3> 
  <div class="pull-right">
      <a href="javascript:window.print()" class="btn btn-xs btn-primary hidden-print">IMPRIMIR</a>
      <a href="{{ isCoordinator() ? route('simulation.turn.show', ['simulation' => $ranking[0]->turn->simulation_id, 'id' => $ranking[0]->turn_id]) : route('company') }}" class="btn btn-xs btn-default hidden-print">VOLTAR</a>
  </div> 
@stop

@section('content')
<div class="col-md-8 col-md-offset-2">
  <div class="table-responsive">
   {{-- <table class="table  table-bordered">
      <thead>
        <tr>
          <th class="table-collapse text-center">#</th>
          <th class="text-center">Empresa</th>
          <th class="text-center">Patrimônio Líquido ($)</th>
        </tr>
      </thead>
      <tbody>
        @foreach($ranking as $k => $company)
        <tr class="{{ $company->user_id == auth()->user()->id ? 'info' : '' }}">
          <td class="text-center table-collapse">
            @if($k+1 == 1)
              <i class="fa fa-trophy" style="font-size: 24px; color: #e7e042"></i>
            @elseif ($k+1 == 2)
              <i class="fa fa-trophy" style="font-size: 24px; color: #d4dadd"></i>
            @elseif ($k+1 == 3)
              <i class="fa fa-trophy" style="font-size: 24px; color: #d37724"></i>
            @else
              <span style="font-size: 24px;">{{ $k+1 }}</span>
            @endif
          </td>
          <td class="text-center table-collapse">{{ $company->company->name }}</td>
          <td class="text-center table-collapse">{{ numberDot($company->net_worth) }},00</td>
        </tr>
        @endforeach
      </tbody>
    </table> --}}
    

    <table>
      <tr>
        <td style="padding: 15px">
            <table class="table table-bordered" style="font-size:80%">
                <thead>
                  <tr style="height: 70px">
                    <th class="text-center">Empresa</th>
                    <th class="text-center">Patrimônio Líquido ($)</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($ranking_pl as $k => $company)
                  <tr class="{{ $company->user_id == auth()->user()->id ? 'info' : '' }}">
                    <td height="10" class="text-center table-collapse">{{ $company->company->name }}</td>
                    <td height="10" class="text-center table-collapse">{{ $company->net_worth_pontos }}</td>
                  </tr>
                  @endforeach
                </tbody>
              </table> 
        </td>
        <td>
            <table class="table table-bordered" style="font-size:80%">
                <thead>
                  <tr style="height: 70px">
                    <th class="text-center">Empresa</th>
                    <th class="text-center">Participação de mercado (%)</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($ranking_pl as $k => $company)
                  <tr class="{{ $ranking_ms[$k]->company->id == auth()->user()->id ? 'info' : '' }}">
                    <td height="10" class="text-center table-collapse">{{ $ranking_ms[$k]->company->name }}</td>
                    <td height="10" class="text-center table-collapse">{{ $ranking_ms[$k]->market_share_pontos }}</td>
                  </tr>
                  @endforeach
                </tbody>
              </table> 
        </td>
        <td style="padding: 15px">
            <table class="table table-bordered" style="font-size:80%">
                <thead>
                  <tr style="height: 70px">
                    <th class="text-center">Empresa</th>
                    <th class="text-center">Empréstimos e financiamentos ($)</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($ranking_pl as $k => $company)
                  <tr class="{{ $ranking_ef[$k]->company->id == auth()->user()->id ? 'info' : '' }}">
                    <td height="10" class="text-center table-collapse">{{ $ranking_ef[$k]->company->name }}</td>
                    <td height="10" class="text-center table-collapse">{{ $ranking_ef[$k]->loans_financing_pontos }}</td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
        </td>
      </tr>
    </table>
    <div align="center">
      <script src="https://unpkg.com/@lottiefiles/lottie-player@latest/dist/lottie-player.js"></script>
      <lottie-player
          src="{{ asset('images/lf20_to9cPV.json') }}"  background="transparent"  speed="1"  style="width: 300px; height: 300px;"  loop autoplay >
      </lottie-player>
      </div>
    <table class="table  table-bordered">
        <thead>
          <tr>
            <th class="table-collapse text-center">
            <th class="text-center">Empresa</th>
            <th class="text-center">Pontuação total</th>
          </tr>
        </thead>
        <tbody>
          @php
              $tp=0;
              $tp1 = 0;
          @endphp
          @foreach($ranking as $k => $company)
          <tr class="{{ $company->user_id == auth()->user()->id ? 'info' : '' }}">
            <td class="text-center table-collapse">
              @if($k+1 == 1)
                @php
                  $tp=$company->total_pontos;
                  $tp1=$company->total_pontos;
                @endphp
                <i class="fa fa-trophy" style="font-size: 24px; color: #e7e042"></i>
              @elseif ($k+1 == 2)
                @if($tp==$company->total_pontos)
                  <i class="fa fa-trophy" style="font-size: 24px; color: #e7e042"></i>
                @else
                  <i class="fa fa-trophy" style="font-size: 24px; color: #d4dadd"></i>
                @endif
                @php
                  $tp=$company->total_pontos;
                @endphp
              @elseif ($k+1 == 3)
                @if($tp==$company->total_pontos)
                  @if($tp1==$company->total_pontos)
                    <i class="fa fa-trophy" style="font-size: 24px; color: #e7e042"></i>
                  @else
                    <i class="fa fa-trophy" style="font-size: 24px; color: #d4dadd"></i>
                  @endif
                @else
                  <i class="fa fa-trophy" style="font-size: 24px; color: #d37724"></i>
                @endif
              @else
                <span style="font-size: 24px;">{{ $k+1 }}</span>
              @endif
            </td>
            <td class="text-center table-collapse">{{ $company->company->name }}</td>
            <td class="text-center table-collapse">{{ $company->total_pontos }}</td>
          </tr>
          @endforeach
        </tbody>
      </table>
  {{--  <table class="table table-bordered" style="font-size:80%">
      <thead>
        <tr>
          <th class="text-center">Empresa</th>
          <th class="text-center">Patrimônio Líquido ($)</th>
          <th class="text-center">Empresa</th>
          <th class="text-center">Participação de mercado (%)</th>
          <th class="text-center">Empresa</th>
          <th class="text-center">Empréstimos e financiamentos ($)</th>
        </tr>
      </thead>
      <tbody>
        @foreach($ranking_pl as $k => $company)
        <tr class="{{ $company->user_id == auth()->user()->id ? 'info' : '' }}">
          <td height="10" class="text-center table-collapse">{{ $company->company->name }}</td>
          <td height="10" class="text-center table-collapse">{{ $company->net_worth_pontos }}</td>
          <td height="10" class="text-center table-collapse">{{ $ranking_ms[$k]->company->name }}</td>
          <td height="10" class="text-center table-collapse">{{ $ranking_ms[$k]->market_share_pontos }}</td>
          <td height="10" class="text-center table-collapse">{{ $ranking_ef[$k]->company->name }}</td>
          <td height="10" class="text-center table-collapse">{{ $ranking_ef[$k]->loans_financing_pontos }}</td>
        </tr>
        @endforeach
      </tbody>
    </table> --}}
  {{--  <table class="table  table-bordered">
      <thead>
        <tr>
          <th class="table-collapse text-center">#</th>
          <th class="text-center">Empresa</th>
          <th class="text-center">Participação de mercado (%)</th>
        </tr>
      </thead>
      <tbody>
        @foreach($ranking_ms as $k => $company)
        <tr class="{{ $company->user_id == auth()->user()->id ? 'info' : '' }}">
          <td class="text-center table-collapse">
            @if($k+1 == 1)
              <i class="fa fa-trophy" style="font-size: 24px; color: #e7e042"></i>
            @elseif ($k+1 == 2)
              <i class="fa fa-trophy" style="font-size: 24px; color: #d4dadd"></i>
            @elseif ($k+1 == 3)
              <i class="fa fa-trophy" style="font-size: 24px; color: #d37724"></i>
            @else
              <span style="font-size: 24px;">{{ $k+1 }}</span>
            @endif
          </td>
          <td class="text-center table-collapse">{{ $company->company->name }}</td>
          <td class="text-center table-collapse">{{ numberDot($company->market_share) }},00</td>
        </tr>
        @endforeach
      </tbody>
    </table>
    <table class="table table-condensed">
      <thead>
        <tr>
          <th class="table-collapse text-center">
          <th class="text-center">Empresa</th>
          <th class="text-center">Empréstimos e financiamentos ($)</th>
        </tr>
      </thead>
      <tbody>
        @foreach($ranking_ef as $k => $company)
        <tr class="{{ $company->user_id == auth()->user()->id ? 'info' : '' }}">
          <td class="text-center table-collapse">
            @if($k+1 == 1)
              <i class="fa fa-trophy" style="font-size: 24px; color: #e7e042"></i>
            @elseif ($k+1 == 2)
              <i class="fa fa-trophy" style="font-size: 24px; color: #d4dadd"></i>
            @elseif ($k+1 == 3)
              <i class="fa fa-trophy" style="font-size: 24px; color: #d37724"></i>
            @else
              <span style="font-size: 24px;">{{ $k+1 }}</span>
            @endif
          </td>
          <td class="text-center table-collapse">{{ $company->company->name }}</td>
          <td class="text-center table-collapse">{{ $company->loans_financing }}</td>
        </tr>
        @endforeach
      </tbody>
    </table>  --}}
  </div>
</div>
@stop

@section('js')
    @include('layouts.partials.storage')
@stop