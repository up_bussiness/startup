@extends('owner.layout')

@section('content')
<div class="page-header">
	<div class="row">
		<div class="col-md-6">
			<h3>LogisticsGame</h3>
			<p class="page-subtitle">Painel do desenvolvedor</p>
		</div>
		<div class="col-md-6 text-right">
			@if(!empty($instituition))
			<a href="{{ route('instituition.create') }}" class="btn btn-primary">Editar</a>
			<a href="{{ route('reset') }}" onclick="return confirm('Tem certeza que deseja resetar as licenças? É um tipo de decisão que deve ser tomada antes de iniciar o semestre.')" class="btn btn-danger">Resetar</a>
			@else
			<a href="{{ route('instituition.create') }}" class="btn btn-success">Instalar</a>
			@endif
		</div>
	</div>
	<hr>
	<p>Este painel é de uso exclusivo da empresa LogisticsGame. Aqui poderá ser gerenciado informações referente a instituição cadastrada. Informações como: número de licenças, logotipo, nome da instituição, email do responsável.</p>

	@if(!empty($instituition))
	<hr>
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="table-responsive">
				<table class="table table-striped table-bordered">
					<tbody>
						<tr>
							<td><img src="{{ asset('storage/'.LocalUploadUUID().'/'.'public/uploads'.'/'.$instituition->logo) }}" class="img-responsive" height="100">
							</td>
							<td style="vertical-align: middle">
								<h4>{{ $instituition->slug }} ({{ $instituition->name }})</h4>
							</td>
						</tr>
						<tr>
							<td>Responsável</td>
							<td>{{ $instituition->user->email }}</td>
						</tr>
						<tr>
							<td>Licenças</td>
							<td>{{ $instituition->license }}</td>
						</tr>
						<tr>
							<td>Licenças em uso</td>
							<td>{{ $licenses }}</td>
						</tr>
						<tr>
							<td>Licenças restantes</td>
							<td>{{ $instituition->license - $licenses }}</td>
						</tr>
						<tr>
							<td>Professores</td>
							<td>{{ $coordinators->count() }}</td>
						</tr>
						<tr>
							<td>Alunos</td>
							<td>{{ $students }}
								<a href="{{ route('owner.students') }}" class="btn btn-xs btn-info">Lista de alunos</a>
							</td>
							{{-- <td>{{ $students }} {!! $students > 0 ? '- <a href="'.route(" owner.students").'" class="btn btn-info btn-xs"><i class="fa fa-file-text-o"></i> Visualizar</a>' : '' !!}</td>--}}
						</tr>
					</tbody>
				</table>
			</div>
		</div>

		<div class="col-md-12">
			<hr>
		</div>

		<div class="col-md-8 col-md-offset-2">

			@include('layouts.partials.flash')

			<div class="table-responsive">
				<table class="table  table-bordered">
					<thead>
						<tr>
							<th>Nome</th>
							<th>Email</th>
							<th style="width:1%; white-space: nowrap">Licenças</th>
							<th class="text-center">Ações</th>
						</tr>
					</thead>
					<tbody>
						@forelse($coordinators as $c)
						<tr>
							<td>{{$c->name}}</td>
							<td>{{$c->email}}</td>
							<td class="text-center"><b>{{$c->license - $c->simulations->count()}} / {{$c->license}}</b></td>
							<td style="width:1%; white-space: nowrap">
								<a href="{{ route('owner.coordinator.manage', $c->id) }}" class="btn btn-info btn-xs"><i class="fa fa-gear"></i> Gerenciar</a>
								<a href="{{ route('owner.admin.destroy', $c->id) }}" class="btn btn-danger btn-xs" onclick="return confirm('Tem certeza que deseja excluír este coordenador?');"><i class="fa fa-trash"></i> Deletar</a>
							</td>
						</tr>
						@empty
						<tr>
							<td colspan="100" class="text-center">
								<i>A instituição ainda não possui nenhum professor criado.</i>
							</td>
						</tr>
						@endforelse
					</tbody>
				</table>
			</div>
		</div>
	</div>
	@endif
</div>
@stop