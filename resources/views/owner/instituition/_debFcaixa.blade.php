@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Listagem p/Análise</h1>
@stop

@section('content_title')
  <h3 class="box-title"><i class="fa fa-balance-scale"></i> Fluxo de Caixa - Todas as Rodadas</h3> 
  <div class="pull-right">
        <a href="{{ route('owner') }}" class="btn btn-xs btn-success hidden-print" title="Exportar para planilha Excel">EXPORTAR Excel</a>
        
      <a href="javascript:window.print()" class="btn btn-xs btn-primary hidden-print">IMPRIMIR</a>
      <a href="{{ route('owner') }}" class="btn btn-xs btn-default hidden-print">VOLTAR</a>
  </div> 
@stop

@section('content')
<div class="table-responsive">
<table class="table  table-bordered table-sm table-striped">
    <thead>
        <tr>
            <th colspan="100" class="active">
                ENTRADAS
            </th>
        </tr>
        <tr>
            <th>
                Rodada
            </th>
            <th>
                Saldo inicial
            </th>
            <th>
                Recebimento<br /> àVista
            </th>
            <th>
                Recebimento<br /> àPrazo
            </th>
            <th>
                Recebíveis<br />antecipados
            </th>
            <th>
                Resgate<br />Aplicação
            </th>
            <th>
                Venda<br />máquinas
            </th>
            <th>
                Receitas<br />diversas
            </th>
            <th>
                Financiamento<br />máquinas
            </th>
            <th>
                Empréstimo<br />programado
            </th>
            <th>
                Total
            </th>
        </tr>
    </thead>
    <tbody>
        @foreach ($fcaixa->sortBy('turn.month',SORT_NATURAL) as $bal)
			    <tr>
                        <td>{{ $bal->turn->month }}</td>
						<td>{{ $bal->opening_balance }}</td>
                        <td>{{ $bal->receiving_sight }}</td>
                        <td>{{ $bal->time_receipt }}</td>
                        <td>{{ $bal->anticipated_receivables }}</td>
                        <td>{{ $bal->application_redemption }}</td>
                        <td>{{ $bal->sale_of_machines }}</td>
                        <td>{{ $bal->miscellaneous_income }}</td>
                        <td>{{ $bal->machine_financing }}</td>
                        <td>{{ $bal->scheduled_loan }}</td>
                        <td>{{ $bal->entry }}</td>
			    </tr>
		@endforeach
    </tbody>
</table>
</div>

<div class="table-responsive">
<table class="table  table-bordered table-sm table-striped">
    <thead>
        <tr>
            <th colspan="100" class="active">
                SAÍDAS
            </th>
        </tr>
        <tr>
            <th>
                Rodada
            </th>
            <th>
                Folha<br />pagamento
            </th>
            <th>
                Treinamento
            </th>
            <th>
                Propaganda
            </th>
            <th>
                Despesas<br />diversas
            </th>
            <th>
                Gastos<br />estocagem
            </th>
            <th>
                Atrasos<br />Gerais
            </th>
            <th>
                Pagamento<br />fornecedores
            </th>
            <th>
                Compra<br />máquinas
            </th>
            <th>
                Manutenção<br />máquinas
            </th>
            <th>
                Amortização
            </th>
            <th>
                Juros <br />bancários
            </th>
            <th>
                Imposto<br />deRenda
            </th>
            <th>
                Aplicação
            </th>
            <th>
                Total
            </th>
            <th>
                Saldo
            </th>
        </tr>
    </thead>
    <tbody>
        @foreach ($fcaixa->sortBy('turn.month',SORT_NATURAL) as $bal)
			    <tr>
                        <td>{{ $bal->turn->month }}</td>
						<td>{{ $bal->payroll }}</td>
                        <td>{{ $bal->training }}</td>
                        <td>{{ $bal->adv }}</td>
                        <td>{{ $bal->expense }}</td>
                        <td>{{ $bal->stocking_costs }}</td>
                        <td>{{ $bal->general_delay }}</td>
                        <td>{{ $bal->payment_suppliers }}</td>
                        <td>{{ $bal->purchase_machines }}</td>
                        <td>{{ $bal->machine_maintenance }}</td>
                        <td>{{ $bal->amortization_loans }}</td>
                        <td>{{ $bal->bir }}</td>
                        <td>{{ $bal->income_tax }}</td>
                        <td>{{ $bal->application }}</td>
                        <td>{{ $bal->exit }}</td>
                        <td>{{ $bal->final_balance }}</td>
			    </tr>
		@endforeach
    </tbody>
</table>
</div>
@stop