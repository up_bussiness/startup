@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Listagem p/Análise</h1>
    <style>
        .break { page-break-before: always; }
        </style>
@stop

@section('content_title')
  <h3 class="box-title"><i class="fa fa-balance-scale"></i> DRE - Todas as Rodadas</h3> 
  <div class="pull-right">
        <a href="{{ route('owner') }}" class="btn btn-xs btn-success hidden-print" title="Exportar para planilha Excel">EXPORTAR Excel</a>
        
      <a href="javascript:window.print()" class="btn btn-xs btn-primary hidden-print">IMPRIMIR</a>
      <a href="{{ route('owner') }}" class="btn btn-xs btn-default hidden-print">VOLTAR</a>
  </div> 
@stop

@section('content')
<div class="table-responsive">
<table class="table  table-bordered table-sm table-striped">
    <thead>
        <tr>
            <th colspan="100" class="active">
                dre pag:1
            </th>
        </tr>
        <tr>
            <th>
                Rodada
            </th>
            <th>
                Preço
            </th>
            <th>
                
            </th>
            <th>
                CPV
            </th>
            <th>
                
            </th>
            <th>
                MP Quadro
            </th>
            <th>
                
            </th>
            <th>
                MP Kit 1
            </th>
            <th>
                
            </th>
            <th>
                MP Kit 2
            </th>
            <th>
                
            </th>
            <th>
                Estocagem<br />MP
            </th>
            <th>
                
            </th>
            <th>
              Estocagem<br />PA
          </th>
          <th>
              
          </th>
          <th>
            Folha<br />Produção
        </th>
        <th>
            
        </th>
        <th>
          Manutenção<br />Máquinas
      </th>
      <th>
          
      </th>
      <th>
        Treinamento<br />Produção
    </th>
    <th>
        
    </th>

        </tr>
    </thead>
    <tbody>
        @foreach ($dre->sortBy('turn.month',SORT_NATURAL) as $bal)
			    <tr>
                        <td>{{ $bal->turn->month }}</td>
						            <td>{{ $bal->price }}</td>
                        <td>{{ $bal->price_total }}</td>
                        <td>{{ $bal->cpv }}</td>
                        <td>{{ $bal->cpv_total }}</td>
                        <td>{{ $bal->raw_a }}</td>
                        <td>{{ $bal->raw_a_total }}</td>
                        <td>{{ $bal->raw_b }}</td>
                        <td>{{ $bal->raw_b_total }}</td>
                        <td>{{ $bal->raw_c }}</td>
                        <td>{{ $bal->raw_c_total }}</td>
                        <td>{{ $bal->stock_raw }}</td>
                        <td>{{ $bal->stock_raw_total }}</td>
                        <td>{{ $bal->stock_pa }}</td>
                        <td>{{ $bal->stock_pa_total }}</td>
                        <td>{{ $bal->salary_production }}</td>
                        <td>{{ $bal->salary_production_total }}</td>
                        <td>{{ $bal->machine_maintain }}</td>
                        <td>{{ $bal->machine_maintain_total }}</td>
                        <td>{{ $bal->training }}</td>
                        <td>{{ $bal->training_total }}</td>
			    </tr>
		@endforeach
    </tbody>
</table>
</div>

<div class="table-responsive">
    <table class="table  table-bordered table-sm table-striped">
        <thead>
            <tr>
                <th colspan="100" class="active">
                    dre pag:2
                </th>
            </tr>
            <tr>
                <th>
                    Rodada
                </th>
                <th>
                    Revalorização
                </th>
                <th>
                    Lucro bruto
                </th>
                <th>
                    
                </th>
                <th>
                    Despesas
                </th>
                <th>
                    
                </th>
                <th>
                    Folha<br />pgV
                </th>
                <th>
                    
                </th>
                <th>
                    Folha<br />pg Adm
                </th>
                <th>
                    
                </th>
                <th>
                    Propaganda
                </th>
                <th>
                    
                </th>
                <th>
                  Juros<br />aPrazo
              </th>
              <th>
                  
              </th>
              <th>
                Lucro<br />Oper.
            </th>
            <th>
                
            </th>
            <th>
                Depreciação<br />Máq.
          </th>
          <th>
              
          </th>
          <th>
            Depreciação<br />Pred.
        </th>
        <th>
            
        </th>
    
            </tr>
        </thead>
        <tbody>
            @foreach ($dre->sortBy('turn.month',SORT_NATURAL) as $bal)
                    <tr>
                            <td>{{ $bal->turn->month }}</td>
                            <td>{{ $bal->revalorizacao_stock }}</td>
                            <td>{{ $bal->gross_profit }}</td>
                            <td>{{ $bal->gross_profit_total }}</td>
                            <td>{{ $bal->income_sell }}</td>
                            <td>{{ $bal->income_sell_total }}</td>
                            <td>{{ $bal->salary_sellers }}</td>
                            <td>{{ $bal->salary_sellers_total }}</td>
                            <td>{{ $bal->salary_admin }}</td>
                            <td>{{ $bal->salary_admin_total }}</td>
                            <td>{{ $bal->adv }}</td>
                            <td>{{ $bal->adv_total }}</td>
                            <td>{{ $bal->difference }}</td>
                            <td>{{ $bal->difference_total }}</td>
                            <td>{{ $bal->operational_profit }}</td>
                            <td>{{ $bal->operational_profit_total }}</td>
                            <td>{{ $bal->machine_depreciation }}</td>
                            <td>{{ $bal->machine_depreciation_total }}</td>
                            <td>{{ $bal->building_depreciation }}</td>
                            <td>{{ $bal->building_depreciation_total }}</td>
                    </tr>
            @endforeach
        </tbody>
    </table>
    </div>

    <div class="table-responsive break">
        <table class="table  table-bordered table-sm table-striped">
            <thead>
                <tr>
                    <th colspan="100" class="active">
                        dre pag:3
                    </th>
                </tr>
                <tr>
                    <th>
                        Rodada
                    </th>
                    <th>
                        Outras<br />R/D
                    </th>
                    <th>
                        
                    </th>
                    <th>
                        Lucro<br />AntesIR
                    </th>
                    <th>
                        
                    </th>
                    <th>
                        Provisão<br />IR
                    </th>
                    <th>
                        
                    </th>
                    <th>
                        Prejuízo<br />VendaMaq
                    </th>
                    <th>
                        
                    </th>
                    <th>
                        Lucro<br />AposIR
                    </th>
                    <th>
                        
                    </th>
                    <th>
                        Margem<br />Lucro
                  </th>
                  <th>
                      
                  </th>
                  
        
                </tr>
            </thead>
            <tbody>
                @foreach ($dre->sortBy('turn.month',SORT_NATURAL) as $bal)
                        <tr>
                                <td>{{ $bal->turn->month }}</td>
                                <td>{{ $bal->others_value }}</td>
                                <td>{{ $bal->others_value_total }}</td>
                                <td>{{ $bal->profit_before_ir }}</td>
                                <td>{{ $bal->profit_before_ir_total }}</td>
                                <td>{{ $bal->ir }}</td>
                                <td>{{ $bal->ir_total }}</td>
                                <td>-</td>
                                <td>{{ $bal->loss_on_sale }}</td>
                                <td>{{ $bal->profit_after_ir }}</td>
                                <td>{{ $bal->profit_after_ir_total }}</td>
                                <td>{{ $bal->profit_margin }}</td>
                                <td>{{ $bal->profit_margin }}</td>

                        </tr>
                @endforeach
            </tbody>
        </table>
        </div>
@stop