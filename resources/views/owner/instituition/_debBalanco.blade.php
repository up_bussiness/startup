@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Listagem p/Análise</h1>
@stop

@section('content_title')
  <h3 class="box-title"><i class="fa fa-balance-scale"></i> Balanço Patrimonial - Todas as Rodadas</h3> 
  <div class="pull-right">
        <a href="{{ route('owner') }}" class="btn btn-xs btn-success hidden-print" title="Exportar para planilha Excel">EXPORTAR Excel</a>
        
      <a href="javascript:window.print()" class="btn btn-xs btn-primary hidden-print">IMPRIMIR</a>
      <a href="{{ route('owner') }}" class="btn btn-xs btn-default hidden-print">VOLTAR</a>
  </div> 
@stop

@section('content')
<table class="table  table-bordered table-sm table-striped">
    <thead>
        <tr>
            <th colspan="100" class="active">
                ATIVO
            </th>
        </tr>
        <tr>
            <th>
                Rodada
            </th>
            <th>
                Caixa
            </th>
            <th>
                Aplicação
            </th>
            <th>
                Clientes
            </th>
            <th>
                Est.PA
            </th>
            <th>
                Est.Quadro
            </th>
            <th>
                Est.Kit 1
            </th>
            <th>
                Est.Kit 2
            </th>
            <th>
                Máquinas
            </th>
            <th>
                Depreciação
            </th>
            <th>
                Prédios
            </th>
            <th>
                Depreciação
            </th>
            <th>
                Total
            </th>
        </tr>
    </thead>
    <tbody>
        @foreach ($balanco->sortBy('turn.month',SORT_NATURAL) as $bal)
			    <tr>
                        <td>{{ $bal->turn->month }}</td>
						<td>{{ $bal->active_cash }}</td>
                        <td>{{ $bal->active_application }}</td>
                        <td>{{ $bal->active_clients }}</td>
                        <td>{{ $bal->active_stock_product }}</td>
                        <td>{{ $bal->active_raw_a }}</td>
                        <td>{{ $bal->active_raw_b }}</td>
                        <td>{{ $bal->active_raw_c }}</td>
                        <td>{{ $bal->active_machines }}</td>
                        <td>{{ $bal->active_depreciation_machine }}</td>
                        <td>{{ $bal->active_buildings }}</td>
                        <td>{{ $bal->active_depreciation_buildings }}</td>
                        <td>{{ $bal->active_total }}</td>
			    </tr>
		@endforeach
    </tbody>
</table>
<div class="table-responsive">
<table class="table  table-bordered table-sm table-striped">
    <thead>
        <tr>
            <th colspan="100" class="active">
                PASSIVO
            </th>
        </tr>
        <tr>
            <th>
                Rodada
            </th>
            <th>
                Forn.Vencer
            </th>
            <th>
                Forn<br />EmAtraso
            </th>
            <th>
                Contas<br />EmAtraso
            </th>
            <th>
                IR aPagar
            </th>
            <th>
                Emp.Fin.<br />aVencer
            </th>
            <th>
                Emp.Fin.<br />EmAtraso
            </th>
            <th>
                Juros<br />Aproriar
            </th>
            <th>
                Capital<br />Social
            </th>
            <th>
                Lucros<br />Prejuízos
            </th>
            <th>
                Total
            </th>
        </tr>
    </thead>
    <tbody>
        @foreach ($balanco->sortBy('turn.month',SORT_NATURAL) as $bal)
			    <tr>
                        <td>{{ $bal->turn->month }}</td>
						<td>{{ $bal->passive_suppliers_win }}</td>
                        <td>{{ $bal->passive_suppliers_arrears }}</td>
                        <td>{{ $bal->passive_overdue_account }}</td>
                        <td>{{ $bal->passive_tax_pay }}</td>
                        <td>{{ $bal->passive_loans_financing_mature }}</td>
                        <td>{{ $bal->passive_loans_financing_interest_arrears }}</td>
                        <td>{{ $bal->juros_apropriar }}</td>
                        <td>{{ $bal->share_capital }}</td>
                        <td>{{ $bal->accumulated_profits }}</td>
                        <td>{{ $bal->total }}</td>
			    </tr>
		@endforeach
    </tbody>
</table>
</div>
@stop