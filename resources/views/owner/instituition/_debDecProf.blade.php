@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Listagem p/Análise</h1>
@stop

@section('content_title')
  <h3 class="box-title"><i class="fa fa-balance-scale"></i> Decisão Professor - Todas as Rodadas</h3> 
  <div class="pull-right">
        <a href="{{ route('owner') }}" class="btn btn-xs btn-success hidden-print" title="Exportar para planilha Excel">EXPORTAR Excel</a>
        
      <a href="javascript:window.print()" class="btn btn-xs btn-primary hidden-print">IMPRIMIR</a>
      <a href="{{ route('owner') }}" class="btn btn-xs btn-default hidden-print">VOLTAR</a>
  </div> 
@stop

@section('content')
<div class="table-responsive">
<table class="table  table-bordered table-sm table-striped">
    <thead>
        <tr>
            <th colspan="100" class="active">
                
            </th>
        </tr>
        <tr>
            <th>
                Rodada
            </th>
            <th>
                Macro setor
            </th>
            <th>
                Inflação
            </th>
            <th>
                TaxaBásica<br />Juros
            </th>
            <th>
                Juros<br />Forn.
            </th>
            <th>
                IR
            </th>
            <th>
                Quadro
            </th>
            <th>
                Kit 1
            </th>
            <th>
                Kit 2
            </th>
            <th>
                LP<br />Pequena
            </th>
            <th>
                LP<br />Média
            </th>
            <th>
                LP<br />Grande
            </th>
            <th>
                Radio
            </th>
            <th>
                Jornal
            </th>
            <th>
                Mídias
            </th>
            <th>
                Outdoor
            </th>
            <th>
                TV
            </th>
            <th>
                Id
            </th>
        </tr>
    </thead>
    <tbody>
        @foreach ($decprof as $bal)
			    <tr>
                        <td>{{ $bal->month }}</td>
						<td>{{ $bal->decisionCoordinator->macro_industry }}</td>
                        <td>{{ $bal->decisionCoordinator->inflation }}</td>
                        <td>{{ $bal->decisionCoordinator->bir }}</td>
                        <td>{{ $bal->decisionCoordinator->interest_providers }}</td>
                        <td>{{ $bal->decisionCoordinator->income_tax }}</td>
                        <td>{{ $bal->decisionCoordinator->raw_material_a }}</td>
                        <td>{{ $bal->decisionCoordinator->raw_material_b }}</td>
                        <td>{{ $bal->decisionCoordinator->raw_material_c }}</td>
                        <td>{{ $bal->decisionCoordinator->lp_small }}</td>
                        <td>{{ $bal->decisionCoordinator->lp_medium }}</td>
                        <td>{{ $bal->decisionCoordinator->lp_large }}</td>
                        <td>{{ $bal->decisionCoordinator->adv_radio }}</td>
                        <td>{{ $bal->decisionCoordinator->adv_journal }}</td>
                        <td>{{ $bal->decisionCoordinator->adv_social }}</td>
                        <td>{{ $bal->decisionCoordinator->adv_outdoor }}</td>
                        <td>{{ $bal->decisionCoordinator->adv_tv }}</td>
                        <td>{{ $bal->decisionCoordinator->id }}</td>
			    </tr>
		@endforeach
    </tbody>
</table>
</div>

@stop