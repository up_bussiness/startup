@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1></h1>
@stop

@section('content_title')
  <h3 class="box-title"><i class="fa fa-balance-scale"></i> Relatórios e Processamentos da Simulação : <b> {{$simNome}} </b></h3>
  <div class="pull-right">
      <a href="{{ route('owner') }}" class="btn btn-xs btn-default hidden-print">VOLTAR</a>
  </div> 
@stop


@section('content')
<div class="col-md-6">
    <ul class="nav nav-tabs">
        <li class="nav-item">
          <a class="nav-link active" data-toggle="tab" href="#home">Balanço</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" data-toggle="tab" href="#menu1">DRE</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" data-toggle="tab" href="#menu2">Fluxo de Caixa</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" data-toggle="tab" href="#menu3">Copiar simulação</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" data-toggle="tab" href="#menu4">Informações</a>
        </li>
      </ul>
    </div>      
      <!-- Tab panes -->
      <div class="tab-content">
        <div class="tab-pane active" id="home">
            {!! Form::open(['route' => 'owner.debug.balanco']) !!}
                {!! Form::hidden('turn_id', 4) !!}
                <div class="form-group">
                  {!! Form::select('id',  $empresas, null, ['class' => 'form-control', 'required']); !!}
                </div>
                {!! Form::submit('Processar', ['class' => 'btn btn-warning']); !!}
            {!! Form::close() !!}
        </div>

        <div class="tab-pane fade" id="menu1">
          {!! Form::open(['route' => 'owner.debug.dre']) !!}
            {!! Form::hidden('turn_id', 4) !!}
            <div class="form-group">
              {!! Form::select('id',  $empresas, null, ['class' => 'form-control', 'required']); !!}
            </div>
            {!! Form::submit('Processar', ['class' => 'btn btn-primary']); !!}
          {!! Form::close() !!}
        </div>
        
        <div class="tab-pane container fade" id="menu2">
          {!! Form::open(['route' => 'owner.debug.fcaixa']) !!}
            {!! Form::hidden('turn_id', 4) !!}
            <div class="form-group">
              {!! Form::select('id',  $empresas, null, ['class' => 'form-control', 'required']); !!}
            </div>
            {!! Form::submit('Processar', ['class' => 'btn btn-default']); !!}
          {!! Form::close() !!}
        </div>

        <div class="tab-pane container fade" id="menu3">
          {!! Form::open(['route' => 'owner.debug.copiar']) !!}
            {!! Form::hidden('simId', $simIdcorrente) !!}
            <div class="form-group">
              {!! Form::select('id',  $simulations, null, ['class' => 'form-control', 'required']); !!}
            </div>
            {!! Form::submit('passo 1 : Copiar', ['class' => 'btn btn-default']); !!}
          {!! Form::close() !!}
          <br />
          <b>Atualizar e-mails, acrescentando ou tirando X- antes do email :</b>
          {!! Form::open(['route' => 'owner.debug.emails']) !!}
            {!! Form::hidden('simId', $simIdcorrente) !!}
            <div class="form-group">
              {!! Form::select('tipo', ['A' => 'Acrescentar', 'T' => 'Tirar']); !!}
            </div>
            {!!Form::text('letra'); !!}
            {!! Form::submit('Processar', ['class' => 'btn btn-default']); !!}
          {!! Form::close() !!}
        </div>
        
        <div class="tab-pane container fade" id="menu4">
          tabela Users : {{ $tabelasQR['Users'] }}
        </div>
      </div>

@stop