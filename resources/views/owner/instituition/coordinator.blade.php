@extends('owner.layout')

@section('content')
	<div class="page-header">
		<div class="row">
			<div class="col-md-6">
				<h3>LogisticsGame</h3>
				<p class="page-subtitle">Gerenciamento de professor</p>
			</div>
			<div class="col-md-6 text-right">
				<a href="{{ route('owner') }}" class="btn btn-default">Voltar</a>
			</div>
		</div>
	    <hr>
    	@include('owner.instituition._simulations')
    	<br>
    	<br>
    </div>
@stop