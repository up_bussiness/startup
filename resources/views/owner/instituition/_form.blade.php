@include('layouts.partials.flash')

@if(!empty($instituition))
  {!! Form::model($instituition, ['route' => ['instituition.update', $instituition->id], 'method' => 'put', 'class'=>'form-horizontal', 'files' => true]) !!}

  {!! Form::hidden('instituition_id', $instituition->id) !!}
@else
  {!! Form::open(['route' => 'instituition.store', 'class'=>'form-horizontal', 'files' => true]) !!}
@endif

<div class="form-group">
  {!! Form::label('name', 'Instituição', ['class' => 'control-label col-sm-3 col-md-offset-2']) !!}
  <div class="col-sm-3 input-group">
    <div class="input-group-addon"><i class="fa fa-institution"></i></div>
    {!! Form::text('name', null, ['class' => 'form-control']); !!}
  </div>
</div>

<div class="form-group">
  {!! Form::label('slug', 'Abreviação', ['class' => 'control-label col-sm-3 col-md-offset-2']) !!}
  <div class="col-sm-3 input-group">
    <div class="input-group-addon"><i class="fa fa-bookmark"></i></div>
    {!! Form::text('slug', null, ['class' => 'form-control']); !!}
  </div>
</div>

<div class="form-group">
  {!! Form::label('logo', 'Logo', ['class' => 'control-label col-sm-3 col-md-offset-2']) !!}
  <div class="col-sm-3 input-group">
    <div class="input-group-addon"><i class="fa fa-file-photo-o"></i></div>
    {!! Form::file('logo', ['class' => 'form-control']); !!}
  </div>
</div>

<div class="form-group">
  {!! Form::label('license', 'Licenças', ['class' => 'control-label col-sm-3 col-md-offset-2']) !!}
  <div class="col-sm-3 input-group">
    <div class="input-group-addon"><i class="fa fa-cubes"></i></div>
    {!! Form::number('license',  null, ['class' => 'form-control']); !!}
  </div>
</div>

<div class="form-group">
  {!! Form::label('email', 'Email', ['class' => 'control-label col-sm-3 col-md-offset-2']) !!}
  <div class="col-sm-3 input-group">
    <div class="input-group-addon"><i class="fa fa-envelope-o"></i></div>
    {!! Form::text('email', !empty($instituition) ? $instituition->user->email : null, ['class' => 'form-control']); !!}
  </div>
</div>

<div class="form-group">
  {!! Form::label('password', 'Senha', ['class' => 'control-label col-sm-3 col-md-offset-2']) !!}
  <div class="col-sm-3 input-group">
    <div class="input-group-addon"><i class="fa fa-unlock-alt"></i></div>
    {!! Form::text('password', null, ['class' => 'form-control']); !!}
  </div>
</div>

@if(!empty($instituition))
  <p class="text-center"><b>Senha atual: {{ $instituition->user->password_plain }}</b></p>
  {!! Form::hidden('coordinator_id', $instituition->user->id) !!}
@endif

<div class="col-md-offset-5 col-md-3 text-center">
    {!! Form::submit(!empty($instituition) ? 'Atualizar' : 'Criar', ['class' => 'btn btn-success']); !!}
</div>

{!! Form::close() !!}