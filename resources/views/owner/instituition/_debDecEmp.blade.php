@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Listagem p/Análise</h1>
@stop

@section('content_title')
  <h3 class="box-title"><i class="fa fa-balance-scale"></i> Decisão Empresa - Todas as Rodadas</h3> 
  <div class="pull-right">
        <a href="{{ route('owner') }}" class="btn btn-xs btn-success hidden-print" title="Exportar para planilha Excel">EXPORTAR Excel</a>
        
      <a href="javascript:window.print()" class="btn btn-xs btn-primary hidden-print">IMPRIMIR</a>
      <a href="{{ route('owner') }}" class="btn btn-xs btn-default hidden-print">VOLTAR</a>
  </div> 
@stop

@section('content')
<div class="table-responsive">
<table class="table  table-bordered table-sm table-striped">
    <thead>
        <tr>
            <th colspan="100" class="active">
                
            </th>
        </tr>
        <tr>
            <th>
                Rodada
            </th>
            <th>
                Preço
            </th>
            <th>
                Prazo
            </th>
            <th>
                Quadro
            </th>
            <th>
                Kit 1
            </th>
            <th>
                Kit 2
            </th>
            <th>
                Nível
            </th>
            <th>
                Juros<br />Venda
            </th>
            <th>
                Kit 2
            </th>
            <th>
                Nível
            </th>
            <th>
                ID
            </th>
            <th>
                LP<br />Grande
            </th>
            <th>
                Radio
            </th>
            <th>
                Jornal
            </th>
            <th>
                Mídias
            </th>
            <th>
                Outdoor
            </th>
            <th>
                TV
            </th>
            <th>
                Id
            </th>
        </tr>
    </thead>
    <tbody>
        @foreach ($decemp as $bal)
			    <tr>
                        <td>{{ $bal->month }}</td>
						<td>{{ $bal->decisionCompany[0]->price }}</td>
                        <td>{{ $bal->decisionCompany[0]->term }}</td>
                        <td>{{ $bal->decisionCompany[0]->scheduled_rma }}</td>
                        <td>{{ $bal->decisionCompany[0]->scheduled_rmb }}</td>
                        <td>{{ $bal->decisionCompany[0]->scheduled_rmc }}</td>
                        <td>{{ $bal->decisionCompany[0]->activity_level }}</td>
                        <td>{{ $bal->decisionCompany[0]->term_interest }}</td>
                        <td>{{ 0 }}</td>
                        <td>{{ 0 }}</td>
                        <td>{{ $bal->decisionCompany[0]->id }}</td>
                        <td>{{ 0 }}</td>
                        <td>{{ $bal->decisionCompany[0]->adv_radio }}</td>
                        <td>{{ $bal->decisionCompany[0]->adv_journal }}</td>
                        <td>{{ $bal->decisionCompany[0]->adv_social }}</td>
                        <td>{{ $bal->decisionCompany[0]->adv_outdoor }}</td>
                        <td>{{ $bal->decisionCompany[0]->adv_tv }}</td>
                        <td>{{ 0 }}</td>
			    </tr>
		@endforeach
    </tbody>
</table>
</div>

@stop