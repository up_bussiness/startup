<div class="row">

<div class="col-md-6 col-md-offset-3">

  @include('layouts.partials.flash')

  <h4>{{ $coordinator->name }}</h4>

        {!! Form::model($coordinator, ['route' => 'owner.coordinator.update', 'class'=>'form-horizontal', 'method' => 'PUT']) !!}

          <div class="form-group">
            {!! Form::label('license', 'Licença', ['class' => 'control-label col-sm-3']) !!}
            <div class="col-sm-7 input-group">
              <div class="input-group-addon"><i class="fa fa-ticket"></i></div>
              {!! Form::selectRange('license', 1, (($instituitionVC->license - $licenses) + $coordinator->license), null, ['class' => 'form-control', 'required']); !!}
            </div>
          </div>

          <div class="text-center">
            {!! Form::submit('Atualizar', ['class' => 'btn btn-success']); !!}
        </div>

        {!! Form::hidden('coordinator_id', $coordinator->id) !!}

      {!! Form::close() !!}

  <hr>

  <div class="table-responsive">
    <table class="table  table-bordered">
      <thead>
        <tr>
          <th>Simulação</th>
          <th>Rodada</th>
          <th>Ações</th>
        </tr>
      </thead>
      <tbody>
        @forelse($coordinator->simulations as $simulation)
        <tr>
            <td>{{$simulation->name}}</td>
            <td class="text-center">{{$simulation->turns->count()}}</td>
            <td style="width:1%; white-space: nowrap" >
              {{ Form::open(['route' => ['owner.simulation.destroy', $simulation->id], 'method' => 'delete', 'style'=>'display: inline']) }}
                  <button type="submit" onclick="return confirm('Tem certeza que deseja excluir esta simulação?');" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i> Deletar</button>
              {{ Form::close() }}
            </td>
        </tr>
        @empty
        <tr>
          <td colspan="100" class="text-center">
            <i>O professor ainda não possui nenhuma simulação criada.</i>
          </td>
        </tr>
        @endforelse
      </tbody>
    </table>
  </div>

</div>