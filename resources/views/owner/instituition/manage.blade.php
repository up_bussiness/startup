@extends('owner.layout')

@section('content')
	<div class="page-header">
		<div class="row">
			<div class="col-md-6">
				<h3>LogisticsGame</h3>
				<p class="page-subtitle">Instalação</p>
			</div>
			<div class="col-md-6 text-right">
				<a href="{{ route('owner') }}" class="btn btn-default">Voltar</a>
				@if (!empty($instituition))
				<p><small>Licenças em atribuídas: {{ $licenses }}</small></p>
				@endif
			</div>
		</div>
	    <hr>
    	@include('owner.instituition._form')
    	<br>
    	<br>
    </div>
@stop