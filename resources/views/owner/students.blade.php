@extends('owner.layout')

@section('content')
<div class="page-header">
	<div class="row">
		<div class="col-md-6">
			<h3>LogisticsGame</h3>
			<p class="page-subtitle">Lista de alunos</p>
		</div>
		<div class="col-md-6 text-right no-print">
			<a href="javascript:window.print()" class="btn btn-primary">Imprimir</a>
			<a href="{{ route('owner') }}" class="btn btn-default">Voltar</a> 
		</div>
	</div>

	@if(!empty($students))
	<hr class="no-print">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="table-responsive">
				<table class="table table-striped table-bordered">
					<thead>
						<tr>
							<th class="text-center">#</th>
							<th class="text-center">Nome</th>
							<th class="text-center">Email</th>
							<th class="text-center">Telefone</th>
							<th class="text-center">Nasc.</th>
							<th class="text-center"><i class="fa fa-instagram"></i></th>
							<th class="text-center"><i class="fa fa-facebook"></i></th>
						</tr>
					</thead>
					<tbody>
						@foreach($students as $student)
						<tr>
							<td class="table-collapse text-center">{{ $student->registration }}</td>
							<td class="table-collapse text-center name">{{ $student->name }}</td>
							<td class="table-collapse text-center">{{ $student->email }}</td>
							<td class="table-collapse text-center">{{ $student->phone }}</td>
							<td class="table-collapse text-center">{{ !empty($student->date) ? dateBRL($student->date) : ' - ' }}</td>
							<td class="table-collapse text-center">{{ !empty($student->insta) ? $student->insta : ' - ' }}</td>
							<td class="table-collapse text-center">{{ !empty($student->face) ? $student->face : ' - ' }}</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
    @endif
</div>
@stop