<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<title>Logistics Game - Gerenciamento</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="{{ asset('css/setup.css') }}">
	<style>
		.table-collapse {
			width: 1%;
			white-space: nowrap;
		}
		@media print
		{    
		    .no-print, .no-print *
		    {
		        display: none !important;
		    }
		    .table-collapse {
		    	font-size:12px;
		    	width: unset;
		    	white-space: unset;
		    }
		    .table-responsive {
		        min-height: unset;
		        overflow-x: unset;
		    }
		    .name {
		    	min-width:150px;
		    }
		}
	</style>
</head>
<body>
	<div class="container">
		<div class="row">
			<div class="col-md-4 col-md-offset-4 text-center no-print">
				<a href="{{ route('owner') }}"><img src="{{ asset('images/logo.png') }}" width="280"></a>
			</div>
			<div class="col-md-12">
				@yield('content')
			</div>
			<div class="col-md-12 text-right no-print">
				<a href="#"
				   onclick="event.preventDefault(); document.getElementById('logout-form').submit();"
				   class="btn btn-danger" 
				>
				    <i class="fa fa-fw fa-power-off"></i> {{ trans('adminlte::adminlte.log_out') }}
				</a>
				<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
				    {{ csrf_field() }}
				</form>
			</div>
		</div>
	</div>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</body>
</html>