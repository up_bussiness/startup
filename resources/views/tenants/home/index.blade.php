@extends('tenants.layouts.base')

@section('content')

<br />
<div class="card text-black mb-3" style="max-width: 18rem;background-color: #e4ae8c;">
        <div class="row no-gutters">
          <div class="col-md-4">
            <div style="padding: 30px 0;text-align: center;color :#ca6b2f">
                <i class="fa fa-4x fa-archive"></i>
            </div>
          </div>
          <div class="col-md-8" align="right">
            <div class="card-body">
              <div class="card-text">Quantidade</div>
              <div class="card-title"><h3>{{ $clientes->count() }}</h3></div>
            </div>
          </div>
        </div>
        <div class="card-footer bg-transparent">
            <small class="text-muted">Quantidade de clientes</small>
        </div>
</div>
@endsection