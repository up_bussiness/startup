@extends('tenants.layouts.base')

@section('content')

<h1>
    Empresas
    <a href="{{ route('cliente.create') }}" class="btn btn-primary">
        <i class="fa fa-plus-square"></i>
    </a>
</h1>

@include('tenants.includes.alerts')

<ul class="media-list">
    @forelse($clientes as $cliente)
    <li class="media">
        <div class="media-body">
            <span class="text-muted pull-right">
                <small class="text-muted">{{ $cliente->created_at->format('d/m/Y') }}</small>
            </span>
            <strong class="text-success">{{ $cliente->domain }}</strong>
            <p>
                {{ $cliente->name }}
                <br>
                <a href="{{ route('cliente.show', $cliente->domain) }}">Detalhes</a> |
                <a href="{{ route('cliente.edit', $cliente->domain) }}">Editar</a>
            </p>
        </div>
    </li>
    <hr>
    @empty
    <li class="media">
        <p>Nenhuma empresa cadastrada!</p>
    </li>
    @endforelse

    {!! $clientes->links() !!}
</ul>
 
@endsection 