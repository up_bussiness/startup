@extends('tenants.layouts.base')

@section('content')

<h1>Cadastrar nova empresa</h1>

<form action="{{ route('cliente.store') }}" method="post">
    @include('tenants.clientes._partials.form')
</form>

@endsection