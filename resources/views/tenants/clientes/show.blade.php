@extends('tenants.layouts.base')

@section('content')

<h1>Detalhes da empresa <b>{{ $cliente->name }}</b></h1>

<ul>
    <li><strong>Nome:</strong> {{ $cliente->name }}</li>
    <li><strong>Domínio:</strong> {{ $cliente->domain }}</li>
    <li><strong>Database:</strong> {{ $cliente->bd_database }}</li>
    <li><strong>Host:</strong> {{ $cliente->bd_hostname }}</li>
    <li><strong>Usuário:</strong> {{ $cliente->bd_username }}</li>
    <li><strong>Senha:</strong></li>
</ul>

<hr>

<form action="{{ route('cliente.destroy', $cliente->id) }}" method="post">
    @csrf

    <input type="hidden" name="_method" value="DELETE">

    <button type="submit" class="btn btn-danger">Deletar Empresa: {{ $cliente->name }}</button>
</form>


@endsection