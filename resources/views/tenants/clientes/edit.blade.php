@extends('tenants.layouts.base')

@section('content')

<h1>Detalhes da empresa <b>{{ $cliente->name }}</b></h1>

<form action="{{ route('cliente.update', $cliente->id) }}" method="post">
    <input type="hidden" name="_method" value="PUT">

    @include('tenants.clientes._partials.form')
</form>

@endsection