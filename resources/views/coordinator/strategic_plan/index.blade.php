@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
<h1>Planejamento Estratégico</h1>
@stop

@section('content_title')
<h3 class="box-title"><i class="fa fa-users"></i> Visualizar</h3>
<div class="pull-right">
  <a href="{{ route('simulation.show', $simulation) }}" class="btn btn-xs btn-default">VOLTAR</a>
</div>
@stop

@section('content')

<div class="row control">
  <div class="col-md-4 col-md-offset-4">
    <div class="form-group">
      <div class="col-sm-12 input-group">
        <div class="input-group-addon"><b>PLANEJAMENTO</b></div>
        {!! Form::select('planning_select', $companies, null, ['class' => 'form-control', 'id' => 'planning_select']); !!}
      </div>
    </div>
  </div>
</div>

<div id="team-box" class="col-md-12">
  <div class="table-responsive">
    <table id="team" class="table table-striped table-bordered">
      <thead>
        <tr>
          <th>Matrícula</th>
          <th>Nome</th>
          <th>Email</th>
          <th>Cargo</th>
          <th>Telefone</th>
          <th>Nascimento</th>
          <th>Facebook</th>
          <th>Instagram</th>
        </tr>
      </thead>
      <tbody>
      </tbody>
    </table>
  </div>
</div>
<hr>
<div id="planning">
  <div class="row">
    <div class="col-md-2 col-md-offset-5 text-center">
      <img src="#" class="img-circle img-responsive avatar" style="display: inline">
    </div>
  </div>
  <br>
  <div class="row">
    <div class="col-md-6 col-md-offset-3">
      <div class="table-responsive">
        <table class="table table-striped table-bordered">
          <tr>
            <td class="table-collapse text-right"><b>Nome fantasia:</b></td>
            <td class="name">XXX</td>
          </tr>
          <tr>
            <td class="table-collapse text-right"><b>Slogan:</b></td>
            <td class="slogan">XXX</td>
          </tr>
          <tr>
            <td class="table-collapse text-right"><b>Missão:</b></td>
            <td class="mission">XXX</td>
          </tr>
          <tr>
            <td class="table-collapse text-right"><b>Visão:</b></td>
            <td class="view">XXX</td>
          </tr>
          <tr>
            <td class="table-collapse text-right"><b>Valores:</b></td>
            <td class="values">XXX</td>
          </tr>
          <tr>
            <td class="table-collapse text-right"><b>Fraquezas:</b></td>
            <td class="weaknesses">XXX</td>
          </tr>
          <tr>
            <td class="table-collapse text-right"><b>Ameaças:</b></td>
            <td class="threats">XXX</td>
          </tr>
          <tr>
            <td class="table-collapse text-right"><b>Forças:</b></td>
            <td class="forces">XXX</td>
          </tr>
          <tr>
            <td class="table-collapse text-right"><b>Oportunidades:</b></td>
            <td class="opportunities">XXX</td>
          </tr>
          <tr>
            <td class="table-collapse text-right"><b>Objetivos gerais:</b></td>
            <td class="goals_general">XXX</td>
          </tr>
          <tr>
            <td class="table-collapse text-right"><b>Objetivos específicos:</b></td>
            <td class="goals_specific">XXX</td>
          </tr>
          <tr>
            <td class="table-collapse text-right"><b>Objetivos estratégicos:</b></td>
            <td class="goals">XXX</td>
          </tr>
        </table>
      </div>
    </div>
  </div>
</div>
@stop

@section('js')
<script>
  $(function() {
    getStaff("{{ array_keys($companies->toArray())[0] }}");
    getInformation("{{ array_keys($companies->toArray())[0] }}");

    $('#planning_select').change(function(e) {
      let company = $(this).val();

      getInformation(company);
      getStaff(company);
    })
  });

  function getInformation(id) {
    $.get("/coordinator/planning/api/" + id, function(data) {
        $('#planning').show();
        $('.error-message').remove();
        $('.name').html(data.name);
        data.avatar.encoded ? $('.avatar').attr('src', data.avatar.encoded) : $('.avatar').attr('src', "{{ asset('storage/companies') }}" + "/" + data.avatar);
        $.each(data.planning, function(key, value) {
          $('.' + key).html(value);
        });
      })
      .fail(function(data) {
        $('.error-message').remove();
        $('.control').append('<div class="col-md-12 error-message"><h1 class="text-center">Esta empresa ainda não preencheu seu planejamento!</h1></div>');
        $('#planning').hide();
      });
  }

  function getStaff(id) {
    $.get("/coordinator/student/api/" + id, function(data) {
        $('#team-box').show();
        $('.team-message').remove();
        $('#team > tbody').empty();
        $.each(data, function(index, key, value) {
          $('#team > tbody').append('<tr><td>' + data[index].registration + '</td><td>' + data[index].name + '</td><td>' + data[index].email + '</td><td>' + data[index].occupation.name + '</td><td>' + data[index].phone + '</td><td>' + (data[index].date == null ? '-' : data[index].date.split('-').reverse().join('/')) + '</td><td>' + (data[index].face == null ? '-' : data[index].face) + '</td><td>' + (data[index].insta == null ? '-' : data[index].insta) + '</td></tr>');
        });
      })
      .fail(function(data) {
        $('.team-message').remove();
        $('.control').append('<div class="col-md-12 team-message"><h1 class="text-center">Esta empresa ainda não cadastrou seus estudantes!</h1></div>');
        $('#team-box').hide();
      });
  }
</script>
@stop