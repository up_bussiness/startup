@extends('adminlte::page')

@section('title', 'Encerramento da rodada')

@section('content_header')
    <h1>{{ $turn->simulation->name }}</h1>
@stop

@section('content_title')
    <h3 class="box-title">Encerramento - Rodada {{ $turn->month }}</h3>
    <div class="pull-right">
      @if($turn->published)
        <a href="{{ route('simulation.turn.show', ['simulation' => $turn->simulation_id, 'id' => $turn->id]) }}" class="btn btn-xs btn-default hidden-print">VOLTAR</a>
      @endif
    </div>
@stop

@section('content')
  <div class="row">
    <div class="col-md-12">

      {!! Form::open(['route' => ['turn.date.store'], 'class' => 'form-horizontal']) !!}

        <div class="form-group">
          {!! Form::label('end_date', 'Data final', ['class' => 'control-label col-sm-3 col-md-offset-2']) !!}
          <div class="col-sm-3 input-group">
            <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
            {!! Form::date('end_date', \Carbon\Carbon::now()->addDays(7), ['class' => 'form-control']); !!}
          </div>
        </div>
        <div class="form-group">
          {!! Form::label('hour', 'Hora', ['class' => 'control-label col-sm-3 col-md-offset-2']) !!}
          <div class="col-sm-3 input-group">
            <div class="input-group-addon"><i class="fa fa-clock-o"></i></div>
            {!! Form::time('hour', date('H:00'), ['class' => 'form-control']); !!}
          </div>
        </div>

        {{ Form::hidden('turn_id', $turn->id) }}

        <div class="text-center">
          {!! Form::submit('Salvar', ['class' => 'btn btn-primary', 'onclick' => 'submitForm(this)']) !!}
        </div>

      {!! Form::close() !!}

    </div>
  </div>
@stop