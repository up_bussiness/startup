@extends('adminlte::page')

@section('title', 'Nova simulação')

@section('content_header')
<h1>{{ $turn->simulation->name }}</h1>
@stop

@section('content_title')
<h3 class="box-title">Decisão do coordenador - Rodada {{ $turn->month }}</h3>
@stop

@section('content')
@if(!empty($decision))
{!! Form::model($decision, ['route' => ['decision.update', $decision->id], 'method' => 'put', 'class'=>'form-horizontal']) !!}
@else
{!! Form::open(['route' => 'decision.store', 'class'=>'form-horizontal', 'autocomplete' => 'off']) !!}
@endif
<div class="form-group">
  {!! Form::label('macro_industry', 'Macro setor', ['class' => 'control-label col-sm-3 col-md-offset-2']) !!}
  <div class="col-sm-3 input-group">
    <div class="input-group-addon">%</div>
    {!! Form::text('macro_industry', !empty($decision) ? null : $lastTurn->macro_industry, ['class' => 'form-control', 'id' => 'macro_industry', 'required']); !!}
  </div>
</div>
{{--
<div class="form-group">
  {!! Form::label('inflation', 'Inflação', ['class' => 'control-label col-sm-3 col-md-offset-2']) !!}
  <div class="col-sm-3 input-group">
    <div class="input-group-addon"></div>
    
  </div>
</div> --}}
{!! Form::hidden('inflation', !empty($decision) ? null : $lastTurn->inflation, ['class' => 'form-control','id' => 'inflation', 'required']); !!}
<div class="form-group">
  {!! Form::label('loss_on_sale', 'Prejuízo na venda de máquina', ['class' => 'control-label col-sm-3 col-md-offset-2']) !!}
  <div class="col-sm-3 input-group">
    <div class="input-group-addon">%</div>
    {!! Form::text('loss_on_sale', !empty($decision) ? null : $lastTurn->loss_on_sale, ['class' => 'form-control', 'id' => 'loss_on_sale', 'required']); !!}
  </div>
</div>

<div class="form-group">
  {!! Form::label('bir', 'Taxa básica de juros', ['class' => 'control-label col-sm-3 col-md-offset-2']) !!}
  <div class="col-sm-3 input-group">
    <div class="input-group-addon">%</div>
    {!! Form::text('bir', !empty($decision) ? null : $lastTurn->bir, ['class' => 'form-control', 'id' => 'bir', 'required']); !!}
  </div>
</div>

<div class="form-group">
  {!! Form::label('interest_providers', 'Juros dos fornecedores', ['class' => 'control-label col-sm-3 col-md-offset-2']) !!}
  <div class="col-sm-3 input-group">
    <div class="input-group-addon">%</div>
    {!! Form::text('interest_providers', !empty($decision) ? null : $lastTurn->interest_providers, ['class' => 'form-control', 'id' => 'interest_providers', 'required']); !!}
  </div>
</div>

<div class="form-group">
  {!! Form::label('import', 'Importação dos produtos', ['class' => 'control-label col-sm-3 col-md-offset-2']) !!}
  <div class="col-sm-3 input-group">
    <div class="input-group-addon">%</div>
    {!! Form::text('import', !empty($decision) ? null : $lastTurn->import, ['class' => 'form-control', 'id' => 'import', 'required']); !!}
  </div>
</div>

<div class="form-group">
  {!! Form::label('income_tax', 'Imposto de Renda', ['class' => 'control-label col-sm-3 col-md-offset-2']) !!}
  <div class="col-sm-3 input-group">
    <div class="input-group-addon">%</div>
    {!! Form::text('income_tax', !empty($decision) ? null : $lastTurn->income_tax, ['class' => 'form-control', 'id' => 'income_tax', 'required']); !!}
  </div>
</div>

<hr>

<div class="form-group">
  {!! Form::label('raw_material_a', 'Quadro', ['class' => 'control-label col-sm-3 col-md-offset-2']) !!}
  <div class="col-sm-3 input-group">
    <div class="input-group-addon">$</div>
    {!! Form::text('raw_material_a', !empty($decision) ? null : $lastTurn->raw_material_a, ['class' => 'form-control', 'id' => 'raw_material_a', 'required']); !!}
  </div>
</div>

<div class="form-group">
  {!! Form::label('raw_material_b', 'Kit 1', ['class' => 'control-label col-sm-3 col-md-offset-2']) !!}
  <div class="col-sm-3 input-group">
    <div class="input-group-addon">$</div>
    {!! Form::text('raw_material_b', !empty($decision) ? null : $lastTurn->raw_material_b, ['class' => 'form-control', 'id' => 'raw_material_b', 'required']); !!}
  </div>
</div>

<div class="form-group">
  {!! Form::label('raw_material_c', 'Kit 2', ['class' => 'control-label col-sm-3 col-md-offset-2']) !!}
  <div class="col-sm-3 input-group">
    <div class="input-group-addon">$</div>
    {!! Form::text('raw_material_c', !empty($decision) ? null : $lastTurn->raw_material_c, ['class' => 'form-control', 'id' => 'raw_material_c', 'required']); !!}
  </div>
</div>

<div class="form-group">
  {!! Form::label('lp_small', 'Linha de Produção - Pequena', ['class' => 'control-label col-sm-3 col-md-offset-2']) !!}
  <div class="col-sm-3 input-group">
    <div class="input-group-addon">$</div>
    {!! Form::text('lp_small', !empty($decision) ? null : $lastTurn->lp_small, ['class' => 'form-control', 'id' => 'lp_small', 'required']); !!}
  </div>
</div>

<div class="form-group">
  {!! Form::label('lp_medium', 'Linha de Produção - Média', ['class' => 'control-label col-sm-3 col-md-offset-2']) !!}
  <div class="col-sm-3 input-group">
    <div class="input-group-addon">$</div>
    {!! Form::text('lp_medium', !empty($decision) ? null : $lastTurn->lp_medium, ['class' => 'form-control', 'id' => 'lp_medium', 'required']); !!}
  </div>
</div>

<div class="form-group">
  {!! Form::label('lp_large', 'Linha de Produção - Grande', ['class' => 'control-label col-sm-3 col-md-offset-2']) !!}
  <div class="col-sm-3 input-group">
    <div class="input-group-addon">$</div>
    {!! Form::text('lp_large', !empty($decision) ? null : $lastTurn->lp_large, ['class' => 'form-control', 'id' => 'lp_large', 'required']); !!}
  </div>
</div>

<div class="form-group">
  {!! Form::label('adv_radio', 'Propanga - Rádio', ['class' => 'control-label col-sm-3 col-md-offset-2']) !!}
  <div class="col-sm-3 input-group">
    <div class="input-group-addon">$</div>
    {!! Form::text('adv_radio', !empty($decision) ? null : $lastTurn->adv_radio, ['class' => 'form-control', 'id' => 'adv_radio', 'required']); !!}
  </div>
</div>

<div class="form-group">
  {!! Form::label('adv_journal', 'Propanga - Jornal', ['class' => 'control-label col-sm-3 col-md-offset-2']) !!}
  <div class="col-sm-3 input-group">
    <div class="input-group-addon">$</div>
    {!! Form::text('adv_journal', !empty($decision) ? null : $lastTurn->adv_journal, ['class' => 'form-control', 'id' => 'adv_journal', 'required']); !!}
  </div>
</div>

<div class="form-group">
  {!! Form::label('adv_social', 'Propanga - Mídia Social', ['class' => 'control-label col-sm-3 col-md-offset-2']) !!}
  <div class="col-sm-3 input-group">
    <div class="input-group-addon">$</div>
    {!! Form::text('adv_social', !empty($decision) ? null : $lastTurn->adv_social, ['class' => 'form-control', 'id' => 'adv_social', 'required']); !!}
  </div>
</div>

<div class="form-group">
  {!! Form::label('adv_outdoor', 'Propanga - Outdoor', ['class' => 'control-label col-sm-3 col-md-offset-2']) !!}
  <div class="col-sm-3 input-group">
    <div class="input-group-addon">$</div>
    {!! Form::text('adv_outdoor', !empty($decision) ? null : $lastTurn->adv_outdoor, ['class' => 'form-control', 'id' => 'adv_outdoor', 'required']); !!}
  </div>
</div>

<div class="form-group">
  {!! Form::label('adv_tv', 'Propanga - TV', ['class' => 'control-label col-sm-3 col-md-offset-2']) !!}
  <div class="col-sm-3 input-group">
    <div class="input-group-addon">$</div>
    {!! Form::text('adv_tv', !empty($decision) ? null : $lastTurn->adv_tv, ['class' => 'form-control', 'id' => 'adv_tv', 'required']); !!}
  </div>
</div>

<div class="col-md-offset-5 col-md-3 text-center">
  {!! Form::submit(!empty($decision) ? 'Atualizar' : 'Criar', ['class' => 'btn btn-success']); !!}
</div>

{!! Form::hidden('turn_id', $turn->id) !!}

{!! Form::close() !!}
@stop