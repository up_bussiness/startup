@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Decisão do Cordenador</h1>
@stop

@section('content')
    
    <p>{!! nl2br(e($decision->macro_industry)) !!},</p>
    <p>{!! nl2br(e($decision->inflation)) !!},</p>
    <p>{!! nl2br(e($decision->loss_on_sale)) !!},</p>
    <p>{!! nl2br(e($decision->bir)) !!},</p>
    <p>{!! nl2br(e($decision->interest_providers)) !!},</p>
    <p>{!! nl2br(e($decision->import)) !!},</p>
    <p>{!! nl2br(e($decision->income_tax)) !!},</p>
    <p>{!! nl2br(e($decision->forbidden_buy_machine)) !!}</p>
    
@stop