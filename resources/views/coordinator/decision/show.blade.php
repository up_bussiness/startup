@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>{{ $decision->turn->simulation->name }}</h1>
@stop

@section('content_title')
    <h3 class="box-title"><i class="fa fa-gear"></i> Decisão do Coordenador - Rodada {{ $decision->turn->month }}</h3>
    <div class="pull-right">
      <a href="{{ route('simulation.show', $decision->turn->simulation_id) }}" class="btn btn-xs btn-default">VOLTAR</a>
    </div>
@stop

@section('content')
  <div class="col-md-5 col-md-offset-1">
    <div class="table-responsive">
    <table class="table  table-bordered table-striped">
      <tbody>
        <tr>
          <td><b>Propriedade</b></td>
          <td class="text-center">%</td>
        </tr>
        <tr>
          <td>Macro setor</td>
          <td class="text-center table-collapse">{!! ($decision->macro_industry) !!}</td>
        </tr>
        <tr>
          <td>Inflação</td>
          <td class="text-center table-collapse">{!! ($decision->inflation) !!}</td>
        </tr>
        <tr>
          <td>Prejuízo na venda de máquina</td>
          <td class="text-center table-collapse">{!! ($decision->loss_on_sale) !!}</td>
        </tr>
        <tr>
          <td>Taxa básica de juros</td>
          <td class="text-center table-collapse">{!! ($decision->bir) !!}</td>
        </tr>
        <tr>
          <td>Juros dos fornecedores</td>
          <td class="text-center table-collapse">{!! ($decision->interest_providers) !!}</td>
        </tr>
        <tr>
          <td>Importação dos produtos</td>
          <td class="text-center table-collapse">{!! ($decision->import) !!}</td>
        </tr>
        <tr>
          <td>Impostos</td>
          <td class="text-center table-collapse">{!! ($decision->income_tax) !!}</td>
        </tr>
      </tbody>
    </table>
    </div>
  </div>
  <div class="col-md-5">
    <div class="table-responsive">
    <table class="table  table-bordered table-striped">
      <tbody>
        <tr>
          <td><b>Propriedade</b></td>
          <td class="text-center table-collapse">%</td>
        </tr>
        <tr>
          <td>Quadro</td>
          <td class="text-center table-collapse">{!! numberDot($decision->raw_material_a) !!}</td>
        </tr>
        <tr>
          <td>Kit 1</td>
          <td class="text-center table-collapse">{!! numberDot($decision->raw_material_b) !!}</td>
        </tr>
        <tr>
          <td>Kit 2</td>
          <td class="text-center table-collapse">{!! numberDot($decision->raw_material_c) !!}</td>
        </tr>
        <tr>
          <td>Linha de Produção Pequena</td>
          <td class="text-center table-collapse">{!! numberDot($decision->lp_small) !!}</td>
        </tr>
        <tr>
          <td>Linha de Produção Média</td>
          <td class="text-center table-collapse">{!! numberDot($decision->lp_medium) !!}</td>
        </tr>
        <tr>
          <td>Linha de Produção Grande</td>
          <td class="text-center table-collapse">{!! numberDot($decision->lp_large) !!}</td>
        </tr>
        <tr>
          <td>Propaganda - Radio</td>
          <td class="text-center table-collapse">{!! numberDot($decision->adv_radio) !!}</td>
        </tr>
        <tr>
          <td>Propaganda - Jornal</td>
          <td class="text-center table-collapse">{!! numberDot($decision->adv_journal) !!}</td>
        </tr>
        <tr>
          <td>Propaganda - Mídia Social</td>
          <td class="text-center table-collapse">{!! numberDot($decision->adv_social) !!}</td>
        </tr>
        <tr>
          <td>Propaganda - Outdoor</td>
          <td class="text-center table-collapse">{!! numberDot($decision->adv_outdoor) !!}</td>
        </tr>
        <tr>
          <td>Propaganda - TV</td>
          <td class="text-center table-collapse">{!! numberDot($decision->adv_tv) !!}</td>
        </tr>
      </tbody>
    </table>
    </div>
  </div>
@stop

@section('js')
  @include('layouts.partials.storage')
@stop