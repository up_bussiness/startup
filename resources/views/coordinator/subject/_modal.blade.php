<!-- MODAL -->
<div class="modal fade" id="modal-newspaper" tabindex="-1" role="dialog" aria-labelledby="modal-newspaperLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="modal-newspaperLabel">Nova matéria</h4>
      </div>
      {!! Form::open(['route' => ['newspaper.store'], 'class' => 'form-horizontal', 'id'=>'form-newspaper']) !!}
      <div class="modal-body">
        <div class="form-group">
          {!! Form::label('theme_id', 'Tema', ['class' => 'control-label col-sm-2 col-md-offset-1 text-right']) !!}
          <div class="col-sm-7 input-group">
            <div class="input-group-addon"><i class="fa fa-newspaper-o"></i></div>
            {!! Form::select('theme_id', $themes->pluck('name', 'id'), null, ['class' => 'form-control']); !!}
          </div>
        </div>

        <div class="form-group">
          {!! Form::label('newspaper', 'Matéria original', ['class' => 'control-label col-sm-2 col-md-offset-1 text-right']) !!}
          <div class="col-sm-7 input-group">
            <div class="input-group-addon"><i class="fa fa-newspaper-o"></i></div>
            {!! Form::select('newspaper', $themes[0]->matters->pluck('title', 'id'), null, ['class' => 'form-control', 'placeholder' => '-']); !!}
          </div>
        </div>

        <hr>

        <div class="form-group">
          {!! Form::label('title', 'Nova matéria', ['class' => 'control-label col-sm-2 col-md-offset-1 text-right']) !!}
          <div class="col-sm-7 input-group">
            <div class="input-group-addon"><i class="fa fa-newspaper-o"></i></div>
            {!! Form::text('title', null, ['class' => 'form-control', 'required']); !!}
          </div>
        </div>

        <div class="form-group">
          {!! Form::label('description', 'Nova matéria', ['class' => 'control-label col-sm-2 col-md-offset-1 text-right']) !!}
          <div class="col-sm-7 input-group">
            <div class="input-group-addon"><i class="fa fa-newspaper-o"></i></div>
            {!! Form::textarea('description', null, ['class' => 'form-control', 'required']); !!}
          </div>
        </div>

        {!! Form::hidden('turn_id', $turn->id) !!}
      </div>
      <div class="modal-footer">
        <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Voltar</button> -->
        {!! Form::submit('Salvar', ['class' => 'btn btn-primary']) !!}
        {!! Form::close() !!}
      </div>
    </div>
  </div>
</div>