@extends('adminlte::page')

@section('title', 'Informativo Econômico')

@section('content_header')
<h1>{{ $turn->simulation->name }}</h1>
@stop

@section('content_title')
<h3 class="box-title"><i class="fa fa-newspaper-o"></i> Informativo - Rodada {{ $turn->month }}</h3>
<div class="pull-right">
  @if($turn->published == 0 || turnIsOpen($turn->end_date))
  <a href="#" data-toggle="modal" data-target="#modal-newspaper" class="btn btn-xs btn-success">NOVA MATÉRIA</a>
  @else
  <a href="{{ route('simulation.show', $turn->simulation_id) }}" class="btn btn-xs btn-default">VOLTAR</a>
  @endif
</div>
@stop

@section('content')
<div class="row">
  <div class="col-md-12">

    <div class="table-responsive">
      <table class="table table-bordered" id="table-newspaper">
        <thead>
          <tr>
            <th>Tema</th>
            <th>Matéria</th>
            <th>Descrição</th>
            @if(turnIsOpen($turn->end_date))
            <th>Excluir</th>
            <th>Alterar</th>
            @endif
          </tr>
        </thead>
        <tbody>
          @forelse($newspapers as $newspaper)
          <tr>
            <td>{{$newspaper->theme->name}}</td>
            <td class="table-collapse">{{$newspaper->title}}</td>
            <td>{{$newspaper->description}}</td>
            @if(turnIsOpen($turn->end_date))
            <td class="text-center">
              <a href="{{ route('newspaper.destroy', $newspaper->id) }}" class="btn btn-sm btn-danger" onclick="return confirm('Tem certeza que deseja remover essa matéria?')"><span class="glyphicon glyphicon-remove-circle"></span></a>
            </td>
            <td class="text-center">
              <a href="{{ route('newspaper.edit', $newspaper->id) }}" class="btn btn-sm btn-info"><i class="fa fa-pencil"></i></a>
            </td>
            @endif
          </tr>
          @empty
          <tr>
            <td colspan="100" class="text-center">
              <i>Você ainda não possui nenhuma matéria criada.</i>
            </td>
          </tr>
          @endforelse
        </tbody>
      </table>
    </div>

    @if(turnIsOpen($turn->end_date))
    <div class="text-center">
      <a href="{{ route('turn.date', $turn->id) }}" class="btn btn-success">Avançar</a>
    </div>
    @endif

  </div>
</div>

@include('coordinator.subject._modal')
@stop

@section('js')
<script src="{{ asset('js/newspaper.js') }}"></script>
@include('layouts.partials.storage')
@stop