@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
<h1>Simulação</h1>
@stop

@section('content_title')
<h3 class="box-title"><i class="fa fa-users"></i> {{ $simulation->name }}</h3>
<div class="pull-right">
  @if ($simulation->reseted!=true and $simulation->turns->sortBy('month')->last()->month == 2 and !turnIsOpen($turn->end_date) and $turn->operationalDetail != null)
    <a href="{{ route('turn.restart', $simulation->id) }}" class="btn btn-xs btn-danger">REINICIAR RODADA</a>
  @endif
  <a href="{{ route('coordinator.planning', $simulation->id) }}" class="btn btn-xs btn-default">PLANEJAMENTO ESTRATÉGICO</a>
  <a href="#" data-toggle="modal" data-target="#Companies" class="btn btn-xs btn-success">EMPRESAS</a>
  @if ($simulation->turns->count() < 16) 
    <a href="{{ route('turn.create', $simulation->id) }}" class="btn btn-xs btn-primary">NOVA RODADA</a>
  @endif
</div>
@stop

@section('content')
<div class="row">
  <div class="col-md-2 col-md-offset-5">
    <div class="form-group">
      <div class="col-sm-12 input-group">
        <div class="input-group-addon"><b>RODADA</b></div>
        {!! Form::select('turn_select', $turns, $turn->id, ['class' => 'form-control', 'id' => 'turn_select']); !!}
      </div>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-md-3">
    <div class="turn-box">
      <!-- Rodada -->
      <div class="turn-month text-center">
        <img src="{{ Avatar::create($turn->month) }}" class="img-circle" alt="User Image" width="60">
      </div>
      <br>
      <!-- Data -->
      <div class="row">
        <div class="col-md-12">
          <table class="table table-striped table-bordered">
            <tr>
              <td><b><i class="fa fa-bookmark"></i> Status:</b></td>
              <td>{{ turnStatusLabel($turn->published, $turn->end_date) }}</td>
            </tr>
            <tr>
              <td><b><i class="fa fa-calendar-plus-o"></i> Início:</b></td>
              <td>{{ dateTimeBRL($turn->created_at)}}</td>
            </tr>
            <tr>
              <td><b><i class="fa fa-calendar-times-o"></i> Término:</b></td>
              <td>
                @if(!$turn->published)
                -
                @else
                {{ dateTimeBRL($turn->end_date)}}
                @if(turnIsOpen($turn->end_date))
                <a href="{{ route('turn.date', $turn->id) }}" class="btn btn-xs btn-primary" style="width:72px"><b>EDITAR</b></a>
                @endif
                @endif
              </td>
            </tr>
          </table>
          <div class="text-center">
            @if(!$turn->published)
            <a href="{{ route('turn.publish', $turn->id) }}" class="btn btn-xs btn-primary"><i class="fa fa-check"></i> <b>PUBLICAR</b></a>
            @else

            @if(turnIsOpen($turn->end_date))
            <!-- Se a rodada estiver aberta -->
            <a href="{{ route('turn.close', $turn->id) }}" class="btn btn-xs btn-danger"><i class="fa fa-close"></i> <b>FECHAR PERÍODO</b></a>
            @else
            <!-- Se a rodada estiver fechada -->

            @if($turn->operationalDetail != null)
            <!-- Se o relatório operacional existe -->
            <span class="label label-success">PROCESSADO</span>
            @else

            @if($turn->decisionCompany->count() == $simulation->number_of_company)
                  @if($turn->month == 1)
                    <a href="{{ route('simulation.report.first', $turn->id) }}" id="process-turn" class="btn btn-xs btn-warning"><i class="fa  fa-hourglass-half"></i> <b>PROCESSAR PERÍODO</b></a>
                  @else
                    <a href="{{ route('coordinator.generate.report', $turn->id) }}" id="process-turn" class="btn btn-xs btn-warning"><i class="fa  fa-hourglass-half"></i> <b>PROCESSAR PERÍODO</b></a>
                  @endif
                @else
                  <a href="{{ route('turn.date', $turn->id) }}" class="btn btn-xs btn-info"><i class="fa fa-folder-open-o"></i> <b>REABRIR PERÍODO</b></a>
                @endif

            @endif

            @endif

            @endif
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="col-md-9">
    <div class="nav-tabs-custom">
      <ul class="nav nav-tabs">
        <li class="active"><a href="#turn-summary-{{$turn->id}}" data-toggle="tab">Geral</a></li>
        @foreach($simulation->companies as $key => $company)
        <li><a href="#tab_{{ $turn->id }}_{{ $key }}" data-toggle="tab" data-key="{{ $company->id }}">{{ $company->name }}</a></li>
        @endforeach
      </ul>
      <div class="tab-content">
        <div class="tab-pane active" id="turn-summary-{{$turn->id}}">
          <div class="row">
            <div class="{{ $turn->operational != null ? 'col-md-4' : 'col-md-4 col-md-offset-2'}}">
              <!-- Start Button 1 -->
              <div class="button">
                @if(is_null($turn->decisionCoordinator))
                <div class="button-inner button-take">
                  @else
                  @if(turnIsOpen($turn->end_date))
                  <div class="button-inner button-edit">
                    @else
                    <div class="button-inner button-show">
                      @endif
                      @endif
                      <div class="button-top">
                        <i class="fa fa-file-text-o"></i>
                        <h4>Decisão do Professor</h4>
                      </div>
                      <div class="button-bottom">
                        <i class="fa fa-file-text-o"></i>
                        <h4>Decisão do Professor</h4>
                        @if(is_null($turn->decisionCoordinator))
                        <p>Tome sua decisão.</p>
                        @else
                        @if(turnIsOpen($turn->end_date))
                        <p>Edite sua decisão.</p>
                        @else
                        <p>Visualize sua decisão.</p>
                        @endif
                        @endif
                        @if(is_null($turn->decisionCoordinator))
                        <a href="{{ route('decision.create', $turn->id) }}">Clique Aqui</a>
                        @else
                        @if(turnIsOpen($turn->end_date))
                        <a href="{{ route('decision.edit', $turn->id) }}">Clique Aqui</a>
                        @else
                        <a href="{{ route('decision.show', $turn->decisionCoordinator->id) }}">Clique Aqui</a>
                        @endif
                        @endif
                      </div>
                    </div>
                    <!-- End Button 1 -->
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="button">
                    @if($turn->newspaper->count() == 0)
                    <div class="button-inner button-take">
                      @else
                      @if(turnIsOpen($turn->end_date))
                      <div class="button-inner button-edit">
                        @else
                        <div class="button-inner button-show">
                          @endif
                          @endif
                          <div class="button-top">
                            <i class="fa fa-newspaper-o"></i>
                            <h4>UP News</h4>
                          </div>
                          <div class="button-bottom">
                            <i class="fa fa-newspaper-o"></i>
                            <h4>UP News</h4>
                            @if($turn->newspaper->count() == 0)
                            <p>Escreva o informativo.</p>
                            @else
                            @if(turnIsOpen($turn->end_date))
                            <p>Edite o informativo.</p>
                            @else
                            <p>Visualize o informativo.</p>
                            @endif
                            @endif
                            @if($turn->newspaper->count() == 0)
                            <a href="{{ route('newspaper.index', $turn->id) }}">Clique Aqui</a>
                            @else
                            @if(turnIsOpen($turn->end_date))
                            <a href="{{ route('newspaper.index', $turn->id) }}">Clique Aqui</a>
                            @else
                            <a href="{{ route('newspaper.index', $turn->id) }}">Clique Aqui</a>
                            @endif
                            @endif
                          </div>
                        </div>
                        <!-- End Button 1 -->
                      </div>
                    </div>
                    <div class="{{ $turn->ranking != null ? 'col-md-4' : 'hidden'}}">
                      <div class="button">
                        <div class="button-inner button-show">
                          <div class="button-top">
                            <i class="fa fa-trophy"></i>
                            <h4>Ranking</h4>
                          </div>
                          <div class="button-bottom">
                            <i class="fa fa-trophy"></i>
                            <h4>Ranking</h4>
                            <p>Visualize a colocação das empresas.</p>
                            <a href="{{ route('coordinator.ranking', $turn->id) }}">Clique Aqui</a>
                          </div>
                        </div>
                        <!-- End Button 1 -->
                      </div>
                    </div>
                  </div>
                </div>
                @foreach($simulation->companies as $key => $company)

                @php
                $companyDecision = $company->decisions->where('turn_id', $turn->id)->first();
                @endphp

                <div class="tab-pane" id="tab_{{ $turn->id }}_{{ $key }}">
                  <div class="row">
                    <div class="col-md-12">
                      @if(is_null($companyDecision))
                      @if(turnIsOpen($turn->end_date))
                      <p>Ainda não tomou a decisão</p>
                      @else
                      {!! Form::open(['route' => 'coordinator.company.default', 'class'=>'form-horizontal', 'autocomplete' => 'off', 'id' => 'decision-default-'.$company->id]) !!}
                      {!! Form::hidden('company_id', $company->id) !!}
                      {!! Form::hidden('turn_id', $turn->id) !!}
                      {!! Form::submit('TOMAR DECISÃO PELA EMPRESA', ['class' => 'btn btn-warning', 'style' => 'width: 100%; white-space: unset']); !!}
                      {!! Form::close() !!}
                      <!-- <a href="{{ route('coordinator.company.decision', $company->id) }}" class="btn btn-warning" style="width: 100%; white-space: unset;"><i class="fa fa-legal"></i> TOMAR DECISÃO PADRÃO</a> -->
                      @endif
                      @else
                      <a href="{{ route('coordinator.company.decision', $companyDecision->id) }}" class="btn btn-primary" style="width: 100%; white-space: unset;"><i class="fa fa-legal"></i> DECISÃO</a>
                      @endif
                    </div>
                    <!-- OPERACIONAL -->
                    @if($turn->operational != null)
                    <div class="col-md-3">
                      <div class="button">
                        <div class="button-inner button-show">
                          <div class="button-top">
                            <i class="fa fa-bar-chart"></i>
                            <h4>Rel. Mercadológico</h4>
                          </div>
                          <div class="button-bottom">
                            <i class="fa fa-bar-chart"></i>
                            <h4>Rel. Mercadológico</h4>
                            <a href="{{ route('coordinator.operational.report', ['turn_id' => $turn->id, 'id' => $company->id]) }}">Clique Aqui</a>
                          </div>
                        </div>
                        <!-- End Button 1 -->
                      </div>
                    </div>
                    @endif
                    @if($turn->cashflow != null)
                    <div class="col-md-3">
                      <div class="button">
                        <div class="button-inner button-show">
                          <div class="button-top">
                            <i class="fa fa-exchange"></i>
                            <h4>Fluxo de Caixa</h4>
                          </div>
                          <div class="button-bottom">
                            <i class="fa fa-exchange"></i>
                            <h4>Fluxo de Caixa</h4>
                            <a href="{{ route('coordinator.cashflow.report', ['turn_id' => $turn->id, 'id' => $company->id]) }}">Clique Aqui</a>
                          </div>
                        </div>
                        <!-- End Button 1 -->
                      </div>
                    </div>
                    @endif
                    @if($turn->dre != null)
                    <div class="col-md-3">
                      <div class="button">
                        <div class="button-inner button-show">
                          <div class="button-top">
                            <i class="fa fa-usd"></i>
                            <h4>DRE</h4>
                          </div>
                          <div class="button-bottom">
                            <i class="fa fa-usd"></i>
                            <h4>DRE</h4>
                            <a href="{{ route('coordinator.dre.report', ['turn_id' => $turn->id, 'id' => $company->id]) }}">Clique Aqui</a>
                          </div>
                        </div>
                        <!-- End Button 1 -->
                      </div>
                    </div>
                    @endif
                    @if($turn->balance != null)
                    <div class="col-md-3">
                      <div class="button">
                        <div class="button-inner button-show">
                          <div class="button-top">
                            <i class="fa fa-balance-scale"></i>
                            <h4>Balanço Patrimonial</h4>
                          </div>
                          <div class="button-bottom">
                            <i class="fa fa-balance-scale"></i>
                            <h4>Balanço Patrimonial</h4>
                            <a href="{{ route('coordinator.balance.report', ['turn_id' => $turn->id, 'id' => $company->id]) }}">Clique Aqui</a>
                          </div>
                        </div>
                        <!-- End Button 1 -->
                      </div>
                    </div>
                    @endif
                    <br><br><br><br><br>
                  </div>
                </div>
                @endforeach
                <!-- /.tab-pane -->
              </div>
              <!-- /.tab-content -->
            </div>
            <!-- nav-tabs-custom -->
          </div>
        </div>

        @include('coordinator.simulation._companies')

        @stop

        @section('js')
        <script>
          $(function() {

            var company = localStorage.getItem('COMPANY');

            if (company != '') {
              $('.nav-tabs a[data-key="' + company + '"]').tab('show');
            }

            $('#turn_select').change(function(e) {
              let turn = $(this).val();
              let simulation = "{{ $simulation->id }}";

              window.location.href = `/coordinator/simulation/${simulation}/${turn}`;
            });


            $('#process-turn').click(function(e) {
              $(this).html('<i class="fa fa-spinner fa-spin"></i>');
              $(this).removeClass('btn-warning').addClass('btn-default');
              $(this).css('cursor', 'not-allowed');

              var url = $(this).attr('href');
              $(this).removeAttr('href');

              if (url !== undefined) {
                window.location.href = url;
              }

              return false;
            });

            $("form[id^='decision-default-']").submit(function(e) {
              e.stopPropagation();

              var $bt = $(this).find(':submit');

              var companyID = $(this).find('input[name=company_id]').val();

              $bt.attr('disabled', 'disabled');
              $bt.val('Processando...');
              $bt.removeClass('btn-warning').addClass('btn-default');
              $bt.css('cursor', 'progress');

              localStorage.setItem('COMPANY', companyID);
              $(this).submit();
            });
          });
        </script>
        @stop