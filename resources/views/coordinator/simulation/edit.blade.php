@extends('adminlte::page')

@section('title', 'Editar simulação')

@section('content_header')
    <h1>{{ $instituitionVC->name }}</h1>
@stop

@section('content_title')
    <h3 class="box-title">Editar simulação</h3>
    <div class="pull-right">
      <a href="{{ route('simulation.index') }}" class="btn btn-xs btn-default">VOLTAR</a>
    </div>
@stop

@section('content')
    {!! Form::model($simulation, ['route' => ['simulation.update', $simulation->id], 'method' => 'put', 'class'=>'form-horizontal']) !!}
    
    @include('coordinator.simulation._form')	
       
	<div class="col-md-offset-5 col-md-3 text-center">
		{!! Form::submit('Atualizar', ['class' => 'btn btn-success']); !!}
	</div>

    {!! Form::close() !!}
@stop