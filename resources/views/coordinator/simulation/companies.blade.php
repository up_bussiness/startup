@extends('adminlte::page')

@section('title', 'Simulação')

@section('content_header')
    <h1>Nome da Instituição</h1>
@stop

@section('content_title')
    <h3 class="box-title">Equipes</h3>
@stop

@section('content')
<div class="col-md-8 col-md-offset-2">
    <div class="table-responsive">
      {!! Form::open(['route' => 'coordinator.send.password', 'class'=>'form-horizontal', 'autocomplete' => 'off']) !!}
      <table class="table table-bordered">
        <thead>
          <tr>
            <th>Equipe</th>
            <th>Login</th>
            <th>Senha</th>
            <th>Email</th>
          </tr>
        </thead>
        <tbody>
          @foreach($simulation->companies as $index => $company)
          <tr>
            <td>{{ $company->name }} <input type="hidden" name="name[{{$index}}]" value="{{ $company->name }}"></td>
            <td>{{ $company->email }} <input type="hidden" name="login[{{$index}}]" value="{{ $company->email }}"></td>
            <td>{{ $company->password_plain }} <input type="hidden" name="password[{{$index}}]" value="{{ $company->password_plain }}"></td>
            <td><input type="email" name="email[{{$index}}]" class="form-control"></td>
          </tr>
          @endforeach
        </tbody>
      </table>
      <div class="col-md-offset-5 col-md-3 text-center">
        <input type="hidden" name="simulation_id" value="{{ $simulation->id }}">
        {!! Form::submit('Avançar', ['class' => 'btn btn-primary']); !!}
      </div>

    {!! Form::close() !!}
    </div>
</div>
@stop