@extends('adminlte::page')

@section('title', 'Nova simulação')

@section('content_header')
<h1>{{ $instituitionVC->name }}</h1>
@stop

@section('content_title')
<h3 class="box-title">Nova simulação</h3>
@stop

@section('content')
{!! Form::open(['route' => 'simulation.store', 'class'=>'form-horizontal']) !!}

@include('coordinator.simulation._form')

<div class="col-md-offset-5 col-md-3 text-center">
    {!! Form::submit('Criar simulação', ['class' => 'btn btn-success','onclick' => 'MostraProcesso();','id' => 'btnCriar']); !!}
    <div style="display:none" id="txtProcesasdsasar"><b>Processando...</b></div>


    <div align="center" style="display:none" id="txtProcessar">
        <script src="{{ asset('js/lottie-player.js') }}"></script>
        <lottie-player src="{{ asset('images/35049-data-analyst.json') }}" background="transparent" speed="1" style="width: 300px; height: 300px;" loop autoplay>
        </lottie-player>
    </div>
</div>

{!! Form::close() !!}

<!--<a href="{{ route('simulation.report.first', '75d93f6a-1acf-11e9-90b8-d09466a24655') }}" class="btn btn-info">Link Button</a>-->
<script>
    function MostraProcesso() {
        //alert('okokoko');
        $('#btnCriar').hide();
        $('#txtProcessar').show();
        return true;
    }
</script>

@stop