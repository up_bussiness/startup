@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
<h1>Simulações</h1>
@stop

@section('content_title')
<h3 class="box-title"><i class="fa fa-gear"></i> Visão Geral</h3>
@if($licenses != 0)
<a href="{{ route('simulation.create') }}" class="btn btn-success btn-xs pull-right">NOVA SIMULAÇÃO</a></h3>
@endif
@stop

@section('content')
<div class="col-md-4 col-md-offset-2 col-sm-6 col-xs-12">
  <div class="info-box">
    <span class="info-box-icon" style="border-radius: 5px;background-color: #fb6600;color: #ffffff;"><i class="fa fa-ticket"></i></span>

    <input type="hidden" id="idPrimeiro" value="{!! Session::get('primeiro') !!}">

    <div class="info-box-content">
      <span class="info-box-text">Licenças em uso</span>
      <span class="info-box-number">{{ auth()->user()->simulations->count() }}</span>
    </div>
    <!-- /.info-box-content -->
  </div>
  <!-- /.info-box -->
</div>
<div class="col-md-4 col-sm-6 col-xs-12">
  <div class="info-box">
    <span class="info-box-icon" style="border-radius: 5px;background-color: #fb6600;color: #ffffff;"><i class="fa fa-cubes"></i></span>

    <div class="info-box-content">
      <span class="info-box-text">Licenças Restantes</span>
      <span class="info-box-number">{{ $licenses }}</span>
    </div>
    <!-- /.info-box-content -->
  </div>
  <!-- /.info-box -->
</div>

<div class="col-md-8 col-md-offset-2">

  <div class="table-responsive">
    <table class="table table-striped table-bordered">
      <thead>
        <tr>
          <th>Descrição</th>
          <th class="table-collapse">Empresas</th>
          <th class="table-collapse">Rodada</th>
          <th class="text-center">Início</th>
          <th class="text-center">Ações</th>
        </tr>
      </thead>
      <tbody>
        @forelse($simulations as $simulation)
        <tr>
          <td>{{ $simulation->name }}</td>
          <td class="text-center">{{ $simulation->number_of_company }}</td>
          <td class="text-center">{{ $simulation->turns->count() }}</td>
          <td class="table-collapse">{{ dateBRL($simulation->initial_date) }}</td>
          <td class="table-collapse">
            <a href="{{route('simulation.show', $simulation->id)}}" class="btn btn-xs btn-primary"><i class="fa fa-eye"></i></a>
            <a href="{{route('simulation.edit', $simulation->id)}}" class="btn btn-xs btn-info"><i class="fa fa-pencil"></i></a>
            <!-- Coordinator only remove simulation if up until two turns is played -->
            @if ($simulation->turns->count() <= 2)
        			{{ Form::open(['route' => ['simulation.destroy', $simulation->id], 'method' => 'delete', 'style'=>'display: inline']) }}
        					    <button type="submit" class="btn btn-xs btn-danger" onclick="return confirm('Tem certeza que deseja excluir esta simulação? Você não poderá voltar atrás!');"><i class="fa fa-trash"></i></button>
              {{ Form::close() }}
              @endif
          </td>
        </tr>
        @empty
        <tr>
          <td colspan="100" class="text-center">
            <i>Você ainda não tem nenhuma simulação. Crie agora uma.</i>
          </td>
        </tr>
        @endforelse
      </tbody>
    </table>
    <div class="box box-{{ typeColor() }}" align="center">
      <div class="box-header with-border">
        <a href="https://play.google.com/store/apps/details?id=com.upbusinessgame.chat&hl=pt_BR" target="_blank">
          <img src="{{asset('images/app1.png')}}"></a>
        <span class="info-box-text">
          Baixe o aplicativo, e acesse pelo qrcode, na tela de login do app.
        </span>
      </div>
      {{-- Comentado pois estava dando erro --}}
      {{-- {!! QrCode::size(200)->generate(TextoParaQrcodeAcesso(request()->getHost())); !!} --}}
    </div>
  </div>

</div>
<style>
  .modal {
    width: 100%;
    height: 100%;
    background: #fff;
  }

  .imgmodal {
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
  }
</style>

<div class="modal" id="idModalLogo">
  <div class="imgmodal">
    <img src="{{ asset('storage/'.LocalUploadUUID().'/'.'public/uploads'.'/'.$instituition->logo) }}" width="600px"><br />
    <div style="text-align: center;">
      <img src="{{asset('images/carregando.gif')}}">
    </div>
  </div>
</div>

@stop


@section('js')
@include('layouts.partials.storage')
<script>
  $(document).ready(function() {
    Oncarrega();
  });

  function Oncarrega() {
    var x = $('#idPrimeiro').val();
    //alert(x);
    if (x == 1) {
      $('#idModalLogo').modal({
        backdrop: 'static',
        keyboard: false
      });
      //document.getElementById('idModalLogo').style.top = "0";
      setTimeout(function() {
        $('#idModalLogo').modal('hide');
      }, 3000);
    }
  }
</script>

@stop