<!-- Modal -->
<div class="modal fade" id="Companies" tabindex="-1" role="dialog" aria-labelledby="CompaniesLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header bg-yellow">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title" id="CompaniesLabel">Empresas</h4>
      </div>
      <div class="modal-body">
        <div class="table-responsive">
            <table class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th>Nome da Empresa</th>
                        <th class="text-center">Login</th>
                        <th class="text-center">Senha</th>
                    </tr>
                </thead>
                <tbody>
                @foreach ($simulation->companies as $company)
                <tr>
                        <td>{{ $company->name }}</td>
                        <td class="table-collapse text-center">{{ $company->email }}</td>
                        <td class="table-collapse text-center">{{ $company->password_plain }}</td>
                </tr>
                @endforeach
                </tbody>
            </table>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Voltar</button>
      </div>
    </div>
  </div>
</div> 