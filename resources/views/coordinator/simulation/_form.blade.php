<div class="form-group">
  {!! Form::label('name', 'Nome', ['class' => 'control-label col-sm-3 col-md-offset-2']) !!}
  <div class="col-sm-3 input-group">
    <div class="input-group-addon">...</div>
    {!! Form::text('name', null, ['class' => 'form-control', 'required']); !!}
  </div>
</div>

<div class="form-group">
  {!! Form::label('initial_date', 'Data Inicial', ['class' => 'control-label col-sm-3 col-md-offset-2']) !!}
  <div class="col-sm-3 input-group">
    <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
    {!! Form::date('initial_date', !empty($simulation) ? $simulation->initial_date : \Carbon\Carbon::now(), ['class' => 'form-control', 'required', !empty($simulation) ? 'disabled' : '']); !!}
  </div>
</div>

<div class="form-group">
  {!! Form::label('end_date', 'Data Final', ['class' => 'control-label col-sm-3 col-md-offset-2']) !!}
  <div class="col-sm-3 input-group">
    <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
    {!! Form::date('end_date', !empty($simulation) ? $simulation->end_date : \Carbon\Carbon::parse('last day of December'), ['class' => 'form-control', 'required', !empty($simulation) ? 'disabled' : '']); !!}
  </div>
</div>

<div class="form-group">
  {!! Form::label('number_of_company', 'Número de Empresas', ['class' => 'control-label col-sm-3 col-md-offset-2']) !!}
  <div class="col-sm-3 input-group">
    <div class="input-group-addon">Nº</div>
    {!! Form::selectRange('number_of_company', 3, 10, !empty($simulation) ? $simulation->number_of_company : null, ['class' => 'form-control', 'required', !empty($simulation) ? 'disabled' : '']); !!}
  </div>
</div>
{!! Form::hidden('reseted', 0) !!}
<div class="form-group">
  {!! Form::label('demand', 'Demanda', ['class' => 'control-label col-sm-3 col-md-offset-2']) !!}
  <div class="col-sm-3 input-group">
    <div class="input-group-addon">%</div>
    {!! Form::select('demand', [10=>10, 15=>15, 20=>20], !empty($simulation) ? $simulation->demand : null, ['class' => 'form-control', 'required', !empty($simulation) ? 'disabled' : '']); !!}
  </div>
</div>

<div class="form-group">
  {!! Form::label('in_operational', 'Exibe Mercadológico', ['class' => 'control-label col-sm-3 col-md-offset-2']) !!}
  <div class="col-sm-3 input-group">
    <div class="input-group-addon"><i class="fa fa-bullseye"></i></div>
    {!! Form::select('in_operational', [1=>'Sim', 0=>'Não'], !empty($simulation) ? $simulation->in_operational: null, ['class' => 'form-control', 'required', '']); !!}
  </div>
</div>

<div class="form-group">
  {!! Form::label('in_cashflow', 'Exibe Fluxo de Caixa', ['class' => 'control-label col-sm-3 col-md-offset-2']) !!}
  <div class="col-sm-3 input-group">
    <div class="input-group-addon"><i class="fa fa-bullseye"></i></div>
    {!! Form::select('in_cashflow', [1=>'Sim', 0=>'Não'], !empty($simulation) ? $simulation->in_cashflow : null, ['class' => 'form-control', 'required', '']); !!}
  </div>
</div>

<div class="form-group">
  {!! Form::label('in_dre', 'Exibe DRE', ['class' => 'control-label col-sm-3 col-md-offset-2']) !!}
  <div class="col-sm-3 input-group">
    <div class="input-group-addon"><i class="fa fa-bullseye"></i></div>
    {!! Form::select('in_dre', [1=>'Sim', 0=>'Não'], !empty($simulation) ? $simulation->in_dre: null, ['class' => 'form-control', 'required', '']); !!}
  </div>
</div>

<div class="form-group">
  {!! Form::label('in_balance', 'Exibe Balanço Patrimonial', ['class' => 'control-label col-sm-3 col-md-offset-2']) !!}
  <div class="col-sm-3 input-group">
    <div class="input-group-addon"><i class="fa fa-bullseye"></i></div>
    {!! Form::select('in_balance', [1=>'Sim', 0=>'Não'], !empty($simulation) ? $simulation->in_balance : null, ['class' => 'form-control', 'required', '']); !!}
  </div>
</div>

<div class="form-group">
  {!! Form::label('in_cashflow', 'Exibe Ranking', ['class' => 'control-label col-sm-3 col-md-offset-2']) !!}
  <div class="col-sm-3 input-group">
    <div class="input-group-addon"><i class="fa fa-bullseye"></i></div>
    {!! Form::select('in_ranking', [1=>'Sim', 0=>'Não'], !empty($simulation) ? $simulation->in_ranking : null, ['class' => 'form-control', 'required', '']); !!}
  </div>
</div>

<div class="form-group">
  {!! Form::label('in_cashflow', 'Exibe Transporte', ['class' => 'control-label col-sm-3 col-md-offset-2']) !!}
  <div class="col-sm-3 input-group">
    <div class="input-group-addon"><i class="fa fa-bullseye"></i></div>
    {!! Form::select('in_transport', [1=>'Sim', 0=>'Não'], !empty($simulation) ? $simulation->in_transport : null, ['class' => 'form-control', 'required', '']); !!}
  </div>
</div>

<div class="form-group">
  {!! Form::label('locale', 'Idioma', ['class' => 'control-label col-sm-3 col-md-offset-2']) !!}
  <div class="col-sm-3 input-group">
    <div class="input-group-addon"><i class="fa fa-bullseye"></i></div>
    {!! Form::select('locale', ['pt-br'=>'Português', 'en'=>'Inglês'], !empty($simulation) ? $simulation->locale : null, ['class' => 'form-control', 'required', '']); !!}
  </div>
</div> 
