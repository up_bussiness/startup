<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<title>Logistics Game - Gerenciamento</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
</head>
<body>

<div align="center">
    <img src="{{asset('images/logo.png')}}" style="width:360px;"><br/>
    <h3>Desculpe, sua sessão expirou. Tente novamente:</h3><br/>
    <a href="http://{{ request()->getHost() }}" class="btn btn-primary">Acessar Logisticsgame</a>
</div>


</body>
</html>