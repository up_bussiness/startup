@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
	<h1>{{__('labels.simulationConfiguration')}}</h1>
	<link rel="stylesheet" type="text/css" href="introjs.css">
@stop

@section('content_title')
    <h3 class="box-title"><i class="fa fa-gear"></i> {{__('labels.listConfiguration')}}</h3>    
@stop

@section('content')



	<hr>
	<div class="row" data-intro="Aqui podemos obter informações sobre a configuração das empresas" data-step="5">
    <div class="col-md-11">	
		@include('layouts.partials.configSplash')		
    </div>
      <div class="col-sm-2 col-md-6 col-md-6">
			<div class="table-responsive">
			  <table class="table table-striped table-bordered">
			    <thead>
			      <tr>
			      <th>#</th>
			      <th>{{__('labels.nameCompany')}}</th>
			      <th class="table-collapse text-center">{{__('labels.configured')}}</th>
			      </tr>
			    </thead>
			    <tbody>
              @foreach($companys->sortBy('id') as $key => $company)
			        <tr>
                <td class="table-collapse">{{ ++$key }}</td>
				        <td>{{ $company->name }}</td>
                <td class="table-collapse text-center">
				        	{!! $company->configured == 0 ? '<span class="label label-danger">NÂO</span>' : '<span class="label label-success">SIM</span>' !!}
				        </td>
			        </tr>
			        @endforeach
			    </tbody>
				</table>
			</div>
		</div>
	</div>

	<script type="text/javascript" src="intro.js"></script>
	<!-- Scripts -->
    <script src="{{ asset('js/intro.js') }}" defer></script>
@stop