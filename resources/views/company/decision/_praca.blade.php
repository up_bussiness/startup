@if(!empty($decision))
{!! Form::model($decision, ['route' => ['company.decision.update', $decision->id], 'method' => 'put', 'class'=>'form-horizontal', 'id' => 'decision']) !!}
@else
{!! Form::open(['route' => 'company.decision.store', 'class'=>'form-horizontal', 'autocomplete' => 'off', 'id' => 'decision']) !!}
@endif

<div id="Praca">
  <div>
    <img src="{{ asset('images/praca.jpg') }}" usemap="#image-map" onload="loadImage()">
    <map name="image-map">
      <area target="" alt="propaganda" coords="725,197,88" onclick="funcaoPropaganda();" style="cursor: pointer;" title="Agência Propaganda" shape="circle">
      <area target="" alt="máquinas" coords="1227,261,86" onclick="funcaoMaquinas();" style="cursor: pointer;" title="Máquinas da Produção" shape="circle">
      <area target="" alt="finanças" coords="681,536,74" onclick="funcaoFinancas();" style="cursor: pointer;" title="Banco" shape="circle">
      <area target="" alt="produção" coords="464,419,78" onclick="funcaoProducao();" style="cursor: pointer;" title="Matéria prima" shape="circle">
      <area target="" alt="empresa" coords="1190,551,86" onclick="funcaoEmpresa();" style="cursor: pointer;" title="Empresa" shape="circle">
    </map>
  </div>
  <button class='btn btn-default botao2' type='submit' value='submit'>
    <i class='fa fa-play cfgicon'> </i> {{ !empty($decision) ? 'Atualizar decisão' : 'Tomar decisão'}}
  </button>
</div>
<script>
  function funcaoPropaganda() {
    $('#propaganda').modal();
  }

  function funcaoMaquinas() {
    $('#maquinas').modal();
  }

  function funcaoFinancas() {
    $('#financas').modal();
  }

  function funcaoProducao() {
    $('#producao').modal();
  }

  function funcaoEmpresa() {
    $('#decisao').modal();
  }

  function loadImage() {
    $('img[usemap]').rwdImageMaps();
  }

  function MostrarExplicacao(id) {
    var x = 'explica' + id;
    $('#' + x).removeClass('hidden');
  }

  function controleMais(campo, percentual) {
    var ca = '#' + campo;
    var pe = '#' + percentual;
    var x = parseInt($(ca).val());
    if (x == 0 && pe != '#') {
      $(pe).removeClass('hidden');
    }
    $(ca).val(x + 1);
  }

  function controleMenos(campo, percentual) {
    var ca = '#' + campo;
    var pe = '#' + percentual;
    var x = parseInt($(ca).val());
    if (x != 0) {
      $(ca).val(x - 1);
    }
    if (x == 1 && pe != '#') {
      $(pe).addClass('hidden');
    }
  }
  $(function() {
    $("#decision").submit(function(e) {
      e.stopPropagation();

      var $bt = $(this).find(':submit');

      $bt.attr('disabled', 'disabled');
      $bt.val('Gravando...');
      $bt.css('cursor', 'progress');
    });

    $('[id*=adv_]').change(function(e) {
      if ($(this).val() == null) {
        $(this).val(0);
      }
    })

    if ($('#term').val() > 0) {
      $('#fg_term_interest').removeClass('hidden');
    }
    $('#term').change(function() {
      if ($('#term').val() > 0) {
        $('#fg_term_interest').removeClass('hidden');
      } else {
        $('#term_interest').val(0);
        $('#fg_term_interest').addClass('hidden');
      }
    });

    if ($('#buy_small_machine').val() > 0) {
      $('#b_s_m_id').removeClass('hidden');
    }
    if ($('#buy_medium_machine').val() > 0) {
      $('#b_m_m_id').removeClass('hidden');
    }
    if ($('#buy_large_machine').val() > 0) {
      $('#b_l_m_id').removeClass('hidden');
    }

    $('#buy_small_machine, #buy_medium_machine, #buy_large_machine').change(function() {
      let val = $(this).val();
      let id = $(this).attr('id');
      if (val != 0) {
        if (id == 'buy_small_machine') {
          $('#b_s_m_id').removeClass('hidden');
        }
        if (id == 'buy_medium_machine') {
          $('#b_m_m_id').removeClass('hidden');
        }
        if (id == 'buy_large_machine') {
          $('#b_l_m_id').removeClass('hidden');
        }
      } else {
        if (id == 'buy_small_machine') {
          $('#b_s_m_id').addClass('hidden');
        }
        if (id == 'buy_medium_machine') {
          $('#b_m_m_id').addClass('hidden');
        }
        if (id == 'buy_large_machine') {
          $('#b_l_m_id').addClass('hidden');
        }
      }
    });
  });
</script>

<!--    MODAIS    -->
<div class="modal modal-default" id="propaganda">
  <div class="modal-dialog">
    <div class="modal-content cfgmodalMKT">
      <div class="modal-header modal-header-x">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title letrabranca">COMERCIAL - PROPAGANDA</h4>
      </div>
      <div class="modal-body">
        <div id="explica1" align="justify" class="hidden">Parte da demanda geral de mercado será distribuída aos fabricantes de bicicletas de acordo com as suas estratégias de precificação e marketing. Portanto, decida qual será sua melhor estratégia para conquistar o market share que planejou.</div>
        <span class="fa fa-question fa-2x pull-right" style="cursor: pointer;" onclick="MostrarExplicacao(1)"></span></br>
        <div class="form-group">
          {!! Form::label('adv_radio', 'Radio', ['class' => 'control-label col-sm-3 col-md-offset-2']) !!}
          <div class="col-sm-4 input-group">
            <div class="input-group-addon"><i class="fa fa-bullhorn"></i></div>
            {!! Form::selectRange('adv_radio', 0, 5, !empty($decision) ? null : 0, ['class' => 'form-control']); !!}
          </div>
        </div>

        <div class="form-group">
          {!! Form::label('adv_journal', 'Jornal', ['class' => 'control-label col-sm-3 col-md-offset-2']) !!}
          <div class="col-sm-4 input-group">
            <div class="input-group-addon"><i class="fa fa-bullhorn"></i></div>
            {!! Form::selectRange('adv_journal', 0, 5, !empty($decision) ? null : 0, ['class' => 'form-control']); !!}
          </div>
        </div>

        <div class="form-group">
          {!! Form::label('adv_social', 'Mídias Sociais', ['class' => 'control-label col-sm-3 col-md-offset-2']) !!}
          <div class="col-sm-4 input-group">
            <div class="input-group-addon"><i class="fa fa-bullhorn"></i></div>
            {!! Form::selectRange('adv_social', 0, 5, !empty($decision) ? null : 0, ['class' => 'form-control']); !!}
          </div>
        </div>

        <div class="form-group">
          {!! Form::label('adv_outdoor', 'Outdoor', ['class' => 'control-label col-sm-3 col-md-offset-2']) !!}
          <div class="col-sm-4 input-group">
            <div class="input-group-addon"><i class="fa fa-bullhorn"></i></div>
            {!! Form::selectRange('adv_outdoor', 0, 5, !empty($decision) ? null : 0, ['class' => 'form-control']); !!}
          </div>
        </div>

        <div class="form-group">
          {!! Form::label('adv_tv', 'TV', ['class' => 'control-label col-sm-3 col-md-offset-2']) !!}
          <div class="col-sm-4 input-group">
            <div class="input-group-addon"><i class="fa fa-bullhorn"></i></div>
            {!! Form::selectRange('adv_tv', 0, 5, !empty($decision) ? null : 0, ['class' => 'form-control']); !!}
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success botao1" data-dismiss="modal"> Continuar </button>
      </div>
    </div>
  </div>
</div>
<div class="modal modal-default" id="maquinas">
  <div class="modal-dialog">
    <div class="modal-content cfgmodalFMQ">
      <div class="modal-header modal-header-x">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title letrabranca">COMPRA E VENDA LINHAS DE PRODUÇÃO</h4>
      </div>
      <div class="modal-body">
        <div id="explica2" align="justify" class="hidden">Caso queira comprar máquina, você deve inserir a quantidade e clicar fora do campo, para que o sistema habilite e campo de recurso próprio. Nele, você deve escolher quanto sua empresa vai investir de capital próprio na compra da nova máquina.
          Caso queira vender uma de suas linhas de produção, você deve preencher, no campo da referida linha que deseja vender, a quantidade de linhas que serão vendidas. Vale a pena lembrar que sua empresa não pode vender todas as máquinas, e que o valor da venda é o valor de aquisição, menos a depreciação e a desvalorização de mercado, publicada no informativo de mercado. O valor dessa venda será creditado no final do período.</div>
        <span class="fa fa-question fa-2x pull-right" style="cursor: pointer;" onclick="MostrarExplicacao(2)"></span></br>
        Compra de Máquinas:
        <div class="form-group">
          {!! Form::label('buy_small_machine', 'Pequena', ['class' => 'control-label col-sm-4 col-md-offset-1']) !!}
          <div class="col-sm-4 input-group">
            <div class="input-group-addon">N.</div>
            {!! Form::text('buy_small_machine', !empty($decision) ? null : 0, ['class' => 'form-control']); !!}
            <span class="input-group-btn">
              <button class="btn btn-default" onclick="controleMais('buy_small_machine','b_s_m_id')" type="button"><i class="glyphicon glyphicon-plus"></i></button>
              <button class="btn btn-default" onclick="controleMenos('buy_small_machine','b_s_m_id')" type="button"><i class="glyphicon glyphicon-minus"></i></button>
            </span>
          </div>
        </div>

        <div class="form-group hidden" id="b_s_m_id">
          {!! Form::label('b_s_m', 'Recurso próprio (P)', ['class' => 'control-label col-sm-4 col-md-offset-1']) !!}
          <div class="col-sm-4 input-group">
            {!! Form::selectRange('b_s_m', 30, 100, !empty($decision) ? null : 0, ['class' => 'form-control']); !!}
          </div>
        </div>

        <div class="form-group">
          {!! Form::label('buy_medium_machine', 'Média', ['class' => 'control-label col-sm-4 col-md-offset-1']) !!}
          <div class="col-sm-4 input-group">
            <div class="input-group-addon">N.</div>
            {!! Form::text('buy_medium_machine', !empty($decision) ? null : 0, ['class' => 'form-control']); !!}
            <span class="input-group-btn">
              <button class="btn btn-default" onclick="controleMais('buy_medium_machine','b_m_m_id')" type="button"><i class="glyphicon glyphicon-plus"></i></button>
              <button class="btn btn-default" onclick="controleMenos('buy_medium_machine','b_m_m_id')" type="button"><i class="glyphicon glyphicon-minus"></i></button>
            </span>
          </div>
        </div>

        <div class="form-group hidden" id="b_m_m_id">
          {!! Form::label('b_m_m', 'Recurso próprio (M)', ['class' => 'control-label col-sm-4 col-md-offset-1']) !!}
          <div class="col-sm-4 input-group">
            {!! Form::selectRange('b_m_m', 30, 100, !empty($decision) ? null : 0, ['class' => 'form-control']); !!}
          </div>
        </div>

        <div class="form-group">
          {!! Form::label('buy_large_machine', 'Grande', ['class' => 'control-label col-sm-4 col-md-offset-1']) !!}
          <div class="col-sm-4 input-group">
            <div class="input-group-addon">N.</div>
            {!! Form::text('buy_large_machine', !empty($decision) ? null : 0, ['class' => 'form-control']); !!}
            <span class="input-group-btn">
              <button class="btn btn-default" onclick="controleMais('buy_large_machine','b_l_m_id')" type="button"><i class="glyphicon glyphicon-plus"></i></button>
              <button class="btn btn-default" onclick="controleMenos('buy_large_machine','b_l_m_id')" type="button"><i class="glyphicon glyphicon-minus"></i></button>
            </span>
          </div>
        </div>

        <div class="form-group hidden" id="b_l_m_id">
          {!! Form::label('b_l_m', 'Recurso próprio (G)', ['class' => 'control-label col-sm-4 col-md-offset-1']) !!}
          <div class="col-sm-4 input-group">
            {!! Form::selectRange('b_l_m', 30, 100, !empty($decision) ? null : 0, ['class' => 'form-control']); !!}
          </div>
        </div>
        Venda de Máquinas:
        <div class="form-group">
          {!! Form::label('sell_small_machine', 'Pequena', ['class' => 'control-label col-sm-4 col-md-offset-1']) !!}
          <div class="col-sm-4 input-group">
            <div class="input-group-addon">N.</div>
            {!! Form::text('sell_small_machine', !empty($decision) ? null : 0, ['class' => 'form-control']); !!}
            <span class="input-group-btn">
              <button class="btn btn-default" onclick="controleMais('sell_small_machine','')" type="button"><i class="glyphicon glyphicon-plus"></i></button>
              <button class="btn btn-default" onclick="controleMenos('sell_small_machine','')" type="button"><i class="glyphicon glyphicon-minus"></i></button>
            </span>
          </div>
        </div>

        <div class="form-group">
          {!! Form::label('sell_medium_machine', 'Média', ['class' => 'control-label col-sm-4 col-md-offset-1']) !!}
          <div class="col-sm-4 input-group">
            <div class="input-group-addon">N.</div>
            {!! Form::text('sell_medium_machine', !empty($decision) ? null : 0, ['class' => 'form-control']); !!}
            <span class="input-group-btn">
              <button class="btn btn-default" onclick="controleMais('sell_medium_machine','')" type="button"><i class="glyphicon glyphicon-plus"></i></button>
              <button class="btn btn-default" onclick="controleMenos('sell_medium_machine','')" type="button"><i class="glyphicon glyphicon-minus"></i></button>
            </span>
          </div>
        </div>

        <div class="form-group">
          {!! Form::label('sell_large_machine', 'Grande', ['class' => 'control-label col-sm-4 col-md-offset-1']) !!}
          <div class="col-sm-4 input-group">
            <div class="input-group-addon">N.</div>
            {!! Form::text('sell_large_machine', !empty($decision) ? null : 0, ['class' => 'form-control']); !!}
            <span class="input-group-btn">
              <button class="btn btn-default" onclick="controleMais('sell_large_machine','')" type="button"><i class="glyphicon glyphicon-plus"></i></button>
              <button class="btn btn-default" onclick="controleMenos('sell_large_machine','')" type="button"><i class="glyphicon glyphicon-minus"></i></button>
            </span>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success botao1" data-dismiss="modal"> Continuar </button>
      </div>
    </div>
  </div>
</div>
<div class="modal modal-default" id="financas">
  <div class="modal-dialog">
    <div class="modal-content cfgmodalFIN">
      <div class="modal-header modal-header-x">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title letrabranca">FINANÇAS</h4>
      </div>
      <div class="modal-body">
        <div id="explica3" align="justify" class="hidden">Nestes campos sua empresa deverá tomar decisões que possibilitem uma boa gestão de seus recursos financeiros.
          No caos de empréstimo, importante ressaltar que o limite de crédito concedido pelo banco, é o mesmo para empréstimos e financiamento de linhas de produção.
          Caso solicite antecipação de recebíveis, o recurso será creditado neste mesmo período, descontado as taxas de juros praticadas pelo banco.
          No caso de decidir aplicar recursos, o valor solicitado só será efetivamente aplicado após liquidação das obrigações financeiras com seus funcionários, fornecedores e bancos, e o mesmo deverá retornar ao caixa automaticamente no próximo período acrescido dos juros.
        </div>
        <span class="fa fa-question fa-2x pull-right" style="cursor: pointer;" onclick="MostrarExplicacao(3)"></span></br>
        <div class="form-group">
          {!! Form::label('loan', 'Empréstimo', ['class' => 'control-label col-sm-5']) !!}
          <div class="col-sm-4 input-group">
            <div class="input-group-addon">$</div>
            {!! Form::text('loan', !empty($decision) ? null : 0, ['class' => 'form-control']); !!}
          </div>
        </div>

        <div class="form-group">
          {!! Form::label('anticipation_of_receivables', 'Antecipação de Recebíveis', ['class' => 'control-label col-sm-5']) !!}
          <div class="col-sm-4 input-group">
            <div class="input-group-addon">%</div>
            {!! Form::text('anticipation_of_receivables', !empty($decision) ? null : 0, ['class' => 'form-control']); !!}
          </div>
        </div>

        <div class="form-group">
          {!! Form::label('application', 'Aplicação', ['class' => 'control-label col-sm-5']) !!}
          <div class="col-sm-4 input-group">
            <div class="input-group-addon">%</div>
            {!! Form::text('application', !empty($decision) ? null : 0, ['class' => 'form-control']); !!}
          </div>
        </div>


      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success botao1" data-dismiss="modal"> Continuar </button>
      </div>
    </div>
  </div>
</div>
<div class="modal modal-default" id="producao">
  <div class="modal-dialog">
    <div class="modal-content cfgmodalMP">
      <div class="modal-header modal-header-x">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title letrabranca">COMPRA DE MATERIAS-PRIMAS</h4>
      </div>
      <div class="modal-body">
        <div class="panel">
          <div class="panel-heading" style="background-image:none;background: #2b569f; color: white"><span class="fa fa-question fa-2x pull-right" style="cursor: pointer;" onclick="MostrarExplicacao(4)"></span>EMERGENCIAL</div>
          <div class="panel-body cfgmodalMP">
            <div id="explica4" align="justify" class="hidden">Aqui, seu time deve decidir a compra de matérias-primas para suprir uma necessidade emergencial, por conta de uma compra programada equivocada ou por decisão de produção extra.</div>
            <div class="form-group">
              {!! Form::label('emergency_rma', 'Emergencial - Quadro', ['class' => 'control-label col-sm-4 col-md-offset-1']) !!}
              <div class="col-sm-4 input-group">
                <div class="input-group-addon">N.</div>
                {!! Form::text('emergency_rma', !empty($decision) ? null : 0, ['class' => 'form-control']); !!}
              </div>
            </div>

            <div class="form-group">
              {!! Form::label('emergency_rmb', 'Kit 1 (Conjunto de roda/pneus)', ['class' => 'control-label col-sm-4 col-md-offset-1']) !!}
              <div class="col-sm-4 input-group">
                <div class="input-group-addon">N.</div>
                {!! Form::text('emergency_rmb', !empty($decision) ? null : 0, ['class' => 'form-control']); !!}
              </div>
            </div>

            <div class="form-group">
              {!! Form::label('emergency_rmc', 'Kit 2 (Acessórios)', ['class' => 'control-label col-sm-4 col-md-offset-1']) !!}
              <div class="col-sm-4 input-group">
                <div class="input-group-addon">N.</div>
                {!! Form::text('emergency_rmc', !empty($decision) ? null : 0, ['class' => 'form-control']); !!}
              </div>
            </div>

          </div>
        </div>
        <div class="panel">
          <div class="panel-heading" style="background-image:none;background: #2b569f; color: white"><span class="fa fa-question fa-2x pull-right" style="cursor: pointer;" onclick="MostrarExplicacao(5)"></span>PARA O PRÓXIMO PERÍODO</div>
          <div class="panel-body cfgmodalMP">
            <div id="explica5" align="justify" class="hidden">Neste momento, seu time deverá comprar matéria-prima suficiente para suprir a capacidade produtiva de suas linhas de produção, para o próximo período.</div>
            <div class="form-group">
              {!! Form::label('scheduled_rma', 'Quadro', ['class' => 'control-label col-sm-4 col-md-offset-1']) !!}
              <div class="col-sm-4 input-group">
                <div class="input-group-addon">N.</div>
                {!! Form::text('scheduled_rma', !empty($decision) ? null : 0, ['class' => 'form-control']); !!}
              </div>
            </div>

            <div class="form-group">
              {!! Form::label('scheduled_rmb', 'Kit 1 (Conjunto de roda/pneus)', ['class' => 'control-label col-sm-4 col-md-offset-1']) !!}
              <div class="col-sm-4 input-group">
                <div class="input-group-addon">N.</div>
                {!! Form::text('scheduled_rmb', !empty($decision) ? null : 0, ['class' => 'form-control']); !!}
              </div>
            </div>

            <div class="form-group">
              {!! Form::label('scheduled_rmc', 'Kit 2 (Acessórios)', ['class' => 'control-label col-sm-4 col-md-offset-1']) !!}
              <div class="col-sm-4 input-group">
                <div class="input-group-addon">N.</div>
                {!! Form::text('scheduled_rmc', !empty($decision) ? null : 0, ['class' => 'form-control']); !!}
              </div>
            </div>

            <div class="form-group">
              {!! Form::label('payment_rm', 'Forma de Pagamento', ['class' => 'control-label col-sm-4 col-md-offset-1']) !!}
              <div class="col-sm-4 input-group">
                <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                {!! Form::select('payment_rm', ['à vista', '1+1'], !empty($decision) ? null : 0, ['class' => 'form-control']); !!}
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success botao1" data-dismiss="modal"> Continuar </button>
      </div>
    </div>
  </div>
</div>
<div class="modal modal-default" id="decisao">
  <div class="modal-dialog">
    <div class="modal-content cfgmodalFAB">
      <div class="modal-header modal-header-x">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title letrabranca">DECISÃO DA EMPRESA</h4>
      </div>
      <div class="modal-body">
        <div class="panel">
          <div class="panel-heading" style="background-image:none;background: #0086a3; color: white"><span class="fa fa-question fa-2x pull-right" style="cursor: pointer;" onclick="MostrarExplicacao(6)"></span>COMERCIAL</div>
          <div class="panel-body cfgmodalFAB">
            <div id="explica6" align="justify" class="hidden">Nestes campos, você e seu time deverão decidir qual preço e condições de pagamento serão competitivos para geração de demanda suficiente para venda de toda sua produção e estoque de produtos acabados.</div>
            <div class="col-md-12">
              <div class="alert alert-success text-center">
                <h5><b>Saldo em caixa: $ {{ numberDot($total_cash) }}</b></h5>
                <h5><b>Limite de empréstimo: $ {{ numberDot($limit_loan) }}</b></h5>
              </div>
            </div>
            <div class="form-group">
              {!! Form::label('price', 'Preço à vista', ['class' => 'control-label col-sm-3 col-md-offset-2']) !!}
              <div class="col-sm-4 input-group">
                <div class="input-group-addon">$</div>
                {!! Form::text('price', !empty($decision) ? null : $price, ['class' => 'form-control']); !!}
              </div>
            </div>

            <div class="form-group">
              {!! Form::label('term', 'Prazo', ['class' => 'control-label col-sm-3 col-md-offset-2']) !!}
              <div class="col-sm-4 input-group">
                <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                {!! Form::select('term', ['à vista', '1+1'], !empty($decision) ? null : 0, ['class' => 'form-control']); !!}
              </div>
            </div>

            <div class="form-group hidden" id="fg_term_interest">
              {!! Form::label('term_interest', 'Juros nas vendas a prazo', ['class' => 'control-label col-sm-5']) !!}
              <div class="col-sm-4 input-group">
                <div class="input-group-addon">%</div>
                {!! Form::text('term_interest', !empty($decision) ? null : 0, ['class' => 'form-control']); !!}
              </div>
            </div>
          </div>
        </div>

        <div class="panel">
          <div class="panel-heading" style="background-image:none;background: #0086a3; color: white"><span class="fa fa-question fa-2x pull-right" style="cursor: pointer;" onclick="MostrarExplicacao(8)"></span>PLANEJAMENTO DA PRODUÇÃO DE MÁQUINAS</div>
          <div class="panel-body cfgmodalFAB">
            <div id="explica8" align="justify" class="hidden">Seu time deve registrar o nível de atividade que suas linhas de produção irão operar para produzir a quantidade que desejam. Elas podem operar com níveis entre 50% e 100%.
              Caso necessite de produção extra, essa poderá ser programada com nível de 1% a 25%.para que tudo ocorra como planejado, saiba qual a capacidade produtiva de suas linhas de produção, verifique o estoque de matérias-primas e analise se o número de operários é suficiente.
            </div>
            <div class="form-group">
              {!! Form::label('activity_level', 'Nível de atividade', ['class' => 'control-label col-sm-4 col-md-offset-1']) !!}
              <div class="col-sm-4 input-group">
                <div class="input-group-addon">%</div>
                {!! Form::text('activity_level', !empty($decision) ? null : 0, ['class' => 'form-control']); !!}
              </div>
            </div>

            <div class="form-group">
              {!! Form::label('extra_production', 'Produção extra', ['class' => 'control-label col-sm-4 col-md-offset-1']) !!}
              <div class="col-sm-4 input-group">
                <div class="input-group-addon">%</div>
                {!! Form::text('extra_production', !empty($decision) ? null : 0, ['class' => 'form-control']); !!}
              </div>
            </div>

          </div>
        </div>

        <div class="panel">
          <div class="panel-heading" style="background-image:none;background: #0086a3; color: white"><span class="fa fa-question fa-2x pull-right" style="cursor: pointer;" onclick="MostrarExplicacao(7)"></span>RH</div>
          <div class="panel-body cfgmodalFAB">
            <div id="explica7" align="justify" class="hidden">As decisões de recursos humanos são importantíssimas, elas se dão na contratação ou demissão de operários por conta de compra ou venda de linhas de produção, fique atento ao que o manual orienta.
              Com relação ao salário dos seus operário e treinamento, observe o indicador de produtividade no relatório mercadológico, ele deverá nortear a necessidade de motivar seu time.</div>
            <div class="form-group">
              {!! Form::label('admitted', 'Admitidos', ['class' => 'control-label col-sm-3 col-md-offset-2']) !!}
              <div class="col-sm-4 input-group">
                <div class="input-group-addon">N.</div>
                {!! Form::text('admitted', !empty($decision) ? null : 0, ['class' => 'form-control']); !!}
              </div>
            </div>

            <div class="form-group">
              {!! Form::label('fired', 'Demitidos', ['class' => 'control-label col-sm-3 col-md-offset-2']) !!}
              <div class="col-sm-4 input-group">
                <div class="input-group-addon">N.</div>
                {!! Form::text('fired', !empty($decision) ? null : 0, ['class' => 'form-control']); !!}
              </div>
            </div>

            <div class="form-group">
              {!! Form::label('salary', 'Salário', ['class' => 'control-label col-sm-3 col-md-offset-2']) !!}
              <div class="col-sm-4 input-group">
                <div class="input-group-addon">$</div>
                {!! Form::text('salary', !empty($decision) ? null : $salary, ['class' => 'form-control']); !!}
              </div>
            </div>

            <div class="form-group">
              {!! Form::label('training', 'Treinamento', ['class' => 'control-label col-sm-3 col-md-offset-2']) !!}
              <div class="col-sm-4 input-group">
                <div class="input-group-addon">%</div>
                {!! Form::text('training', !empty($decision) ? null : 0, ['class' => 'form-control']); !!}
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success botao1" data-dismiss="modal"> Continuar </button>
      </div>
    </div>
  </div>
</div>