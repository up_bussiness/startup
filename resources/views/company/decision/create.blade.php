@extends('adminlte::page')

@section('title', 'Decisão')

@section('content_header')
    <h1>{{ !empty($decision) ? $decision->turn->simulation->name : $turn->simulation->name }}</h1>
@stop

@section('content_title')
    <h3 class="box-title">Decisão da empresa - Rodada {{ !empty($decision) ? $decision->turn->month : $turn->month }}</h3>
@stop

@section('content')
    @include('company.decision._create')

		<div class="col-md-12 text-center">
          	{!! Form::submit(!empty($decision) ? 'Atualizar decisão' : 'Tomar decisão', ['class' => 'btn btn-success']); !!}    
		</div>
	
		@if(empty($decision))
			{!! Form::hidden('turn_id', $turn->id) !!}	
		@else
			{!! Form::hidden('turn_id', $decision->turn_id) !!}	
		@endif


    {!! Form::close() !!}
@stop
