<!DOCTYPE html>
<html lang="pt-br">

<head>
  <meta charset="UTF-8">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <title>Logistics Game - Gerenciamento</title>
  {{-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
            <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css"> --}}
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="{{ asset('vendor/adminlte/vendor/bootstrap/dist/css/bootstrap.min.css') }}">
  <style>
    img[usemap] {
      border: none;
      height: auto;
      max-width: 100%;
      width: auto;
    }

    .vcenter {
      white-space: nowrap;
    }

    .vcenter>* {
      white-space: normal;
    }

    .vcenter:before,
    .vcenter>* {
      display: inline-block;
      vertical-align: middle;
    }

    .vcenter:before {
      content: "";
      height: 100%;
    }

    .vcenter>* {
      word-spacing: normal;
    }

    .botao1 {
      border-radius: 5px;
      border-color: #000000;
      border-width: medium;
      color: white;
      font-weight: bold;
      background-color: #0077ee;
    }

    .botao2 {
      position: absolute;
      top: 7%;
      right: 7%;
      border-radius: 5px;
      border-color: #000000;
      border-width: medium;
      color: white;
      font-weight: bold;
      background-color: #0077ee;
    }

    .cfgicon {
      font-size: 16px;
    }

    .cfgmodalMP {
      border-color: #000000;
      border-width: medium;
      background-color: #ff5b74;
    }

    .cfgmodalMPb {
      background: red;
    }

    .cfgmodalFAB {
      border-color: #000000;
      border-width: medium;
      background-color: #ff825c;
    }

    .cfgmodalFMQ {
      border-color: #000000;
      border-width: medium;
      background-color: #ff6445;
    }

    .cfgmodalFIN {
      border-color: #000000;
      border-width: medium;
      background-color: #06cbb7;
    }

    .cfgmodalMKT {
      border-color: #000000;
      border-width: medium;
      background-color: #ff574e;
    }

    .letrabranca {
      color: white;
      font-weight: bold;
    }
  </style>
</head>

<body style="background-color:#3bcd8f">
  {{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script> --}}
  <script src="{{ asset('vendor/adminlte/vendor/jquery/dist/jquery.min.js') }}"></script>
  <script src="{{ asset('vendor/adminlte/vendor/bootstrap/dist/js/bootstrap.min.js') }}"></script>
  <script src="{{ asset('js/jquery.rwdImageMaps.min.js') }}"></script>

  <div class="vcenter" style="border:0px solid #3bcd8f;background: #3bcd8f;height: 30px;color: #000000">
    {{ !empty($decision) ? $decision->turn->simulation->name : $turn->simulation->name }} -
    Decisão da empresa - Rodada {{ !empty($decision) ? $decision->turn->month : $turn->month }}
    <a href="{{ isCoordinator() ? route('simulation.turn.show', ['simulation' => $cashflow->turn->simulation_id, 'id' => $cashflow->turn_id]) : route('company') }}" class="btn btn-xs btn-default botao1 pull-right">VOLTAR</a>
  </div>
  @include('layouts.partials.flash')
  @include('company.decision._praca')
  {{--
  <div class="col-md-12 text-center"></br>
    {!! Form::submit(!empty($decision) ? 'Atualizar decisão' : 'Tomar decisão', ['class' => 'btn btn-success']); !!}
  </div> --}}

  @if(empty($decision))
  {!! Form::hidden('turn_id', $turn->id) !!}
  @else
  {!! Form::hidden('turn_id', $decision->turn_id) !!}
  @endif

  {!! Form::close() !!}


</body>

</html>