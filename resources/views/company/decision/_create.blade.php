@if(!empty($decision))
  {!! Form::model($decision, ['route' => ['company.decision.update', $decision->id], 'method' => 'put', 'class'=>'form-horizontal', 'id' => 'decision']) !!}
@else
  {!! Form::open(['route' => 'company.decision.store', 'class'=>'form-horizontal', 'autocomplete' => 'off', 'id' => 'decision']) !!}
@endif 
<div class="row">

  <div class="col-md-12">
    <div class="alert alert-success text-center">
      <h4><b>Saldo em caixa: $ {{ numberDot($total_cash) }}</b></h4>
      <h4><b>Limite de empréstimo: $ {{ numberDot($limit_loan) }}</b></h4>
    </div>
  </div>

  <div class="col-md-12">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title">COMERCIAL<span class="fa fa-question pull-right" style="cursor: pointer;" onclick="Explicar(7)"></span></h3>
      </div>
      <div class="panel-body">
        <div class="form-group">
          {!! Form::label('price', 'Preço à vista', ['class' => 'control-label col-sm-3 col-md-offset-2']) !!}
          <div class="col-sm-4 input-group">
            <div class="input-group-addon">$</div>
            {!! Form::text('price', !empty($decision) ? null : $price, ['class' => 'form-control']); !!}
          </div>
        </div>

        <div class="form-group">
          {!! Form::label('term', 'Prazo', ['class' => 'control-label col-sm-3 col-md-offset-2']) !!}
          <div class="col-sm-4 input-group">
            <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
            {!! Form::select('term', ['à vista', '1+1'], !empty($decision) ? null : 0, ['class' => 'form-control']); !!}
          </div>
        </div>

        <div class="form-group hidden" id="fg_term_interest">
          {!! Form::label('term_interest', 'Juros nas vendas a prazo', ['class' => 'control-label col-sm-5']) !!}
          <div class="col-sm-4 input-group">
            <div class="input-group-addon">%</div>
            {!! Form::text('term_interest', !empty($decision) ? null : 0, ['class' => 'form-control']); !!}
          </div>
        </div>
      </div>
    </div>
    <hr>
  </div>

  <div class="col-md-12">
    <div class="panel panel-info">
      <div class="panel-heading">
        <h3 class="panel-title">PLANEJAMENTO DA PRODUÇÃO DE MÁQUINAS<span class="fa fa-question pull-right" style="cursor: pointer;" onclick="Explicar(8)"></span></h3>
      </div>
      <div class="panel-body">

        <div class="form-group">
          {!! Form::label('activity_level', 'Nível de atividade', ['class' => 'control-label col-sm-4 col-md-offset-1']) !!}
          <div class="col-sm-4 input-group">
            <div class="input-group-addon">%</div>
            {!! Form::text('activity_level', !empty($decision) ? null : 0, ['class' => 'form-control']); !!}
          </div>
        </div>

        <div class="form-group">
          {!! Form::label('extra_production', 'Produção extra', ['class' => 'control-label col-sm-4 col-md-offset-1']) !!}
          <div class="col-sm-4 input-group">
            <div class="input-group-addon">%</div>
            {!! Form::text('extra_production', !empty($decision) ? null : 0, ['class' => 'form-control']); !!}
          </div>
        </div>

      </div>
    </div>
    <hr>
  </div>

  <div class="col-md-6">
        <div class="panel panel-default panel-primary">
          <div class="panel-heading">
            <h3 class="panel-title">COMERCIAL - PROPAGANDA<span class="fa fa-question pull-right" style="cursor: pointer;" onclick="Explicar(1)"></span></h3>
              
          </div>
          <div class="panel-body">
            <div class="form-group">
              {!! Form::label('adv_radio', 'Radio', ['class' => 'control-label col-sm-3 col-md-offset-2']) !!}
              <div class="col-sm-4 input-group">
                <div class="input-group-addon"><i class="fa fa-bullhorn"></i></div>
                {!! Form::selectRange('adv_radio', 0, 5, !empty($decision) ? null : 0, ['class' => 'form-control']); !!}
              </div>
            </div>

            <div class="form-group">
              {!! Form::label('adv_journal', 'Jornal', ['class' => 'control-label col-sm-3 col-md-offset-2']) !!}
              <div class="col-sm-4 input-group">
                <div class="input-group-addon"><i class="fa fa-bullhorn"></i></div>
                {!! Form::selectRange('adv_journal', 0, 5, !empty($decision) ? null : 0, ['class' => 'form-control']); !!}
              </div>
            </div>

            <div class="form-group">
              {!! Form::label('adv_social', 'Mídias Sociais', ['class' => 'control-label col-sm-3 col-md-offset-2']) !!}
              <div class="col-sm-4 input-group">
                <div class="input-group-addon"><i class="fa fa-bullhorn"></i></div>
                {!! Form::selectRange('adv_social', 0, 5, !empty($decision) ? null : 0, ['class' => 'form-control']); !!}
              </div>
            </div>

            <div class="form-group">
              {!! Form::label('adv_outdoor', 'Outdoor', ['class' => 'control-label col-sm-3 col-md-offset-2']) !!}
              <div class="col-sm-4 input-group">
                <div class="input-group-addon"><i class="fa fa-bullhorn"></i></div>
                {!! Form::selectRange('adv_outdoor', 0, 5, !empty($decision) ? null : 0, ['class' => 'form-control']); !!}
              </div>
            </div>

            <div class="form-group">
              {!! Form::label('adv_tv', 'TV', ['class' => 'control-label col-sm-3 col-md-offset-2']) !!}
              <div class="col-sm-4 input-group">
                <div class="input-group-addon"><i class="fa fa-bullhorn"></i></div>
                {!! Form::selectRange('adv_tv', 0, 5, !empty($decision) ? null : 0, ['class' => 'form-control']); !!}
              </div>
            </div>
          </div>
        </div>
  </div>

  <div class="col-md-6">
        <div class="panel panel-default panel-primary">
          <div class="panel-heading">
            <h3 class="panel-title">COMPRA DE MATERIAS-PRIMAS EMERGENCIAL<span class="fa fa-question pull-right" style="cursor: pointer;" onclick="Explicar(2)"></span></h3>
          </div>
          <div class="panel-body">
            <div class="form-group">
              {!! Form::label('emergency_rma', 'Emergencial - Quadro', ['class' => 'control-label col-sm-5']) !!}
              <div class="col-sm-4 input-group">
                <div class="input-group-addon">N.</div>
                {!! Form::text('emergency_rma', !empty($decision) ? null : 0, ['class' => 'form-control']); !!}
              </div>
            </div>

            <div class="form-group">
              {!! Form::label('emergency_rmb', 'Emergencial - Kit 1', ['class' => 'control-label col-sm-4 col-md-offset-1']) !!}
              <div class="col-sm-4 input-group">
                <div class="input-group-addon">N.</div>
                {!! Form::text('emergency_rmb', !empty($decision) ? null : 0, ['class' => 'form-control']); !!}
              </div>
            </div>

            <div class="form-group">
              {!! Form::label('emergency_rmc', 'Emergencial - Kit 2', ['class' => 'control-label col-sm-4 col-md-offset-1']) !!}
              <div class="col-sm-4 input-group">
                <div class="input-group-addon">N.</div>
                {!! Form::text('emergency_rmc', !empty($decision) ? null : 0, ['class' => 'form-control']); !!}
              </div>
            </div>

            
          </div>
        </div>
  </div>

  <div class="col-md-12">
    <hr>
  </div>

  <div class="col-md-6">
    <div class="panel panel-default" style="border:1px solid #f39c12">
          <div class="panel-heading" style="background: #f39c12; color: #fff">
            <h3 class="panel-title">COMPRA E VENDA LINHAS DE PRODUÇÃO<span class="fa fa-question pull-right" style="cursor: pointer;" onclick="Explicar(3)"></span></h3>
          </div>
          <div class="panel-body"><span class="badge badge-primary">Compra de Máquinas</span>
            <div class="form-group">
              {!! Form::label('buy_small_machine', 'Pequena', ['class' => 'control-label col-sm-4 col-md-offset-1']) !!}
              <div class="col-sm-4 input-group">
                <div class="input-group-addon">N.</div>
                {!! Form::text('buy_small_machine', !empty($decision) ? null : 0, ['class' => 'form-control']); !!}
              </div>
            </div>

            <div class="form-group hidden" id="b_s_m_id">
                {!! Form::label('b_s_m', 'Recurso próprio (P)', ['class' => 'control-label col-sm-4 col-md-offset-1']) !!}
              <div class="col-sm-4 input-group">
                  <div class="input-group-addon">%</div>
            {{--      {!! Form::selectRange('b_s_m', 30, 100, !empty($decision) ? null : 0, ['class' => 'form-control']); !!}  --}}
            {!! Form::select('b_s_m', array_combine(range(30, 100, 5), range(30, 100, 5)), !empty($decision) ? null : 0, ['class' => 'form-control']); !!}
              </div>
            </div>

            <div class="form-group">
              {!! Form::label('buy_medium_machine', 'Média', ['class' => 'control-label col-sm-4 col-md-offset-1']) !!}
              <div class="col-sm-4 input-group">
                <div class="input-group-addon">N.</div>
                {!! Form::text('buy_medium_machine', !empty($decision) ? null : 0, ['class' => 'form-control']); !!}
              </div>
            </div>

            <div class="form-group hidden" id="b_m_m_id">
                {!! Form::label('b_m_m', 'Recurso próprio (M)', ['class' => 'control-label col-sm-4 col-md-offset-1']) !!}
              <div class="col-sm-4 input-group">
                  <div class="input-group-addon">%</div>
            {{--    {!! Form::selectRange('b_m_m', 30, 100, !empty($decision) ? null : 0, ['class' => 'form-control']); !!}  --}}
                {!! Form::select('b_m_m', array_combine(range(30, 100, 5), range(30, 100, 5)), !empty($decision) ? null : 0, ['class' => 'form-control']); !!}
              </div>
            </div>

            <div class="form-group">
              {!! Form::label('buy_large_machine', 'Grande', ['class' => 'control-label col-sm-4 col-md-offset-1']) !!}
              <div class="col-sm-4 input-group">
                <div class="input-group-addon">N.</div>
                {!! Form::text('buy_large_machine', !empty($decision) ? null : 0, ['class' => 'form-control']); !!}
              </div>
            </div>

            <div class="form-group hidden" id="b_l_m_id">
                {!! Form::label('b_l_m', 'Recurso próprio (G)', ['class' => 'control-label col-sm-4 col-md-offset-1']) !!}
              <div class="col-sm-4 input-group">
                  <div class="input-group-addon">%</div>
            {{--    {!! Form::selectRange('b_l_m', 30, 100, !empty($decision) ? null : 0, ['class' => 'form-control']); !!}  --}}
                {!! Form::select('b_l_m', array_combine(range(30, 100, 5), range(30, 100, 5)), !empty($decision) ? null : 0, ['class' => 'form-control']); !!}
              </div>
            </div>
            <span class="badge badge-primary">Venda de Máquinas</span>
            <div class="form-group">
              {!! Form::label('sell_small_machine', 'Pequena', ['class' => 'control-label col-sm-4 col-md-offset-1']) !!}
              <div class="col-sm-4 input-group">
                <div class="input-group-addon">N.</div>
                {!! Form::text('sell_small_machine', !empty($decision) ? null : 0, ['class' => 'form-control']); !!}
              </div>
            </div>

            <div class="form-group">
              {!! Form::label('sell_medium_machine', 'Média', ['class' => 'control-label col-sm-4 col-md-offset-1']) !!}
              <div class="col-sm-4 input-group">
                <div class="input-group-addon">N.</div>
                {!! Form::text('sell_medium_machine', !empty($decision) ? null : 0, ['class' => 'form-control']); !!}
              </div>
            </div>

            <div class="form-group">
              {!! Form::label('sell_large_machine', 'Grande', ['class' => 'control-label col-sm-4 col-md-offset-1']) !!}
              <div class="col-sm-4 input-group">
                <div class="input-group-addon">N.</div>
                {!! Form::text('sell_large_machine', !empty($decision) ? null : 0, ['class' => 'form-control']); !!}
              </div>
            </div>
          </div>
        </div>
  </div>

  <div class="col-md-6">
    <div class="panel panel-default" style="border:1px solid #f39c12">
      <div class="panel-heading" style="background: #f39c12; color: #fff">
        <h3 class="panel-title">COMPRA DE MATÉRIAS-PRIMAS PARA O PRÓXIMO PERÍODO<span class="fa fa-question pull-right" style="cursor: pointer;" onclick="Explicar(4)"></span></h3>
      </div>
      <div class="panel-body">
        <div class="form-group">
          {!! Form::label('scheduled_rma', 'Quadro', ['class' => 'control-label col-sm-3 col-md-offset-2']) !!}
          <div class="col-sm-4 input-group">
            <div class="input-group-addon">N.</div>
            {!! Form::text('scheduled_rma', !empty($decision) ? null : 0, ['class' => 'form-control']); !!}
          </div>
        </div>

        <div class="form-group">
          {!! Form::label('scheduled_rmb', 'Kit 1', ['class' => 'control-label col-sm-3 col-md-offset-2']) !!}
          <div class="col-sm-4 input-group">
            <div class="input-group-addon">N.</div>
            {!! Form::text('scheduled_rmb', !empty($decision) ? null : 0, ['class' => 'form-control']); !!}
          </div>
        </div>

        <div class="form-group">
          {!! Form::label('scheduled_rmc', 'Kit 2', ['class' => 'control-label col-sm-3 col-md-offset-2']) !!}
          <div class="col-sm-4 input-group">
            <div class="input-group-addon">N.</div>
            {!! Form::text('scheduled_rmc', !empty($decision) ? null : 0, ['class' => 'form-control']); !!}
          </div>
        </div>

        <div class="form-group">
          {!! Form::label('payment_rm', 'Pagamento MP', ['class' => 'control-label col-sm-4 col-md-offset-1']) !!}
          <div class="col-sm-4 input-group">
            <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
            {!! Form::select('payment_rm', ['à vista', '1+1'], !empty($decision) ? null : 0, ['class' => 'form-control']); !!}
          </div>
        </div>
        <br><br><br><br><br>
      </div>
    </div>
  </div>

  <div class="col-md-12">
    <hr>
  </div>

  </div>

  <div class="row">
  <div class="col-md-6">

        <div class="panel panel-default" style="border:1px solid #00a65a">
          <div class="panel-heading" style="background: #00a65a; color: #fff">
            <h3 class="panel-title">RECURSOS HUMANOS<span class="fa fa-question pull-right" style="cursor: pointer;" onclick="Explicar(5)"></span></h3>
          </div>
          <div class="panel-body">
            <div class="form-group">
              {!! Form::label('admitted', 'Admitidos', ['class' => 'control-label col-sm-3 col-md-offset-2']) !!}
              <div class="col-sm-4 input-group">
                <div class="input-group-addon">N.</div>
                {!! Form::text('admitted', !empty($decision) ? null : 0, ['class' => 'form-control']); !!}
              </div>
            </div>

            <div class="form-group">
              {!! Form::label('fired', 'Demitidos', ['class' => 'control-label col-sm-3 col-md-offset-2']) !!}
              <div class="col-sm-4 input-group">
                <div class="input-group-addon">N.</div>
                {!! Form::text('fired', !empty($decision) ? null : 0, ['class' => 'form-control']); !!}
              </div>
            </div>

            <div class="form-group">
              {!! Form::label('salary', 'Salário', ['class' => 'control-label col-sm-3 col-md-offset-2']) !!}
              <div class="col-sm-4 input-group">
                <div class="input-group-addon">$</div>
                {!! Form::text('salary', !empty($decision) ? null : $salary, ['class' => 'form-control']); !!}
              </div>
            </div>

            <div class="form-group">
              {!! Form::label('training', 'Treinamento', ['class' => 'control-label col-sm-3 col-md-offset-2']) !!}
              <div class="col-sm-4 input-group">
                <div class="input-group-addon">%</div>
                {!! Form::text('training', !empty($decision) ? null : 0, ['class' => 'form-control']); !!}
              </div>
            </div>
          </div>
        </div>

  </div>

  <div class="col-md-6">

        <div class="panel panel-default" style="border:1px solid #00a65a">
          <div class="panel-heading" style="background: #00a65a; color: #fff">
            <h3 class="panel-title">FINANÇAS<span class="fa fa-question pull-right" style="cursor: pointer;" onclick="Explicar(6)"></span></h3>
          </div>
          <div class="panel-body">
            <div class="form-group">
              {!! Form::label('loan', 'Empréstimo', ['class' => 'control-label col-sm-3 col-md-offset-2']) !!}
              <div class="col-sm-4 input-group">
                <div class="input-group-addon">$</div>
                {!! Form::text('loan', !empty($decision) ? null : 0, ['class' => 'form-control']); !!}
              </div>
            </div>

            <div class="form-group">
              {!! Form::label('anticipation_of_receivables', 'Antecipação de Recebíveis', ['class' => 'control-label col-sm-5']) !!}
              <div class="col-sm-4 input-group">
                <div class="input-group-addon">%</div>
                {!! Form::text('anticipation_of_receivables', !empty($decision) ? null : 0, ['class' => 'form-control']); !!}
              </div>
            </div>

            <div class="form-group">
              {!! Form::label('application', 'Aplicação', ['class' => 'control-label col-sm-5']) !!}
              <div class="col-sm-4 input-group">
                <div class="input-group-addon">%</div>
                {!! Form::text('application', !empty($decision) ? null : 0, ['class' => 'form-control']); !!}
              </div>
            </div>

            
          </div>
        </div>

  </div>
</div>
<div class="modal modal-default" id="id_explicar">
    <div class="modal-dialog" style="border:2px solid #673a09">
      <div class="modal-content">
        <div class="modal-header" style="background: #f2b26c">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h3><div id="cabeca_explicar"></div></h3>
        </div>
        <div class="modal-body">  
          <div align="justify" id="texto_explicar"></div>
        </div>
      </div>
    </div>
</div>
@section('js')
<script>
  function Explicar(id) {
    if (id==1) {
      $('#cabeca_explicar').html('Comercial - Propaganda');
      $('#texto_explicar').html('PARTE DA DEMANDA GERAL DE MERCADO SERÁ DISTRIBUÍDA AOS FABRICANTES DE BICICLETAS DE ACORDO COM AS SUAS ESTRATÉGIAS DE PRECIFICAÇÃO E MARKETING.  PORTANTO, DECIDA QUAL SERÁ SUA MELHOR ESTRATÉGIA PARA CONQUISTAR O MARKET SHARE QUE PLANEJOU.');
    } 
    if (id==2) {
      $('#cabeca_explicar').html('Matérias-Primas Emergencial');
      $('#texto_explicar').html('AQUI, SEU TIME DEVE DECIDIR A COMPRA DE MATÉRIAS-PRIMAS PARA SUPRIR UMA NECESSIDADE EMERGENCIAL, POR CONTA DE UMA COMPRA PROGRAMADA EQUIVOCADA OU POR DECISÃO DE PRODUÇÃO EXTRA.');
    }
    if (id==3) {
      $('#cabeca_explicar').html('Linhas de Produção');
      $('#texto_explicar').html('CASO QUEIRA COMPRAR MÁQUINA, VOCÊ DEVE INSERIR A QUANTIDADE E CLICAR FORA DO CAMPO,  PARA QUE O SISTEMA HABILITE E CAMPO DE RECURSO PRÓPRIO.  NELE, VOCÊ DEVE ESCOLHER QUANTO SUA EMPRESA VAI INVESTIR DE CAPITAL PRÓPRIO NA COMPRA DA NOVA MÁQUINA.</br>CASO QUEIRA VENDER UMA DE SUAS LINHAS DE PRODUÇÃO,  VOCÊ DEVE PREENCHER,  NO CAMPO DA REFERIDA LINHA QUE DESEJA VENDER, A QUANTIDADE DE LINHAS QUE SERÃO VENDIDAS.VALE A PENA LEMBRAR QUE SUA EMPRESA NÃO PODE VENDER TODAS AS MÁQUINAS, E QUE O VALOR DA VENDA É O VALOR DE AQUISIÇÃO, MENOS A DEPRECIAÇÃO E A DESVALORIZAÇÃO DE MERCADO, PUBLICADA NO INFORMATIVO DE MERCADO. O  VALOR DESSA VENDA SERÁ CREDITADO NO FINAL DO PERÍODO.');
    }
    if (id==4) {
      $('#cabeca_explicar').html('Produção - Próximo Período');
      $('#texto_explicar').html('NESTE MOMENTO, SEU TIME DEVERÁ COMPRAR MATÉRIA-PRIMA SUFICIENTE PARA SUPRIR A CAPACIDADE PRODUTIVA DE SUAS LINHAS DE PRODUÇÃO, PARA O PRÓXIMO PERÍODO.');
    }
    if (id==5) {
      $('#cabeca_explicar').html('RH');
      $('#texto_explicar').html('AS DECISÕES DE RECURSOS HUMANOS SÃO IMPORTANTÍSSIMAS, ELAS SE DÃO NA CONTRATAÇÃO OU DEMISSÃO DE OPERÁRIOS POR CONTA DE COMPRA OU VENDA DE LINHAS DE PRODUÇÃO, FIQUE ATENTO AO QUE O MANUAL ORIENTA.</br>COM RELAÇÃO AO SALÁRIO DOS SEUS OPERÁRIO E TREINAMENTO, OBSERVE O INDICADOR DE PRODUTIVIDADE NO RELATÓRIO MERCADOLÓGICO, ELE DEVERÁ NORTEAR A NECESSIDADE DE MOTIVAR SEU TIME.');
    }
    if (id==6) {
      $('#cabeca_explicar').html('Finanças');
      $('#texto_explicar').html('NESTES CAMPOS SUA EMPRESA DEVERÁ TOMAR DECISÕES QUE POSSIBILITEM UMA BOA GESTÃO DE SEUS RECURSOS FINANCEIROS.</br>NO CAOS DE EMPRÉSTIMO, IMPORTANTE RESSALTAR QUE O LIMITE DE CRÉDITO CONCEDIDO PELO BANCO, É O MESMO PARA EMPRÉSTIMOS E FINANCIAMENTO DE LINHAS DE PRODUÇÃO.</br>CASO SOLICITE ANTECIPAÇÃO DE RECEBÍVEIS, O RECURSO SERÁ CREDITADO NESTE MESMO PERÍODO, DESCONTADO AS TAXAS DE JUROS PRATICADAS PELO BANCO.</br>NO CASO DE DECIDIR APLICAR RECURSOS, O VALOR SOLICITADO SÓ SERÁ EFETIVAMENTE APLICADO APÓS LIQUIDAÇÃO DAS OBRIGAÇÕES FINANCEIRAS COM SEUS FUNCIONÁRIOS, FORNECEDORES E BANCOS, E O MESMO DEVERÁ RETORNAR AO CAIXA AUTOMATICAMENTE NO PRÓXIMO PERÍODO ACRESCIDO DOS JUROS.');
    }
    if (id==7) {
      $('#cabeca_explicar').html('Comercial');
      $('#texto_explicar').html('NESTES CAMPOS, VOCÊ E SEU TIME DEVERÃO DECIDIR QUAL PREÇO E CONDIÇÕES DE PAGAMENTO SERÃO COMPETITIVOS PARA GERAÇÃO DE DEMANDA SUFICIENTE  PARA VENDA DE TODA SUA PRODUÇÃO E ESTOQUE DE PRODUTOS ACABADOS.');
    }
    if (id==8) {
      $('#cabeca_explicar').html('Produção de Máquinas');
      $('#texto_explicar').html('SEU TIME DEVE REGISTRAR O NÍVEL DE ATIVIDADE QUE SUAS LINHAS DE PRODUÇÃO IRÃO OPERAR PARA PRODUZIR A QUANTIDADE QUE DESEJAM. ELAS PODEM OPERAR COM NÍVEIS ENTRE 50% E 100%.</br>CASO NECESSITE DE PRODUÇÃO EXTRA, ESSA PODERÁ SER PROGRAMADA COM NÍVEL DE 1% A 25%.PARA QUE TUDO OCORRA COMO PLANEJADO, SAIBA QUAL A CAPACIDADE PRODUTIVA DE SUAS LINHAS DE PRODUÇÃO, VERIFIQUE O ESTOQUE DE MATÉRIAS-PRIMAS E ANALISE SE O NÚMERO DE OPERÁRIOS É SUFICIENTE.');
    }
    $('#id_explicar').modal();
  }
  $(function() {

    $("#decision").submit(function(e) {
      e.stopPropagation();

      var $bt = $(this).find(':submit');

      $bt.attr('disabled', 'disabled');
      $bt.val('Gravando...');
      $bt.css('cursor', 'progress');
    });

    $('[id*=adv_]').change(function(e) {
      if ($(this).val() == null) {
        $(this).val(0);
      }
    })

    if ($('#buy_small_machine').val() > 0) {
      $('#b_s_m_id').removeClass('hidden');
    }
    if ($('#buy_medium_machine').val() > 0) {
      $('#b_m_m_id').removeClass('hidden');
    }
    if ($('#buy_large_machine').val() > 0) {
      $('#b_l_m_id').removeClass('hidden');
    }

    if ($('#term').val()>0) {$('#fg_term_interest').removeClass('hidden');}
    $('#term').change(function() {
      if($('#term').val()>0) {
        $('#fg_term_interest').removeClass('hidden');
      }else{
        $('#term_interest').val(0);
        $('#fg_term_interest').addClass('hidden');
      }
    });

    $('#buy_small_machine, #buy_medium_machine, #buy_large_machine').change(function() {
      let val = $(this).val();
      let id = $(this).attr('id');
      if (val != 0) {
        if (id == 'buy_small_machine') {
          $('#b_s_m_id').removeClass('hidden');
        }
        if (id == 'buy_medium_machine') {
          $('#b_m_m_id').removeClass('hidden');
        }
        if (id == 'buy_large_machine') {
          $('#b_l_m_id').removeClass('hidden');
        }
      } else {
        if (id == 'buy_small_machine') {
          $('#b_s_m_id').addClass('hidden');
        }
        if (id == 'buy_medium_machine') {
          $('#b_m_m_id').addClass('hidden');
        }
        if (id == 'buy_large_machine') {
          $('#b_l_m_id').addClass('hidden');
        }
      }
    });
  })
</script>
@stop