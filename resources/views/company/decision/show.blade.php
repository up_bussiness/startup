@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
<h1>{{ $decision->turn->simulation->name }}</h1>
@stop

@section('content_title')
<h3 class="box-title"><i class="fa fa-gear"></i> Decisão - {{ $decision->company->name }} - Rodada {{ $decision->turn->month }}</h3>
<a href="{{ isCoordinator() ? route('simulation.show', $decision->turn->simulation_id) : route('company') }}" class="btn btn-xs btn-default pull-right">VOLTAR</a>
@stop

@section('content')
<div class="row">
  <div class="col-md-5 col-md-offset-1">
    <table class="table  table-bordered">
      <thead>
        <tr>
          <th colspan="100" class="active">
            VENDAS
          </th>
        </tr>
      </thead>
      <tbody class="text-right">
        <tr>
          <td class="text-left">Preço ($)</td>
          <td class="decision-price">{{ numberDot($decision->price) }}</td>
        </tr>
        <tr>
          <td class="text-left">Prazo</td>
          <td class="decision-term">{{ $decision->term == 0 ? 'À vista' : '1+1' }}</td>
        </tr>
      </tbody>
    </table>
    <table class="table  table-bordered">
      <thead>
        <tr>
          <th colspan="100" class="active">
            PROPAGANDA
          </th>
        </tr>
      </thead>
      <tbody class="text-right">
        <tr>
          <td class="text-left">Radio</td>
          <td class="decision-adv-radio">{{$decision->adv_radio}}</td>
        </tr>
        <tr>
          <td class="text-left">Jornal</td>
          <td class="decision-adv-journal">{{$decision->adv_journal}}</td>
        </tr>
        <tr>
          <td class="text-left">Mídias Sociais</td>
          <td class="decision-adv-social">{{$decision->adv_social}}</td>
        </tr>
        <tr>
          <td class="text-left">Outdoor</td>
          <td class="decision-adv-outdoor">{{$decision->adv_outdoor}}</td>
        </tr>
        <tr>
          <td class="text-left">TV</td>
          <td class="decision-adv-panfletch">{{$decision->adv_tv}}</td>
        </tr>
      </tbody>
    </table>
    <table class="table  table-bordered">
      <thead>
        <tr>
          <th colspan="100" class="active">
            PRODUÇÃO
          </th>
        </tr>
      </thead>
      <tbody class="text-right">
        <tr>
          <td class="text-left">Compras - Quadro (Nº)</td>
          <td class="decision-scheduled-rma">{{ numberDot($decision->scheduled_rma) }}</td>
        </tr>
        <tr>
          <td class="text-left">Compras - Kit 1 (Nº)</td>
          <td class="decision-scheduled-rmb">{{ numberDot($decision->scheduled_rmb) }}</td>
        </tr>
        <tr>
          <td class="text-left">Compras - Kit 2 (Nº)</td>
          <td class="decision-scheduled-rmc">{{ numberDot($decision->scheduled_rmc) }}</td>
        </tr>
        <tr>
          <td class="text-left">Pagamento das MP</td>
          <td class="decision-payment-rm">{{ $decision->payment_rm == 0 ? 'À vista' : '1+1' }}</td>
        </tr>
        <tr>
          <td class="text-left">Nível de atividade (%)</td>
          <td class="decision-activity-level">{{$decision->activity_level}}</td>
        </tr>
        <tr>
          <td class="text-left">Produção extra (%)</td>
          <td class="decision-extra-production">{{$decision->extra_production}}</td>
        </tr>
      </tbody>
    </table>
  </div>
  <div class="col-md-5">
    <table class="table  table-bordered">
      <thead>
        <tr class="active">
          <th>Linha de Produção</th>
          <th>Pequena</th>
          <th>Média</th>
          <th>Grande</th>
        </tr>
      </thead>
      <tbody class="text-right">
        <tr>
          <td class="text-left">Compras</td>
          <td class="decision-buy-small-machine">{{$decision->buy_small_machine}}</td>
          <td class="decision-buy-medium-machine">{{$decision->buy_medium_machine}}</td>
          <td class="decision-buy-large-machine">{{$decision->buy_large_machine}}</td>
        </tr>
        <tr>
          <td class="text-left">Recurso Próprio (%)</td>
          <td class="decision-buy-small-machine">{{$decision->b_s_m}}</td>
          <td class="decision-buy-medium-machine">{{$decision->b_m_m}}</td>
          <td class="decision-buy-large-machine">{{$decision->b_l_m}}</td>
        </tr>
        <tr>
          <td class="text-left">Vendas</td>
          <td class="decision-sell-small-machine">{{$decision->sell_small_machine}}</td>
          <td class="decision-sell-medium-machine">{{$decision->sell_medium_machine}}</td>
          <td class="decision-sell-large-machine">{{$decision->sell_large_machine}}</td>
        </tr>
      </tbody>
    </table>
    <table class="table  table-bordered">
      <thead>
        <tr>
          <th colspan="100" class="active">
            RECURSOS HUMANOS
          </th>
        </tr>
      </thead>
      <tbody class="text-right">
        <tr>
          <td class="text-left">Admitidos (Nº)</td>
          <td class="decision-admitted">{{$decision->admitted}}</td>
        </tr>
        <tr>
          <td class="text-left">Demitidos (Nº)</td>
          <td class="decision-fired">{{$decision->fired}}</td>
        </tr>
        <tr>
          <td class="text-left">Salário ($)</td>
          <td class="decision-salary">{{ numberDot($decision->salary) }}</td>
        </tr>
        <tr>
          <td class="text-left">Treinamento (%)</td>
          <td class="decision-training">{{$decision->training}}</td>
        </tr>
      </tbody>
    </table>

    <table class="table  table-bordered">
      <thead>
        <tr>
          <th colspan="100" class="active">
            FINANÇAS
          </th>
        </tr>
      </thead>
      <tbody class="text-right">
        <tr>
          <td class="text-left">Empréstimo ($)</td>
          <td class="decision-loan">{{ numberDot($decision->loan) }}</td>
        </tr>
        <tr>
          <td class="text-left">Antecipação de recebíveis ($)</td>
          <td class="decision-anticipation-of-receivables">{{ numberDot($decision->anticipation_of_receivables) }}</td>
        </tr>
        <tr>
          <td class="text-left">Aplicação ($)</td>
          <td class="decision-application">{{ numberDot($decision->application) }}</td>
        </tr>
        <tr>
          <td class="text-left">Juros na venda a prazo (%)</td>
          <td class="decision-term-interest">{{ numberDot($decision->term_interest) }}</td>
        </tr>
        <tr>
          <td class="text-left">Receitas diversas ($)</td>
          <td class="decision-income">{{ numberDot($decision->income) }}</td>
        </tr>
        <tr>
          <td class="text-left">Despesas diversas ($)</td>
          <td class="decision-expense">{{ numberDot($decision->expense) }}</td>
        </tr>
      </tbody>
    </table>
    <table class="table  table-bordered">
      <thead>
        <tr>
          <th colspan="100" class="active">
            COMPRAS EMERGENCIAIS
          </th>
        </tr>
      </thead>
      <tbody class="text-right">
        <tr>
          <td class="text-left">Quadro (Nº)</td>
          <td class="decision-emergency-rma">{{ numberDot($decision->emergency_rma) }}</td>
        </tr>
        <tr>
          <td class="text-left">Kit 1 (Nº)</td>
          <td class="decision-emergency-rmb">{{ numberDot($decision->emergency_rmb) }}</td>
        </tr>
        <tr>
          <td class="text-left">Kit 2 (Nº)</td>
          <td class="decision-emergency-rmc">{{ numberDot($decision->emergency_rmc) }}</td>
        </tr>
      </tbody>
    </table>
  </div>
</div>
@can('is-coordinator')
@if($decision->turn->operationalDetail == null)
<hr>
<div class="col-md-4 col-md-offset-4">
  {!! Form::open(['route' => ['coordinator.company.extra']]) !!}
  <div class="form-group">
    {!! Form::label('income', 'Receitas diversas ($)', ['class' => 'control-label']) !!}
    {!! Form::text('income', $decision->income, ['class' => 'form-control decision-income']); !!}
  </div>
  <div class="form-group">
    {!! Form::label('expense', 'Despesas diversas ($)', ['class' => 'control-label']) !!}
    {!! Form::text('expense', $decision->expense, ['class' => 'form-control decision-expense']); !!}
  </div>
  {{ Form::hidden('decision_company_id', $decision->id, ['id' => 'decision_company_id']) }}
  <div class="text-center">{!! Form::submit('Salvar', ['class' => 'btn btn-primary text-center additional-things']) !!}</div>
  {!! Form::close() !!}
  @endif
  @endcan
</div>
@stop

@section('js')
@include('layouts.partials.storage', ['key' => $decision->user_id])
@stop