@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
	<h1>{{__('labels.dashboard')}}</h1>
	<link rel="stylesheet" type="text/css" href="introjs.css">
@stop

@section('content_title')
    <h3 class="box-title"><i class="fa fa-gear"></i> {{__('labels.management')}}</h3>    
@stop

@section('content')
<div class="col-md-3 col-sm-6 col-xs-12">
	<div class="info-box" data-intro="Este é o local onde é informado o coordenador responsável" data-step="1">
		<span class="info-box-icon" style="background: #f15638; color: white; border-radius: 5px;"><i class="fa fa-user"></i></span>

		<div class="info-box-content">
			<span class="info-box-text"> {{__('labels.coordinator')}}</span>
			<span class="info-box-number"> {{ $simulation->user->name }} </span>
		</div>
		<!-- /.info-box-content -->
	</div>
	<!-- /.info-box -->
</div>
<div class="col-md-3 col-sm-6 col-xs-12">
	<div class="info-box" data-intro="Aqui é possível ver qual é a rodada corrente" data-step="2">
		<span class="info-box-icon" style="background: #f15638; color: white; border-radius: 5px;"><i class="fa fa-hourglass-1"></i></span>

		<div class="info-box-content">
			<span class="info-box-text"> {{__('labels.turn')}}</span>
			<span class="info-box-number"> {{ $simulation->turns->where('published', true)->count()}} </span>
		</div>
		<!-- /.info-box-content -->
	</div>
	<!-- /.info-box -->
</div>
<div class="col-md-3 col-sm-6 col-xs-12">
	<div class="info-box" data-intro="Aqui é possível saber a data de início da rodada" data-step="3">
		<span class="info-box-icon" style="background: #f15638; color: white; border-radius: 5px;"><i class="fa fa-calendar-check-o"></i></span>

		<div class="info-box-content">
			<span class="info-box-text"> {{__('labels.start')}}</span>
			<span class="info-box-number"> {{ dateTimeBRL($simulation->turns->last()->created_at) }} </span>
		</div>
		<!-- /.info-box-content -->
	</div>
	<!-- /.info-box -->
</div>
<div class="col-md-3 col-sm-6 col-xs-12">
	<div class="info-box" data-intro="Aqui é possível saber a data de fim da rodada" data-step="4">
		<span class="info-box-icon" style="background: #f15638; color: white; border-radius: 5px;"><i class="fa fa-calendar-times-o"></i></span>

		<div class="info-box-content">
			<span class="info-box-text"> {{__('labels.end')}}</span>
			<span class="info-box-number"> {{ dateTimeBRL($simulation->turns->last()->end_date) }} </span>
		</div>
		<!-- /.info-box-content -->
	</div>
	<!-- /.info-box -->
</div>

<hr>
<div class="row" data-intro="Aqui podemos obter informações sobre as rodadas existentes na simulação" data-step="5">
	<div class="col-md-12">
		@include('layouts.partials.welcome')
		<div class="table-responsive">
			<table class="table  table-bordered">
				<thead>
				<tr>
			      <th>#</th>
			      <th>{{__('labels.closing')}}</th>
			      <th class="text-center">{{__('labels.status')}}</th>
			      <th class="text-center">{{__('labels.communicatory')}}</th>
			      <th class="text-center">{{__('labels.decision')}}</th>

						@if ($simulation->in_operational)
			      	<th class="text-center">{{__('labels.operational')}}</th>
						@endif

						@if ($simulation->in_cashflow)
			      	<th class="text-center">{{__('labels.cashflow')}}</th>
						@endif

						@if ($simulation->in_dre)
			      	<th class="text-center">{{__('labels.annual_income_statement')}}</th>
						@endif

						@if ($simulation->in_balance)
			      	<th class="text-center">{{__('labels.balance_sheet')}}</th>
						@endif

						@if ($simulation->in_ranking)
			      	<th class="text-center">{{__('labels.ranking')}}</th>
						@endif

			    </tr>
				</thead>
				<tbody>
					@forelse($simulation->turns->where('published', true)->sortByDesc('month') as $turn)

					@php
					$companyDecision = $turn->decisionCompany->where('user_id', auth()->user()->id)->first();
					@endphp

					<tr>
						<td class="table-collapse text-center">{{ $turn->month }}</td>

						<td class="table-collapse text-center">{{ date("d/m/Y \à\s H:i", strtotime($turn->end_date)) }}</td>

						<td class="table-collapse text-center">
							{!! turnIsOpen($turn->end_date) == 0 ? '<span class="label label-danger">ENCERRADA</span>' : '<span class="label label-success">ANDAMENTO</span>' !!}
						</td>

						<td class="text-center">
							<a href="{{ route('company.newspaper.show', $turn->id) }}" class="btn btn-xs btn-primary">VISUALIZAR</a>
						</td>

						@if( $companyDecision != null )
						@if(!turnIsOpen($turn->end_date))
						<td class="text-center">
							<a href="{{ route('company.decision.show', $companyDecision->id) }}" class="btn btn-xs btn-primary">VISUALIZAR</a>
						</td>
						@else
						<td class="text-center">
							<a href="{{ route('company.decision.edit', $companyDecision->id) }}" class="btn btn-xs btn-warning">EDITAR</a>
						</td>
						@endif
						@else
						<td class="text-center">
							<a href="{{ route('company.decision.create', $turn->id) }}" class="btn btn-xs btn-success">DECIDIR</a>
						</td>
						@endif

						@if ($simulation->in_operational)
								@if( $turn->operational != null )
									<td class="text-center">
										<a href="{{ route('company.operational.report', $turn->id) }}" class="btn btn-xs btn-primary">VISUALIZAR</a>
									</td>
								@else
									<td class="text-center">
										-
									</td>
								@endif
							@endif

							@if ($simulation->in_cashflow)
								@if( $turn->cashflow != null )
									<td class="text-center">
										<a href="{{ route('company.cashflow.report', $turn->id) }}" class="btn btn-xs btn-primary">VISUALIZAR</a>
									</td>
								@else
									<td class="text-center">
										-
									</td>
								@endif
							@endif

							@if ($simulation->in_dre)
								@if( $turn->dre != null )
									<td class="text-center">
										<a href="{{ route('company.dre.report', $turn->id) }}" class="btn btn-xs btn-primary">VISUALIZAR</a>
									</td>
								@else
									<td class="text-center">
										-
									</td>
								@endif
							@endif

							@if ($simulation->in_balance)
								@if( $turn->balance != null )
									<td class="text-center">
										<a href="{{ route('company.balance.report', $turn->id) }}" class="btn btn-xs btn-primary">VISUALIZAR</a>
									</td>
								@else
									<td class="text-center">
										-
									</td>
								@endif
							@endif

							@if ($simulation->in_ranking)
								@if( $turn->ranking != null )
									<td class="text-center">
										<a href="{{ route('company.ranking.report', $turn->id) }}" class="btn btn-xs btn-primary">VISUALIZAR</a>
									</td>
								@else
									<td class="text-center">
										-
									</td>
								@endif
							@endif
							
			        </tr>
			        @empty
			        <tr>
			          <td colspan="100" class="text-center">
			            <i>Ainda não há nenhuma rodada criada.</i>
			          </td>
			        </tr>
			        @endforelse
			    </tbody>
				</table>
		{{-- 		 comentario 		
		    <a href="#" onclick="ShowPraca();" class="btn btn-xs btn-success">Praça</a> --}}
			</div>
		</div>
	</div>
	{{--  @include('company._praca')  --}}
	<script type="text/javascript" src="intro.js"></script>
	<!-- Scripts -->
    <script src="{{ asset('js/intro.js') }}" defer></script>
@stop