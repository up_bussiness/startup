@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
<h1>Equipe</h1>
@stop

@section('content_title')
<h3 class="box-title"><i class="fa fa-users"></i> Visão Geral</h3>
<a href="{{ route('student.create') }}" class="btn btn-success btn-xs pull-right">NOVO ESTUDANTE</a></h3>
@stop

@section('content')

<div class="table-responsive">
	<table class="table table-striped table-bordered">
		<thead>
			<tr>
				<th class="table-collapse">Matrícula</th>
				<th>Nome</th>
				<th class="table-collapse">Email</th>
				<th class="text-center">Telefone</th>
				<th class="text-center">Cargo</th>
				<th class="table-collapse">Nascimento</th>
				<th class="text-center">Facebook</th>
				<th class="text-center">Instagram</th>
				<th class="text-center">Ações</th>
			</tr>
		</thead>
		<tbody>
			@forelse($students as $student)
			<tr>
				<td class="text-center">{{ $student->registration }}</td>
				<td>{{ $student->name }}</td>
				<td class="table-collapse">{{ $student->email }}</td>
				<td class="table-collapse">{{ $student->phone }}</td>
				<td class="table-collapse">{{ $student->occupation->name}}</td>
				<td class="text-center">{{ !empty($student->date) ? dateBRL($student->date) : '-' }}</td>
				<td class="table-collapse text-center">{{ !empty($student->face) ? $student->face : '-' }}</td>
				<td class="table-collapse text-center">{{ !empty($student->insta) ? $student->insta : '-' }}</td>
				<td class="table-collapse">
					<a href="{{ route('student.edit', $student->id) }}" class="btn btn-xs btn-info"><i class="fa fa-pencil"></i></a>
					{{ Form::open(['route' => ['student.destroy', $student->id], 'method' => 'delete', 'style'=>'display: inline']) }}
					<button type="submit" class="btn btn-xs btn-danger" onclick="return confirm('Tem certeza que deseja excluír esta pessoa?');"><i class="fa fa-trash"></i></button>
					{{ Form::close() }}
					{{-- <a href="{{ route('usuario.qrcode', $student->email) }}" class="btn btn-xs btn-success"><i class="fa fa-qrcode"></i></a> --}}
					<a href="{{ route('usuario.acessoapp', $student->email) }}" class="btn btn-xs btn-info"><i class="fa fa-envelope-o"></i></a>
					{{-- <button type="button" class="btn btn-xs btn-success" onclick="funcQRCode('{{ $student->email }}');"><i class="fa fa-qrcode"></i></button> --}}
				</td>
			</tr>

			@empty
			<p>Ainda não tem nenhum estudante. Adicione agora um.</p>
			@endforelse

		</tbody>
	</table>
	<div align="center">
		<img src="{{asset('images/playstore.png')}}" width="64" height="64"><br>
		Up Business Chat<br>
		<a href="https://play.google.com/store/apps/details?id=com.upbusinessgame.chat&hl=pt_BR" target="_blank">
			<img src="{{asset('images/app1.png')}}"></a>
	</div>
</div>
{{--
<div class="modal modal-default" id="modalqrcode">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header modal-header-x">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Abrir o aplicativo e usar o QRcode abaixo para acessar</h4>
			</div>
			<div class="modal-body">
				<div class="panel panel-info">
					<div class="panel-heading">
						<div id="modalUsuario"></div>
					</div>
					<div class="panel-body" align="center">
						{!! QrCode::size(200)->generate(TextoParaQrcodeAcesso(request()->getHost())); !!}

					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</div>
<script>
	function funcQRCode(email) {

		$.get('/usuario/' + email, function(usuario) {
			$('#modalUsuario').html(usuario);
			//alert(usuario);
		});
		$('#modalqrcode').modal();
	}
</script> --}}
@stop