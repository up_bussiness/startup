@if(!empty($student))
  {!! Form::model($student, ['route' => ['student.update', $student->id], 'method' => 'put', 'class'=>'form-horizontal']) !!}
@else
  {!! Form::open(['route' => 'student.store', 'class'=>'form-horizontal']) !!}
@endif

<div class="form-group">
  {!! Form::label('name', 'Nome', ['class' => 'control-label col-sm-3 col-md-offset-2']) !!}
  <div class="col-sm-3 input-group">
    <div class="input-group-addon">...</div>
    {!! Form::text('name', null, ['class' => 'form-control', 'required']); !!}
  </div>
</div>

<div class="form-group">
  {!! Form::label('registration', 'Matricula', ['class' => 'control-label col-sm-3 col-md-offset-2']) !!}
  <div class="col-sm-3 input-group">
    <div class="input-group-addon"><i class="fa fa-barcode"></i></div>
    {!! Form::text('registration',  null, ['class' => 'form-control', 'required']); !!}
  </div>
</div>

<div class="form-group">
  {!! Form::label('date', 'Data de nascimento', ['class' => 'control-label col-sm-3 col-md-offset-2']) !!}
  <div class="col-sm-3 input-group">
    <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
    {!! Form::date('date',  null, ['class' => 'form-control']); !!}
  </div>
</div>

<div class="form-group">
  {!! Form::label('email', 'Email', ['class' => 'control-label col-sm-3 col-md-offset-2']) !!}
  <div class="col-sm-3 input-group">
    <div class="input-group-addon"><i class="fa fa-envelope-o"></i></div>
    {!! Form::email('email',  null, ['class' => 'form-control', 'required']); !!}
  </div>
</div>

<div class="form-group">
  {!! Form::label('phone', 'Telefone', ['class' => 'control-label col-sm-3 col-md-offset-2']) !!}
  <div class="col-sm-3 input-group">
    <div class="input-group-addon"><i class="fa fa-phone"></i></div>
    {!! Form::text('phone',  null, ['class' => 'form-control', 'required']); !!}
  </div>
</div>

<div class="form-group">
  {!! Form::label('face', 'Facebook', ['class' => 'control-label col-sm-3 col-md-offset-2']) !!}
  <div class="col-sm-3 input-group">
    <div class="input-group-addon"><i class="fa fa-facebook-square"></i></div>
    {!! Form::text('face',  null, ['class' => 'form-control']); !!}
  </div>
</div>

<div class="form-group">
  {!! Form::label('insta', 'Instagram', ['class' => 'control-label col-sm-3 col-md-offset-2']) !!}
  <div class="col-sm-3 input-group">
    <div class="input-group-addon"><i class="fa fa-instagram"></i></div>
    {!! Form::text('insta',  null, ['class' => 'form-control']); !!}
  </div>
</div>

<div class="form-group">
  {!! Form::label('occupation_id', 'Cargo', ['class' => 'control-label col-sm-3 col-md-offset-2']) !!}
  <div class="col-sm-3 input-group">
    <div class="input-group-addon"><i class="fa fa-black-tie"></i></div>
    {!! Form::select('occupation_id',  $occupation, null, ['class' => 'form-control', 'required']); !!}
  </div>
</div>

<div class="col-md-offset-5 col-md-3 text-center">
    {!! Form::submit(!empty($student) ? 'Atualizar' : 'Criar', ['class' => 'btn btn-success']); !!}
</div>

{!! Form::close() !!}