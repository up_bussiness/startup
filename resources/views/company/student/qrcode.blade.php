@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
<h1>Para acesso ao aplicativo :</h1>
@stop

@section('content_title')
<h3 class="box-title"><i class="fa fa-users"></i> QRcode de : {{ $usuario->name }}</h3>
<div class="pull-right">
    <a href="{{ route('student.index') }}" class="btn btn-xs btn-default hidden-print">VOLTAR</a>
</div>
@stop

@section('content')
<table>
    <tr>
        <td>
        <div class="form-group">
            <label for="pwd">Senha:</label>
            <input type="password" class="form-control" id="pwd">
        </div>
        </td>
    </tr>
    <tr>
        <td>
        <button type="button" class="btn btn-primary" onclick="btnVisualizar('{{ $usuario->password_plain }}');">Visualizar QRCode</button>
        </td>
    </tr>
    <tr>
        <td>
        <div style="display:none;" id="idQrcode">
        {!! QrCode::size(200)->generate(TextoParaQrcodeAcessoUsuario(request()->getHost(),$usuario->email,$usuario->password_plain)); !!}        
        </div>
        </td>
    </tr>
</table>


<script>
function btnVisualizar(senha) {
    var campo = $('#pwd').val();
    if (campo == senha) {
        $('#idQrcode').show();
    }else{
        alert("Senha não confere, veja no e-mail enviado");
    }
}
</script>
@stop