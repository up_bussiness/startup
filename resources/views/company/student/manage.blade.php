@extends('adminlte::page')

@section('title', 'Nova simulação')

@section('content_header')
    <h1>Nome da Instituição</h1>
@stop

@section('content_title')
    <h3 class="box-title">{{ !empty($student) ? 'Atualizar Estudante' : 'Novo Estudante' }}</h3>
@stop

@section('content')    
    @include('company.student._form')
@stop