@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Simulações</h1>
@stop

@section('content')

		<div class="table-responsive">
			<table class="table table-striped table-bordered">
				<thead>
					<tr>
						<th>Nome</th>
						<th>Email</th>
						<th>Senha</th>
					</tr>
				</thead>
				<tbody>
			    @foreach ($simulation->companies as $company)
			    <tr>
						<td>{{ $company->name }}</td>
						<td>{{ $company->email }}</td>
						<td>{{ $company->password_plain }}</td>
			    </tr>
			    @endforeach
				</tbody>
			</table>
		</div>
@stop