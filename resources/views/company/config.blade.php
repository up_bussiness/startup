@extends('adminlte::page')

@section('content_header')
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
  <h1>Configuração Inicial da Empresa</h1>
@stop


@section('content')
        {!! Form::model(auth()->user(), ['route' => 'company.preparationConfig', 'method' => 'put',  'class'=>'form-horizontal', 'files' => true]) !!}
        	<div class="form-group">
        	  {!! Form::label('name', 'Nome da Empresa (até 12 caracteres)', ['class' => 'control-label col-sm-3 col-md-offset-2']) !!}
        	  <div class="col-sm-3 input-group">
        	    <div class="input-group-addon">...</div>
        	    {!! Form::text('name', null, ['class' => 'form-control', 'required','maxlength' => 12]); !!}
        	  </div>
        	</div>

            <div class="form-group">
              {!! Form::label('photo', 'Foto', ['class' => 'control-label col-sm-3 col-md-offset-2']) !!}
              <div class="col-sm-3 input-group">
                <div class="input-group-addon"><i class="fa fa-camera"></i></div>
                {!! Form::file('photo', ['class' => 'form-control']); !!}
              </div>
            </div>
			<br>	
			<div class="form-group">
				<div class="input-group col-md-offset-3">
					<h4>Selecione o produto que deseja produzir</h2>  
				</div>
				{!! Form::label('', '', ['class' => 'control-label col-sm-3']) !!}
				<div class="col-sm-3 input-group">
					<label>
						{!! Form::radio('product_id', '1'); !!}
						Bike
					</label>
				</div>
				{!! Form::label('', '', ['class' => 'control-label col-sm-3']) !!}
				<div class="col-sm-3 input-group">
					<label>
						{!! Form::radio('product_id', '2'); !!}
						TV
					</label>
				</div>
				{!! Form::label('', '', ['class' => 'control-label col-sm-3']) !!}
				<div class="col-sm-3 input-group">
					<label>
						{!! Form::radio('product_id', '3'); !!}
						Tablet
					</label>
				</div>
				{!! Form::label('', '', ['class' => 'control-label col-sm-3']) !!}
				<div class="col-sm-3 input-group">
					<label>
						{!! Form::radio('product_id', '4'); !!}
						Vídeo-Game
					</label>
				</div>
				{!! Form::label('', '', ['class' => 'control-label col-sm-3']) !!}
				<div class="col-sm-3 input-group">
					<label>
						{!! Form::radio('product_id', '5'); !!}
						Computador
					</label>
				</div>

			</div>

			<div class="col-md-offset-5 col-md-3 text-center">
				{!! Form::submit('Seguinte', ['class' => 'btn btn-success']); !!}
			</div>

        {!! Form::close() !!}
@stop