@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    <h1>Editar</h1>
@stop

@section('content')
        {!! Form::model(auth()->user(), ['route' => 'company.update', 'method' => 'put',  'class'=>'form-horizontal', 'files' => true]) !!}
        	<div class="form-group">
        	  {!! Form::label('name', 'Nome da Empresa (até 12 caracteres)', ['class' => 'control-label col-sm-3 col-md-offset-2']) !!}
        	  <div class="col-sm-3 input-group">
        	    <div class="input-group-addon">...</div>
        	    {!! Form::text('name', null, ['class' => 'form-control', 'required','maxlength' => 12]); !!}
        	  </div>
        	</div>

            <div class="form-group">
              {!! Form::label('photo', 'Foto', ['class' => 'control-label col-sm-3 col-md-offset-2']) !!}
              <div class="col-sm-3 input-group">
                <div class="input-group-addon"><i class="fa fa-camera"></i></div>
                {!! Form::file('photo', ['class' => 'form-control']); !!}
              </div>
            </div>
           
			<div class="col-md-offset-5 col-md-3 text-center">
				{!! Form::submit('Atualizar', ['class' => 'btn btn-success']); !!}
			</div>

        {!! Form::close() !!}
@stop