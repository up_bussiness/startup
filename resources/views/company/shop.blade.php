@extends('adminlte::page')

@section('content_header')
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
  {{--  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css"> --}}
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="{{ asset('vendor/adminlte/vendor/bootstrap/dist/css/bootstrap.min.css') }}">
  <h1>Aquisições Iniciais da empresa</h1>
 
  <style>
                    img[usemap] {
                        border: none;
                        height: auto;
                        max-width: 100%;
                        width: auto;
                    }
                    .vcenter {white-space:nowrap;}
                    .vcenter > * {white-space:normal;}
                    .vcenter:before,
                    .vcenter >* {display:inline-block; vertical-align:middle;}
                    .vcenter:before {content:"";  height:100%;}
                    .vcenter > * {word-spacing:normal;}
                    .botao1 {
                      border-radius: 5px;
                      border-color: #000000;
                      border-width: medium;
                      color: white;
                      font-weight: bold;
                      background-color: #289c6b;
                    }
                    .cfgicon {
                      font-size:20px;
                    }
                    .cfgmodalMP {
                      border-color: #000000;
                      border-width: medium;
                      background-color: #ff5b74;
                    }
                    .cfgmodalMPb {
                      background: red;
                    }
                    .cfgmodalFAB {
                      border-color: #000000;
                      border-width: medium;
                      background-color: #ff825c;
                    }
                    .cfgmodalFMQ {
                      border-color: #000000;
                      border-width: medium;
                      background-color: #ff6445;
                    }
                    .cfgmodalFIN {
                      border-color: #000000;
                      border-width: medium;
                      background-color: #06cbb7;
                    }
                    .cfgmodalMKT {
                      border-color: #000000;
                      border-width: medium;
                      background-color: #ff574e;
                    }
                    .letrabranca {
                      color: white;
                      font-weight: bold;
                    }
                </style>

@stop


@section('content')

  @php
    $turn = $simulation->turns->sortBy('month')->last();
    // @dump($turn->month);
    $decision = $turn->decisionCompany->where('user_id', auth()->user()->id)->first();
    // @dump($decision);
    $total_cash = 0;
    $limit_loan = 0;
    //$total_cash = $decision->balance->findWhere(['turn_id' => $lastTurn->id, 'user_id' => auth()->user()->id], ['active_cash'])->first()->active_cash;
    
    //$limit_loan = $decision->limit->findWhere(['turn_id' => $lastTurn->id, 'user_id' => auth()->user()->id], ['new_limit'])->first()->new_limit;

    $salary = $decision[0];
    $price = $decision[1];
  @endphp
  

  {!! Form::model($decision, ['route' => ['company.configCompany', $decision->id], 'method' => 'put',  'class'=>'form-horizontal', 'files' => true]) !!} 
  {{--  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script> --}}
        <script src="{{ asset('vendor/adminlte/vendor/jquery/dist/jquery.min.js') }}"></script>
        <script src="{{ asset('vendor/adminlte/vendor/bootstrap/dist/js/bootstrap.min.js') }}"></script>
        <script src="{{ asset('js/jquery.rwdImageMaps.min.js') }}"></script>

  <div id="Purchase">
            <div class="input-group col-md-offset-2"> 
                  <img src="{{ asset('images/purchase.jpg') }}" usemap="#image-map" style="height: 100%; width: 100%; object-fit: contain">
                  <map name="image-map">
                    <!-- coords -- inicial x, inicial y , diametro do círculo-->
                    <area target="" alt="propaganda" coords="550,240,60" onclick="funcaoPropaganda();" style="cursor: pointer;" title="Agência Propaganda" shape="circle">
                    <area target="" alt="máquinas" coords="190,453,60" onclick="funcaoMaquinas();" style="cursor: pointer;" title="Máquinas da Produção" shape="circle">
                    <area target="" alt="finanças" coords="145,250,60" onclick="funcaoFinancas();" style="cursor: pointer;" title="Banco" shape="circle">
                    <area target="" alt="produção" coords="330,123,60" onclick="funcaoProducao();" style="cursor: pointer;" title="Matéria prima" shape="circle">
                    <area target="" alt="empresa" coords="520,460,80" onclick="funcaoEmpresa();" style="cursor: pointer;" title="Empresa" shape="circle">
                  </map>
            </div>
            <div class="footer">
            </div>
</div>


      <div class="col-md-offset-4 col-md-1 text-center">
      <br>
        <button type="button" class="btn btn-warning" onclick="goBack()">Voltar</button>
			</div>
			<div class="col-md-offset-0 col-md-1 text-center">
        <br>
				{!! Form::submit('Finalizar Configuração', ['class' => 'btn btn-success']); !!}
      </div>


<!--    MODAIS    --> 

<div class="modal modal-default" id="propaganda">
    <div class="modal-dialog">
      <div class="modal-content cfgmodalMKT">
        <div class="modal-header modal-header-x">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title letrabranca">COMERCIAL - PROPAGANDA</h4>
        </div>
        <div class="modal-body">
          <div id="explica1" align="justify" class="hidden">PARTE DA DEMANDA GERAL DE MERCADO SERÁ DISTRIBUÍDA AOS FABRICANTES DE BICICLETAS DE ACORDO COM AS SUAS ESTRATÉGIAS DE PRECIFICAÇÃO E MARKETING.  PORTANTO, DECIDA QUAL SERÁ SUA MELHOR ESTRATÉGIA PARA CONQUISTAR O MARKET SHARE QUE PLANEJOU.</div>
          <span class="fa fa-question fa-2x pull-right" style="cursor: pointer;" onclick="MostrarExplicacao(1)"></span></br>
          <div class="form-group">
              {!! Form::label('adv_radio', 'Radio', ['class' => 'control-label col-sm-3 col-md-offset-2']) !!}
              <div class="col-sm-4 input-group">
                <div class="input-group-addon"><i class="fa fa-bullhorn"></i></div>
                {!! Form::selectRange('adv_radio', 0, 5, !empty($decision) ? null : 0, ['class' => 'form-control']); !!}
              </div>
          </div>

          <div class="form-group">
              {!! Form::label('adv_journal', 'Jornal', ['class' => 'control-label col-sm-3 col-md-offset-2']) !!}
              <div class="col-sm-4 input-group">
                <div class="input-group-addon"><i class="fa fa-bullhorn"></i></div>
                {!! Form::selectRange('adv_journal', 0, 5, !empty($decision) ? null : 0, ['class' => 'form-control']); !!}
              </div>
          </div>

          <div class="form-group">
              {!! Form::label('adv_social', 'Mídias Sociais', ['class' => 'control-label col-sm-3 col-md-offset-2']) !!}
              <div class="col-sm-4 input-group">
                <div class="input-group-addon"><i class="fa fa-bullhorn"></i></div>
                {!! Form::selectRange('adv_social', 0, 5, !empty($decision) ? null : 0, ['class' => 'form-control']); !!}
              </div>
          </div>

          <div class="form-group">
              {!! Form::label('adv_outdoor', 'Outdoor', ['class' => 'control-label col-sm-3 col-md-offset-2']) !!}
              <div class="col-sm-4 input-group">
                <div class="input-group-addon"><i class="fa fa-bullhorn"></i></div>
                {!! Form::selectRange('adv_outdoor', 0, 5, !empty($decision) ? null : 0, ['class' => 'form-control']); !!}
              </div>
          </div>

          <div class="form-group">
              {!! Form::label('adv_tv', 'TV', ['class' => 'control-label col-sm-3 col-md-offset-2']) !!}
              <div class="col-sm-4 input-group">
                <div class="input-group-addon"><i class="fa fa-bullhorn"></i></div>
                {!! Form::selectRange('adv_tv', 0, 5, !empty($decision) ? null : 0, ['class' => 'form-control']); !!}
              </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-success botao1" data-dismiss="modal"> Continuar </button>
        </div>
      </div>
    </div>
</div>


<div class="modal modal-default" id="maquinas">
        <div class="modal-dialog">
          <div class="modal-content cfgmodalFMQ">
            <div class="modal-header modal-header-x">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title letrabranca">COMPRA E VENDA LINHAS DE PRODUÇÃO</h4>
            </div>
            <div class="modal-body">
              <div id="explica2" align="justify" class="hidden">CASO QUEIRA COMPRAR MÁQUINA, VOCÊ DEVE INSERIR A QUANTIDADE E CLICAR FORA DO CAMPO,  PARA QUE O SISTEMA HABILITE E CAMPO DE RECURSO PRÓPRIO.  NELE, VOCÊ DEVE ESCOLHER QUANTO SUA EMPRESA VAI INVESTIR DE CAPITAL PRÓPRIO NA COMPRA DA NOVA MÁQUINA.</br>CASO QUEIRA VENDER UMA DE SUAS LINHAS DE PRODUÇÃO,  VOCÊ DEVE PREENCHER,  NO CAMPO DA REFERIDA LINHA QUE DESEJA VENDER, A QUANTIDADE DE LINHAS QUE SERÃO VENDIDAS.VALE A PENA LEMBRAR QUE SUA EMPRESA NÃO PODE VENDER TODAS AS MÁQUINAS, E QUE O VALOR DA VENDA É O VALOR DE AQUISIÇÃO, MENOS A DEPRECIAÇÃO E A DESVALORIZAÇÃO DE MERCADO, PUBLICADA NO INFORMATIVO DE MERCADO. O  VALOR DESSA VENDA SERÁ CREDITADO NO FINAL DO PERÍODO.</div>
              <span class="fa fa-question fa-2x pull-right" style="cursor: pointer;" onclick="MostrarExplicacao(2)"></span></br>
              Compra de Máquinas:
              <div class="form-group">
                {!! Form::label('buy_small_machine', 'Pequena', ['class' => 'control-label col-sm-4 col-md-offset-1']) !!}
                <div class="col-sm-4 input-group">
                  <div class="input-group-addon">N.</div>
                  {!! Form::text('buy_small_machine', !empty($decision) ? null : 0, ['class' => 'form-control']); !!}
                </div>
              </div>
  
              <div class="form-group hidden" id="b_s_m_id">
                  {!! Form::label('b_s_m', 'Recurso próprio (P)', ['class' => 'control-label col-sm-4 col-md-offset-1']) !!}
                <div class="col-sm-4 input-group">
                  {!! Form::selectRange('b_s_m', 30, 100, !empty($decision) ? null : 0, ['class' => 'form-control']); !!}
                </div>
              </div>
  
              <div class="form-group">
                {!! Form::label('buy_medium_machine', 'Média', ['class' => 'control-label col-sm-4 col-md-offset-1']) !!}
                <div class="col-sm-4 input-group">
                  <div class="input-group-addon">N.</div>
                  {!! Form::text('buy_medium_machine', !empty($decision) ? null : 0, ['class' => 'form-control']); !!}
                </div>
              </div>
  
              <div class="form-group hidden" id="b_m_m_id">
                  {!! Form::label('b_m_m', 'Recurso próprio (M)', ['class' => 'control-label col-sm-4 col-md-offset-1']) !!}
                <div class="col-sm-4 input-group">
                  {!! Form::selectRange('b_m_m', 30, 100, !empty($decision) ? null : 0, ['class' => 'form-control']); !!}
                </div>
              </div>
  
              <div class="form-group">
                {!! Form::label('buy_large_machine', 'Grande', ['class' => 'control-label col-sm-4 col-md-offset-1']) !!}
                <div class="col-sm-4 input-group">
                  <div class="input-group-addon">N.</div>
                  {!! Form::text('buy_large_machine', !empty($decision) ? null : 0, ['class' => 'form-control']); !!}
                </div>
              </div>
  
              <div class="form-group hidden" id="b_l_m_id">
                  {!! Form::label('b_l_m', 'Recurso próprio (G)', ['class' => 'control-label col-sm-4 col-md-offset-1']) !!}
                <div class="col-sm-4 input-group">
                  {!! Form::selectRange('b_l_m', 30, 100, !empty($decision) ? null : 0, ['class' => 'form-control']); !!}
                </div>
              </div>
              Venda de Máquinas:
              <div class="form-group">
                {!! Form::label('sell_small_machine', 'Pequena', ['class' => 'control-label col-sm-4 col-md-offset-1']) !!}
                <div class="col-sm-4 input-group">
                  <div class="input-group-addon">N.</div>
                  {!! Form::text('sell_small_machine', !empty($decision) ? null : 0, ['class' => 'form-control']); !!}
                </div>
              </div>
  
              <div class="form-group">
                {!! Form::label('sell_medium_machine', 'Média', ['class' => 'control-label col-sm-4 col-md-offset-1']) !!}
                <div class="col-sm-4 input-group">
                  <div class="input-group-addon">N.</div>
                  {!! Form::text('sell_medium_machine', !empty($decision) ? null : 0, ['class' => 'form-control']); !!}
                </div>
              </div>
  
              <div class="form-group">
                {!! Form::label('sell_large_machine', 'Grande', ['class' => 'control-label col-sm-4 col-md-offset-1']) !!}
                <div class="col-sm-4 input-group">
                  <div class="input-group-addon">N.</div>
                  {!! Form::text('sell_large_machine', !empty($decision) ? null : 0, ['class' => 'form-control']); !!}
                </div>
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-success botao1" data-dismiss="modal"> Continuar </button>
            </div>
          </div>
        </div>
</div>



<div class="modal modal-default" id="financas">
  <div class="modal-dialog">
    <div class="modal-content cfgmodalFIN">
      <div class="modal-header modal-header-x">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title letrabranca">FINANÇAS</h4>
      </div>
      <div class="modal-body">
        <div id="explica3" align="justify" class="hidden">NESTES CAMPOS SUA EMPRESA DEVERÁ TOMAR DECISÕES QUE POSSIBILITEM UMA BOA GESTÃO DE SEUS RECURSOS FINANCEIROS.</br>NO CAOS DE EMPRÉSTIMO, IMPORTANTE RESSALTAR QUE O LIMITE DE CRÉDITO CONCEDIDO PELO BANCO, É O MESMO PARA EMPRÉSTIMOS E FINANCIAMENTO DE LINHAS DE PRODUÇÃO.</br>CASO SOLICITE ANTECIPAÇÃO DE RECEBÍVEIS, O RECURSO SERÁ CREDITADO NESTE MESMO PERÍODO, DESCONTADO AS TAXAS DE JUROS PRATICADAS PELO BANCO.</br>NO CASO DE DECIDIR APLICAR RECURSOS, O VALOR SOLICITADO SÓ SERÁ EFETIVAMENTE APLICADO APÓS LIQUIDAÇÃO DAS OBRIGAÇÕES FINANCEIRAS COM SEUS FUNCIONÁRIOS, FORNECEDORES E BANCOS, E O MESMO DEVERÁ RETORNAR AO CAIXA AUTOMATICAMENTE NO PRÓXIMO PERÍODO ACRESCIDO DOS JUROS.</div>
        <span class="fa fa-question fa-2x pull-right" style="cursor: pointer;" onclick="MostrarExplicacao(3)"></span></br>
        <div class="form-group">
          {!! Form::label('loan', 'Empréstimo', ['class' => 'control-label col-sm-5']) !!}
          <div class="col-sm-4 input-group">
            <div class="input-group-addon">$</div>
            {!! Form::text('loan', !empty($decision) ? null : 0, ['class' => 'form-control']); !!}
          </div>
        </div>

        <div class="form-group">
          {!! Form::label('anticipation_of_receivables', 'Antecipação de Recebíveis', ['class' => 'control-label col-sm-5']) !!}
          <div class="col-sm-4 input-group">
            <div class="input-group-addon">%</div>
            {!! Form::text('anticipation_of_receivables', !empty($decision) ? null : 0, ['class' => 'form-control']); !!}
          </div>
        </div>

        <div class="form-group">
          {!! Form::label('application', 'Aplicação', ['class' => 'control-label col-sm-5']) !!}
          <div class="col-sm-4 input-group">
            <div class="input-group-addon">%</div>
            {!! Form::text('application', !empty($decision) ? null : 0, ['class' => 'form-control']); !!}
          </div>
        </div>

        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success botao1" data-dismiss="modal"> Continuar </button>
      </div>
    </div>
  </div>
</div>



<div class="modal modal-default" id="producao">
  <div class="modal-dialog">
    <div class="modal-content cfgmodalMP">
      <div class="modal-header modal-header-x">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title letrabranca">COMPRA DE MATERIAS-PRIMAS</h4>
      </div>
      <div class="modal-body">
        <div class="panel">
           <div class="panel-heading" style="background-image:none;background: #2b569f; color: white"><span class="fa fa-question fa-2x pull-right" style="cursor: pointer;" onclick="MostrarExplicacao(4)"></span>EMERGENCIAL</div>
           <div class="panel-body cfgmodalMP">
            <div id="explica4" align="justify" class="hidden">AQUI, SEU TIME DEVE DECIDIR A COMPRA DE MATÉRIAS-PRIMAS PARA SUPRIR UMA NECESSIDADE EMERGENCIAL, POR CONTA DE UMA COMPRA PROGRAMADA EQUIVOCADA OU POR DECISÃO DE PRODUÇÃO EXTRA.</div>
            <div class="form-group">
              {!! Form::label('emergency_rma', 'Emergencial - Quadro', ['class' => 'control-label col-sm-4 col-md-offset-1']) !!}
              <div class="col-sm-4 input-group">
                <div class="input-group-addon">N.</div>
                {!! Form::text('emergency_rma', !empty($decision) ? null : 0, ['class' => 'form-control']); !!}
              </div>
            </div>

            <div class="form-group">
              {!! Form::label('emergency_rmb', 'Kit 1 (Conjunto de roda/pneus)', ['class' => 'control-label col-sm-4 col-md-offset-1']) !!}
              <div class="col-sm-4 input-group">
                <div class="input-group-addon">N.</div>
                {!! Form::text('emergency_rmb', !empty($decision) ? null : 0, ['class' => 'form-control']); !!}
              </div>
            </div>

            <div class="form-group">
              {!! Form::label('emergency_rmc', 'Kit 2 (Acessórios)', ['class' => 'control-label col-sm-4 col-md-offset-1']) !!}
              <div class="col-sm-4 input-group">
                <div class="input-group-addon">N.</div>
                {!! Form::text('emergency_rmc', !empty($decision) ? null : 0, ['class' => 'form-control']); !!}
              </div>
            </div>

           </div>
        </div>
        <div class="panel">
          <div class="panel-heading" style="background-image:none;background: #2b569f; color: white"><span class="fa fa-question fa-2x pull-right" style="cursor: pointer;" onclick="MostrarExplicacao(5)"></span>PARA O PRÓXIMO PERÍODO</div>
          <div class="panel-body cfgmodalMP">
            <div id="explica5" align="justify" class="hidden">NESTE MOMENTO, SEU TIME DEVERÁ COMPRAR MATÉRIA-PRIMA SUFICIENTE PARA SUPRIR A CAPACIDADE PRODUTIVA DE SUAS LINHAS DE PRODUÇÃO, PARA O PRÓXIMO PERÍODO.</div>
            <div class="form-group">
              {!! Form::label('scheduled_rma', 'Quadro', ['class' => 'control-label col-sm-4 col-md-offset-1']) !!}
              <div class="col-sm-4 input-group">
                <div class="input-group-addon">N.</div>
                {!! Form::text('scheduled_rma', !empty($decision) ? null : 0, ['class' => 'form-control']); !!}
              </div>
            </div>
    
            <div class="form-group">
              {!! Form::label('scheduled_rmb', 'Kit 1 (Conjunto de roda/pneus)', ['class' => 'control-label col-sm-4 col-md-offset-1']) !!}
              <div class="col-sm-4 input-group">
                <div class="input-group-addon">N.</div>
                {!! Form::text('scheduled_rmb', !empty($decision) ? null : 0, ['class' => 'form-control']); !!}
              </div>
            </div>
    
            <div class="form-group">
              {!! Form::label('scheduled_rmc', 'Kit 2 (Acessórios)', ['class' => 'control-label col-sm-4 col-md-offset-1']) !!}
              <div class="col-sm-4 input-group">
                <div class="input-group-addon">N.</div>
                {!! Form::text('scheduled_rmc', !empty($decision) ? null : 0, ['class' => 'form-control']); !!}
              </div>
            </div>
    
            <div class="form-group">
              {!! Form::label('payment_rm', 'Forma de Pagamento', ['class' => 'control-label col-sm-4 col-md-offset-1']) !!}
              <div class="col-sm-4 input-group">
                <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                {!! Form::select('payment_rm', ['à vista', '1+1'], !empty($decision) ? null : 0, ['class' => 'form-control']); !!}
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success botao1" data-dismiss="modal"> Continuar </button>
      </div>
    </div>
  </div>
</div>


<div class="modal modal-default" id="decisao">
    <div class="modal-dialog">
      <div class="modal-content cfgmodalFAB">
        <div class="modal-header modal-header-x">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title letrabranca">DECISÃO DA EMPRESA</h4>
        </div>
        <div class="modal-body">
          <div class="panel">
             <div class="panel-heading" style="background-image:none;background: #0086a3; color: white"><span class="fa fa-question fa-2x pull-right" style="cursor: pointer;" onclick="MostrarExplicacao(6)"></span>COMERCIAL</div>
             <div class="panel-body cfgmodalFAB">
                <div id="explica6" align="justify" class="hidden">NESTES CAMPOS, VOCÊ E SEU TIME DEVERÃO DECIDIR QUAL PREÇO E CONDIÇÕES DE PAGAMENTO SERÃO COMPETITIVOS PARA GERAÇÃO DE DEMANDA SUFICIENTE  PARA VENDA DE TODA SUA PRODUÇÃO E ESTOQUE DE PRODUTOS ACABADOS.</div>
                <div class="col-md-12">
                    <div class="alert alert-success text-center">
                      <h5><b>Saldo em caixa: $ {{ numberDot($total_cash) }}</b></h5>
                      <h5><b>Limite de empréstimo: $ {{ numberDot($limit_loan) }}</b></h5>
                    </div>
                  </div>
                  <div class="form-group">
                    {!! Form::label('price', 'Preço à vista', ['class' => 'control-label col-sm-3 col-md-offset-2']) !!}
                    <div class="col-sm-4 input-group">
                      <div class="input-group-addon">$</div>
                      {!! Form::text('price', !empty($decision) ? null : $price, ['class' => 'form-control']); !!}
                    </div>
                </div>
          
                <div class="form-group">
                    {!! Form::label('term', 'Prazo', ['class' => 'control-label col-sm-3 col-md-offset-2']) !!}
                    <div class="col-sm-4 input-group">
                      <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                      {!! Form::select('term', ['à vista', '1+1'], 0, ['class' => 'form-control']); !!}
                    </div>
                </div>

                <div class="form-group hidden" id="fg_term_interest">
                  {!! Form::label('term_interest', 'Juros nas vendas a prazo', ['class' => 'control-label col-sm-5']) !!}
                  <div class="col-sm-4 input-group">
                    <div class="input-group-addon">%</div>
                    {!! Form::text('term_interest', !empty($decision) ? null : 0, ['class' => 'form-control']); !!}
                  </div>
                </div>
             </div>
          </div>

          <div class="panel">
              <div class="panel-heading" style="background-image:none;background: #0086a3; color: white"><span class="fa fa-question fa-2x pull-right" style="cursor: pointer;" onclick="MostrarExplicacao(8)"></span>PLANEJAMENTO DA PRODUÇÃO DE MÁQUINAS</div>
              <div class="panel-body cfgmodalFAB">
                  <div id="explica8" align="justify" class="hidden">SEU TIME DEVE REGISTRAR O NÍVEL DE ATIVIDADE QUE SUAS LINHAS DE PRODUÇÃO IRÃO OPERAR PARA PRODUZIR A QUANTIDADE QUE DESEJAM. ELAS PODEM OPERAR COM NÍVEIS ENTRE 50% E 100%.</br>CASO NECESSITE DE PRODUÇÃO EXTRA, ESSA PODERÁ SER PROGRAMADA COM NÍVEL DE 1% A 25%.PARA QUE TUDO OCORRA COMO PLANEJADO, SAIBA QUAL A CAPACIDADE PRODUTIVA DE SUAS LINHAS DE PRODUÇÃO, VERIFIQUE O ESTOQUE DE MATÉRIAS-PRIMAS E ANALISE SE O NÚMERO DE OPERÁRIOS É SUFICIENTE.</div>
                <div class="form-group">
                  {!! Form::label('activity_level', 'Nível de atividade', ['class' => 'control-label col-sm-4 col-md-offset-1']) !!}
                  <div class="col-sm-4 input-group">
                    <div class="input-group-addon">%</div>
                    {!! Form::text('activity_level', !empty($decision) ? null : 0, ['class' => 'form-control']); !!}
                  </div>
                </div>
        
                <div class="form-group">
                  {!! Form::label('extra_production', 'Produção extra', ['class' => 'control-label col-sm-4 col-md-offset-1']) !!}
                  <div class="col-sm-4 input-group">
                    <div class="input-group-addon">%</div>
                    {!! Form::text('extra_production', !empty($decision) ? null : 0, ['class' => 'form-control']); !!}
                  </div>
                </div>
        
              </div>
          </div>

          <div class="panel">
            <div class="panel-heading" style="background-image:none;background: #0086a3; color: white"><span class="fa fa-question fa-2x pull-right" style="cursor: pointer;" onclick="MostrarExplicacao(7)"></span>RH</div>
            <div class="panel-body cfgmodalFAB">
                <div id="explica7" align="justify" class="hidden">AS DECISÕES DE RECURSOS HUMANOS SÃO IMPORTANTÍSSIMAS, ELAS SE DÃO NA CONTRATAÇÃO OU DEMISSÃO DE OPERÁRIOS POR CONTA DE COMPRA OU VENDA DE LINHAS DE PRODUÇÃO, FIQUE ATENTO AO QUE O MANUAL ORIENTA.</br>COM RELAÇÃO AO SALÁRIO DOS SEUS OPERÁRIO E TREINAMENTO, OBSERVE O INDICADOR DE PRODUTIVIDADE NO RELATÓRIO MERCADOLÓGICO, ELE DEVERÁ NORTEAR A NECESSIDADE DE MOTIVAR SEU TIME.</div>
                <div class="form-group">
                    {!! Form::label('admitted', 'Admitidos', ['class' => 'control-label col-sm-3 col-md-offset-2']) !!}
                    <div class="col-sm-4 input-group">
                      <div class="input-group-addon">N.</div>
                      {!! Form::text('admitted', !empty($decision) ? null : 0, ['class' => 'form-control']); !!}
                    </div>
                  </div>
      
                  <div class="form-group">
                    {!! Form::label('fired', 'Demitidos', ['class' => 'control-label col-sm-3 col-md-offset-2']) !!}
                    <div class="col-sm-4 input-group">
                      <div class="input-group-addon">N.</div>
                      {!! Form::text('fired', !empty($decision) ? null : 0, ['class' => 'form-control']); !!}
                    </div>
                  </div>
      
                  <div class="form-group">
                    {!! Form::label('salary', 'Salário', ['class' => 'control-label col-sm-3 col-md-offset-2']) !!}
                    <div class="col-sm-4 input-group">
                      <div class="input-group-addon">$</div>
                      {!! Form::text('salary', 0, ['class' => 'form-control']); !!}
                    </div>
                  </div>
      
                  <div class="form-group">
                    {!! Form::label('training', 'Treinamento', ['class' => 'control-label col-sm-3 col-md-offset-2']) !!}
                    <div class="col-sm-4 input-group">
                      <div class="input-group-addon">%</div>
                      {!! Form::text('training', !empty($decision) ? null : 0, ['class' => 'form-control']); !!}
                    </div>
                  </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-success botao1" data-dismiss="modal"> Continuar </button>
        </div>
      </div>
    </div>
</div>
{!! Form::close() !!}
@stop

@section('js')
<script>
  function goBack() {
    window.history.back();
  }

  $(document).ready(function(e) {
        // $('img[usemap]').rwdImageMaps();
    });
    function funcaoPropaganda() {
      $('#propaganda').modal();
    }
    function funcaoMaquinas() {
      $('#maquinas').modal();
    }
    function funcaoFinancas() {
	    $('#financas').modal();
    }
    function funcaoProducao() {
      console.log ('Modal Produção');
      $('#producao').modal();
    }
    function funcaoEmpresa() {
      $('#decisao').modal();
    }
    function teste() {
       alert('teste kjkjka');
    }
</script>
<script>
  $(function() {

    $("#decision").submit(function(e) {
      
      e.stopPropagation();

      var $bt = $(this).find(':submit');

      $bt.attr('disabled', 'disabled');
      $bt.val('Gravando...');
      $bt.css('cursor', 'progress');
    });

    $('[id*=adv_]').change(function(e) {
      if ($(this).val() == null) {
        $(this).val(0);
      }
    })

    if ($('#buy_small_machine').val() > 0) {
      $('#b_s_m_id').removeClass('hidden');
    }
    if ($('#buy_medium_machine').val() > 0) {
      $('#b_m_m_id').removeClass('hidden');
    }
    if ($('#buy_large_machine').val() > 0) {
      $('#b_l_m_id').removeClass('hidden');
    }

    $('#buy_small_machine, #buy_medium_machine, #buy_large_machine').change(function() {
      let val = $(this).val();
      let id = $(this).attr('id');
      if (val != 0) {
        if (id == 'buy_small_machine') {
          $('#b_s_m_id').removeClass('hidden');
        }
        if (id == 'buy_medium_machine') {
          $('#b_m_m_id').removeClass('hidden');
        }
        if (id == 'buy_large_machine') {
          $('#b_l_m_id').removeClass('hidden');
        }
      } else {
        if (id == 'buy_small_machine') {
          $('#b_s_m_id').addClass('hidden');
        }
        if (id == 'buy_medium_machine') {
          $('#b_m_m_id').addClass('hidden');
        }
        if (id == 'buy_large_machine') {
          $('#b_l_m_id').addClass('hidden');
        }
      }
    });
  })
</script>
{!! Form::close() !!}
@stop

