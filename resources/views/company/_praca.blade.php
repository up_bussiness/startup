<!-- Modal da Praça -->
@section('css')
<style>
    .modalTodaTela {
        width: 100%;
        height: 100%;
        margin: 0;
        padding: 0;
    }
    .modal-header-x {
        color:#fff;
          padding:9px 15px;
          border-bottom:1px solid #eee;
          background-color: #428bca;
          -webkit-border-top-left-radius: 5px;
          -webkit-border-top-right-radius: 5px;
          -moz-border-radius-topleft: 5px;
          -moz-border-radius-topright: 5px;
           border-top-left-radius: 5px;
           border-top-right-radius: 5px;
    }
</style>
@endsection
<div class="modal fade" id="Praca" tabindex="-1" role="dialog" aria-labelledby="CompaniesLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg modalTodaTela" role="document">
    <div class="modal-content">
      <div class="modal-header bg-yellow">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title" id="CompaniesLabel">Praça</h4>
      </div>
      <div class="modal-body">
        <div class="table-responsive">
            <img src="{{ asset('images/praca.png') }}" usemap="#image-map" style="height: 100%; width: 100%; object-fit: contain">
            <map name="image-map">
              <!--  <area target="" alt="detran" coords="66,579,79,860,227,835,218,574" onclick="funcaoDetran();" title="Detran" shape="poly">
                <area target="" alt="noticias" coords="996,525,981,759,1167,770,1162,526" onclick="funcaoArea2();" title="Notícias" shape="poly">
                <area target="" alt="bicicleta" coords="1298,761,1291,835,1480,860,1481,782" onclick="funcaobicicletaDecisao();" title="Loja de Bicicletas" shape="poly">
                <area target="" alt="concessionaria" coords="602,708,609,811,775,802,768,698" onclick="funcaoArea2();" title="Concessionária" shape="poly">
                <area target="" alt="banco" coords="156,1008,162,1084,282,1051,277,978" onclick="funcaoFinancas();" title="Banco" shape="poly">
                <area target="" alt="rastreador" coords="1434,938,1436,1023,1662,1067,1663,968" onclick="funcaoArea2();" title="Rastreador" shape="poly">
                <area target="" alt="industria" coords="1603,632,1598,823,1633,825,1633,638" onclick="funcaoArea2();" title="Fornecedor de Máquinas" shape="poly">
                <area target="" alt="pneus" coords="1672,915,1678,993,1987,1049,1996,968,1948,934,1764,874" onclick="funcaoArea2();" title="Loja de Pneus" shape="poly">
                <area target="" alt="albatroz" coords="598,258,136" onclick="funcaoPropaganda();" title="Agência Albatroz" shape="circle">
                <area target="" alt="fornecedor de maquinas" coords="1655,399,1817,814" onclick="funcaoMaquinas();" title="Fornecedor de Máquinas" shape="rect">
                <area target="" alt="materia prima" coords="1363,487,1223,710" onclick="materiaprima();" title="Matéria Prima" shape="rect"> -->

              <area target="" alt="albatroz" coords="636,244,81" onclick="funcaoPropaganda();" title="Agência Albatroz" shape="circle">
              <area target="" alt="máquinas" coords="1684,256,93" onclick="funcaoMaquinas();" title="Máquinas da Produção" shape="circle">
              <area target="" alt="finanças" coords="206,871,107" onclick="funcaoFinancas();" title="Finanças" shape="circle">
              <area target="" alt="produção" coords="1234,368,45" onclick="funcaoProducao();" title="Produção" shape="circle">
              <area target="" alt="empresa" coords="1385,700,89" onclick="funcaoEmpresa();" title="Empresa" shape="circle">

           <!--   <area target="" alt="Notícias" coords="1033,454,93" onclick="funcaoMaquinas();" title="Jornal" shape="circle"> -->
            </map>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Voltar</button>
        <a href="#" class="btn btn-info" role="button" onclick="teste();">Link Button</a>
      </div>
    </div>
  </div>
</div>
<!--    MODAIS    --> 
<div class="modal modal-default" id="propaganda">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header modal-header-x">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">COMERCIAL - PROPAGANDA</h4>
        </div>
        <div class="modal-body">
          <div class="form-group">
              {!! Form::label('adv_radio', 'Radio', ['class' => 'control-label col-sm-3 col-md-offset-2']) !!}
              <div class="col-sm-4 input-group">
                <div class="input-group-addon"><i class="fa fa-bullhorn"></i></div>
                {!! Form::selectRange('adv_radio', 0, 5, !empty($decision) ? null : 0, ['class' => 'form-control']); !!}
              </div>
          </div>

          <div class="form-group">
              {!! Form::label('adv_journal', 'Jornal', ['class' => 'control-label col-sm-3 col-md-offset-2']) !!}
              <div class="col-sm-4 input-group">
                <div class="input-group-addon"><i class="fa fa-bullhorn"></i></div>
                {!! Form::selectRange('adv_journal', 0, 5, !empty($decision) ? null : 0, ['class' => 'form-control']); !!}
              </div>
          </div>

          <div class="form-group">
              {!! Form::label('adv_social', 'Mídias Sociais', ['class' => 'control-label col-sm-3 col-md-offset-2']) !!}
              <div class="col-sm-4 input-group">
                <div class="input-group-addon"><i class="fa fa-bullhorn"></i></div>
                {!! Form::selectRange('adv_social', 0, 5, !empty($decision) ? null : 0, ['class' => 'form-control']); !!}
              </div>
          </div>

          <div class="form-group">
              {!! Form::label('adv_outdoor', 'Outdoor', ['class' => 'control-label col-sm-3 col-md-offset-2']) !!}
              <div class="col-sm-4 input-group">
                <div class="input-group-addon"><i class="fa fa-bullhorn"></i></div>
                {!! Form::selectRange('adv_outdoor', 0, 5, !empty($decision) ? null : 0, ['class' => 'form-control']); !!}
              </div>
          </div>

          <div class="form-group">
              {!! Form::label('adv_tv', 'TV', ['class' => 'control-label col-sm-3 col-md-offset-2']) !!}
              <div class="col-sm-4 input-group">
                <div class="input-group-addon"><i class="fa fa-bullhorn"></i></div>
                {!! Form::selectRange('adv_tv', 0, 5, !empty($decision) ? null : 0, ['class' => 'form-control']); !!}
              </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-success"> Ok </button>
        </div>
      </div>
    </div>
</div>
<div class="modal modal-default" id="maquinas">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header modal-header-x">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title">COMPRA E VENDA LINHAS DE PRODUÇÃO</h4>
            </div>
            <div class="modal-body">
              <div class="form-group">
                {!! Form::label('buy_small_machine', 'Compra P', ['class' => 'control-label col-sm-4 col-md-offset-1']) !!}
                <div class="col-sm-4 input-group">
                  <div class="input-group-addon">N.</div>
                  {!! Form::text('buy_small_machine', !empty($decision) ? null : 0, ['class' => 'form-control']); !!}
                </div>
              </div>
  
              <div class="form-group hidden" id="b_s_m_id">
                  {!! Form::label('b_s_m', 'Recurso próprio (P)', ['class' => 'control-label col-sm-4 col-md-offset-1']) !!}
                <div class="col-sm-4 input-group">
                  {!! Form::selectRange('b_s_m', 30, 100, !empty($decision) ? null : 0, ['class' => 'form-control']); !!}
                </div>
              </div>
  
              <div class="form-group">
                {!! Form::label('buy_medium_machine', 'Compra M', ['class' => 'control-label col-sm-4 col-md-offset-1']) !!}
                <div class="col-sm-4 input-group">
                  <div class="input-group-addon">N.</div>
                  {!! Form::text('buy_medium_machine', !empty($decision) ? null : 0, ['class' => 'form-control']); !!}
                </div>
              </div>
  
              <div class="form-group hidden" id="b_m_m_id">
                  {!! Form::label('b_m_m', 'Recurso próprio (M)', ['class' => 'control-label col-sm-4 col-md-offset-1']) !!}
                <div class="col-sm-4 input-group">
                  {!! Form::selectRange('b_m_m', 30, 100, !empty($decision) ? null : 0, ['class' => 'form-control']); !!}
                </div>
              </div>
  
              <div class="form-group">
                {!! Form::label('buy_large_machine', 'Compra G', ['class' => 'control-label col-sm-4 col-md-offset-1']) !!}
                <div class="col-sm-4 input-group">
                  <div class="input-group-addon">N.</div>
                  {!! Form::text('buy_large_machine', !empty($decision) ? null : 0, ['class' => 'form-control']); !!}
                </div>
              </div>
  
              <div class="form-group hidden" id="b_l_m_id">
                  {!! Form::label('b_l_m', 'Recurso próprio (G)', ['class' => 'control-label col-sm-4 col-md-offset-1']) !!}
                <div class="col-sm-4 input-group">
                  {!! Form::selectRange('b_l_m', 30, 100, !empty($decision) ? null : 0, ['class' => 'form-control']); !!}
                </div>
              </div>
  
              <div class="form-group">
                {!! Form::label('sell_small_machine', 'Venda P', ['class' => 'control-label col-sm-4 col-md-offset-1']) !!}
                <div class="col-sm-4 input-group">
                  <div class="input-group-addon">N.</div>
                  {!! Form::text('sell_small_machine', !empty($decision) ? null : 0, ['class' => 'form-control']); !!}
                </div>
              </div>
  
              <div class="form-group">
                {!! Form::label('sell_medium_machine', 'Venda M', ['class' => 'control-label col-sm-4 col-md-offset-1']) !!}
                <div class="col-sm-4 input-group">
                  <div class="input-group-addon">N.</div>
                  {!! Form::text('sell_medium_machine', !empty($decision) ? null : 0, ['class' => 'form-control']); !!}
                </div>
              </div>
  
              <div class="form-group">
                {!! Form::label('sell_large_machine', 'Venda G', ['class' => 'control-label col-sm-4 col-md-offset-1']) !!}
                <div class="col-sm-4 input-group">
                  <div class="input-group-addon">N.</div>
                  {!! Form::text('sell_large_machine', !empty($decision) ? null : 0, ['class' => 'form-control']); !!}
                </div>
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-success"> Ok </button>
            </div>
          </div>
        </div>
</div>
<div class="modal modal-default" id="financas">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header modal-header-x">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">FINANÇAS</h4>
      </div>
      <div class="modal-body">
        <div class="form-group">
          {!! Form::label('loan', 'Empréstimo', ['class' => 'control-label col-sm-5']) !!}
          <div class="col-sm-4 input-group">
            <div class="input-group-addon">$</div>
            {!! Form::text('loan', !empty($decision) ? null : 0, ['class' => 'form-control']); !!}
          </div>
        </div>

        <div class="form-group">
          {!! Form::label('anticipation_of_receivables', 'Antecipação de Recebíveis', ['class' => 'control-label col-sm-5']) !!}
          <div class="col-sm-4 input-group">
            <div class="input-group-addon">%</div>
            {!! Form::text('anticipation_of_receivables', !empty($decision) ? null : 0, ['class' => 'form-control']); !!}
          </div>
        </div>

        <div class="form-group">
          {!! Form::label('application', 'Aplicação', ['class' => 'control-label col-sm-5']) !!}
          <div class="col-sm-4 input-group">
            <div class="input-group-addon">%</div>
            {!! Form::text('application', !empty($decision) ? null : 0, ['class' => 'form-control']); !!}
          </div>
        </div>

        <div class="form-group">
          {!! Form::label('term_interest', 'Juros nas vendas a prazo', ['class' => 'control-label col-sm-5']) !!}
          <div class="col-sm-4 input-group">
            <div class="input-group-addon">%</div>
            {!! Form::text('term_interest', !empty($decision) ? null : 0, ['class' => 'form-control']); !!}
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success"> Ok </button>
      </div>
    </div>
  </div>
</div>
<div class="modal modal-default" id="producao">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header modal-header-x">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">PROGRAMAÇÃO DA PRODUÇÃO</h4>
      </div>
      <div class="modal-body">
        <div class="panel panel-info">
           <div class="panel-heading">PERÍODO ATUAL</div>
           <div class="panel-body">
            <div class="form-group">
              {!! Form::label('emergency_rma', 'Emergencial - Quadro', ['class' => 'control-label col-sm-4 col-md-offset-1']) !!}
              <div class="col-sm-4 input-group">
                <div class="input-group-addon">N.</div>
                {!! Form::text('emergency_rma', !empty($decision) ? null : 0, ['class' => 'form-control']); !!}
              </div>
            </div>

            <div class="form-group">
              {!! Form::label('emergency_rmb', 'Emergencial - Kit 1', ['class' => 'control-label col-sm-4 col-md-offset-1']) !!}
              <div class="col-sm-4 input-group">
                <div class="input-group-addon">N.</div>
                {!! Form::text('emergency_rmb', !empty($decision) ? null : 0, ['class' => 'form-control']); !!}
              </div>
            </div>

            <div class="form-group">
              {!! Form::label('emergency_rmc', 'Emergencial - Kit 2', ['class' => 'control-label col-sm-4 col-md-offset-1']) !!}
              <div class="col-sm-4 input-group">
                <div class="input-group-addon">N.</div>
                {!! Form::text('emergency_rmc', !empty($decision) ? null : 0, ['class' => 'form-control']); !!}
              </div>
            </div>

            <div class="form-group">
              {!! Form::label('activity_level', 'Nível de atividade', ['class' => 'control-label col-sm-4 col-md-offset-1']) !!}
              <div class="col-sm-4 input-group">
                <div class="input-group-addon">%</div>
                {!! Form::text('activity_level', !empty($decision) ? null : 0, ['class' => 'form-control']); !!}
              </div>
            </div>

            <div class="form-group">
              {!! Form::label('extra_production', 'Produção extra', ['class' => 'control-label col-sm-4 col-md-offset-1']) !!}
              <div class="col-sm-4 input-group">
                <div class="input-group-addon">%</div>
                {!! Form::text('extra_production', !empty($decision) ? null : 0, ['class' => 'form-control']); !!}
              </div>
            </div>
           </div>
        </div>
        <div class="panel panel-warning">
          <div class="panel-heading">PRÓXIMO PERÍODO</div>
          <div class="panel-body">
            <div class="form-group">
              {!! Form::label('scheduled_rma', 'Quadro', ['class' => 'control-label col-sm-4 col-md-offset-1']) !!}
              <div class="col-sm-4 input-group">
                <div class="input-group-addon">N.</div>
                {!! Form::text('scheduled_rma', !empty($decision) ? null : 0, ['class' => 'form-control']); !!}
              </div>
            </div>
    
            <div class="form-group">
              {!! Form::label('scheduled_rmb', 'Kit 1', ['class' => 'control-label col-sm-4 col-md-offset-1']) !!}
              <div class="col-sm-4 input-group">
                <div class="input-group-addon">N.</div>
                {!! Form::text('scheduled_rmb', !empty($decision) ? null : 0, ['class' => 'form-control']); !!}
              </div>
            </div>
    
            <div class="form-group">
              {!! Form::label('scheduled_rmc', 'Kit 2', ['class' => 'control-label col-sm-4 col-md-offset-1']) !!}
              <div class="col-sm-4 input-group">
                <div class="input-group-addon">N.</div>
                {!! Form::text('scheduled_rmc', !empty($decision) ? null : 0, ['class' => 'form-control']); !!}
              </div>
            </div>
    
            <div class="form-group">
              {!! Form::label('payment_rm', 'Pagamento MP', ['class' => 'control-label col-sm-4 col-md-offset-1']) !!}
              <div class="col-sm-4 input-group">
                <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                {!! Form::select('payment_rm', ['à vista', '1+1'], !empty($decision) ? null : 0, ['class' => 'form-control']); !!}
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success"> Salvar </button>
      </div>
    </div>
  </div>
</div>
<div class="modal modal-default" id="decisao">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header modal-header-x">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">DECISÃO DA EMPRESA</h4>
        </div>
        <div class="modal-body">
          <div class="panel panel-info">
             <div class="panel-heading">COMERCIAL</div>
             <div class="panel-body">
                <div class="form-group">
                    {!! Form::label('price', 'Preço à vista', ['class' => 'control-label col-sm-3 col-md-offset-2']) !!}
                    <div class="col-sm-4 input-group">
                      <div class="input-group-addon">$</div>
                      {!! Form::text('price', !empty($decision) ? null : 7, ['class' => 'form-control']); !!}
                    </div>
                  </div>
          
                  <div class="form-group">
                    {!! Form::label('term', 'Prazo', ['class' => 'control-label col-sm-3 col-md-offset-2']) !!}
                    <div class="col-sm-4 input-group">
                      <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                      {!! Form::select('term', ['à vista', '1+1'], !empty($decision) ? null : 0, ['class' => 'form-control']); !!}
                    </div>
                  </div>
             </div>
          </div>
          <div class="panel panel-warning">
            <div class="panel-heading">RH</div>
            <div class="panel-body">
                <div class="form-group">
                    {!! Form::label('admitted', 'Admitidos', ['class' => 'control-label col-sm-3 col-md-offset-2']) !!}
                    <div class="col-sm-4 input-group">
                      <div class="input-group-addon">N.</div>
                      {!! Form::text('admitted', !empty($decision) ? null : 0, ['class' => 'form-control']); !!}
                    </div>
                  </div>
      
                  <div class="form-group">
                    {!! Form::label('fired', 'Demitidos', ['class' => 'control-label col-sm-3 col-md-offset-2']) !!}
                    <div class="col-sm-4 input-group">
                      <div class="input-group-addon">N.</div>
                      {!! Form::text('fired', !empty($decision) ? null : 0, ['class' => 'form-control']); !!}
                    </div>
                  </div>
      
                  <div class="form-group">
                    {!! Form::label('salary', 'Salário', ['class' => 'control-label col-sm-3 col-md-offset-2']) !!}
                    <div class="col-sm-4 input-group">
                      <div class="input-group-addon">$</div>
                      {!! Form::text('salary', !empty($decision) ? null : 7, ['class' => 'form-control']); !!}
                    </div>
                  </div>
      
                  <div class="form-group">
                    {!! Form::label('training', 'Treinamento', ['class' => 'control-label col-sm-3 col-md-offset-2']) !!}
                    <div class="col-sm-4 input-group">
                      <div class="input-group-addon">%</div>
                      {!! Form::text('training', !empty($decision) ? null : 0, ['class' => 'form-control']); !!}
                    </div>
                  </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-success"> Salvar </button>
        </div>
      </div>
    </div>
  </div>
{{--
<div class="modal modal-default" id="decisao">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header modal-header-x">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">DECISÃO DA EMPRESA: RODADA 03</h4>
      </div>
      <div class="modal-body">
        <div class="panel panel-info">
          <div class="panel-heading">COMERCIAL</div>
            <div class="panel-body">
        
        
        
         
         
            </div>
          </div>
        </div>
        <div class="panel panel-info">
          <div class="panel-heading">COMERCIAL</div>
            <div class="panel-body">
          
          
           
           
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success"> Salvar </button>
      </div>
    </div>
  </div>
</div> --}}
@section('js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jQuery-rwdImageMaps/1.6/jquery.rwdImageMaps.min.js"></script>

<script>
    $(document).ready(function(e) {
        $('img[usemap]').rwdImageMaps();
    });
    $('#Praca').on('shown.bs.modal', function (e) {
      $('img[usemap]').rwdImageMaps();
    })
    function ShowPraca() {
      $('#Praca').modal('show');
    }
    function funcaoPropaganda() {
      $('#propaganda').modal();
    }
    function funcaoMaquinas() {
      $('#maquinas').modal();
    }
    function funcaoFinancas() {
	    $('#financas').modal();
    }
    function funcaoProducao() {
      $('#producao').modal();
    }
    function funcaoEmpresa() {
      $('#decisao').modal();
    }
    function teste() {
       alert('teste kjkjka');
    }
</script>
<script>
  $(function() {

    $("#decision").submit(function(e) {
      e.stopPropagation();

      var $bt = $(this).find(':submit');

      $bt.attr('disabled', 'disabled');
      $bt.val('Gravando...');
      $bt.css('cursor', 'progress');
    });

    $('[id*=adv_]').change(function(e) {
      if ($(this).val() == null) {
        $(this).val(0);
      }
    })

    if ($('#buy_small_machine').val() > 0) {
      $('#b_s_m_id').removeClass('hidden');
    }
    if ($('#buy_medium_machine').val() > 0) {
      $('#b_m_m_id').removeClass('hidden');
    }
    if ($('#buy_large_machine').val() > 0) {
      $('#b_l_m_id').removeClass('hidden');
    }

    $('#buy_small_machine, #buy_medium_machine, #buy_large_machine').change(function() {
      let val = $(this).val();
      let id = $(this).attr('id');
      if (val != 0) {
        if (id == 'buy_small_machine') {
          $('#b_s_m_id').removeClass('hidden');
        }
        if (id == 'buy_medium_machine') {
          $('#b_m_m_id').removeClass('hidden');
        }
        if (id == 'buy_large_machine') {
          $('#b_l_m_id').removeClass('hidden');
        }
      } else {
        if (id == 'buy_small_machine') {
          $('#b_s_m_id').addClass('hidden');
        }
        if (id == 'buy_medium_machine') {
          $('#b_m_m_id').addClass('hidden');
        }
        if (id == 'buy_large_machine') {
          $('#b_l_m_id').addClass('hidden');
        }
      }
    });
  })
</script>
@stop