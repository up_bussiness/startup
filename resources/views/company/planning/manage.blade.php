@extends('adminlte::page')

@section('title', 'Planejamento Estratégico')

@section('content_header')
    <h1>Planejamento Estratégico</h1>
@stop

@section('content_title')
	@if(auth()->user()->simulation->planning)
    <h3 class="box-title">{{ !empty($planning) ? 'Atualizar Planejamento Estratégico' : 'Novo Planejamento Estratégico' }}</h3>
  @else
		<h3 class="box-title">Informações</h3>
	@endif
@stop

@section('content')    

		@if(auth()->user()->simulation->planning)
			@include('company.planning._form')
		@else
			@include('company.planning._show')
		@endif
		
@stop