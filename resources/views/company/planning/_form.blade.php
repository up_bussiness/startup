@if(!empty($planning))
{!! Form::model($planning, ['route' => ['planning.update', $planning->id], 'method' => 'put']) !!}
@else
{!! Form::open(['route' => 'planning.store']) !!}
@endif

<div class="col-md-8 col-md-offset-2">

  <div class="form-group">
    {!! Form::label('slogan', 'Slogan: <small>(até 190 caracteres)</small>', ['class' => 'control-label'], false) !!}
    {!! Form::text('slogan', null, ['class' => 'form-control', 'required','maxlength' => 190]); !!}
  </div>

  <div class="form-group">
    {!! Form::label('mission', '1. Missão : É o propósito da organização, bem como sua amplitude de operações e responde às perguntas: qual é a razão de existir daquela organização? Qual é seu trabalho? Quem é seu público-alvo? A declaração de missão não deve ter mais do que um parágrafo e deve ser motivadora e inspiradora para todos os envolvidos com a organização. Também deve ser sucinta e clara. <small>(até 190 caracteres)</small>', ['class' => 'control-label'], false) !!}
    {!! Form::text('mission', $planning->mission ?? '&nbsp;', ['class' => 'form-control', 'required','maxlength' => 190]); !!}
  </div>

  <div class="form-group">
    {!! Form::label('view', '2. Visão : Como a organização quer estar daqui a 3 ou 5 anos? A elaboração da visão serve para inspirar e motivar o trabalho feito. <small>(até 190 caracteres)</small>', ['class' => 'control-label'], false) !!}
    {!! Form::text('view', $planning->view ?? '&nbsp;', ['class' => 'form-control', 'required','maxlength' => 190]); !!}
  </div>

  <div class="form-group">
    {!! Form::label('values', '3. Valores : São as crenças e paradigmas de conduta, que refletem a personalidade dos colaboradores e gestores da organização.', ['class' => 'control-label']) !!}
    {!! Form::textarea('values', $planning->values ?? '&nbsp;', ['class' => 'form-control', 'required', 'rows' => 3]); !!}
  </div>

  <div class="form-group">
    <p><b>
        <div align="justify">4. Análise do Ambiente Interno e Externo da Organização – Análise FOFA ou SWOT: em inglês, SWOT representa as iniciais das palavras Strenghs (forças), Weaknesses (fraquezas), Opportunities (oportunidades) e Threats (ameaças). A análise FOFA ou SWOT é uma ferramenta de gestão muito utilizada por empresas privadas como parte do planejamento estratégico dos negócios. Como o próprio nome já diz, a ideia central dessa análise é avaliar a organização e sua posição frente ao mercado onde está atuando: o ambiente externo à organização (oportunidades e ameaças) e o ambiente interno à organização (pontos fortes e pontos fracos):</div>
      </b></p>
    <div class="table-responsive"">
    <table class=" table table-bordered">
      <tr class="text-center">
        <td><b>Fraquezas:</b></td>
        <td><b>Forças:</b></td>
      </tr>
      <tr>
        <td>
          <ul>
            <li>O que pode ser melhorado?</li>
            <li>O que deve ser evitado?</li>
            <li>Que recursos relevantes faltam à minha organização?</li>
          </ul>
        </td>
        <td>
          <ul>
            <li>O que a minha organização faz bem?</li>
            <li>Que recursos relevantes estão disponíveis à organização?</li>
            <li>Quais as habilidades e competências que fortalecem minha organização?</li>
          </ul>
        </td>
      </tr>
      <tr class="text-center">
        <td><b>Ameaças:</b></td>
        <td><b>Oportunidades:</b></td>
      </tr>
      <tr>
        <td>
          <ul>
            <li>Que obstáculos externos a minha organização enfrenta?</li>
            <li>Há algum concorrente ou desafio a ser superado?</li>
          </ul>
        </td>
        <td>
          <ul>
            <li>Há oportunidades para serem aproveitadas?</li>
            <li>Quais são as tendências que me interessam?</li>
          </ul>
        </td>
      </tr>
      </table>
    </div>
    {!! Form::label('weaknesses', 'Fraquezas:', ['class' => 'control-label']) !!}
    {!! Form::textarea('weaknesses', $planning->weaknesses ?? '&nbsp;', ['class' => 'form-control', 'required', 'rows' => 3]); !!}
  </div>

  <div class="form-group">
    {!! Form::label('threats', 'Ameaças:', ['class' => 'control-label']) !!}
    {!! Form::textarea('threats', $planning->threats ?? '&nbsp;', ['class' => 'form-control', 'required', 'rows' => 3]); !!}
  </div>

  <div class="form-group">
    {!! Form::label('forces', 'Forças:', ['class' => 'control-label']) !!}
    {!! Form::textarea('forces', $planning->forces ?? '&nbsp;', ['class' => 'form-control', 'required', 'rows' => 3]); !!}
  </div>

  <div class="form-group">
    {!! Form::label('opportunities', 'Oportunidades:', ['class' => 'control-label']) !!}
    {!! Form::textarea('opportunities', $planning->opportunities ?? '&nbsp;', ['class' => 'form-control', 'required', 'rows' => 3]); !!}
  </div>

  <div class="form-group">
    {!! Form::label('goals_general', '5.1 Objetivos Gerais – O que se deseja alcançar a curto, médio e longo prazo, e deve estar alinhado com a visão da empresa:', ['class' => 'control-label']) !!}
    {!! Form::textarea('goals_general', $planning->goals_general ?? '&nbsp;', ['class' => 'form-control', 'required', 'rows' => 3]); !!}
  </div>

  <div class="form-group">
    {!! Form::label('goals_specific', '5.2 Objetivos Específicos – O que se deseja alcançar para chegar ao objetivo geral:', ['class' => 'control-label']) !!}
    {!! Form::textarea('goals_specific', $planning->goals_specific ?? '&nbsp;', ['class' => 'form-control', 'required', 'rows' => 3]); !!}
  </div>

  <div class="form-group">
    {!! Form::label('goals', '5.3 Objetivos Estratégicos – A visão se desdobra em alvos ou fins que o administrador deseja atingir; os objetivos estratégicos devem ser quantitativos e limitados por um prazo:', ['class' => 'control-label']) !!}
    {!! Form::textarea('goals', $planning->goals ?? '&nbsp;', ['class' => 'form-control', 'required', 'rows' => 3]); !!}
  </div>

  <div class="col-md-offset-5 col-md-3 text-center">
    {!! Form::submit(!empty($planning) ? 'Atualizar' : 'Salvar', ['class' => 'btn btn-success']); !!}
  </div>

  {!! Form::close() !!}

</div>