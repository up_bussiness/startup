@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Planejamento Estratégico</h1>
@stop

@section('content_title')
  <h3 class="box-title"><i class="fa fa-users"></i>  {{ $planning->user->name }}</h3>  
@stop

@section('content')
<div class="row">
  <div class="col-md-10 col-md-offset-1">
    <p><b>Nome Fantasia da Empresa :</b> {{$planning->user->name}}</p>
    <p><b>Slogan :</b> {{$planning->slogan}}</p>
    <p><b>1. Missão : É o propósito da organização, bem como sua amplitude de operações e responde às perguntas: qual é a razão de existir daquela organização? Qual é seu trabalho? Quem é seu público-alvo? A declaração de missão não deve ter mais do que um parágrafo e deve ser motivadora e inspiradora para todos os envolvidos com a organização. Também deve ser sucinta e clara.</b></p>
    <p>{{$planning->mission}}</p>
    <p><b>2. Visão : Como a organização quer estar daqui a 3 ou 5 anos? A elaboração da visão serve para inspirar e motivar o trabalho feito.</b></p>
    <p>{{$planning->view}}</p>
    <p><b>3. Valores : São as crenças e paradigmas de conduta, que refletem a personalidade dos colaboradores e gestores da organização.</b></p>
    <p>{{$planning->values}}</p>
    <p><b>4. Análise do Ambiente Interno e Externo da Organização – Análise FOFA ou SOWT - Em inglês, SWOT representa as iniciais das palavras Strenghs (forças), Weaknesses (fraquezas), Opportunities (oportunidades) e Threats (ameaças). A análise FOFA é uma ferramenta de gestão muito utilizada por empresas privadas como parte do planejamento estratégico dos negócios. Como o próprio nome já diz, a idéia central da análise é avaliar a organização e sua posição frente ao mercado onde está atuando: o ambiente externo à organização (oportunidades e ameaças) e o ambiente interno à organização (pontos fortes e pontos fracos):</b></p>
    <div class="table-responsive"">
      <table class="table table-bordered">
        <tr class="text-center">
          <td><b>Fraquezas:</b></td>
          <td><b>Forças:</b></td>
        </tr>
        <tr>
          <td>
            <ul>
              <li>O que pode ser,melhorado?</li>
              <li>O que deve ser,evitado?</li>
              <li>Que recursos relevantes faltam à minha,organização?</li>
            </ul>
          </td>
          <td>
            <ul>
              <li>O que a minha,organização faz bem?</li>
              <li>Que recursos,relevantes estão disponíveis à organização?</li>
              <li>Quais as,habilidades e competências que fortalecem minha organização?</li>
            </ul>
          </td>
        </tr>          
        <tr class="text-center">
          <td><b>Ameaças:</b></td>
          <td><b>Oportunidades:</b></td>
        </tr>
        <tr>
          <td>
            <ul>
              <li>Que obstáculos,externos a minha organização enfrenta?</li>
              <li>Há algum,concorrente ou desafio a ser superado?</li>
            </ul>
          </td>
          <td>
            <ul>                
              <li>Há oportunidades,para serem aproveitadas?</li>
              <li>Quais são as tendências que me interessam?</li>
            </ul>
          </td>
        </tr>      
      </table>
    </div>
    <p><b>5. Objetivos Estratégicos – A visão se desdobra em alvos ou fins que o administrador deseja atingir; os objetivos estratégicos devem ser quantitativos e limitados por um prazo.:</b></p>
    <p>{{$planning->goals}}</p>
  </div>
</div>

@stop