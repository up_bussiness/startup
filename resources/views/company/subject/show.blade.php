@extends('adminlte::page')

@section('title', 'Informativo Econômico')

@section('content_header')
    <h1>{{ $newspapers[0]->turn->simulation->name }}</h1>
@stop

@section('content_title')
    <h3 class="box-title"><i class="fa fa-newspaper-o"></i> Informativo - Rodada {{ $newspapers[0]->turn->month }}</h3>
    <div class="pull-right">
      <a href="{{route('company')}}" class="btn btn-xs btn-default">VOLTAR</a>
    </div>
@stop

@section('content')
<div class="row">
<div class="col-md-12">
  <div class="paper">
    <div class="titleDiv">
      <div class="title">Informativo</div>
    </div>
    <hr class="topHr">
    <hr class="bottomHr">
    <div class="aboutTitle">
      <div class="vol">VOL# {{ randomDigits(4) }}</div>
      <div class="date">{{ $newspapers[0]->created_at->format('d/m/Y') }}</div>
      <div class="price">$ 50,00</div>
    </div>
    <hr class="dateHr">
    @foreach($newspapers as $new)
      <div class="col-md-12">
        <hr>
        <h4 class="storyTitle storyTitleX5">{{$new->title}}</h4>
        <p class="by byX5">Temática: {{$new->theme->name}}</p>
        <p class="story storyX5">
          {{$new->description}}
        </p>
      </div>
    @endforeach
    <hr class="dateHr" style="margin-bottom: 10px;">
    <div class="content-news">
      <div class="item" id="item4">
        <h4 class="storyTitle storyTitleX4">Fornecedores ($)</h4>
        <p class="by byX4">Fonte: BikeFactory</p>
        <p class="story storyX4">
          <div class="table-responsive">
            <table class="table">
              <tbody class="text-right">
                <tr>
                  <td class="text-left">Quadro</td>
                  <td class="economy-emergency-rma">{{ $dc->raw_material_a }}</td>
                </tr>
                <tr>
                  <td class="text-left">Kit 1</td>
                  <td class="economy-emergency-rmb">{{ $dc->raw_material_b }}</td>
                </tr>
                <tr>
                  <td class="text-left">Kit 2</td>
                  <td class="economy-emergency-rmc">{{ $dc->raw_material_c }}</td>
                </tr>
                <tr>
                  <td class="text-left">Linha de Produção - Pequena</td>
                  <td class="economy-machine-s">{{ numberDot($dc->lp_small) }}</td>
                </tr>
                <tr>
                  <td class="text-left">Linha de Produção - Média</td>
                  <td class="economy-machine-m">{{ numberDot($dc->lp_medium) }}</td>
                </tr>
                <tr>
                  <td class="text-left">Linha de Produção - Grande</td>
                  <td class="economy-machine-l">{{ numberDot($dc->lp_large) }}</td>
                </tr>
              </tbody>
            </table>
          </div>
        </p>
        
      </div>
      <div class="item" id="item5">
        
        <h4 class="storyTitle storyTitleX5">Propaganda ($)</h4>
        <p class="by byX5">Fonte: LGPropag</p>
        <p class="story storyX5">
          <div class="table-responsive">
            <table class="table">
              <tbody class="text-right">
                <tr>
                  <td class="text-left">Radio</td>
                  <td>{{ numberDot($dc->adv_radio) }}</td>
                </tr>
                <tr>
                  <td class="text-left">Jornal</td>
                  <td>{{ numberDot($dc->adv_journal) }}</td>
                </tr>
                <tr>
                  <td class="text-left">Mídias Sociais</td>
                  <td>{{ numberDot($dc->adv_social) }}</td>
                </tr>
                <tr>
                  <td class="text-left">Outdoor</td>
                  <td>{{ numberDot($dc->adv_outdoor) }}</td>
                </tr>
                <tr>
                  <td class="text-left">TV</td>
                  <td>{{ numberDot($dc->adv_tv) }}</td>
                </tr>
                <tr>
                  <td colspan="100">.</td>
                </tr>
              </tbody>
            </table>
          </div>
        </p>
        
      </div>
      <div class="item" id="item6">
        
        <h4 class="storyTitle storyTitleX6">Indicadores (%)</h4>
        <p class="by byX6">Fonte: InfoMoney</p>
        <p class="story storyX6">
          <div class="table-responsive">
            <table class="table">
              <tbody class="text-right">
                <tr>
                  <td class="text-left">Macro setor</td>
                  <td class="economy-macro-industry">{{ $dc->macro_industry }}</td>
                </tr>
                <tr>
                  <td class="text-left">Prejuízo na venda de máquinas</td>
                  <td class="economy-macro-industry">{{ $dc->loss_on_sale }}</td>
                </tr>
                <tr>
                  <td class="text-left">Juros dos fornecedores</td>
                  <td class="economy-interest-providers">{{ $dc->interest_providers }}</td>
                </tr>
                <tr>
                  <td class="text-left">Taxa básica de juros</td>
                  <td class="economy-bir">{{ $dc->bir }}</td>
                </tr>
                <tr>
                  <td class="text-left">Importação para o período</td>
                  <td class="economy-import-products">{{ $dc->import }}</td>
                </tr>
                <tr>
                  <td class="text-left">Impostos</td>
                  <td class="economy-import-products">{{ $dc->income_tax }}</td>
                </tr>
              </tbody>
            </table>
          </div>
        </p>
        
      </div>
    </div>

    <hr class="dateHr">
  </div>
</div>
</div>
@stop

@section('js')
<script src="{{ asset('js/newspaper.js') }}"></script>
@stop