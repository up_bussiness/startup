<div class="row">
  <div class="col-md-12">
    <div class="box box-solid">
      <div class="box-header with-border text-center">
        <h3 class="box-title">Este relatório é baseado nas soluções Fortes</h3>
      </div>
      <!-- /.box-header -->
      <div class="box-body text-center">
        <div class="row">
          <div class="col-md-2 col-md-offset-5">
            <img src="{{ asset('images/fortes.gif') }}" class="img-responsive" style="display: inline; width: 150px;">
          </div>
        </div>
      </div>
      <!-- /.box-body -->
    </div>
    <!-- /.box -->
  </div>
  <!-- /.col -->
</div>