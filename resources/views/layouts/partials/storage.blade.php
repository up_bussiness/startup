<script>
(function() {
	var isCoordinator = window.location.href.indexOf('coordinator') !== -1;
	var companyID = "{{ !empty($key) ? $key : '' }}";

	if (isCoordinator) {
		if (companyID != '') {
			localStorage.setItem('COMPANY', companyID);
		} else {
			localStorage.setItem('COMPANY', '');
		}
	}
})();
</script>