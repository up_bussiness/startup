<div class="user-panel" style="height:185px">
  <div class="text-center">
  	@if(auth()->user()->logo)
  		<img src="{{ asset('storage/'.LocalUploadUUID().'/'.auth()->user()->logo) }}" class="img-circle" alt="User Image" style="border: 2px solid;height:120px;width:120px;padding: 4px; ;color:#f3c425">
  	@else
    	<img src="{{ Avatar::create(auth()->user()->name) }}" class="img-circle" alt="User Image" style="border: 2px solid;padding: 4px; ;color:#f3c425;">
    @endif
  </div>
  <div class="text-center" style="color: white">
    <p>
      @if(auth()->user()->role == 'admin')
        Administrador
      @else
        {{ auth()->user()->name }}
      @endif
    </p>
    <p>
      @if(auth()->user()->role == 'company')
        {{ auth()->user()->simulation->name }}
      @else
        {{ $instituitionVC->slug }}
      @endif
    </p>
  </div>
</div>