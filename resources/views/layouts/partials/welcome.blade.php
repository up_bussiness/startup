<style>
  #tarjamsg {
    width: auto;
    display: inline-block;
  }

  @media (max-width: 500px) {
    #tarjamsg {
      width: auto;
      display: inline-block;
    }
  }
</style>

@if ( $simulation->turns->count() <= 2 ) <div id="tarjamsg" class="text-center">
  <div class="alert alert" style="background-color: #0077ee; color:white">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button>
    <p>Olá, é incrível que vocês estejam aqui, nossa empresa conta muito com a experiência profissional de vocês.</p>
    <p>Para que possamos produzir bem e estarmos buscando a liderança de mercado é de suma importância que os senhores conheçam nossa empresa, para isso, estamos disponibilizando alguns relatórios para que, após análise de vocês, possamos juntos elaborar do nosso planejamento estratégico para os próximos dois anos.</p>
    <p>Com isso, desejamos sucesso neste novo desafio.</p>
    <p>Juntos seremos mais fortes.</p>
  </div>
  </div>
  @endif