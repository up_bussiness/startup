@if ( Session::has('flash_message') )
  <div class="alert {{ Session::has('flash_type') ? Session::get('flash_type') : 'alert-success'}}">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
    <p>{!! Session::get('flash_message') !!}</p>
  </div>
@endif

@if (count($errors) > 0)
  <div class="alert alert-danger alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button>
    <ul>
      @foreach ($errors->all() as $error)
        <li>{!! $error !!}</li>
      @endforeach
    </ul>
  </div>
@endif