<style>
  #tarjamsg {
    width: auto;
    display: inline-block;
  }

  @media (max-width: 500px) {
    #tarjamsg {
      width: auto;
      display: inline-block;
    }
  }
</style>

<div class="alert alert" style="background-color: #0077ee; color:white">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
  <p>Olá, é muito bom saber que sua empresa já fez a configuração inicial para o jogo, entretanto nem todas as equipe concluíram esses passos.</p>
  <p>Neste momento é necessário aguardar que todas as empresas façam sua configuração.</p>
  <p>Bom jogo!!!!</p>
</div>
</div>
