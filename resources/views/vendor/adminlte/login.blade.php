@extends('adminlte::master')

@section('adminlte_css')
    <link rel="stylesheet" href="{{ asset('vendor/adminlte/plugins/iCheck/square/blue.css') }}">
    <link rel="stylesheet" href="{{ asset('css/login.css')}} ">
    @yield('css')
@stop

@section('body_class', 'login-page')

@section('body')
    <div class="hero">
        <div class="wrapper">
            <div class="row" style="padding-top: 40px;">
                    <div class="text-center"><img src="{{asset('images/logo.png')}}" class="img-responsive" style="width:360px; display: inline;"></div>
                    <div class="lg-login-box">
                        <p class="lg-login-box-msg text-center">{{__('labels.login_msg')}}</p>
                        <form action="{{ url(config('adminlte.login_url', 'login')) }}" method="post">
                            {!! csrf_field() !!}

                            <div class="form-group has-feedback {{ $errors->has('email') ? 'has-error' : '' }}">
                                <input type="email" name="email" class="form-control" value="{{ old('email') }}"
                                       placeholder="{{ trans('adminlte::adminlte.email') }}">
                                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group has-feedback {{ $errors->has('password') ? 'has-error' : '' }}">
                                <input type="password" name="password" class="form-control"
                                       placeholder="{{ trans('adminlte::adminlte.password') }}">
                                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="row">
                                <div class="col-xs-8">
                                    <div class="checkbox icheck">
                                        <label>
                                            <input type="checkbox" name="remember"> {{ trans('adminlte::adminlte.remember_me') }}
                                        </label>
                                    </div>
                                </div>
                                <!-- /.col -->
                                <div class="col-xs-4">
                                    <button type="submit"
                                            class="btn btn-primary btn-block btn-flat">{{ trans('adminlte::adminlte.sign_in') }}</button>
                                </div>
                                <!-- /.col -->
                            </div>
                        </form>
                        <div class="lg-versao-cl" style="font-size:10px;">
                            <p class="lg-versao-box text-right">Versão: 3.03.002</p>
                        </div>
                    </div>
                </div>
        </div>
      <div class="parallax-layer layer-6"></div>
      <div class="parallax-layer layer-5"></div>
      <div class="parallax-layer layer-4"></div>
      <div class="parallax-layer bike-1"></div>
      <div class="parallax-layer bike-2"></div>
      <div class="parallax-layer layer-3"></div>
      <div class="parallax-layer layer-2"></div>
      <div class="parallax-layer layer-1"></div>
    </div>

@stop

@section('adminlte_js')
    <script src="{{ asset('vendor/adminlte/plugins/iCheck/icheck.min.js') }}"></script>
    <script>
        $(function () {
            $('input').iCheck({
                checkboxClass: 'icheckbox_square-blue',
                radioClass: 'iradio_square-blue',
                increaseArea: '20%' // optional
            });
        });
    </script>
    @yield('js')
@stop
