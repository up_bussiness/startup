@extends('adminlte::master')

@section('adminlte_css')
<link rel="stylesheet" href="{{ asset('vendor/adminlte/css/auth.css') }}">
@yield('css')
@stop

@section('body_class', 'login-page')

<body style="background: #debfa6;">
    {{-- @section('body') --}}
    <div class="login-box">
        <div class="login-logo">
            <a href="{{ url(config('adminlte.dashboard_url', 'home')) }}">{!! config('adminlte.logo', '<b>Admin</b>LTE') !!}</a>
        </div>
        <!-- /.login-logo -->
        <div align="center">
            <img src="{{asset('images/logo.png')}}" style="width:360px;"><br />
        </div>
        <br />
        <div class="login-box-body" style="background: #ffa262;">
            <p class="login-box-msg">{{ trans('adminlte::adminlte.password_reset_message') }}</p>
            @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
            @endif
            @if(session('error'))
            <div class="alert alert-warning">
                {{session('error')}}
            </div>
            @endif
            {{-- <form action="{{ url(config('adminlte.password_email_url', 'password/email')) }}" method="post"> --}}
            <form action="{{ route('enviarsenha') }}" method="post">
                {!! csrf_field() !!}

                <div class="form-group has-feedback {{ $errors->has('id') ? 'has-error' : '' }}">
                    <input type="text" name="id" class="form-control" value="{{ old('id') }}" placeholder="ID">
                    <span class="glyphicon glyphicon-home form-control-feedback"></span>
                    @if ($errors->has('id'))
                    <span class="help-block">
                        <strong>{{ $errors->first('id') }}</strong>
                    </span>
                    @endif
                </div>
                <div class="form-group has-feedback {{ $errors->has('email') ? 'has-error' : '' }}">
                    <input type="email" name="email" class="form-control" value="{{ $email or old('email') }}" placeholder="{{ trans('adminlte::adminlte.email') }}">
                    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                    @if ($errors->has('email'))
                    <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                    @endif
                </div>
                <button type="submit" class="btn btn-primary btn-block btn-flat">{{ trans('adminlte::adminlte.send_password_reset_link') }}</button>
            </form>
        </div>
        <br><br><br>
        <div align="center">
            <a href="http://{{ request()->getHost() }}" class="btn btn-default">Acessar Up Business Game</a>
        </div>
        <!-- /.login-box-body -->
    </div><!-- /.login-box -->
    </div>
    {{-- @stop --}}
</body>

@section('adminlte_js')
@yield('js')
@stop