@extends('adminlte::master')

@section('adminlte_css')
<link rel="stylesheet" href="{{ asset('vendor/adminlte/plugins/iCheck/square/blue.css') }}">
<link rel="stylesheet" href="{{ asset('css/login.css')}} ">
@yield('css')
@stop

@section('body_class', 'login-page')

@section('body')
<div class="hero">
    <div class="wrapper">
        <div class="row" style="padding-top: 40px;">
            <br />
            <div class="text-center"><img src="{{asset('images/logo.png')}}" class="img-responsive" style="width:360px; display: inline;"></div>
            <br />


            <div class="lg-login-box">
                <p class="lg-login-box-msg text-center">Up Business Game é uma ferramenta de simulação de mercado que proporciona aos seus usuários a oportunidade de aprimorar seus conhecimentos em gestão.</p>
                @if(session('error'))
                <div class="alert alert-warning">
                    {{session('error')}}
                </div>
                @endif
                @if(isset($errors) && $errors->any())
                <div class="alert alert-warning">
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{$error}}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
                <form action="{{ route('autenticando') }}" method="post">
                    {!! csrf_field() !!}

                    <div class="form-group has-feedback {{ $errors->has('id') ? 'has-error' : '' }}">
                        <input type="text" name="id" class="form-control" value="{{ old('id') }}" placeholder="ID">
                        <span class="glyphicon glyphicon-home form-control-feedback"></span>
                        @if ($errors->has('id'))
                        <span class="help-block">
                            <strong>{{ $errors->first('id') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group has-feedback {{ $errors->has('email') ? 'has-error' : '' }}">
                        <input type="email" name="email" class="form-control" value="{{ old('email') }}" placeholder="Usuário">
                        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                        @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group has-feedback {{ $errors->has('password') ? 'has-error' : '' }}">
                        <input type="password" name="password" class="form-control" placeholder="{{ trans('adminlte::adminlte.password') }}">
                        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                        @if ($errors->has('password'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="row">
                        <div class="col-xs-8">
                            {{-- <div class="checkbox icheck">
                                <label>
                                    <input type="checkbox" name="remember"> {{ trans('adminlte::adminlte.remember_me') }}
                            </label>
                        </div> --}}
                        <a class="btn btn-link" href="{{ route('password.request') }}">
                            {{ __('Esqueceu sua senha?') }}
                        </a>
                    </div>
                    <!-- /.col -->
                    <div class="col-xs-4">
                        {{-- <button type="submit" class="btn btn-primary btn-block btn-flat">{{ trans('adminlte::adminlte.sign_in') }}</button>--}}
                        <button type="submit" class="btn btn-primary" style="border-radius: 5px;border-color: #000000;border-width: medium;color: white;font-weight: bold;background-color: #FB6600;"> Jogar </button>
                    </div>
                    <!-- /.col -->
            </div>
            </form>

        </div>
    </div>
</div>
<div class="parallax-layer layer-6"></div>
<div class="parallax-layer layer-5"></div>
<div class="parallax-layer layer-4"></div>
<div class="parallax-layer bike-1"></div>
<div class="parallax-layer bike-2"></div>
<div class="parallax-layer layer-3"></div>
<div class="parallax-layer layer-2"></div>
<div class="parallax-layer layer-1"></div>
</div>

@stop

@section('adminlte_js')
<script src="{{ asset('vendor/adminlte/plugins/iCheck/icheck.min.js') }}"></script>
<script>
    $(function() {
        $('input').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            radioClass: 'iradio_square-blue',
            increaseArea: '20%' // optional
        });
    });
</script>
@yield('js')
@stop