@extends('adminlte::page')

@section('title', 'Gerenciamento de Professores/Licenças')

@section('content_header')
<h1>Dashboard</h1>
@stop

@section('content_title')
<h3 class="box-title"><i class="fa fa-gear"></i> Gerenciamento</h3>
@if($instituitionVC->license - $licenses > 0)
<a href="#" data-toggle="modal" data-target="#coordinator" class="btn btn-success pull-right btn-xs">NOVO PROFESSOR</a>
@endif
@stop

@section('content')

<div class="col-md-4 col-md-offset-2 col-sm-6 col-xs-12">
	<div class="info-box">
		<span class="info-box-icon bg-aqua"><i class="fa fa-ticket"></i></span>

		<div class="info-box-content">
			<span class="info-box-text">Licenças em Uso</span>
			<span class="info-box-number">{{ $licenses }}</span>
		</div>
		<!-- /.info-box-content -->
	</div>
	<!-- /.info-box -->
</div>
<div class="col-md-4 col-sm-6 col-xs-12">
	<div class="info-box">
		<span class="info-box-icon bg-aqua"><i class="fa fa-cubes"></i></span>

		<div class="info-box-content">
			<span class="info-box-text">Licenças Restantes</span>
			<span class="info-box-number">{{ $instituitionVC->license - $licenses }}</span>
		</div>
		<!-- /.info-box-content -->
	</div>
	<!-- /.info-box -->
</div>

<div class="col-md-8 col-md-offset-2">
	<div class="table-responsive">
		<table class="table  table-bordered">
			<thead>
				<tr>
					<th>Nome</th>
					<th>Email</th>
					<th class="table-collapse">Licenças</th>
					<th class="text-center">Ações</th>
				</tr>
			</thead>
			<tbody>
				@forelse($coordinators as $c)
				<tr>
					<td>{{$c->name}}</td>
					<td>{{$c->email}}</td>
					<td class="text-center"><b>{{$c->license - $c->simulations->count()}} / {{$c->license}}</b></td>
					<td class="table-collapse">
						<a href="{{ route('admin.edit', $c->id) }}" class="btn btn-info btn-xs"><i class="fa fa-pencil"></i> Editar</a>
						@if ($c->simulations->count() < 1) <a href="{{ route('admin.destroy', $c->id) }}" class="btn btn-danger btn-xs" onclick="return confirm('Tem certeza que deseja excluír este coordenador?');"><i class="fa fa-trash"></i> Deletar</a>
							@endif
					</td>
				</tr>
				@empty
				<tr>
					<td colspan="100" class="text-center">
						<i>Você ainda não possui nenhum professor criado.</i>
					</td>
				</tr>
				@endforelse
			</tbody>
		</table>
	</div>
</div>
@if($instituitionVC->license - $licenses > 0)
@include('admin._new')
@endif
@stop