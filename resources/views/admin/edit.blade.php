@extends('adminlte::page')

@section('title', 'Gerenciamento de Professores/Licenças')

@section('content_header')
    <h1>Professor</h1>
@stop

@section('content_title')
    <h3 class="box-title"><i class="fa fa-pencil"></i> Edição</h3>
    <a href="{{ route('admin') }}" class="btn btn-default pull-right btn-xs"><i class="fa fa-chevron-circle-left"></i> Voltar</a>
@stop

@section('content')
    <div class="col-md-6 col-md-offset-3">
    	{!! Form::model($coordinator, ['route' => 'admin.update', 'class'=>'form-horizontal', 'method' => 'PUT']) !!}

	    	<div class="form-group">
	    	  {!! Form::label('name', 'Nome', ['class' => 'control-label col-sm-3']) !!}
	    	  <div class="col-sm-7 input-group">
	    	    <div class="input-group-addon">...</div>
	    	    {!! Form::text('name', null, ['class' => 'form-control', 'required']); !!}
	    	  </div>
	    	</div>

	    	<div class="form-group">
	    	  {!! Form::label('email', 'Email', ['class' => 'control-label col-sm-3']) !!}
	    	  <div class="col-sm-7 input-group">
	    	    <div class="input-group-addon"><i class="fa fa-envelope"></i></div>
	    	    {!! Form::email('email', null, ['class' => 'form-control', 'required']); !!}
	    	  </div>
	    	</div>

	    	<div class="form-group">
	    	  {!! Form::label('license', 'Licença', ['class' => 'control-label col-sm-3']) !!}
	    	  <div class="col-sm-7 input-group">
	    	    <div class="input-group-addon"><i class="fa fa-ticket"></i></div>
	    	    {!! Form::selectRange('license', 1, (($instituitionVC->license - $licenses) + $coordinator->license), null, ['class' => 'form-control', 'required']); !!}
	    	  </div>
	    	</div>

	    	<div class="form-group">
	    	  {!! Form::label('password', 'Senha', ['class' => 'control-label col-sm-3']) !!}
	    	  <div class="col-sm-7 input-group">
	    	    <div class="input-group-addon"><i class="fa fa-lock"></i></div>
	    	    {!! Form::password('password', ['class' => 'form-control']); !!}
	    	  </div>
	    	</div>

	    	<p class="text-center"><b>Senha atual: {{ $coordinator->password_plain }}</b></p>

	    	<div class="text-center">
    			{!! Form::submit('Atualizar', ['class' => 'btn btn-success']); !!}
			</div>

			{!! Form::hidden('coordinator_id', $coordinator->id) !!}

		{!! Form::close() !!}

		</div>

		<div class="col-md-6 col-md-offset-3">

			<hr>

			<div class="table-responsive">
				<table class="table  table-bordered">
				  <thead>
				    <tr>
				      <th>Simulação</th>
				      <th class="table-collapse">Rodadas</th>
				      <th class="text-center">Ações</th>
				    </tr>
				  </thead>
				  <tbody>
				    @forelse($coordinator->simulations as $simulation)
				    <tr>
				      	<td>{{$simulation->name}}</td>
				      	<td class="text-center">{{$simulation->turns->count()}}</td>
				        <td class="table-collapse text-center">
				        	@if ($simulation->turns->count() <= 2)
				        	{{ Form::open(['route' => ['admin.simulation.destroy', $simulation->id], 'method' => 'delete', 'style'=>'display: inline']) }}
				        	    <button type="submit" onclick="return confirm('Tem certeza que deseja excluir esta simulação?');" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i> Deletar</button>
				        	{{ Form::close() }}
				        	@else
								-
				        	@endif
				        </td>
				    </tr>
				    @empty
				    <tr>
				      <td colspan="100" class="text-center">
				        <i>O professor ainda não possui nenhuma simulação criada.</i>
				      </td>
				    </tr>
				    @endforelse
				  </tbody>
				</table>
			</div>
		</div>
@stop
