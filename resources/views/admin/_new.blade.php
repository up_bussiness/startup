<div class="modal fade" id="coordinator" tabindex="-1" role="dialog" aria-labelledby="coordinatorLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header bg-light-blue">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-close"></i></button>
				<h4 class="modal-title text-bold" id="coordinatorLabel">Cadastrar professor</h4>
			</div>
			<div class="modal-body">
			    {!! Form::open(['route' => 'admin.create', 'class'=>'form-horizontal']) !!}

		    	<div class="form-group">
		    	  {!! Form::label('name', 'Nome', ['class' => 'control-label col-sm-3']) !!}
		    	  <div class="col-sm-7 input-group">
		    	    <div class="input-group-addon">...</div>
		    	    {!! Form::text('name', null, ['class' => 'form-control', 'required']); !!}
		    	  </div>
		    	</div>

		    	<div class="form-group">
		    	  {!! Form::label('email', 'Email', ['class' => 'control-label col-sm-3']) !!}
		    	  <div class="col-sm-7 input-group">
		    	    <div class="input-group-addon"><i class="fa fa-envelope"></i></div>
		    	    {!! Form::email('email', null, ['class' => 'form-control', 'required']); !!}
		    	  </div>
		    	</div>

		    	<div class="form-group">
		    	  {!! Form::label('password', 'Senha', ['class' => 'control-label col-sm-3']) !!}
		    	  <div class="col-sm-7 input-group">
		    	    <div class="input-group-addon"><i class="fa fa-lock"></i></div>
		    	    {!! Form::password('password', ['class' => 'form-control', 'required']); !!}
		    	  </div>
		    	</div>

		    	<div class="form-group">
		    	  {!! Form::label('license', 'Licença', ['class' => 'control-label col-sm-3']) !!}
		    	  <div class="col-sm-7 input-group">
		    	    <div class="input-group-addon"><i class="fa fa-ticket"></i></div>
		    	    {!! Form::selectRange('license', 1, ($instituitionVC->license - $licenses), null, ['class' => 'form-control', 'required']); !!}
		    	  </div>
		    	</div>

			</div>
			<div class="modal-footer">
				{!! Form::submit('Criar', ['class' => 'btn btn-success']); !!}
				<button type="button" class="btn btn-default" data-dismiss="modal">Voltar</button>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>
