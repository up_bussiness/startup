<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'version'   => 'Versão',
    'logged'    => 'Você está logado!',
    'dashboard' => 'Painel de controle',
    'register'  => 'Registar',
    'home'      => 'Início',
    'email'     => 'Email',
    'management' => 'Gerenciamento',
    'coordinator' => 'COORDENADOR',
    'turn' => 'RODADA',
    'start' => 'INÍCIO',
    'end' => 'FIM',
    'login_msg' => 'Logistics Game é uma ferramenta de simulação de mercado que proporciona aos seus usuários a oportunidade de testar seus conhecimentos em gestão.',
    'closing' => 'Encerramento',
    'status' => 'Situação',
    'communicatory' => 'Informativo',
    'decision' => 'Decisão',
    'operational' => 'Mercadológico',
    'cashflow' => 'Fluxo de caixa',
    'annual_income_statement' => 'DRE',
    'balance_sheet' => 'BP',
    'ranking' => 'Classificação',
    'closed' => 'ENCERRADA',
    'nameCompany' => 'Nome da Empresa',
    'configured' => 'Configurada',
    'simulationConfiguration' => 'Configuração das Empresas',
    'listConfiguration' => 'Situação da Configuração das Empresas',

];
