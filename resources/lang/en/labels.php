<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'version' => 'Version',
    'logged' => 'You are logged in!',
    'dashboard' => 'Dashboard',
    'register' => 'Register',
    'home' => 'Home',
    'email'     => 'Email',
    'management' => 'Management',
    'coordinator' => 'COORDINATOR',
    'turn' => 'TURN',
    'start' => 'START',
    'end' => 'END',
    'login_msg' => 'Logistics Game is a market simulation tool that provides its users with the opportunity to test their management knowledge.',
    'closing' => 'Closing',
    'status' => 'Status',
    'communicatory' => 'Communicatory',
    'decision' => 'Decision',
    'operational' => 'Operational',
    'cashflow' => 'Cash Flow',
    'annual_income_statement' => 'Annual Report',
    'balance_sheet' => 'Balance Sheet',
    'ranking' => 'Ranking',
    'closed' => 'CLOSED',
    'nameCompany' => 'Name of Company',
    'configured' => 'Configured',
    'simulationConfiguration' => 'Company Configuration',
    'listConfiguration' => 'Company Configuration Status',
];
