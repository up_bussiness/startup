let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/js')
   .js('resources/assets/js/intro.js', 'public/js')
   .js('resources/assets/js/jquery-maskmoney.js', 'public/js')
   .js('resources/assets/js/jquery-inputmask.js', 'public/js')
   .js('resources/assets/js/mask.js', 'public/js')
   .js('resources/assets/js/newspaper.js', 'public/js')
   .js('resources/assets/js/util.js', 'public/js')
   .sass('resources/assets/sass/app.scss', 'public/css')
   .sass('resources/assets/sass/login.scss', 'public/css')
   .sass('resources/assets/sass/setup.scss', 'public/css')
   .sass('resources/assets/sass/newspaper.scss', 'public/css')
   .copyDirectory('resources/assets/images', 'public/images')
	 .copyDirectory('resources/assets/favicon', 'public/favicon')
	 .browserSync({
			proxy: 'localhost',
      port: 8000,
      files: [
          'public/css/{*,**/*}.css',
          'public/js/{*,**/*}.js',
      ]
	 });
