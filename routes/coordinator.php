<?php

Route::resource('simulation', 'Coordinator\SimulationController');
Route::get('decision/{id}', 'Coordinator\DecisionCoordinatorController@create')->name('decision.create');
Route::post('decision', 'Coordinator\DecisionCoordinatorController@store')->name('decision.store');
Route::post('simulation/decision', 'Coordinator\DecisionCoordinatorController@index')->name('decision.index');

Route::get('simulation/turn/{id}', 'Coordinator\TurnController@create')->name('turn.create');
Route::get('simulation/turn/restart/{id}', 'Coordinator\TurnController@restart')->name('turn.restart');

Route::get('simulation/{simulation}/{id}', 'Coordinator\SimulationController@showTurn')->name('simulation.turn.show');
Route::get('decision/edit/{id}', 'Coordinator\DecisionCoordinatorController@edit')->name('decision.edit');
Route::put('decision/edit/{id}', 'Coordinator\DecisionCoordinatorController@update')->name('decision.update');
Route::get('decision/show/{id}', 'Coordinator\DecisionCoordinatorController@show')->name('decision.show');
Route::get('decision/company/show/{id}', 'Company\DecisionCompanyController@show')->name('coordinator.company.decision');
Route::post('decision/company/extra', 'Company\DecisionCompanyController@updateExtra')->name('coordinator.company.extra');

Route::get('email/{simulation}', 'Coordinator\SimulationController@team')->name('simulation.email');


/*
 * Planning
 */
Route::get('planning/simulation/{id}', 'Company\StrategicPlanningController@show')->name('coordinator.planning');
Route::get('planning/api/{id}', 'Company\StrategicPlanningController@apiPlanning')->name('coordinator.planning.api');

/*
 * studant
 */

Route::get('student/api/{id}', 'Company\StudentController@apiStudent')->name('coordinator.student.api');
Route::get('manualjogo', 'Coordinator\DecisionCoordinatorController@getManualJogo')->name('coordinator.manual.jogo');
Route::get('/linkmanualjogo', 'Coordinator\DecisionCoordinatorController@getLinkManual')->name('linkmanualjogo');

/*
 * Turn
 */
Route::group(['prefix' => 'turn'], function () {
    Route::get('{id}/date', 'Coordinator\TurnController@chooseDate')->name('turn.date');
    Route::get('{id}/publish', 'Coordinator\TurnController@publish')->name('turn.publish');
    Route::get('{id}/close', 'Coordinator\TurnController@closeTurn')->name('turn.close');
    Route::post('date', 'Coordinator\TurnController@setDate')->name('turn.date.store');
});

/*
 * Newspaper
 */
Route::group(['prefix' => 'newspaper'], function () {
    Route::get('{id}', 'Coordinator\NewspaperController@create')->name('newspaper.index');
    Route::get('{id}/destroy', 'Coordinator\NewspaperController@destroy')->name('newspaper.destroy');
    Route::post('store', 'Coordinator\NewspaperController@store')->name('newspaper.store');
    Route::get('edit/{id}', 'Coordinator\NewspaperController@edit')->name('newspaper.edit');
    Route::put('edit/{id}', 'Coordinator\NewspaperController@update')->name('newspaper.update');
    Route::get('api/matters/{id}', 'Coordinator\NewspaperController@getMatters')->name('newspaper.matters');
    Route::get('api/matters/content/{id}', 'Coordinator\NewspaperController@getMattersContent')->name('newspaper.matters.content');
});

/*
 * Company
 */
Route::group(['prefix' => 'company'], function () {
    Route::get('decision/{id}', 'Company\DecisionCompanyController@show')->name('coordinator.company.decision');
});

/*
 * Reports
 */
Route::group(['prefix' => 'reports'], function () {
    Route::get('generate/{id}', 'ReportsController@generate')->name('coordinator.generate.report');
    Route::get('generateFirst/{id}', 'ReportsController@generateFirst')->name('simulation.report.first');
    Route::get('operational/{turn_id}/{id}', 'ReportsController@operational')->name('coordinator.operational.report');
    Route::get('cashflow/{turn_id}/{id}', 'ReportsController@cashflow')->name('coordinator.cashflow.report');
    Route::get('balance/{turn_id}/{id}', 'ReportsController@balance')->name('coordinator.balance.report');
    Route::get('dre/{turn_id}/{id}', 'ReportsController@dre')->name('coordinator.dre.report');
    Route::get('ranking/{turn_id}', 'ReportsController@ranking')->name('coordinator.ranking');
    Route::get('exportarfdc/{turn_id}/{id}', 'CashflowController@export')->name('exportarfdc');
    Route::get('exportardre/{turn_id}/{id}', 'DreController@export')->name('exportardre');
    Route::get('exportarbalance/{turn_id}/{id}', 'BalanceController@export')->name('exportarbalance');
    Route::get('exportaroperational/{turn_id}/{id}', 'OperationalController@export')->name('exportaroperational');
});



Route::post('mail', 'Coordinator\SimulationController@sendPasswordMail')->name('coordinator.send.password');

// Decisão default (plus)
Route::post('decision/company/default', 'Company\DecisionCompanyController@default')->name('coordinator.company.default');
