<?php

Route::group(['middleware' => ['auth', 'owner']], function () {
    Route::get('/', 'Tenant\ClienteController@home')->name('tenant');
    $this->get('cliente/create', 'Tenant\ClienteController@create')->name('cliente.create');
    $this->get('cliente/{domain}', 'Tenant\ClienteController@show')->name('cliente.show');
    $this->post('cliente', 'Tenant\ClienteController@store')->name('cliente.store');
    $this->get('cliente/edit/{domain}', 'Tenant\ClienteController@edit')->name('cliente.edit');
    $this->delete('cliente/{id}', 'Tenant\ClienteController@destroy')->name('cliente.destroy');
    $this->put('cliente/{id}', 'Tenant\ClienteController@update')->name('cliente.update');
    $this->get('clientes', 'Tenant\ClienteController@index')->name('cliente.index');
});
