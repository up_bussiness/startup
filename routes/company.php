<?php


Route::get('/', 'Company\CompanyController@index')->name('company');

Route::resource('student', 'Company\StudentController');
Route::get('usuarioqrcode/{email}', 'Company\StudentController@getUsuarioQrCode')->name('usuario.qrcode');
Route::get('usuarioacessoapp/{email}', 'Company\StudentController@getUsuarioAcessoApp')->name('usuario.acessoapp');
Route::get('/mailable', function () {
    $user = ['name' => 'Paulo', 'url' => 'www', 'login' => 'xx@', 'password' => '1234', 'emailemp' => 'pw@1234.com', 'passemp' => '12345'];

    return new \LogisticsGame\Mail\SendMailAluno($user);
});
Route::get('manualjogo', 'Company\StudentController@getManualJogo')->name('manual.jogo');
Route::get('/linkmanualjogo', 'Company\StudentController@getLinkManual')->name('company.linkmanualjogo');

Route::get('edit', 'Company\CompanyController@edit')->name('company.edit');
Route::put('update', 'Company\CompanyController@update')->name('company.update');
Route::get('initConfig', 'Company\CompanyController@initConfig')->name('company.initConfig');
Route::put('preparationConfig', 'Company\CompanyController@preparationConfig')->name('company.preparationConfig');
Route::put('configCompany', 'Company\CompanyController@configCompany')->name('company.configCompany');
Route::get('waitConfigSimulation', 'Company\CompanyController@waitConfigSimulation')->name('company.waitConfigSimulation');


Route::get('planning', 'Company\StrategicPlanningController@create')->name('planning.create');
Route::post('planning', 'Company\StrategicPlanningController@store')->name('planning.store');
Route::put('planning/{id}', 'Company\StrategicPlanningController@update')->name('planning.update');
Route::get('newspaper/{id}', 'Coordinator\NewspaperController@showForCompanies')->name('company.newspaper.show');

/*
 * Decision
 */
Route::group(['prefix' => 'decision'], function () {
    Route::get('create/{id}', 'Company\DecisionCompanyController@create')->name('company.decision.create');
    Route::get('edit/{id}', 'Company\DecisionCompanyController@edit')->name('company.decision.edit');
    Route::post('store', 'Company\DecisionCompanyController@store')->name('company.decision.store');
    Route::put('update/{id}', 'Company\DecisionCompanyController@update')->name('company.decision.update');
    Route::get('show/{id}', 'Company\DecisionCompanyController@show')->name('company.decision.show');
});

/*
 * Reports
 */
Route::group(['prefix' => 'reports'], function () {
    Route::get('operational/{id}', 'ReportsController@operational')->name('company.operational.report');
    Route::get('balance/{id}', 'ReportsController@balance')->name('company.balance.report');
    Route::get('dre/{id}', 'ReportsController@dre')->name('company.dre.report');
    Route::get('cashflow/{id}', 'ReportsController@cashflow')->name('company.cashflow.report');
    Route::get('ranking/{id}', 'ReportsController@ranking')->name('company.ranking.report');
    Route::get('exportarfdc/{turn_id}/{id}', 'CashflowController@export')->name('company.exportarfdc');
    Route::get('exportardre/{turn_id}/{id}', 'DreController@export')->name('company.exportardre');
    Route::get('exportarbalance/{turn_id}/{id}', 'BalanceController@export')->name('company.exportarbalance');
    Route::get('exportaroperational/{turn_id}/{id}', 'OperationalController@export')->name('company.exportaroperational');
});
