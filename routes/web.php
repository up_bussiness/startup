<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::view('/404-tenant', 'errors.404-tenant')->name('404.tenant');
Route::get('/play', 'Tenant\ClienteController@playing')->name('playing');
Route::post('/autenticando', 'Tenant\ClienteController@autenticando')->name('autenticando');
Route::post('/enviandosenha', 'Tenant\ClienteController@enviarSenha')->name('enviarsenha');

Route::prefix('setup')->middleware('owner')->group(function () {
    Route::get('/', 'Owner\InstituitionController@manage')->name('owner');
    Route::get('/reset', 'Owner\InstituitionController@reset')->name('reset');
    Route::get('/students', 'Owner\InstituitionController@students')->name('owner.students');
    Route::get('/install', 'Owner\InstituitionController@create')->name('instituition.create');
    Route::post('/install', 'Owner\InstituitionController@store')->name('instituition.store');
    Route::put('/update', 'Owner\InstituitionController@update')->name('instituition.update');
    Route::delete('/remove/simulation/{id}', 'Owner\InstituitionController@destroySimulation')->name('owner.simulation.destroy');

    Route::put('/update/coordinator', 'Owner\InstituitionController@updateCoordinator')->name('owner.coordinator.update');

    Route::get('/manage/coordinator/{id}', 'Owner\InstituitionController@manageCoordinator')->name('owner.coordinator.manage');
    Route::get('/remove/coordinator/{id}', 'Admin\AdminController@destroy')->name('owner.admin.destroy');
});
