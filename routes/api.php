<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

$this->group(['prefix' => 'v1'], function () {
    $this->get('users', 'Api\v1\AcessoController@logar');
    $this->get('simulacoes', 'Api\v1\SimulacoesController@lista');
    $this->get('empresas', 'Api\v1\EmpresasController@lista');
    $this->get('alunos', 'Api\v1\AlunosController@lista');
    $this->get('salvartoken', 'Api\v1\SalvarToken@salvartoken');
    $this->get('enviarpn', 'Api\v1\AcessoController@enviarpn');
});
