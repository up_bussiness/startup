<?php

Route::get('/', 'Admin\AdminController@list')->name('admin');
Route::post('/create', 'Admin\AdminController@store')->name('admin.create');
Route::get('/edit/{id}', 'Admin\AdminController@edit')->name('admin.edit');
Route::put('/edit', 'Admin\AdminController@updateCoordinator')->name('admin.update');
Route::get('/remove/{id}', 'Admin\AdminController@destroy')->name('admin.destroy');
Route::delete('/remove/simulation/{id}', 'Admin\AdminController@destroySimulation')->name('admin.simulation.destroy');